# COVID-19 Collaboration

## Description
This project involves the usage of modified epidemiological models and learning machines to understand and predict the spread of COVID-19. 
The original approach stems from the SEIR model, an epidemiological system that models susceptible, exposed, infected, and recovered populations as a function of time.

The later approach (implemented in the Covid-19 ForecastHub) relies on the use of ridge regression to predict future covid spread based on previous data. This approach was inspired by this paper: 
https://arxiv.org/pdf/2003.00122.pdf. This repository has modified the original approach such that confirmed case predictions are made independently of disease spread characteristics like beta and gamma. This allows for the model to 
make predictions purely motivated by previous trends, not external assumptions. The approach was also modified to predict mortality. 

## Installation
To clone this repository locally, use the command line to navigate to your chosen container directory. From this directory, type "git clone https://gitlab.com/sabcorse/covid-19-collaboration.git".

To download the case and death data used in this project, **within the same shell directory** type "git clone https://github.com/CSSEGISandData/COVID-19.git". To update this data daily, execute "git pull" from the
command line inside the COVID-19 directory. 

To update the social distancing data, navigate to the Social_Distancing folder, and replace the existing csv with the "Global CSV" from Google, available here: https://www.google.com/covid19/mobility/. 

**Dependencies:** Several dependencies are required for this project, such as sklearn and numpy. In the event that one is missing, execute "pip3 install dependency" from the command line, 
where "dependency" is replaced with the library name. 

## Usage

### Ridge regression:
The script "ridge_with_social_distancing.py" walks through the prediction-making process for a region of choice. To run the script for a given province/state or country, execute "python3 ridge_with_social_distancing.py \<Region\> \<RegionType\> \<NumDays\>", where \<Region\> is replaced with the region's name; \<RegionType\> is replaced with "country" if the region is a country, or "province" if the region is a state or province; and \<NumDays\> is replaced with the number of days desired to train the algorithm. 14 days are suggested for the states and 7 for the national aggregate. Note that the \<Region\> field is case-sensitive.

The ridge regression approach uses FIR filtration to predict future COVID-19 spread based on previous data. Bootstrapping is used to create a spectrum of predictions at each future datapoint, from which prediction interval quantiles are determined. 

The script produces a csv containing forecasts regarding cumulative deaths along with case and death increases. This csv will be saved under a directory named for the \<Region\>.

### Weekly Plotting:
To run the script regarding weekly testing data, execute "python3 covid_weekly.py" from the command line. When prompted for a region, type your area of interest (e.g. US, Michigan, Illinois, etc.). Please note that this script only works for the US or US States. This input is case sensitive. 

You will be prompted "country or province". For a country, type US. For a province *or* state, type province. This is not case sensitive. The first plot produced will give an overview of case counts, testing counts, and death rate. The second plot produced will show a plot of new cases per week along with a projection of how many cases per day there could have been had the positivity rate from weeks ago not changed.

### Workspace Directory:
The Workspace Directory is used for active development.

## Funding

This project is funded by the University of Michigan Physics Department and the University of Michigan Office of Research.

## Current Team

**Sabrina Corsetti:** B.S. Physics, Mathematics ('21);  University of Michigan; sabcorse@umich.edu

**Robert Myers:** B.S. Physics, Mathematics ('22); University of Michigan; romyers@umich.edu

**Yitao Huang:** B.S. Physics ('23); University of Michigan; yitao@umich.edu

**Karl Falb:** B.S. Physics, B.M. Violin Performance ('23); University of Michigan; falbkarl@umich.edu

**Marisa Eisenberg:** Associate Professor of Epidemiology, Complex Systems, Mathematics; University of Michigan; marisae@umich.edu

**Emily Martin:** Associate Professor of Epidemiology; University of Michigan; etmartin@umich.edu

**Thomas Schwarz:** Associate Professor of Physics; University of Michigan; schwarzt@umich.edu

## Collaborators

**Ella McCauley:** Physics Research Assistant; University of Michigan

**Thomas Baer:** University of Michigan Physics REU Student ('20); Trinity University
