import numpy as np
import glob
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt

def Get_File_Date(file_name):
    month, day, year = file_name[-14:-12], file_name[-11:-9], file_name[-8:-4]
    return month, day, year

def Get_JHU_Data(county_fips_list) :
    # List and sort JHU data files
    path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
    file_set = glob.glob(path + "/*.csv" )
    file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

    case_list_list = []
    # Loop over JHU data, and update cases
    for j in range(len(county_fips_list)) :
        county = county_fips_list[j]
        cases_list = []
        for file in file_set:
            file_mo, file_day, file_yr = Get_File_Date(file)
            if (int(file_mo) == 3 and int(file_day) > 29) or (int(file_mo) > 3) or (int(file_yr) > 2020) :
                data = pd.read_csv( file, index_col = None, header = 0 )
                data_county = data.loc[data['FIPS'] == int(county)]
                cases_list.append(data_county['Confirmed'].values[0])
        incident_case_list = []
        for i in range(1, len(cases_list)) :
            incident_num = cases_list[i]-cases_list[i-1]
            if incident_num < 0 :
                incident_case_list.append(0)
            else :
                incident_case_list.append(incident_num/(cases_list[-1]-cases_list[0]))
        case_list_list.append(incident_case_list)
    print('Finished loading case data')
    return case_list_list

def Get_Density_Distribution():
    # imports population data as strings, specifies encoding to prevent errors
    population_df = pd.read_csv('./gen_csvs/co-est2019-alldata.csv', dtype='str', encoding='latin1')
    # selects only necessary data
    population_df = population_df[['STATE','COUNTY','STNAME','CTYNAME','POPESTIMATE2019']]
    population_df['fips'] = population_df['STATE'] + population_df['COUNTY']

    # imports land area data as strings
    land_df = pd.read_csv('./gen_csvs/LND01.csv', dtype='str')
    # selects only necessary data
    land_df = land_df[['STCOU','LND110210D']]
    # creates lists for iteration
    land_fips = land_df['STCOU'].to_list()
    land_values = land_df['LND110210D'].to_list()
    population_fips = population_df['fips'].to_list()
    population_values = population_df['POPESTIMATE2019'].to_list()
    # creates an empty list for assigning values
    pop_density_values = [None]*len(population_fips)

    unused_pop_fips = []
    for i in range(len(population_fips)) :
        # land data (2010) and pop data (2019)
        # between these dates, counties have changed
        # skips over cases where population/land changes are unable to be calculated
        if population_fips[i] in ['02063','02266','02195','02105'] :
            pop_density_values[i] = -1
        # gets rid of entire states
        elif population_fips[i][2:] == '000' :
            pop_density_values[i] = -1
        # deals with cases of merging or reassignment
        elif population_fips[i] in ['51019','02158','46102'] :
            if population_fips[i] == '51019' :
                land_index_1 = land_fips.index('51019')
                land_index_2 = land_fips.index('51515')
                total_land = float(land_values[land_index_1]) + float(land_values[land_index_2])
            elif population_fips[i] == '02158' :
                land_index = land_fips.index('02270')
                total_land = float(land_values[land_index])
            elif population_fips[i] == '46102' :
                land_index_1 = land_fips.index('46113')
                total_land = float(land_values[land_index])
            pop_density_values[i] = float(population_values[i])/total_land
        # deals with the rest of the counties
        elif population_fips[i] in land_fips :
            land_index = land_fips.index(population_fips[i])
            pop_density_values[i] = float(population_values[i])/float(land_values[land_index])
        # for debugging
        else :
            unused_pop_fips.append(population_fips[i])
    data_df = pd.concat([population_df['fips'], population_df['STNAME'], population_df['CTYNAME']],keys=['FIPS','State','County'],axis=1)
    data_df['Density'] = pop_density_values
    data_df = data_df.sort_values('Density', axis = 0, ascending = False)
    data_df = data_df.loc[data_df['Density'] > 0]
    data_df.reset_index(drop = True)
    data_df_tenth = int(len(data_df.index)/10)
    high_density = data_df.iloc[0:data_df_tenth]
    low_density = data_df.iloc[-data_df_tenth:]
    medium_density = data_df.iloc[int(data_df_tenth*4.5):int(data_df_tenth*5.5)]
    print(high_density)
    print(medium_density)
    print(low_density)

    return data_df

high_density = Get_Density_Distribution()
