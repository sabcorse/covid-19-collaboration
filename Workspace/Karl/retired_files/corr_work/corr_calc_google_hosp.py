import matplotlib.pyplot as plt
import seaborn as sn
import pandas as pd
import numpy as np
import sys
import glob
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from dateutil.parser import parse

def Split_Loc_Name(loc):
    i = 0
    while i < len(loc):
        # Scan over name, and split when capital letters found
        if loc[i].isupper() and not i == 0 and not loc == 'US':
            loc = loc[:i]+' '+loc[i:]
            i += 2
        else:
            i += 1
    return loc

def Init_Analysis(given_loc):
    # Split name if necessary, and convert type (country or state/province) to 1 or 0
    location = Split_Loc_Name(given_loc)
    print('Location: ', location)
    return location

# Run initialization
# Arg 1 <- location name, arg 2 <- country or province
location = Init_Analysis(sys.argv[1])

def Get_First_Date():
    first_day_US = parse('January 22, 2020')
    return (first_day_US + timedelta(days = 135)).strftime('%Y-%m-%d')

social_distancing_reach_back = 0
# Get Google's mobility data for a given region
def Get_Mobility(region):
    global social_distancing_reach_back
    # Get first day to include
    start_date = Get_First_Date()
    # For use in Forecasting Hub - automatically set country to US
    country = 'United States'
    # Read in all data, and narrow down to US-only for both states and national level
    mobility_data = pd.read_csv('../gen_csvs/Global_Mobility_Report.csv', low_memory = False)
    this_region_mobility = mobility_data.loc[mobility_data['country_region'] == 'United States']

    this_region_mobility = this_region_mobility.loc[this_region_mobility['sub_region_1'] == region]
    no_counties_rows = pd.isnull(this_region_mobility['sub_region_2'])
    this_region_mobility = this_region_mobility[no_counties_rows]

    # Find row index where "start_date" occurs, and cut out all previous data
    first_date_row = this_region_mobility.loc[this_region_mobility['date'] == start_date].index.item()
    #print(this_region_mobility.loc[first_date_row])
    this_region_mobility = this_region_mobility.loc[first_date_row:]

    # Social distancing data not updated every day. Find difference b/w today & most recent update
    most_recent_date = this_region_mobility['date'].iloc[-1]
    #print(most_recent_date)
    most_recent_year = int(most_recent_date[0:4])
    most_recent_month = int(most_recent_date[5:7])
    most_recent_day = int(most_recent_date[8:])
    social_distancing_reach_back = (calendar.today() - calendar(most_recent_year, most_recent_month, most_recent_day)).days
    print('Will reach ' + str(social_distancing_reach_back) + ' days back for social distancing')

    # get data for each type of social outing, and convert to lists
    add_on = '_percent_change_from_baseline'
    this_retail_and_rec = this_region_mobility['retail_and_recreation'+add_on].tolist()
    this_groc_and_pharm = this_region_mobility['grocery_and_pharmacy'+add_on].tolist()
    this_parks = this_region_mobility['parks'+add_on].tolist()
    this_transit = this_region_mobility['transit_stations'+add_on].tolist()
    this_work = this_region_mobility['workplaces'+add_on].tolist()
    this_residential = this_region_mobility['residential'+add_on].tolist()

    list_of_locs = [this_retail_and_rec, this_groc_and_pharm, this_parks, this_transit, this_work, this_residential]
    loc_strings = ['retail and recreation', 'grocery and pharmacy', 'parks', 'transit', 'work', 'residences']

    # Eliminate any NaN values in the social distancing data, by using previous value
    j = 0
    for loc in list_of_locs:
        #print(loc)
        num_nan = 0
        for i in range(len(this_retail_and_rec)):
            if np.isnan(loc[i]):
                num_nan += 1
                loc[i] = 0
                if not i == 0:
                    loc[i] = loc[i-1]
        print('Number of NaN values converted for ' + loc_strings[j] + ': ' + str(num_nan))
        j += 1

    return this_retail_and_rec, this_groc_and_pharm, this_parks, this_transit, this_work, this_residential

# Loop over Google file to get social distancing data
#retail_rec_list, groc_pharm_list, parks_list, transit_list, work_list, residential_list = Get_Mobility(location)

##############################################################
#                                                            #
#                      Get Hospital Data                     #
#                                                            #
##############################################################

# identical to mobility Get_First_Date() but added for redundancy (is this necessary?)
def Get_First_Date_Hospital():
    first_day_US = parse('January 22, 2020')
    return (first_day_US + timedelta(days = 135)).strftime('%Y-%m-%d')

# Reads in data and separates Michigan's data
def Get_Hospital_Data(region) :
    # data sheet file name may vary, this may need to be adjusted
    # reads data
    fileList = glob.glob('../gen_csvs/reported_hospital_utilization_timeseries_*.csv')
    # stand in handler for multiple files
    if len(fileList) != 1 :
        print("multiple or no files, correct file may not be selected")
        # latest date first
        fileList.sort(reverse=True)
    preprocess_hospital_data = pd.read_csv(fileList[0])
    print("Reading in hospital data.")

    # lists of states and abbreviations to work with the hosptial file
    # this only contains the 50 states, no DC PR etc
    states = ['Virgin Islands','Puerto Rico','District of Colombia', 'Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']
    abbreviations = ['VI','PR','DC','AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY']

    # converts to abbreviations
    if region in states :
        state_abbreviation = abbreviations[states.index(region)]

    # gets the date range
    start_date = Get_First_Date_Hospital()

    # sorts by date (original out of order)
    preprocess_hospital_data = preprocess_hospital_data.sort_values('date', axis=0)
    # selects state data
    preprocess_hospital_data = preprocess_hospital_data.loc[preprocess_hospital_data['state'] == state_abbreviation]
    # trims the abbreviation off every row
    preprocess_hospital_data = preprocess_hospital_data.drop('state', axis=1)
    # gets row index of first date
    first_date_row = preprocess_hospital_data.loc[preprocess_hospital_data['date'] == start_date].index.item()
    # gets the data for analysis
    preprocess_hospital_data = preprocess_hospital_data.loc[first_date_row:]
    # selects column for use and converts it to a python list
    inpatient_covid_beds = preprocess_hospital_data["inpatient_beds_used_covid"].tolist()
    hospital_NaN_count = 0
    for i in range(len(inpatient_covid_beds)) :
        if np.isnan(inpatient_covid_beds[i]) :
            inpatient_covid_beds[i] = 0
            hospital_NaN_count += 1
    return inpatient_covid_beds

# Gets hospoital data
hospital_data = Get_Hospital_Data(location)

##############################################################
#                                                            #
#                      Get Testing Data                      #
#                                                            #
##############################################################

def Get_COVIDTRACKING_Test_Data(region) :
    preprocess_testing_data = pd.read_csv('../gen_csvs/all-states-history.csv')

    # lists of states and abbreviations to work with the hosptial file
    # this only contains the 50 states, no DC PR etc
    states = ['Virgin Islands','Puerto Rico','District of Colombia', 'Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']
    abbreviations = ['VI','PR','DC','AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY']

    # converts to abbreviations
    if region in states :
        state_abbreviation = abbreviations[states.index(region)]

    # gets the date range
    start_date = Get_First_Date_Hospital()

    # selects state data
    preprocess_testing_data = preprocess_testing_data.loc[preprocess_testing_data['state'] == state_abbreviation]
    # trims the abbreviation off every row
    preprocess_testing_data = preprocess_testing_data.drop('state', axis=1)
    # gets row index of first date
    first_date_row = preprocess_testing_data.loc[preprocess_testing_data['date'] == start_date].index.item()
    # gets the data for analysis (including one extra data point for a cumulative to daily calculation)
    preprocess_testing_data = preprocess_testing_data.loc[:first_date_row+2]
    # gets the lists (note: cumulative)
    pos_test_list_cum = preprocess_testing_data['positive'].tolist()
    pos_test_list_cum.reverse()
    total_test_list_cum = preprocess_testing_data['totalTestResults'].tolist()
    total_test_list_cum.reverse()
    date_test_list = preprocess_testing_data['date'].tolist()
    date_test_list.reverse()
    rating_list = preprocess_testing_data['dataQualityGrade'].tolist()
    rating_list.reverse()
    print('Note data rating: ' + str(rating_list[0]))
    # makes into daily
    pos_test_list = []
    total_test_list = []
    for i in range(len(pos_test_list_cum)):
        if i == 0 :
            continue
        pos_test_list.append(pos_test_list_cum[i]-pos_test_list_cum[i-1])
        total_test_list.append(total_test_list_cum[i]-total_test_list_cum[i-1])
    positivity_test_list = []
    for i in range(len(pos_test_list)) :
        if total_test_list[i] == 0 :
            positivity_test_list.append(0)#positivity_test_list[i-1])
            continue
        elif total_test_list[i] <= pos_test_list[i] :
            positivity_test_list.append(0)#positivity_test_list[i-1])
            continue
        positivity_test_list.append(pos_test_list[i]/total_test_list[i])
    print("Read in and calculated positivity data for " + str(region))
    return positivity_test_list

covid_tracker_test_data_positivity = Get_COVIDTRACKING_Test_Data(location)


#######################
#                     #
#       MI Tests      #
#                     #
#######################

def Parse_Pos_Dates(dateStr):
    return parse(dateStr).strftime('%Y-%m-%d')

def Get_MI_Covid_Tests():
    testing_data = pd.read_csv('../gen_csvs/Diagnostic_Tests_by_Result_and_County_2020-12-14_710500_7.csv')
    # trims the county off every row
    testing_data = testing_data.drop('COUNTY', axis=1)
    # sorts by date
    corrected_dates = testing_data['MessageDate'].apply(Parse_Pos_Dates)
    testing_data = pd.concat([corrected_dates,testing_data["Positive"], testing_data["Total"]], axis=1)
    testing_data = testing_data.sort_values('MessageDate', axis=0)
    # makes some lists because I don't know pandas that well
    positive_tests_list = testing_data["Positive"].tolist()
    total_tests_list = testing_data["Total"].tolist()
    date_test_list = testing_data["MessageDate"].tolist()
    # for the summations
    positive_tests_summed = []
    total_tests_summed = []
    positivity_tests = []
    start_point = Get_First_Date()
    not_hit_start_point = True
    last_date = start_point
    work_pos_sum = 0
    work_tot_sum = 0
    for i in range(len(positive_tests_list)):
        if not_hit_start_point and date_test_list[i] != start_point :
            continue
        not_hit_start_point = False
        if date_test_list[i] != last_date:
            positive_tests_summed.append(work_pos_sum)
            total_tests_summed.append(work_tot_sum)
            positivity_tests.append(work_pos_sum/work_tot_sum)
            work_pos_sum = 0
            work_tot_sum = 0
        work_pos_sum += positive_tests_list[i]
        work_tot_sum += total_tests_list[i]
        last_date = date_test_list[i]
    positive_tests_summed.append(work_pos_sum)
    total_tests_summed.append(work_tot_sum)
    positivity_tests.append(work_pos_sum/work_tot_sum)
    #print(len(positivity_tests),len(hospital_data))
    return positivity_tests

#positivity_tests = Get_MI_Covid_Tests()

#######################
#                     #
#        Matrix       #
#                     #
#######################

sample_df = pd.DataFrame(list(zip(covid_tracker_test_data_positivity,hospital_data)),columns=["positivity","hospital_data"])
corrMatrix = sample_df.corr()
sn.heatmap(corrMatrix, annot=True)
#plt.savefig('heatmaps/michigan_google_pos_hosp.pdf')
plt.show()

# corr for labeling the plot
corr_str = str(corrMatrix['positivity'][1])[:6]
label_str = 'positivity, corr: ' + corr_str

fig, ax1 = plt.subplots()
ax1.set_xlabel('Days')
ax1.set_ylabel('Daily Hospital')
ax1.plot(hospital_data, 'k', label = 'hospital')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.set_ylabel('Positivity')
ax2.plot(covid_tracker_test_data_positivity, 'g', label=label_str)
#ax2.plot(positivity_tests, 'r', label='MI positivity')
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.legend(loc = 'upper left')
plt.title("Hospitalizatons and Positivity Rate: " + location)
#plt.savefig('hospital_positivity.pdf')
plt.show()
