import matplotlib.pyplot as plt
import seaborn as sn
import pandas as pd
import numpy as np
import sys
import glob
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from dateutil.parser import parse

def Get_First_Date() :
    return '2020-12-22'

minRange = 335

location = 'Upper Peninsula'

reg_county_dict = {'Upper Peninsula':['alger', 'baraga', 'chippewa', 'delta', 'dickinson', 'gogebic', 'houghton', 'iron', 'luce', 'mackinac', 'marquette', 'menominee', 'ontonagon', 'schoolcraft']}

def Get_Unacast_UP(region) :
    unacast_df = pd.read_csv('../gen_csvs/20210522_Unacast_County.csv')
    unacast_df = unacast_df.loc[unacast_df['county_name'] == region]
    unacast_df = unacast_df.sort_values('date', axis = 0)
    daily_dist_diff = unacast_df['daily_distance_diff'].tolist()
    date_list = unacast_df['date'].tolist()
    county_pop = unacast_df['county_population'].tolist()[0]
    unacast_df.to_csv('test_unacast.csv')
    first_date_index = date_list.index(Get_First_Date())
    daily_dist_diff = daily_dist_diff[first_date_index:]

    # Social distancing data not updated every day. Find difference b/w today & most recent update
    most_recent_date = date_list[-1]

    most_recent_year = int(most_recent_date[0:4])
    most_recent_month = int(most_recent_date[5:7])
    most_recent_day = int(most_recent_date[8:])

    #social_distancing_reach_back = (calendar.today() - calendar(most_recent_year, most_recent_month, most_recent_day)).days - 1
    social_distancing_reach_back = (calendar(2021,5,24) - calendar(most_recent_year, most_recent_month, most_recent_day)).days - 1

    print('Will reach ' + str(social_distancing_reach_back) + ' days back for social distancing')

    for i in range(len(daily_dist_diff)) :
        daily_dist_diff[i] *= county_pop
    return daily_dist_diff, county_pop


for i in range(len(reg_county_dict[location])):
    county = reg_county_dict[location][i]
    county = county.title() + " County"
    if i == 0:
        dist_diff_list, county_pop = Get_Unacast_UP(county)
    else:
        temp_dist_diff_list, temp_county_pop = Get_Unacast_UP(county)
        if len(temp_dist_diff_list) > len(dist_diff_list):
            for i in range(len(temp_dist_diff_list) - len(dist_diff_list)):
                dist_diff_list.insert(0,0)

        if len(dist_diff_list) > len(temp_dist_diff_list):
            for i in range(len(dist_diff_list) - len(temp_dist_diff_list)):
                temp_dist_diff_list.insert(0,0)

        for j in range(len(dist_diff_list)):
            dist_diff_list[j] += temp_dist_diff_list[j]

        county_pop += temp_county_pop

loc_list = [dist_diff_list]

for i in range(len(loc_list[0])):
	loc_list[0][i] = loc_list[0][i]/county_pop

hosp_csv = pd.read_csv('../gen_csvs/covid-19-hospitalization-upperpeninsula.csv')
dates_hosps = hosp_csv['DateTime'].tolist()
dates_hosps = [date.split(' ')[0] for date in dates_hosps]
dates_hosps = hosp_csv['DateTime'].tolist()
dates_hosps = [date.split(' ')[0] for date in dates_hosps]

#print(dates_hosps)

start_date = Get_First_Date()
for i in range(len(dates_hosps)):
    #print(str(dates_hosps[i]) + ' ' + str(start_date))
    if dates_hosps[i] == start_date:
        first_date_row = i
        #print('first row: ' + str(first_date_row))

hosps_per_mil = hosp_csv.iloc[first_date_row:]['COVID-19 Hospitalizations per Million'].tolist()
hosps_per_mil = hosps_per_mil[:len(loc_list[0])]

sample_df = pd.DataFrame(list(zip(loc_list[0],hosps_per_mil)),columns=["dist_diff","hosps_per_mil"])
corrMatrix = sample_df.corr()
sn.heatmap(corrMatrix, annot=True)
#plt.savefig('heatmaps/michigan_google_pos_hosp.pdf')
plt.show()

corr_str = str(corrMatrix['dist_diff'][1])[:6]
label_str = 'dist_diff, corr: ' + corr_str

fig, ax1 = plt.subplots()
ax1.set_xlabel('Days')
ax1.set_ylabel('Daily Hospital per Mil')
ax1.plot(hosps_per_mil, 'k', label = 'hospital')
ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
ax2.set_ylabel('dist_diff')
ax2.plot(loc_list[0], 'g', label=label_str)
#ax2.plot(positivity_tests, 'r', label='MI positivity')
fig.tight_layout()  # otherwise the right y-label is slightly clipped
plt.legend(loc = 'upper left')
plt.title("Hospitalizatons and dist diff: " + location)
#plt.savefig('hospital_positivity.pdf')
plt.show()
