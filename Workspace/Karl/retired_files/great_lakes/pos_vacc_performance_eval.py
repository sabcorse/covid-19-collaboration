import numpy as np
import glob
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from dateutil.parser import parse
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from scipy import signal

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

on_server = True
with_vacc = False

# Split country/state name input into separate words if necessary
def Split_Loc_Name(loc):
    i = 0
    while i < len(loc):
        # Scan over name, and split when capital letters found
        if loc[i].isupper() and not i == 0 and not loc == 'US':
            loc = loc[:i]+' '+loc[i:]
            i += 2
        else:
            i += 1
    return loc

# Convert country or state/province to integers
# Country -> 1, State/province -> 0
def Loc_Type_to_Num(loc_type):
    loc_type_num = 1
    if loc_type.lower() == 'province':
        loc_type_num = 0
    return loc_type_num

# Initialize analysis, using location name splitter and location type -> number functions
def Init_Analysis(given_loc, given_loc_type):
    # Split name if necessary, and convert type (country or state/province) to 1 or 0
    location = Split_Loc_Name(given_loc)
    location_type = Loc_Type_to_Num(given_loc_type)
    print('Location: ', location, '\nType: ', str(location_type))
    # Make a directory for saving the location's data, if necessary
    if not os.path.exists(location):
        os.makedirs(location)
        print('New directory created for', location)
    return location, location_type

# Run initialization
# Arg 1 <- location name, arg 2 <- country or province
location, countryOrState = Init_Analysis(sys.argv[1], sys.argv[2])
days_to_include = int(sys.argv[3])
order = int(sys.argv[4])

###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Handle JHU South Korea name changes
def Handle_South_Korea(file_name):
    if file_name == path + '03-10-2020.csv':
        location = 'Republic of Korea'
    if file_name == path + '03-11-2020.csv':
        location = 'Korea, South'
    return location

# Work-around, since Grok library not often available
# Convert JHU file names to usable dates
def Get_File_Date(file_name):
    month, day, year = file[-14:-12], file[-11:-9], file[-8:-4]
    if '{m}/{d}/{Y}'.format(m = month, d = day, Y = year) == sunday_stop:
        month, day, year = 'stop', 'stop', 'stop'
    return month, day, year

# Handle JHU region type changes
def Get_Loc_Type_JHU(month, day, year):
    areaLabel = 'Country/Region'
    if countryOrState == 0:
        areaLabel = 'Province/State'
    if calendar(int(year), int(month), int(day)) > calendar(2020, 3, 21):
        # After certain date, "/" changed to "_"
        areaLabel = 'Country_Region'
        if countryOrState == 0:
                areaLabel = 'Province_State'
    return areaLabel

# Append data from JHU
def Append_Data(daily_data):
    confirmedCases.append(0.)
    deaths.append(0.)
    recovered.append(0.)

    # Use global variables to update relevant values
    confirmedCases[day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Confirmed']
    deaths        [day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Deaths']
    recovered     [day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Recovered']

    # Related sources on making this function efficient:
    # https://engineering.upside.com/a-beginners-guide-to-optimizing-pandas-code-for-speed-c09ef2c6a4d6
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/missing_data.html

# Tells the data when to stop reading in
# Predictions made Saturday -> Saturday, but are created on Monday, so Sunday data dropped
def Get_Last_Sunday():
    today = calendar.today()
    idx = (today.weekday() + 1) % 7
    sun = today - timedelta(idx)
    sun = '{:%m/%d/%Y}'.format(sun)
    return sun

# Prepare most recent Sunday
sunday_stop = Get_Last_Sunday()

# List and sort JHU data files
if on_server :
    path = r'COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
else :
    path = r'../../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

confirmedCases, deaths, recovered, active = [], [], [], []
day_count = 0

# Loop over JHU data, and update cases, deaths, recoveries
for file in file_set:
        # Handles South Korea name changes, if necessary
        if location == 'South Korea' or location == 'Republic of Korea':
            location = Handle_South_Korea(file)

        # Break the loop once most recent Sunday hit; do not read in
        # Predictions made Saturday -> Saturday
        month, day_index, year = Get_File_Date(file)
        if month == 'stop':
            print('Stopping data this past Sunday: ', str(sunday_stop))
            break
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Deal with JHU region type label changes
        areaLabel = Get_Loc_Type_JHU(month, day_index, year)

        # Update confirmed, death, and recovery counts
        Append_Data(data)

        day_count += 1

# Revert South Korea name, if location is South Korea
if location == 'Korea, South':
    location = 'South Korea'

# Store most recent deaths and most recent confirmed cases
# Predicted new cases and deaths are later added to these to get aggregates
most_recent_deaths = deaths[-1]
most_recent_confirmed = confirmedCases[-1]

###############################################################
#                                                             #
#            Trim Data and Create Active Case List            #
#                                                             #
###############################################################

# Create cutoff to remove early days with too few cases
def Get_First_Day(untrimmed_list):
    # If cases in a region never above 2500, include all days
    if max(untrimmed_list) < 2500:
        threshold = 0
    # Else, if cases never above 100000, include all days after .5% of max reached
    elif max(untrimmed_list) < 100000:
        threshold = .005 * max(confirmedCases)
    # Else, include all days after first 1000 cases
    else:
        threshold = 1000

    # Loop over days, and see when cases get high enough to include
    day = 0
    for val in untrimmed_list:
        # If cases high enough, return given day and stop loop
        if val > threshold:
            first_day = day
            print('\nFirst day of count: ' + str(first_day))
            if location == 'US':
                return 145, [i for i in range(day_count - 145)]
            if location == 'Michigan':
                return 295, [i for i in range(day_count - 295)]
            return 135, [i for i in range(day_count - 135)]
            return first_day, [i for i in range(day_count - first_day)]
        day += 1

# Cut off first few days, based on thresholds in Get_First_Day
def Truncate_From_Min(untrimmed_lists):
    trimmed_lists = []
    for list_i in untrimmed_lists:
        trimmed_lists.append(list_i[minRange:])
    return trimmed_lists

# Set first day for inclusion, as well as a list of day indices
minRange, float_dates = Get_First_Day(confirmedCases)

# Active cases are total cases - deaths - recoveries
active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]
# Total "recoveries" include both recoveries and deaths
r_sum = [recovered[i] + deaths[i] for i in range(len(recovered))]
# Cut off days with too few cases
r_sum_cut, deaths, confirmed = Truncate_From_Min([r_sum, deaths, confirmedCases])

print('length of confirmed: ' + str(len(confirmed)))
##########################################################
#                                                        #
#                   Get Positivity Data                  #
#                                                        #
##########################################################

# Get date of day corresponding to minRange
# JHU data starts on January 22, so add minRange to January 22
def Get_First_Date():
    first_day_US = parse('January 22, 2020')
    return (first_day_US + timedelta(days = minRange)).strftime('%Y-%m-%d')

def Get_Testing_Data(region) :
    if on_server :
        fileList = glob.glob('COVID-19_Diagnostic_Laboratory_Testing__PCR_Testing__Time_Series.csv')
    else :
        fileList = glob.glob('../gen_csvs/COVID-19_Diagnostic_Laboratory_Testing__PCR_Testing__Time_Series.csv')
    # stand in handler for multiple files
    if len(fileList) != 1 :
        print("multiple or no files, correct file may not be selected")
        # latest date first
        fileList.sort(reverse=True)
    # reads in data
    preprocess_testing_data = pd.read_csv(fileList[0])
    # if state, selects subset of data, else gives warning message
    if region != 'US' :
        states = ['Virgin Islands','Puerto Rico','District of Colombia', 'Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

        if region not in states :
            print("Region not yet accepted")

        preprocess_testing_data = preprocess_testing_data.loc[preprocess_testing_data["state_name"] == region]

    # sorts by date
    preprocess_testing_data = preprocess_testing_data.sort_values("date",axis=0)
    # creates dates for iteration
    parsed_first_date = parse(Get_First_Date())
    total_days = (parse(sunday_stop) - parsed_first_date).days
    # list to be added to
    positivity_rate_list = []
    # iterates through the days
    for day in range(total_days) :
        # selects the date
        current_date = (parsed_first_date+timedelta(day)).strftime('%Y/%m/%d')
        # selects the data
        selected_days_data = preprocess_testing_data.loc[preprocess_testing_data['date'] == current_date]
        #selects positive/inconclusive/negative data and calculates positivity accordingly
        positive_tests = selected_days_data.loc[selected_days_data['overall_outcome'] == 'Positive']
        num_pos_tests = positive_tests['new_results_reported'].sum()
        inconclusive_tests = selected_days_data.loc[selected_days_data['overall_outcome'] == 'Inconclusive']
        num_inc_tests = inconclusive_tests['new_results_reported'].sum()
        negative_tests = selected_days_data.loc[selected_days_data['overall_outcome'] == 'Negative']
        num_neg_tests = negative_tests['new_results_reported'].sum()
        denominator_value = num_pos_tests+num_inc_tests+num_neg_tests
        # checks for zeros
        # currently selects previous value, but may need something like SD reachback for unupdated dataset
        if denominator_value == 0 :
            positivity_rate = positivity_rate_list[-1]
        else :
            positivity_rate = num_pos_tests/denominator_value
        positivity_rate_list.append(positivity_rate)
    return positivity_rate_list

covid_test_data = Get_Testing_Data(location)
positivity_list = [covid_test_data[:days_to_include]]

##########################################################
#                                                        #
#                     Get Vaccine Data                   #
#                                                        #
##########################################################

def Get_Vaccine_Data_New(region) :
    # file
    if on_server :
        path_to_file = 'us_state_vaccinations.csv'
    else :
        path_to_file = '../gen_csvs/us_state_vaccinations.csv'
    unprocessed_df = pd.read_csv(path_to_file)
    if region != 'US' :
        # selects state data
        unprocessed_df = unprocessed_df.loc[unprocessed_df['location'] == region]
    # sorts by date
    unprocessed_df = unprocessed_df.sort_values('date', axis=0)
    # sets up iteration
    start_date = parse('January 12, 2021')
    time_difference = (parse(sunday_stop) - start_date).days
    pre_start_date = parse(Get_First_Date())
    unfilled_days = (start_date-pre_start_date).days
    vaccine_data = []
    for day in range(unfilled_days) :
        # days before data is available or days before vaccines, fill 0
        vaccine_data.append(0)
    for day in range(time_difference) :
        selected_data = unprocessed_df.loc[unprocessed_df['date'] == (start_date + timedelta(day)).strftime('%Y-%m-%d')]
        if selected_data.empty :
            vaccine_data.append(vaccine_data[-1])
        else :
            vaccines_admin = selected_data['people_vaccinated'].sum()
            vaccine_data.append(vaccines_admin)

    # patches '2021-01-16','2021-01-17','2021-01-18','2021-02-15' - all missing from the data
    # averages the increase over the gap and gives each day the average increase per day
    three_day_jan_patch_daily = (vaccine_data[7+unfilled_days]-vaccine_data[3+unfilled_days])/4
    for i in range(3) :
        i += 1
        vaccine_data[3+i+unfilled_days] = vaccine_data[3+unfilled_days] + three_day_jan_patch_daily*i
    vaccine_data[34+unfilled_days] = (vaccine_data[35+unfilled_days] + vaccine_data[33+unfilled_days])/2

    return vaccine_data
if with_vacc :
    vaccination_data = Get_Vaccine_Data_New(location)
    positivity_list.append(vaccination_data[:days_to_include])


##############################################################
#                                                            #
#                      Get Hospital Data                     #
#                                                            #
##############################################################

# Reads in hospital data
def Get_Hospital_Data(region) :
    # data sheet file name may vary, this may need to be adjusted
    # reads data
    if on_server :
        fileList = glob.glob('COVID-19_Reported_Patient_Impact_and_Hospital_Capacity_by_State_Timeseries.csv')
    else :
        fileList = glob.glob('../gen_csvs/COVID-19_Reported_Patient_Impact_and_Hospital_Capacity_by_State_Timeseries.csv')
    # stand in handler for multiple files
    if len(fileList) != 1 :
        print("multiple or no files, correct file may not be selected")
        # latest date first
        fileList.sort(reverse=True)
    # reads in file
    preprocess_hospital_data = pd.read_csv(fileList[0])
    # sorts
    preprocess_hospital_data = preprocess_hospital_data.sort_values('date', axis=0)
    # resets index
    preprocess_hospital_data = preprocess_hospital_data.reset_index(drop=True)

    # starting list and time difference
    inpatient_covid_beds = []
    start_date = parse(Get_First_Date())
    time_difference = (parse(sunday_stop)-start_date).days

    if region != 'US' :
        # lists of states and abbreviations to work with the hosptial file
        states = ['Virgin Islands','Puerto Rico','District of Colombia', 'Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']
        abbreviations = ['VI','PR','DC','AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA','WA','WV','WI','WY']

        # converts to abbreviations
        if region in states :
            state_abbreviation = abbreviations[states.index(region)]
        else :
            print("region not implemented")

        # selects only the states data
        preprocess_hospital_data = preprocess_hospital_data.loc[preprocess_hospital_data['state'] == state_abbreviation]
        # resets index
        preprocess_hospital_data = preprocess_hospital_data.reset_index(drop=True)
        # gets index of the first day, for the uses of the predate sum
        day_after_sum_index = preprocess_hospital_data.loc[preprocess_hospital_data['date'] == (start_date).strftime('%Y/%m/%d')].index.item()
    else :
        # gets index of the first day, for the uses of the predate sum
        day_after_sum_index = preprocess_hospital_data.loc[preprocess_hospital_data['date'] == (start_date).strftime('%Y/%m/%d')].index[0].item()

    # sums all data from before the first day
    predate_total = preprocess_hospital_data['inpatient_beds_used_covid'][:day_after_sum_index].sum()

    # iterates through selected days
    for day in range(time_difference) :
        # selects day
        selected_date = (start_date+timedelta(day)).strftime('%Y/%m/%d')
        # selects data
        selected_data = preprocess_hospital_data.loc[preprocess_hospital_data['date'] == selected_date]
        # gets number of covid beds
        beds_num = selected_data['inpatient_beds_used_covid'].sum()
        # adds to list
        inpatient_covid_beds.append(beds_num)

    return inpatient_covid_beds, predate_total

# Gets hospoital data
hospital_data, hospital_predate_sum = Get_Hospital_Data(location)

# Copied but modified
def Daily_To_Cumulative_Hospital(data_list,start_val) :
    cumulative = []
    for i in range(len(data_list)):
        # Start from baseline saved cumulative value
        if i == 0:
            cumulative.append(int(start_val + data_list[i]))
        # Add most recent cumulative value to most recent incident value
        else:
            cumulative.append(int(cumulative[i-1] + data_list[i]))
    return cumulative

# Uses outputs from the read hospital data to form a cumulative data list similar to the others
hospital = Daily_To_Cumulative_Hospital(hospital_data,hospital_predate_sum)

##############################################################
#                                                            #
#                 Prepare Data for Regression                #
#                                                            #
##############################################################

# For testing: using a savitzky-golay filter to smooth the data and improve predictions
def Sav_Gol(unsmooth_data):
    daily_data = signal.savgol_filter(unsmooth_data, 53, 3).tolist()
    x_ax = [i for i in range(len(unsmooth_data))]
    plt.plot(x_ax, unsmooth_data)
    plt.plot(x_ax, daily_data)
    plt.show()
    return daily_data

# Convert cumulative data to daily data, since predictions based based off of incident values
def cumulativeToDaily(cumulative_lists, smooth = False):
    daily_list = []
    for list_i in cumulative_lists:
        daily = []
        for i in range(len(list_i)):
            if not i == 0:
                # Subtract each "yesterday's" values from the current day's
                daily.append(list_i[i] - list_i[i-1])
        # For testing, currently unused: smooth case and death data to remove spikes
        #if smooth:
            #daily = Sav_Gol(daily)
        daily_list.append(daily)
    return daily_list

# Convert daily data to cumulative data
# Can be used to test aggregate predictions based off the sum of incident predictions
def dailyToCumulative(daily_list_start_pairs):
    cumulative_list = []
    for (list_i, start_val) in daily_list_start_pairs:
        cumulative = []
        for i in range(len(list_i) + 1):
            # Start from baseline saved cumulative value
            if i == 0:
                cumulative.append(start_val)
            # Add most recent cumulative value to most recent incident value
            else:
                cumulative.append(cumulative[i-1] + list_i[i-1])
        cumulative_list.append(cumulative)
    return cumulative_list

# Remove first <order> days of data for fitting the ridge model
# Order is the number of days used to predict each upcoming day
# E.g., if 21 days are used to predict each upcoming day, the first 21 should be removed, since they can't be predicted
def Truncate_For_Ridge(data_list):
    truncated_lists = []
    for list_i in data_list:
        truncated_lists.append(list_i[order:])
    return truncated_lists

# Create constant arrays for aggregate model performance checks
active_arr_checker, deaths_arr_checker, confirmed_arr_checker, recovered_arr_checker, hospital_arr_checker = active[:], deaths[:], confirmed[:], r_sum_cut[:], hospital[:]
#print (deaths_arr_checker)
# Create constant arrays for incident model performance checks
daily_active_arr_checker, daily_recovered_arr_checker, daily_deaths_arr_checker, daily_confirmed_arr_checker, daily_hospital_arr_checker = cumulativeToDaily([active_arr_checker, recovered_arr_checker, deaths_arr_checker, confirmed_arr_checker, hospital_arr_checker])
#print(daily_deaths_arr_checker)


for i in range(len(daily_deaths_arr_checker)):
    if daily_deaths_arr_checker[i] == 0 or daily_deaths_arr_checker[i] < 0:
        daily_deaths_arr_checker[i] = 1.
    if daily_confirmed_arr_checker[i] == 0 or daily_confirmed_arr_checker[i] < 0:
        daily_confirmed_arr_checker[i] = 1.
    if daily_hospital_arr_checker[i] == 0 or daily_hospital_arr_checker[i] < 0:
        daily_hospital_arr_checker[i] = 1.

### TEST
daily_active_arr_checker, daily_recovered_arr_checker, daily_deaths_arr_checker, daily_confirmed_arr_checker, daily_hospital_arr_checker = daily_active_arr_checker[:days_to_include+28], daily_recovered_arr_checker[:days_to_include+28], daily_deaths_arr_checker[:days_to_include+28], daily_confirmed_arr_checker[:days_to_include+28], daily_hospital_arr_checker[:days_to_include+28]

# Create daily active, recovered, deaths, and confirmed lists for incident value prediction making
daily_active, daily_recovered = cumulativeToDaily([active, r_sum_cut])
daily_confirmed, daily_deaths = cumulativeToDaily([confirmed, deaths], False)
daily_hospital = cumulativeToDaily([hospital])[0]
### TEST
daily_active, daily_recovered, daily_confirmed, daily_deaths, daily_hospital = daily_active[:days_to_include], daily_recovered[:days_to_include], daily_confirmed[:days_to_include], daily_deaths[:days_to_include], daily_hospital[:days_to_include]

if location == 'Michigan' :
    if len(daily_deaths_arr_checker) > 93:
        daily_deaths_arr_checker[93] = 26.
        daily_confirmed_arr_checker[93] = 400
    if len(daily_deaths_arr_checker) > 94 :
        daily_deaths_arr_checker[94] = 26.
        daily_confirmed_arr_checker[94] = 400
    if len(daily_deaths_arr_checker) > 95 :
        daily_deaths_arr_checker[95] = 26.
        daily_confirmed_arr_checker[95] = 400

# Trim first days of data for fitting the model (See function comments)
active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge, hospital_for_ridge = Truncate_For_Ridge([active, r_sum_cut, deaths, confirmed, hospital])
### TEST
active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge, hospital_for_ridge = active_for_ridge[:days_to_include], recovered_for_ridge[:days_to_include], deaths_for_ridge[:days_to_include], confirmed_for_ridge[:days_to_include], hospital_for_ridge[:days_to_include]

# Create prediction matrix, as in LaTeX model description
def Create_Pred_Matrices(full_data_lists):
    matrices = []
    for list_i in full_data_lists:
        pred_matrix = []
        for i in range(len(list_i) - order):
            new_row_daily = [1]
            for j in range(order):
                new_row_daily.append(list_i[i+j])
            for positivity in positivity_list:
                for val in positivity[i:order+i]:
                    new_row_daily.append(val)
            pred_matrix.append(new_row_daily)
        matrices.append(pred_matrix)
    return matrices

# Remove negative case, death, recovery, and active values
# Come from data-reporting anomalies, so don't belong in predictions
def Clean_Sub_Zero(full_data_lists):
    no_zeros_list = []
    for list_i in full_data_lists:
        for i in range(len(list_i)):
            if list_i[i] < 0:
                list_i[i] = 0
        no_zeros_list.append(list_i)
    return no_zeros_list

# Create matrices, as in LaTeX document, for active cases, recoveries, deaths, and confirmed cases
daily_active_pred_matrix, daily_recovered_pred_matrix, daily_deaths_pred_matrix, daily_confirmed_pred_matrix, daily_hospital_pred_matrix = Create_Pred_Matrices([daily_active, daily_recovered, daily_deaths, daily_confirmed, daily_hospital])
# Remove 0's
active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge, hospital_for_ridge = Clean_Sub_Zero([active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge, hospital_for_ridge])
# Trim first days of data for fitting incident data
### TEST
daily_active_for_ridge, daily_recovered_for_ridge, daily_deaths_for_ridge, daily_confirmed_for_ridge, daily_hospital_for_ridge = Truncate_For_Ridge([daily_active, daily_recovered, daily_deaths, daily_confirmed, daily_hospital])
daily_active_for_ridge, daily_recovered_for_ridge, daily_deaths_for_ridge, daily_confirmed_for_ridge, daily_hospital_for_ridge = daily_active_for_ridge[:days_to_include-order], daily_recovered_for_ridge[:days_to_include-order], daily_deaths_for_ridge[:days_to_include-order], daily_confirmed_for_ridge[:days_to_include-order], daily_hospital_for_ridge[:days_to_include-order]


# Make constant values to revert to for both daily and cumulative model
daily_active_arr_const, daily_active_for_ridge_const = daily_active[:], daily_active_for_ridge[:]
daily_recovered_arr_const, daily_recovered_for_ridge_const = daily_recovered[:], daily_recovered_for_ridge[:]
daily_deaths_arr_const, daily_deaths_for_ridge_const = daily_deaths[:], daily_deaths_for_ridge[:]
daily_confirmed_arr_const, daily_confirmed_for_ridge_const = daily_confirmed[:], daily_confirmed_for_ridge[:]
daily_hospital_arr_const, daily_hospital_for_ridge_const = daily_hospital[:], daily_hospital_for_ridge[:]
active_for_ridge_const, recovered_for_ridge_const, deaths_for_ridge_const, confirmed_for_ridge_const, hospital_for_ridge_const = active_for_ridge[:], recovered_for_ridge[:], deaths_for_ridge[:], confirmed_for_ridge[:], hospital_for_ridge[:]

# Generate a range of ridge regression penalty alphas to try
def Generate_Alphas(coeff_list):
    alpha_list = []
    i = -3
    while i < 12:
        for coeff in coeff_list:
            alpha_list.append(coeff * (10**(-i)))
        i += 1
    params = {'alpha': alpha_list}
    return params

parameters = Generate_Alphas([1., 2.5, 5., 7.5])

# Placeholders for the best models
best_active_model, best_recovered_model, best_deaths_model, best_confirmed_model, best_hospital_model = [], [], [], [], []
# Empty lists for all models created around the best one
confirmed_model_list, deaths_model_list, hospital_model_list = [], [], []

###################################################
#                                                 #
#            Perform Ridge Regression             #
#                                                 #
###################################################

# Create different types of ridge regressor, based on circumstances
def Make_Ridge(input_list, ridge_type, alpha_list = [0]):
    ridge_list = []

    # Create bare-bones ridge regressors to feed into grid searcher
    if ridge_type == 'no_cv':
        for list_i in input_list:
            ridge_list.append(Ridge(normalize = True))

    # Create ridge regressor with only penalty specified, based on grid search results
    if ridge_type == 'no_cv_alpha_spec':
        for i in range(len(input_list)):
            ridge_list.append(Ridge(normalize = True, alpha = alpha_list[i]))

    # Create grid searcher to find best penalties
    if ridge_type == 'cv':
        for list_i in input_list:
            ridge_list.append(GridSearchCV(list_i, parameters, scoring = 'neg_mean_squared_error', cv = 10))

    return ridge_list

# Create grid searchers to find optimal ridge penalties for active and total cases, deaths, and recoveries
r_active, r_recovered, r_deaths, r_confirmed, r_hospital = Make_Ridge([active, recovered, deaths, confirmed, hospital], 'no_cv')
ridge_active_cv, ridge_recovered_cv, ridge_deaths_cv, ridge_confirmed_cv, ridge_hospital_cv = Make_Ridge([r_active, r_recovered, r_deaths, r_confirmed, r_hospital], 'cv')

# Fit ridge regressors, and return necessary values or predictions, depending on circumstances
def Fit_Ridge(prediction_triads, fit_type):
    # For grid searchers, return optimal penalty and best score
    if fit_type == 'round_1_eval':
        alpha_mid_list, score_list = [], []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            alpha_mid_list.append(regressor.best_params_['alpha'])
            score_list.append(regressor.best_score_)
        return alpha_mid_list, score_list

    # For non-grid searches, create future predictions
    if fit_type == 'make_pred':
        model_list = []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            model = regressor.predict(matrix)
            model_list.append(model.tolist())
        return model_list

    # For non-grid searches, create future predictions
    if fit_type == 'make_coef':
        coef_list = []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            coefs = regressor.coef_
            coef_list.append(coefs)
        return coef_list

# Creating triples of values for fitting
# Triples include the regressor, the prediction matrix, and the vector from the RHS of the LaTeX document
active_triad, recovered_triad, deaths_triad, confirmed_triad, hospital_triad = (ridge_active_cv, daily_active_pred_matrix, daily_active_for_ridge), (ridge_recovered_cv, daily_recovered_pred_matrix, daily_recovered_for_ridge), (ridge_deaths_cv, daily_deaths_pred_matrix, daily_deaths_for_ridge), (ridge_confirmed_cv, daily_confirmed_pred_matrix, daily_confirmed_for_ridge), (ridge_hospital_cv, daily_hospital_pred_matrix, daily_hospital_for_ridge)
# Get best penalties and performance metrics
assigned_alphas, assigned_scores = Fit_Ridge([active_triad, recovered_triad, deaths_triad, confirmed_triad, hospital_triad], 'round_1_eval')
alpha_mid_active, alpha_mid_recovered, alpha_mid_deaths, alpha_mid_confirmed, alpha_mid_hospital = assigned_alphas[0], assigned_alphas[1], assigned_alphas[2], assigned_alphas[3], assigned_alphas[4]
score_active, score_recovered, score_deaths, score_confirmed, score_hospital = assigned_scores[0], assigned_scores[1], assigned_scores[2], assigned_scores[3], assigned_scores[4]

print(str(alpha_mid_active) + ' ' + str(alpha_mid_recovered) + ' ' + str(alpha_mid_deaths) + ' ' + str(alpha_mid_confirmed) + ' ' + str(alpha_mid_hospital))
print('Score Active: ' + str(score_active) + '\nScore Recovered: ' + str(score_recovered) + '\nScore Deaths: ' + str(score_deaths) + '\nScore Confirmed: ' + str(score_confirmed) + '\nScore Hospital: ' + str(score_hospital))

ridge_deaths, ridge_confirmed, ridge_hospital = Make_Ridge([deaths, confirmed, hospital], 'no_cv_alpha_spec', [alpha_mid_deaths, alpha_mid_confirmed, alpha_mid_hospital])

coef_deaths, coef_confirmed, coef_hospital = Fit_Ridge([(ridge_deaths, daily_deaths_pred_matrix, daily_deaths_for_ridge), (ridge_confirmed, daily_confirmed_pred_matrix, daily_confirmed_for_ridge), (ridge_hospital, daily_hospital_pred_matrix, daily_hospital_for_ridge)], 'make_coef')

#print(coef_deaths)
#print(coef_confirmed)

##############################################################
#                                                            #
#                        Bootstrapping                       #
#                                                            #
##############################################################

required_death_quantiles = [.01, .025, .05, .1, .15, .2, .25, .3, .35, .4, .45, .5, .55, .6, .65, .7, .75, .8, .85, .9, .95, .975, .99]
required_conf_quantiles = [0.025, 0.100, 0.250, 0.500, 0.750, 0.900, 0.975]
required_hospital_quantiles = required_death_quantiles[:]
# Fills in missing values from when the "for_ridge" lists were created without the first <order> days
# Useful for plotting, to see the full spectrum of data
def Model_Baseline(unmatched_models, baseline_lists):
    matched_lists = []
    for i in range(len(unmatched_models)):
        list_i = unmatched_models[i]
        baseline_list_i = baseline_lists[i]
        for j in range(0, order):
            list_i.insert(j, baseline_list_i[j])
        matched_lists.append(list_i)
    return matched_lists

def Create_Samples(lists_for_sampling):
    sample_list = []
    for list_i in lists_for_sampling:
        sample = [1]
        i = -order
        while i < 0:
            sample.append(list_i[i])
            i += 1
        for positivity in positivity_list:
           for val in positivity[-order:]:
               sample.append(val)
        sample_list.append(np.asarray(sample, dtype = 'object'))#.reshape(1,-1))
    return sample_list

# Create ridge regressor predictions based on samples
def Predict_Ridge(models_list, samples_list):
    next_list = []
    for i in range(len(models_list)):
        next_val = models_list[i].predict(samples_list[i])
        next_list.append(next_val)
    return next_list

def Append_To_Col_Vect(col_vects, new_vals):
    appended_lists = []
    for i in range(len(col_vects)):
        list_i = col_vects[i]
        new_val = new_vals[i]
        list_i.append(new_val)
        appended_lists.append(list_i)
    return appended_lists

confirmed_cumulative_start = 0
deaths_cumulative_start = 0
hospital_cumulative_start = 0
# Function adapted from: https://saattrupdan.github.io/2020-03-01-bootstrap-prediction/
def Prediction_Interval(model, X_train, y_train, x0, choosingVar):
  X_train = np.asarray(X_train)
  y_train = np.asarray(y_train)
  # Number of training samples
  n = X_train.shape[0]

  # The authors choose the number of bootstrap samples as the square root
  # of the number of samples
  nbootstraps = int(np.sqrt(n))

  # Compute the m_i's and the validation residuals
  bootstrap_preds, val_residuals = np.empty(nbootstraps), []
  for b in range(nbootstraps):
    train_idxs = np.random.choice(range(n), size = n, replace = True)
    val_idxs = np.array([idx for idx in range(n) if idx not in train_idxs])
    model.fit(X_train[train_idxs, :], y_train[train_idxs])
    preds = model.predict(X_train[val_idxs])
    val_residuals.append(y_train[val_idxs] - preds)
    bootstrap_preds[b] = model.predict(x0)
  bootstrap_preds -= np.mean(bootstrap_preds)
  val_residuals = np.concatenate(val_residuals)

  # Compute the prediction and the training residuals
  model.fit(X_train, y_train)
  preds = model.predict(X_train)
  if choosingVar == 0:
      confirmed_cumulative_start = preds[-1]
  elif choosingVar == 1:
      deaths_cumulative_start = preds[-1]
  else :
      hospital_cumulative_start = preds[-1]
  train_residuals = y_train - preds

  # Take percentiles of the training- and validation residuals to enable
  # comparisons between them
  val_residuals = np.quantile(val_residuals, q = np.arange(100)/100)
  train_residuals = np.quantile(train_residuals, q = np.arange(100)/100)

  # Compute the .632+ bootstrap estimate for the sample noise and bias
  no_information_error = np.mean(np.abs(np.random.permutation(y_train) - \
    np.random.permutation(preds)))
  generalisation = np.abs(val_residuals - train_residuals)
  no_information_val = np.abs(no_information_error - train_residuals)
  relative_overfitting_rate = np.mean(generalisation / no_information_val)
  weight = .632 / (1 - .368 * relative_overfitting_rate)
  residuals = (1 - weight) * train_residuals + weight * val_residuals

  # Construct the C set and get the percentiles
  C = np.array([m + o for m in bootstrap_preds for o in residuals])

  if choosingVar == 0:
    percentiles = np.quantile(C, q = required_conf_quantiles)
  elif choosingVar == 1:
    percentiles = np.quantile(C, q = required_death_quantiles)
  else :
    percentiles = np.quantile(C, q = required_hospital_quantiles)

  y_hat = model.predict(x0)
  #percentiles = [y_hat + percentile for percentile in percentiles]
  return percentiles, y_hat

stop = int(len(daily_active_arr_checker)) + 40 - 1
start = len(daily_active) - 1
quantiles_confirmed, quantiles_deaths, quantiles_hospital = [], [], []
points_confirmed, points_deaths, points_hospital = [], [], []

daily_confirmed_for_ridge_orig = daily_confirmed_for_ridge[:]
daily_deaths_for_ridge_orig = daily_deaths_for_ridge[:]
daily_hospital_for_ridge_orig = daily_hospital_for_ridge[:]

# Make ~6 weeks worth of predictions
t = start
while t < stop:
    # Make predictions
    this_day_quantiles_confirmed, this_day_point_confirmed = Prediction_Interval(ridge_confirmed, daily_confirmed_pred_matrix, daily_confirmed_for_ridge_orig, Create_Samples([daily_confirmed_for_ridge]), 0)
    this_day_quantiles_deaths, this_day_point_deaths = Prediction_Interval(ridge_deaths, daily_deaths_pred_matrix, daily_deaths_for_ridge_orig, Create_Samples([daily_deaths_for_ridge]), 1)
    this_day_quantiles_hospital, this_day_point_hospital = Prediction_Interval(ridge_hospital, daily_hospital_pred_matrix, daily_hospital_for_ridge_orig, Create_Samples([daily_hospital_for_ridge]), 2)
    # Append predictions to model
    daily_deaths_for_ridge, daily_deaths, daily_confirmed_for_ridge, daily_confirmed, daily_hospital_for_ridge, daily_hospital = Append_To_Col_Vect([daily_deaths_for_ridge, daily_deaths, daily_confirmed_for_ridge, daily_confirmed, daily_hospital_for_ridge, daily_hospital], [this_day_point_deaths, this_day_point_deaths, this_day_point_confirmed, this_day_point_confirmed, this_day_point_hospital, this_day_point_hospital])

    # Quantiles are additive - like SDev
    quantiles_confirmed.append(this_day_quantiles_confirmed)
    points_confirmed.append(this_day_point_confirmed)
    quantiles_deaths.append(this_day_quantiles_deaths)
    points_deaths.append(this_day_point_deaths)
    quantiles_hospital.append(this_day_quantiles_hospital)
    points_hospital.append(this_day_point_hospital)

    t += 1

points_confirmed_aggregate, points_deaths_aggregate, points_hospital_aggregate = dailyToCumulative([(points_confirmed, confirmed_cumulative_start + confirmedCases[-1]), (points_deaths, deaths_cumulative_start + deaths[-1]), (points_hospital, hospital_cumulative_start + hospital[-1])])

# Plot deaths and confirmed case results
'''
plt.plot(daily_deaths[:start+28], 'g', linestyle = '--', label = 'model')
plt.plot(daily_deaths_arr_checker[:start+28], 'k', label = 'data')
plt.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Deaths')
plt.title('Deaths Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_deaths.pdf')
plt.show()

plt.plot([i for i in range(len(daily_confirmed_arr_checker[:start+28]))], daily_confirmed_arr_checker[:start+28], color = 'black', label = 'data')
plt.plot([i for i in range(len(daily_confirmed[:start+28]))], daily_confirmed[:start+28], color = 'g', linestyle = '--', label = 'model')
plt.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Confirmed')
plt.title('Confirmed Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()

plt.plot([i for i in range(len(daily_hospital_arr_checker[:start+28]))], daily_hospital_arr_checker[:start+28], color = 'black', label = 'data')
plt.plot([i for i in range(len(daily_hospital[:start+28]))], daily_hospital[:start+28], color = 'g', linestyle = '--', label = 'model')
plt.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Hospital')
plt.title('Hospital Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_hospital.pdf')
plt.show()
'''
if on_server is False :
    fig, ax1 = plt.subplots()
    ax1.set_xlabel('Days')
    ax1.set_ylabel('Daily Hospital')
    ax1.plot([i for i in range(len(daily_hospital_arr_checker[:start+28]))], daily_hospital_arr_checker[:start+28], color = 'black', label = 'data')
    #ax1.plot([i for i in range(len(daily_hospital_arr_checker[:start]))], daily_hospital_arr_checker[:start], color = 'black', label = 'data')
    ax1.plot([i for i in range(len(daily_hospital[:start+28]))], daily_hospital[:start+28], color = 'g', linestyle = '--', label = 'model')
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Positivity')
    ax2.plot(positivity_list[0][:start+28], 'g')
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.legend(loc = 'upper left')
    plt.title("Hospitalizatons and Positivity Rate: " + location)
    #plt.savefig('hospital_positivity.pdf')
    plt.show()

    fig, ax1 = plt.subplots()
    ax1.set_xlabel('Days')
    ax1.set_ylabel('Daily Hospital')
    ax1.plot([i for i in range(len(daily_hospital_arr_checker[:start+28]))], daily_hospital_arr_checker[:start+28], color = 'black', label = 'data')
    #ax1.plot([i for i in range(len(daily_hospital_arr_checker[:start]))], daily_hospital_arr_checker[:start], color = 'black', label = 'data')
    ax1.plot([i for i in range(len(daily_hospital[:start+28]))], daily_hospital[:start+28], color = 'g', linestyle = '--', label = 'model')
    ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
    ax2.set_ylabel('Vaccinations')
    ax2.plot(positivity_list[1][:start+28], 'g')
    fig.tight_layout()  # otherwise the right y-label is slightly clipped
    plt.legend(loc = 'upper left')
    plt.title("Hospitalizatons and Cumulative Vaccinations: " + location)
    #plt.savefig('hospital_vaccinations.pdf')
    plt.show()


##############################################################
#                                                            #
#                    Find Future Pred Error                  #
#                                                            #
##############################################################

strings_for_file = []

# Get MPE (Mean Percent Error) over given date range
def Get_MPE(range_list):
    sum_percent_error_conf = 0
    sum_percent_error_death = 0
    sum_percent_error_hospital = 0
    for i in range(range_list[0], range_list[1]):
        sum_percent_error_conf += abs(100*(daily_confirmed[i] - daily_confirmed_arr_checker[i])/daily_confirmed_arr_checker[i])
        sum_percent_error_death += abs(100*(daily_deaths[i] - daily_deaths_arr_checker[i])/daily_deaths_arr_checker[i])
        sum_percent_error_hospital += abs(100*(daily_hospital[i] - daily_hospital_arr_checker[i])/daily_hospital_arr_checker[i])
    num_pts = range_list[1] - range_list[0]
    mean_percent_error_conf = sum_percent_error_conf/num_pts
    mean_percent_error_death = sum_percent_error_death/num_pts
    mean_percent_error_hospital = sum_percent_error_hospital/num_pts

    conf_str = 'Confirmed case MPE for range ' + str(range_list[0]) + ' to ' + str(range_list[1]) + ': ' + str(abs(mean_percent_error_conf)) + '\n'
    death_str = 'Death MPE for range ' + str(range_list[0]) + ' to ' + str(range_list[1]) + ": " + str(abs(mean_percent_error_death)) + '\n'
    hospital_str = 'Hospital MPE for range ' + str(range_list[0]) + ' to ' + str(range_list[1]) + ": " + str(abs(mean_percent_error_hospital)) + '\n'

    strings_for_file.append(conf_str), strings_for_file.append(death_str), strings_for_file.append(hospital_str)
    print(conf_str)
    print(death_str)
    print(hospital_str)

# Print MPE for each of 4 upcoming weeks
for i in range(4):
    Get_MPE([(start+1)+i*7, (start+1)+(i+1)*7])

MPE_file = open(r"./"+location+"/"+str(days_to_include)+"_MPEs.txt", "w")
MPE_file.writelines(strings_for_file)
MPE_file.close()
