import pandas as pd
import matplotlib.pyplot as plt

import statistics as stats

entities = ['US','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

def process_state(state_name) :
    index = state_name.find(' ')
    if index > 0 :
        return state_name[:index]+state_name[index+1:]
    else :
        return state_name

states_to_check_out = set()
individual_checking_out_points = []

trigger_value = -100

diff_1_tot = []
diff_2_tot = []
diff_3_tot = []
diff_4_tot = []
MPE_vacc_1_tot = []
MPE_vacc_2_tot = []
MPE_vacc_3_tot = []
MPE_vacc_4_tot = []
MPE_no_vacc_1_tot = []
MPE_no_vacc_2_tot = []
MPE_no_vacc_3_tot = []
MPE_no_vacc_4_tot = []
'''
for entity in entities :
    folder_str = 'vaccine'
    state_str = entity

    MPE_data = pd.read_csv('./' + folder_str + '/by_date_MPEs_'+state_str + '.csv')
    MPE_data = MPE_data.sort_values('training_days', axis=0)
    training_days = MPE_data['training_days'].tolist()
    MPE_week_1_vacc = MPE_data['1wk_ahead_MPE_hospital'].tolist()
    MPE_week_2_vacc  = MPE_data['2wk_ahead_MPE_hospital'].tolist()
    MPE_week_3_vacc  = MPE_data['3wk_ahead_MPE_hospital'].tolist()
    MPE_week_4_vacc  = MPE_data['4wk_ahead_MPE_hospital'].tolist()

    folder_str = 'no_vaccine'

    MPE_data = pd.read_csv('./' + folder_str + '/by_date_MPEs_'+state_str + '.csv')
    MPE_data = MPE_data.sort_values('training_days', axis=0)
    training_days = MPE_data['training_days'].tolist()
    MPE_week_1 = MPE_data['1wk_ahead_MPE_hospital'].tolist()
    MPE_week_2 = MPE_data['2wk_ahead_MPE_hospital'].tolist()
    MPE_week_3 = MPE_data['3wk_ahead_MPE_hospital'].tolist()
    MPE_week_4 = MPE_data['4wk_ahead_MPE_hospital'].tolist()

    MPE_diff_1 = []
    MPE_diff_2 = []
    MPE_diff_3 = []
    MPE_diff_4 = []
    for i in range(len(MPE_week_4)) :
        diff_1 = MPE_week_1[i]-MPE_week_1_vacc[i]
        diff_2 = MPE_week_2[i]-MPE_week_2_vacc[i]
        diff_3 = MPE_week_3[i]-MPE_week_3_vacc[i]
        diff_4 = MPE_week_4[i]-MPE_week_4_vacc[i]
        MPE_diff_1.append(diff_1)
        MPE_diff_2.append(diff_2)
        MPE_diff_3.append(diff_3)
        MPE_diff_4.append(diff_4)
        diff_1_tot.append(diff_1)
        diff_2_tot.append(diff_2)
        diff_3_tot.append(diff_3)
        diff_4_tot.append(diff_4)

        MPE_vacc_1_tot.append(MPE_week_1_vacc[i])
        MPE_vacc_2_tot.append(MPE_week_2_vacc[i])
        MPE_vacc_3_tot.append(MPE_week_3_vacc[i])
        MPE_vacc_4_tot.append(MPE_week_4_vacc[i])
        MPE_no_vacc_1_tot.append(MPE_week_1[i])
        MPE_no_vacc_2_tot.append(MPE_week_2[i])
        MPE_no_vacc_3_tot.append(MPE_week_3[i])
        MPE_no_vacc_4_tot.append(MPE_week_4[i])

        if diff_1 < trigger_value or diff_2 < trigger_value or diff_3 < trigger_value or diff_4 < trigger_value:
            states_to_check_out.add(entity)
            individual_checking_out_points.append((entity,training_days[i]))
'''
'''
plt.plot(training_days,MPE_diff_1, 'b', label = '1 Wk Ahead')
plt.plot(training_days,MPE_diff_2, 'y', label = '2 Wks Ahead')
plt.plot(training_days,MPE_diff_3, 'r', label = '3 Wks Ahead')
plt.plot(training_days,MPE_diff_4, 'g', label = '4 Wks Ahead')
plt.xlabel('Days Used for Training')
plt.ylabel('Mean Percent Error Difference')
plt.title('Hospitalization MPE by Training Set Size Order 7\n Value Positive -> Vaccination Version Outperforms Regular Version')
plt.legend(loc = 'upper right')
plt.savefig('./plots/hosp_MPE_'+ state_str +'.pdf')
plt.close()
#plt.show()
'''
'''
print(states_to_check_out,len(states_to_check_out),len(individual_checking_out_points))
plt.hist(diff_1_tot,[-100,-90,-80,-70,-60,-50,-40,-30,-20,-10,0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE Diff Wk 1 (Later Data) (Non Vaccine MPE - Vaccine MPE)")
median_diff_1 = str(stats.median(diff_1_tot))[:6]
sd_diff_1 = str(stats.pstdev(diff_1_tot))[:6]
plt.xlabel('Median: ' + median_diff_1 + '\n Standard Deviation: ' + sd_diff_1)
plt.show()
plt.hist(diff_2_tot,[-100,-90,-80,-70,-60,-50,-40,-30,-20,-10,0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE Diff Wk 2 (Later Data) (Non Vaccine MPE - Vaccine MPE)")
median_diff_2 = str(stats.median(diff_2_tot))[:6]
sd_diff_2 = str(stats.pstdev(diff_2_tot))[:6]
plt.xlabel('Median: ' + median_diff_2 + '\n Standard Deviation: ' + sd_diff_2)
plt.show()
plt.hist(diff_3_tot,[-100,-90,-80,-70,-60,-50,-40,-30,-20,-10,0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE Diff Wk 3 (Later Data) (Non Vaccine MPE - Vaccine MPE)")
median_diff_3 = str(stats.median(diff_3_tot))[:6]
sd_diff_3 = str(stats.pstdev(diff_3_tot))[:6]
plt.xlabel('Median: ' + median_diff_3 + '\n Standard Deviation: ' + sd_diff_3)
plt.show()
plt.hist(diff_4_tot,[-100,-90,-80,-70,-60,-50,-40,-30,-20,-10,0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE Diff Wk 4 (Later Data) (Non Vaccine MPE - Vaccine MPE)")
median_diff_4 = str(stats.median(diff_4_tot))[:6]
sd_diff_4 = str(stats.pstdev(diff_4_tot))[:6]
plt.xlabel('Median: ' + median_diff_4 + '\n Standard Deviation: ' + sd_diff_4)
plt.show()

plt.hist(MPE_vacc_1_tot,[0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE Vacc Wk 1 (Later Data)")
plt.show()
plt.hist(MPE_vacc_2_tot,[0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE Vacc Wk 2 (Later Data)")
plt.show()
plt.hist(MPE_vacc_3_tot,[0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE Vacc Wk 3 (Later Data)")
plt.show()
plt.hist(MPE_vacc_4_tot,[0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE Vacc Wk 4 (Later Data)")
plt.show()

plt.hist(MPE_no_vacc_1_tot,[0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE No Vacc Wk 1 (Later Data)")
plt.show()
plt.hist(MPE_no_vacc_2_tot,[0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE No Vacc Wk 2 (Later Data)")
plt.show()
plt.hist(MPE_no_vacc_3_tot,[0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE No Vacc Wk 3 (Later Data)")
plt.show()
plt.hist(MPE_no_vacc_4_tot,[0,10,20,30,40,50,60,70,80,90,100], density = True)
plt.title("MPE No Vacc Wk 4 (Later Data)")
plt.show()
'''



median_list_1 = []
sd_list_1 = []
median_list_2 = []
sd_list_2 = []
median_list_3 = []
sd_list_3 = []
median_list_4 = []
sd_list_4 = []
plot_list = []


for i in range(88) :
    diff_1_tot = []
    diff_2_tot = []
    diff_3_tot = []
    diff_4_tot = []

    for entity in entities :
        folder_str = 'vaccine'
        state_str = entity

        MPE_data = pd.read_csv('./' + folder_str + '/by_date_MPEs_'+state_str + '.csv')
        MPE_data = MPE_data.sort_values('training_days', axis=0)
        MPE_week_1_vacc = MPE_data['1wk_ahead_MPE_hospital'].tolist()
        MPE_week_2_vacc  = MPE_data['2wk_ahead_MPE_hospital'].tolist()
        MPE_week_3_vacc  = MPE_data['3wk_ahead_MPE_hospital'].tolist()
        MPE_week_4_vacc  = MPE_data['4wk_ahead_MPE_hospital'].tolist()

        folder_str = 'no_vaccine'

        MPE_data = pd.read_csv('./' + folder_str + '/by_date_MPEs_'+state_str + '.csv')
        MPE_data = MPE_data.sort_values('training_days', axis=0)
        MPE_week_1 = MPE_data['1wk_ahead_MPE_hospital'].tolist()
        MPE_week_2 = MPE_data['2wk_ahead_MPE_hospital'].tolist()
        MPE_week_3 = MPE_data['3wk_ahead_MPE_hospital'].tolist()
        MPE_week_4 = MPE_data['4wk_ahead_MPE_hospital'].tolist()

        diff_1 = MPE_week_1[i]-MPE_week_1_vacc[i]
        diff_2 = MPE_week_2[i]-MPE_week_2_vacc[i]
        diff_3 = MPE_week_3[i]-MPE_week_3_vacc[i]
        diff_4 = MPE_week_4[i]-MPE_week_4_vacc[i]
        diff_1_tot.append(diff_1)
        diff_2_tot.append(diff_2)
        diff_3_tot.append(diff_3)
        diff_4_tot.append(diff_4)

    median_list_1.append(stats.mean(diff_1_tot))
    sd_list_1.append(stats.pstdev(diff_1_tot))
    median_list_2.append(stats.mean(diff_2_tot))
    sd_list_2.append(stats.pstdev(diff_2_tot))
    median_list_3.append(stats.mean(diff_3_tot))
    sd_list_3.append(stats.pstdev(diff_3_tot))
    median_list_4.append(stats.mean(diff_4_tot))
    sd_list_4.append(stats.pstdev(diff_4_tot))

plt.hlines(0, 0, 88, colors = 'k', linestyles='dashed')
plt.plot(median_list_1, label = 'Week 1')
plt.plot(median_list_2, label = 'Week 2')
plt.plot(median_list_3, label = 'Week 3')
plt.plot(median_list_4, label = 'Week 4')
plt.legend(loc = 'lower right')
plt.xlabel('Days Since Start of Vaccine Data')
plt.ylabel('Margin of Vaccine MPE Difference')
plt.title('Mean MPE Difference in Hospitalizations')
plt.show()
plt.plot(sd_list_1, label = 'Week 1')
plt.plot(sd_list_2, label = 'Week 2')
plt.plot(sd_list_3, label = 'Week 3')
plt.plot(sd_list_4, label = 'Week 4')
plt.legend(loc = 'upper right')
plt.xlabel('Days Since Start of Vaccine Data')
plt.title('Standard Deviation of Hospitalization MPE Difference Data')
plt.show()
