import pandas as pd
import os

entities = ['US','Alabama','Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

states_to_check = set()

for entity in entities :

    training_days = []
    MPEs_conf_week_one, MPEs_conf_week_two, MPEs_conf_week_three, MPEs_conf_week_four = [], [], [], []
    MPEs_death_week_one, MPEs_death_week_two, MPEs_death_week_three, MPEs_death_week_four = [], [], [], []
    MPEs_hospital_week_one, MPEs_hospital_week_two, MPEs_hospital_week_three, MPEs_hospital_week_four = [], [], [], []

    folder_str = 'vaccine'
    state_str = entity

    directory = r'./' + folder_str + '/' + state_str + '/'
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            sep_name = filename.split('_')
            num_days = int(sep_name[0])

            MPE_file = open('./' + folder_str + '/' + state_str + '/'+filename, "r")
            lines = MPE_file.readlines()
            count = 0
            training_days.append(num_days)
            for line in lines:
                sep_line = line.split(' ')

                val_in_brackets = sep_line[-1]
                val_in_brackets_sep = val_in_brackets.split('[')
                val_in_brackets = val_in_brackets_sep[-1]
                val_in_brackets_sep = val_in_brackets.split(']')
                val = val_in_brackets_sep[0]

                if count == 0:
                    MPEs_conf_week_one.append(val)
                if count == 3:
                    MPEs_conf_week_two.append(val)
                if count == 6:
                    MPEs_conf_week_three.append(val)
                if count == 9:
                    MPEs_conf_week_four.append(val)

                if count == 1:
                    MPEs_death_week_one.append(val)
                if count == 4:
                    MPEs_death_week_two.append(val)
                if count == 7:
                    MPEs_death_week_three.append(val)
                if count == 10:
                    MPEs_death_week_four.append(val)

                if count == 2:
                    MPEs_hospital_week_one.append(val)
                if count == 5:
                    MPEs_hospital_week_two.append(val)
                if count == 8:
                    MPEs_hospital_week_three.append(val)
                if count == 11:
                    MPEs_hospital_week_four.append(val)

                count += 1

            MPE_file.close()

    df = pd.DataFrame(list(zip(training_days, MPEs_conf_week_one, MPEs_conf_week_two, MPEs_conf_week_three, MPEs_conf_week_four, MPEs_death_week_one, MPEs_death_week_two, MPEs_death_week_three, MPEs_death_week_four, MPEs_hospital_week_one, MPEs_hospital_week_two, MPEs_hospital_week_three, MPEs_hospital_week_four)), columns = ['training_days', '1wk_ahead_MPE_conf', '2wk_ahead_MPE_conf', '3wk_ahead_MPE_conf', '4wk_ahead_MPE_conf', '1wk_ahead_MPE_death', '2wk_ahead_MPE_death', '3wk_ahead_MPE_death', '4wk_ahead_MPE_death', '1wk_ahead_MPE_hospital', '2wk_ahead_MPE_hospital', '3wk_ahead_MPE_hospital', '4wk_ahead_MPE_hospital'])
    df = df.sort_values('training_days',axis=0)
    if len(df.index) != 88 :
        states_to_check.add(entity)
    df.to_csv('./' + folder_str + '/by_date_MPEs_'+state_str + '.csv')

print(states_to_check)
