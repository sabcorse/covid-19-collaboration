import numpy as np
import glob
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
from matplotlib.widgets import CheckButtons
from datetime import datetime

# gets FIPS from args
fips = str(sys.argv[1])
# wayne - 26163
# cook  - 17031

# returns list of fips of neighboring counties
def Get_FIPS_Neighbors(fips_num) :
    # csv as created by county_adj_to_csv.py from the text document
    fips_neighbors_file = pd.read_csv('./gen_csvs/county_neighbors.csv', dtype='str')
    county_index = fips_neighbors_file.loc[fips_neighbors_file['FIPS'] == fips_num].index.item()
    neighbors_list_str = str(fips_neighbors_file['Neighbors'][county_index])
    # turns the string into a list
    neighbors = neighbors_list_str.split(';')
    # accounts for empty list or final empty item
    if neighbors[0] == 'nan' :
        neighbors = []
    else :
        neighbors = neighbors[:-1]
    return neighbors

def Get_County_Names(fips_list) :
    fips_neighbors_file = pd.read_csv('./gen_csvs/county_neighbors.csv', dtype='str')
    names_list = []
    for num in fips_list :
        county_index = fips_neighbors_file.loc[fips_neighbors_file['FIPS'] == num].index.item()
        name_str = fips_neighbors_file['County'][county_index]
        state_str = fips_neighbors_file['State'][county_index]
        names_list.append(name_str + ', ' + state_str)
    return names_list

neighbors_list = Get_FIPS_Neighbors(fips)

# trims off extra counties for cook neighbors
if fips == '17031' :
    neighbors_list = neighbors_list[:-2]

counties_list = [fips] + neighbors_list
names_list = Get_County_Names(counties_list)

print("Neighboring counties identified: " + str(len(neighbors_list)))

# gets the data on the JHU files
def Get_File_Date(file_name):
    month, day, year = file_name[-14:-12], file_name[-11:-9], file_name[-8:-4]
    return month, day, year

def Get_JHU_Data(county_fips_list) :
    # List and sort JHU data files
    path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
    file_set = glob.glob(path + "/*.csv" )
    file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

    case_list_list = []
    # Loop over JHU data, and update cases
    for j in range(len(county_fips_list)) :
        county = county_fips_list[j]
        print('Collecting cases for: ' + names_list[j])
        cases_list = []
        for file in file_set:
            file_mo, file_day, file_yr = Get_File_Date(file)
            if (int(file_mo) == 3 and int(file_day) > 29) or (int(file_mo) > 3) or (int(file_yr) > 2020) :
                data = pd.read_csv( file, index_col = None, header = 0 )
                data_county = data.loc[data['FIPS'] == int(county)]
                cases_list.append(data_county['Confirmed'].values[0])
        incident_case_list = []
        for i in range(1, len(cases_list)) :
            incident_num = cases_list[i]-cases_list[i-1]
            if incident_num < 0 :
                incident_case_list.append(0)
            # deals with spike for Michigan
            elif names_list[j][-2:] == 'MI' and i == 66 or i == 67 :
                incident_case_list.append(0)
            else :
                incident_case_list.append(incident_num/(cases_list[-1]-cases_list[0]))
        case_list_list.append(incident_case_list)
    print('Finished loading case data')
    return case_list_list

cases_grand_list = Get_JHU_Data(counties_list)

# creates plots for each county
fig, ax = plt.subplots()
different_plots = []
for i in range(len(names_list)) :
    different_plots.append(ax.plot(cases_grand_list[i], label=names_list[i])[0])

button_box_size = len(cases_grand_list)
# button business (mostly from matplotlib example)
rax = plt.axes([0.2, 0.5, 0.4, 0.05*button_box_size])
labels = [str(line.get_label()) for line in different_plots]
visibility = [line.get_visible() for line in different_plots]
check = CheckButtons(rax, labels, visibility)

def Click_Func(label):
    index = labels.index(label)
    different_plots[index].set_visible(not different_plots[index].get_visible())
    plt.draw()

check.on_clicked(Click_Func)

plt.show()
