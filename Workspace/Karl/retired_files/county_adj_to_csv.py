import pandas as pd

text_handler = open('./gen_csvs/county_adjacency.txt','r',errors='ignore')

county_names_list = []
state_names_list = []
county_fips_list = []
adjacent_fips_list = []
neighbor_fips_str = ''

group_count = 0
line_count = 0
for line in text_handler :
    # if a new county grouping is hit
    if line[0] == '"' :
        # adds previous adjacency to the list and clears it
        if group_count != 0 :
            adjacent_fips_list.append(neighbor_fips_str)
        neighbor_fips_str = ''

        # debugging break
        #if group_count > 15 :
        #    break

        # adds state and county names
        comma_index = line.find(',')
        county_names_list.append(line[1:comma_index])
        state_names_list.append(line[comma_index+2:comma_index+4])

        # locates the fips num
        next_county_quote_index = line[comma_index+5:].find('"') + comma_index + 5
        county_fips_list.append(line[comma_index+5:next_county_quote_index].strip())

        # checks the next county on the line, if the fips is different, appends it to neighbor
        second_part_of_line = line[next_county_quote_index+1:]
        next_comma_index = second_part_of_line.find(',')
        next_county_fips = second_part_of_line[next_comma_index+5:].strip()
        if next_county_fips != county_fips_list[-1] :
            neighbor_fips_str += next_county_fips + ';'
        group_count += 1
    else :
        # checks the county on the line, if the fips is different than original, appends it to neighbor
        neighbor_fips = line[-6:].strip()
        if neighbor_fips != county_fips_list[-1] :
            neighbor_fips_str += neighbor_fips + ';'
# appends final neighbor list
adjacent_fips_list.append(neighbor_fips_str)

county_df = pd.DataFrame(list(zip(county_names_list,state_names_list,county_fips_list,adjacent_fips_list)),columns=['County','State','FIPS','Neighbors'])
county_csv = county_df.to_csv('./gen_csvs/county_neighbors.csv')
