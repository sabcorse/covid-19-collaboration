import pandas as pd
from datetime import datetime
from datetime import date as calendar

date_today = calendar.today().strftime("%Y-%m-%d")
date_today_reordered = calendar.today().strftime("%m-%d-%Y")

loc_arr = ['Grand Rapids','Kalamazoo', 'Saginaw','Lansing','Traverse City','Jackson', 'Upper Peninsula']

# Edit days here
df_base = pd.read_csv('JHU_csvs/Detroit-JHU_training.csv')
df_base['location'] = ['Detroit' for i in range(len(df_base))]

# We can exclude bad data with an if statement here. if(1st week 0.1 data bad): ... else: output which data was excluded
for loc in loc_arr:
        # Edit days here
        df = pd.read_csv("JHU_csvs/" + loc + '-JHU_training.csv')
        df['location'] = [loc for i in range(len(df))]
        df_base = pd.concat([df_base, df], ignore_index = True)

#df = pd.read_csv('US/' + date_today + '-UMich-RidgeTfReg.csv')
#df_base = pd.concat([df_base, df], ignore_index = True)

# Edit days here
df_base.to_csv('JHU_csvs/Agg-JHU_training-'+date_today_reordered+'_MERC.csv', index = False)
