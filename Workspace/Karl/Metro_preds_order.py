print('Importing modules...')

import numpy as np
import glob
import pandas as pd
import os
import sys
import string
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn as sn
from matplotlib import gridspec
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from backports.datetime_fromisoformat import MonkeyPatch
from dateutil.parser import parse
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.linear_model import Lasso, LassoCV
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from sklearn import preprocessing
from scipy import signal

MonkeyPatch.patch_fromisoformat()

plt.style.use('seaborn-white')
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)


###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

#order = int(sys.argv[1])
#combined_order = int(sys.argv[2])

###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

print('Importing data...')

df = pd.read_csv('gen_csvs/GrandRapids_Covid_Aggregated.csv')
covid_census = df['Metro_Covid_Census'].loc[162:]
covid_census = covid_census.tolist()

combined_covid_census = df['Total_Area_Covid_Census'].loc[162:].tolist()

combined_covid_census[344] = 55
combined_covid_census[361] = 103

# Store most recent deaths and most recent confirmed cases
# Predicted new cases and deaths are later added to these to get aggregates
most_recent_census = covid_census[-1]

covid_admissions = df['Metro_Covid_Admissions'].loc[7:]
covid_admissions = covid_admissions.tolist()
covid_hosp_agg = [0]

for i in range(0, len(covid_admissions)):
    covid_hosp_agg.append(covid_hosp_agg[-1] + covid_admissions[i])

###############################################################
#                                                             #
#                 Read and Sort JHU Data                      #
#                                                             #
###############################################################

def Get_File_Date(file_name):
    month, day, year = file[-14:-12], file[-11:-9], file[-8:-4]
    return month, day, year

# List and sort JHU data files
path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

confirmedCases, deaths, recovered, active = [], [], [], []
day_count = 0

todays_date = calendar.today()
todays_day = todays_date.weekday()
mondays_date = todays_date - timedelta(todays_day)
'''
if todays_day == 0 :
    mondays_date = todays_date
elif todays_day == 1 :
    mondays_date = todays_date - timedelta(1)
else :
    mondays_date = todays_date - timedelta(4)
    print('Prediction not running on a Monday or Tuesday. Please address')
'''

dates = []
Kent_cases = []
# Loop over JHU data, and update cases, deaths, recoveries
for file in file_set:

        file_mo, file_day, file_yr = Get_File_Date(file)

        if (int(file_mo) > 8) or (int(file_yr) > 2020):
        #if (int(file_mo) == 3 and int(file_day) > 29) or (int(file_mo) > 3) or (int(file_yr) > 2020) :
            if (int(file_mo) == mondays_date.month and int(file_day) == mondays_date.day and int(file_yr) == mondays_date.year):
                #print('Breaking')
                break
            else:
                dates.append(file_mo+'-'+file_day)
            data = pd.read_csv( file, index_col = None, header = 0 )
            data_MI = data.loc[data['Province_State'] == 'Michigan']
            data_Kent = data_MI.loc[data_MI['Admin2'] == 'Kent']

            Kent_cases.append(data_Kent['Confirmed'].values[0])

        day_count += 1

#print(Kent_cases)

Kent_cases_incident = [0]
for i in range(1, len(Kent_cases)):
    Kent_cases_incident.append(Kent_cases[i] - Kent_cases[i-1])
covid_admissions.insert(0,0)
start_day = (calendar.fromisoformat('2020-09-01'))
dates = [(start_day + timedelta(days = i)).strftime('%Y-%m-%d') for i in range(len(Kent_cases_incident))]
df_for_Metro = pd.DataFrame(list(zip(dates, Kent_cases_incident)), columns = ['date', 'incident Kent cases'])
df_for_Metro.to_csv('MetroCsvs/Kent_cases_training.csv')
#print('Training csv made')

for i in range(28):
    dates.append((calendar.fromisoformat(dates[-1]) + timedelta(days=1)).strftime('%Y-%m-%d'))

##############################################################
#                                                            #
#                     Order Testing Loop                     #
#                                                            #
##############################################################

print('Running prediction loop orders 5-56')

metro_scores_list_3 = []
comb_scores_list_3 = []

for k in range(5,57) :
    order = k
    combined_order = k

    ##############################################################
    #                                                            #
    #                 Prepare Data for Regression                #
    #                                                            #
    ##############################################################

    # Convert cumulative data to daily data, since predictions based based off of incident values
    def cumulativeToDaily(cumulative_lists):
        daily_list = []
        for list_i in cumulative_lists:
            daily = []
            for i in range(len(list_i)):
                if not i == 0:
                    # Subtract each "yesterday's" values from the current day's
                    daily.append(list_i[i] - list_i[i-1])
            # For testing, currently unused: smooth case and death data to remove spikes
            #if list_i == confirmed or list_i == deaths:
                #daily = Sav_Gol(daily)
            daily_list.append(daily)
        return daily_list[0]

    # Convert daily data to cumulative data
    # Can be used to test aggregate predictions based off the sum of incident predictions
    def dailyToCumulative(daily_list_start_pairs):
        cumulative_list = []
        for (list_i, start_val) in daily_list_start_pairs:
            cumulative = []
            for i in range(len(list_i) + 1):
                # Start from baseline saved cumulative value
                if i == 0:
                    cumulative.append(start_val)
                # Add most recent cumulative value to most recent incident value
                else:
                    cumulative.append(cumulative[i-1] + list_i[i-1])
            cumulative_list.append(cumulative)
        return cumulative_list[0]

    # Remove first <order> days of data for fitting the ridge model
    # Order is the number of days used to predict each upcoming day
    # E.g., if 21 days are used to predict each upcoming day, the first 21 should be removed, since they can't be predicted
    def Truncate_For_Ridge(data_list, this_order = order):
        truncated_lists = []
        for list_i in data_list:
            truncated_lists.append(list_i[this_order:])
        return truncated_lists[0]

    # Create constant arrays for aggregate model performance checks
    covid_census_arr_checker = covid_census[:]
    combined_covid_census_arr_checker = combined_covid_census[:]

    # Create constant arrays for incident model performance checks
    daily_covid_census_arr_checker = covid_census_arr_checker#cumulativeToDaily([covid_census_arr_checker])
    daily_combined_covid_census_arr_checker = combined_covid_census_arr_checker

    # Create daily active, recovered, deaths, and confirmed lists for incident value prediction making

    daily_covid_census = covid_census#cumulativeToDaily([covid_census])
    daily_combined_covid_census = combined_covid_census

    # Trim first days of data for fitting the model (See function comments)
    covid_census_for_ridge = Truncate_For_Ridge([covid_census])
    combined_covid_census_for_ridge = Truncate_For_Ridge([combined_covid_census], combined_order)

    # Create matrices for prediction, as in LaTeX document
    def Create_Pred_Matrices(full_data_lists, isMetro, this_order = order):
        matrices = []
        for list_i in full_data_lists:
            pred_matrix = []
            for i in range(len(list_i) - this_order):
                #print(i)
                new_row_daily = [1]
                for j in range(this_order):
                    new_row_daily.append(list_i[i+j])
                for j in range(i, this_order+i):
                    #print(j)
                    #new_row_daily.append(case_onset_Kent[j])
                    new_row_daily.append(Kent_cases_incident[j])
                if isMetro :
                    for j in range(i, this_order+i) :
                        new_row_daily.append(combined_covid_census[j])
                pred_matrix.append(new_row_daily)
            matrices.append(pred_matrix)
        return matrices[0]

    # Remove negative case, death, recovery, and active values
    # Come from data-reporting anomalies, so don't belong in predictions
    def Clean_Sub_Zero(full_data_lists):
        no_zeros_list = []
        for list_i in full_data_lists:
            for i in range(len(list_i)):
                if list_i[i] < 0 and not i == 0 and not i == (len(list_i) - 1):
                   list_i[i] = (list_i[i-1]+list_i[i+1])/2.
                   #print('removing a 0')
                   #print(list_i[i])
            no_zeros_list.append(list_i)
        return no_zeros_list

    # Create matrices, as in LaTeX document, for active cases, recoveries, deaths, and confirmed cases
    #daily_covid_census = Clean_Sub_Zero([daily_covid_census])
    daily_covid_census_pred_matrix = Create_Pred_Matrices([daily_covid_census],True)
    #print('Metro_only complete')
    daily_combined_covid_census_pred_matrix = Create_Pred_Matrices([daily_combined_covid_census], False, combined_order)

    # Remove 0's
    #covid_census_for_ridge = Clean_Sub_Zero([covid_census_for_ridge])

    # Trim first days of data for fitting incident data
    daily_covid_census_for_ridge = Truncate_For_Ridge([daily_covid_census])
    daily_combined_covid_census_for_ridge = Truncate_For_Ridge([daily_combined_covid_census], combined_order)

    # Make constant values to revert to for both daily and cumulative model
    daily_covid_census_arr_const, daily_covid_census_for_ridge_const = daily_covid_census[:], daily_covid_census_for_ridge[:]
    covid_census_for_ridge_const = covid_census_for_ridge[:]

    daily_combined_covid_census_arr_const, daily_combined_covid_census_for_ridge_const = daily_combined_covid_census[:], daily_combined_covid_census_for_ridge[:]
    combined_covid_census_for_ridge_const = combined_covid_census_for_ridge[:]

    # Generate a range of ridge regression penalty alphas to try
    def Generate_Alphas(coeff_list):
        alpha_list = []
        #i = -3
        i = -5
        while i < 8:
        #while i < 12:
            for coeff in coeff_list:
                alpha_list.append(coeff * (10**(-i)))
            i += 1
        params = {'alpha': alpha_list}
        return params

    parameters = Generate_Alphas([1., 2.5, 5., 7.5])

    ###################################################
    #                                                 #
    #            Perform Ridge Regression             #
    #                                                 #
    ###################################################

    # Create different types of ridge regressor, based on circumstances
    def Make_Ridge(input_list, ridge_type, alpha_list = [0]):
        ridge_list = []

        # Create bare-bones ridge regressors to feed into grid searcher
        if ridge_type == 'no_cv':
            for list_i in input_list:
                ridge_list.append(Ridge(normalize = True))

        # Create ridge regressor with only penalty specified, based on grid search results
        if ridge_type == 'no_cv_alpha_spec':
            for i in range(len(input_list)):
                ridge_list.append(Ridge(normalize = True, alpha = alpha_list[i]))

        # Create grid searcher to find best penalties
        if ridge_type == 'cv':
            for list_i in input_list:
                ridge_list.append(GridSearchCV(list_i, parameters, scoring = 'neg_mean_squared_error', cv = 10))

        return ridge_list[0]

    # Create grid searchers to find optimal ridge penalties for active and total cases, deaths, and recoveries
    r_covid_census = Make_Ridge([covid_census], 'no_cv')
    ridge_covid_census_cv = Make_Ridge([r_covid_census], 'cv')

    r_combined_covid_census = Make_Ridge([combined_covid_census], 'no_cv')
    ridge_combined_covid_census_cv = Make_Ridge([r_combined_covid_census], 'cv')

    # Fit ridge regressors, and return necessary values or predictions, depending on circumstances
    def Fit_Ridge(prediction_triads, fit_type):
        #print('fitting')

        # For grid searchers, return optimal penalty and best score
        if fit_type == 'round_1_eval':
            alpha_mid_list, score_list = [], []
            for (regressor, matrix, col_vec) in prediction_triads:
                regressor.fit(matrix, col_vec)
                alpha_mid_list.append(regressor.best_params_['alpha'])
                score_list.append(regressor.best_score_)
            return alpha_mid_list, score_list

        # For non-grid searches, create future predictions
        if fit_type == 'make_pred':
            model_list = []
            for (regressor, matrix, col_vec) in prediction_triads:
                regressor.fit(matrix, col_vec)
                model = regressor.predict(matrix)
                model_list.append(model.tolist())
            return model_list

    # Creating triples of values for fitting
    # Triples include the regressor, the prediction matrix, and the vector from the RHS of the LaTeX document
    covid_census_triad = (ridge_covid_census_cv, daily_covid_census_pred_matrix, daily_covid_census_for_ridge)
    combined_covid_census_triad = (ridge_combined_covid_census_cv, daily_combined_covid_census_pred_matrix, daily_combined_covid_census_for_ridge)

    assigned_alphas, assigned_scores = Fit_Ridge([covid_census_triad], 'round_1_eval')
    assigned_alphas_combined, assigned_scores_combined = Fit_Ridge([combined_covid_census_triad], 'round_1_eval')
    alpha_mid_covid_census = assigned_alphas[0]
    alpha_mid_combined_covid_census = assigned_alphas_combined[0]
    score_covid_census = assigned_scores[0]
    score_combined_covid_census = assigned_scores_combined[0]

    #print(str(alpha_mid_covid_census))
    #print('Score: ' + str(score_covid_census))

    ridge_covid_census = Make_Ridge([covid_census], 'no_cv_alpha_spec', [alpha_mid_covid_census])
    #print(ridge_covid_census)

    ridge_combined_covid_census = Make_Ridge([combined_covid_census], 'no_cv_alpha_spec', [alpha_mid_combined_covid_census])

    trained_metro_model_3 = ridge_covid_census.fit(daily_covid_census_pred_matrix[:-4], daily_covid_census_for_ridge[:-4])
    trained_comb_model_3 = ridge_combined_covid_census.fit(daily_combined_covid_census_pred_matrix[:-4],daily_combined_covid_census_for_ridge[:-4])

    metro_score_3 = trained_metro_model_3.score(daily_covid_census_pred_matrix[-4:], daily_covid_census_for_ridge[-4:])
    comb_score_3 = trained_comb_model_3.score(daily_combined_covid_census_pred_matrix[-4:],daily_combined_covid_census_for_ridge[-4:])


    metro_scores_list_3.append(metro_score_3)
    comb_scores_list_3.append(comb_score_3)

    print('Finished Order '+str(order))

print('')
print('Metro Order: ' + str(metro_scores_list_3.index(max(metro_scores_list_3))+5)+string.whitespace[1]+'Combined Order: '+str(comb_scores_list_3.index(max(comb_scores_list_3))+5))
print('\n')
