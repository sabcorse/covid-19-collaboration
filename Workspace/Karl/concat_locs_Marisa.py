import pandas as pd
from datetime import datetime
from datetime import date as calendar

date_today = calendar.today().strftime("%Y-%m-%d")

loc_arr = ['Indiana','Michigan', 'Pennsylvania', 'Wisconsin', 'Ohio']#,'Pennsylvania','Wisconsin']

# Edit days here
df_base = pd.read_csv('Illinois/' + date_today + '-daily.csv')
df_base_wkly = pd.read_csv('Illinois/' + date_today + '-weekly.csv')

# We can exclude bad data with an if statement here. if(1st week 0.1 data bad): ... else: output which data was excluded
for loc in loc_arr:
	# Edit days here
	df = pd.read_csv(loc + '/' + date_today + '-daily.csv')
	df_base = pd.concat([df_base, df], ignore_index = True)
	df_wkly = pd.read_csv(loc + '/' + date_today + '-weekly.csv')
	df_base_wkly = pd.concat([df_base_wkly, df_wkly], ignore_index = True)

#df = pd.read_csv('US/' + date_today + '-UMich-RidgeTfReg.csv')
#df_base = pd.concat([df_base, df], ignore_index = True)

# Edit days here
df_base.to_csv('MarisaCsvs/daily_and_weekly_csvs/' + date_today + '-agg_daily.csv', index = False)
df_base_wkly.to_csv('MarisaCsvs/daily_and_weekly_csvs/' + date_today + '-agg_wkly.csv', index = False)
