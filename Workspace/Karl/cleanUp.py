import glob
import os
import shutil

def delete_file(path) :
    try :
        os.remove(path)
    except :
        print("File "+path+" does not exist")

def delete_dir(path) :
    try :
        shutil.rmtree(path)
    except :
        print("Folder "+path+" does not exist")

# deals with gen_csvs
file_list = glob.glob('./gen_csvs/*.csv')
for file_path in file_list :
    if file_path[11:27] in ['Covid_Vaccine_Co','COVID-19_Diagnos','COVID-19_Reporte','covid-19-hospita','Global_Mobility_','Unacast_County.c','us_state_vaccina'] :
        delete_file(file_path)

# deals with state/region folders
for region in ['Detroit','Grand Rapids','Jackson','Saginaw','Lansing','Kalamazoo','Traverse City', 'Upper Peninsula','Michigan','Indiana','Illinois','Wisconsin','Pennsylvania','Ohio'] :
    delete_dir('./'+region)

# deals with MarisaCsvs & MERCCsvs
files_to_delete = ['MarisaCsvs/Rplots.pdf','MarisaCsvs/Rplots.zip','MarisaCsvs/state_training_hosps.zip','MERCCsvs/MERC_prediction_hosps.zip','MERCCsvs/MERC_region_training_hosps.zip','MERCCsvs/Rplots_MERC.zip','MERCCsvs/Rplots.pdf']
for path in files_to_delete :
    delete_file('./'+path)
dirs_to_clear = ['MarisaCsvs/daily_and_weekly_csvs','MarisaCsvs/JHU_csvs','MarisaCsvs/Rplots','MarisaCsvs/state_training_hosps','MERCCsvs/daily_and_weekly_csvs','MERCCsvs/JHU_csvs','MERCCsvs/MERC_prediction_hosps','MERCCsvs/MERC_region_training_hosps','MERCCsvs/Rplots_MERC']
for path in dirs_to_clear :
    file_list = glob.glob('./'+path+'/*.*')
    if len(file_list) == 0 :
        continue
    else :
        for file_name in file_list :
            delete_file(file_name)
