wget -q 'https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/vaccinations/us_state_vaccinations.csv' -O 'us_state_vaccinations.csv'
wget -q 'https://healthdata.gov/api/views/g62h-syeh/rows.csv' -O 'COVID-19_Reported_Patient_Impact_and_Hospital_Capacity_by_State_Timeseries.csv'
wget -q 'https://www.gstatic.com/covid19/mobility/Global_Mobility_Report.csv' -O 'Global_Mobility_Report.csv'
wget -q 'https://healthdata.gov/api/views/j8mb-icvb/rows.csv' -O 'COVID-19_Diagnostic_Laboratory_Testing__PCR_Testing__Time_Series.csv'
