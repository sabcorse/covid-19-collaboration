import pandas as pd

loc_arr = ['Alabama', 'Alaska','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

loc_nums = [1, 2, 4, 5, 6, 8, 9, 10, 12, 13]

for i in range(15, 43):
    loc_nums.append(i)
for i in range(44, 52):
    loc_nums.append(i)
for i in range(53, 57):
    loc_nums.append(i)

print(len(loc_nums))

df = pd.read_csv('2020-09-14-UMich-RidgeTfReg.csv')

for i in range(len(loc_arr)):
    locat = loc_arr[i]
    df.loc[df.location == locat, 'location'] = loc_nums[i]

df.to_csv('test.csv')
