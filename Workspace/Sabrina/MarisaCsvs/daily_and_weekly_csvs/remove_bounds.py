import pandas as pd

df = pd.read_csv('2021-03-07-agg_daily_with_bounds.csv')

df = df.drop(df[df['target'].str.contains('bound')].index)

df.to_csv('2021-03-07-agg_daily.csv')
