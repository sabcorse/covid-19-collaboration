import pandas as pd 
from datetime import date as calendar
from datetime import timedelta
import matplotlib.pyplot as plt

date_today = calendar.today()
date_saturday = date_today + timedelta(-3)
forecast_date = (date_saturday + timedelta(days=1)).strftime('%Y-%m-%d')

daily_df = pd.read_csv('2021-01-11-agg_daily.csv')
state_list = ['Illinois', 'Indiana', 'Michigan', 'Ohio', 'Pennsylvania', 'Wisconsin']

for state in state_list:
    case_upper_bounds = []
    case_lower_bounds = []
    death_upper_bounds = []
    death_lower_bounds = []

    state_df = daily_df.loc[daily_df['location'] == state]
    state_case_df = state_df.loc[state_df['target'].str.contains('case')]
    state_death_df = state_df.loc[state_df['target'].str.contains('death')]

    for i in range(int(len(state_case_df))):
        target_end_date = (date_saturday + timedelta(days = i+1)).strftime('%Y-%m-%d')

        case_line = state_case_df.iloc[i]
        death_line = state_death_df.iloc[i]

        case_point_val = case_line['value']
        death_point_val = death_line['value']
        
        if i < 14:
            case_upper_bounds.append(case_point_val*1.5)
            case_lower_bounds.append(case_point_val*.5)
            death_upper_bounds.append(death_point_val*1.5)
            death_lower_bounds.append(death_point_val*.5)

        else:
            case_upper_bounds.append(case_point_val*2)
            case_lower_bounds.append(case_point_val*0)
            death_upper_bounds.append(death_point_val*2)
            death_lower_bounds.append(death_point_val*0)
            
        daily_df = daily_df.append(pd.DataFrame([[forecast_date, '{day} day ahead inc case'.format(day = i+1), target_end_date, state, 'upper bound', 'NA', case_upper_bounds[-1]]], columns = daily_df.columns))
        daily_df = daily_df.append(pd.DataFrame([[forecast_date, '{day} day ahead inc case'.format(day = i+1), target_end_date, state, 'lower bound', 'NA', case_lower_bounds[-1]]], columns = daily_df.columns))
        daily_df = daily_df.append(pd.DataFrame([[forecast_date, '{day} day ahead inc death'.format(day = i+1), target_end_date, state, 'upper bound', 'NA', death_upper_bounds[-1]]], columns = daily_df.columns))
        daily_df = daily_df.append(pd.DataFrame([[forecast_date, '{day} day ahead inc death'.format(day = i+1), target_end_date, state, 'lower bound', 'NA', death_lower_bounds[-1]]], columns = daily_df.columns))

daily_df.to_csv(date_today + '-agg_daily_with_bounds.csv') 
