import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn
import numpy as np
import itertools
import glob
from datetime import datetime

colors = plt.rcParams["axes.prop_cycle"]()

days_to_include = 121

countryOrState = 0
location = 'Michigan'

def Get_Loc_Type_JHU(month, day, year):
    areaLabel = 'Country/Region'
    if countryOrState == 0:
        areaLabel = 'Province/State'
    if (int(month) == 3 and int(day) > 21 and int(year) > 2019) or (int(month) > 3 and int(year) > 2019):
        # After certain date, "/" changed to "_"
        areaLabel = 'Country_Region'
        if countryOrState == 0:
                areaLabel = 'Province_State'
    return areaLabel

def Get_File_Date(file_name):
    month, day, year = file[-14:-12], file[-11:-9], file[-8:-4]
    #if '{m}/{d}/{Y}'.format(m = month, d = day, Y = year) == sunday_stop:
    #    month, day, year = 'stop', 'stop', 'stop'
    return month, day, year

# Cases
path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv")
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

cases = []
day_count = 0
for file in file_set:
    month, day_index, year = Get_File_Date(file)
    areaLabel = Get_Loc_Type_JHU(month, day_index, year)
    data = pd.read_csv(file, index_col = None, header = 0)
    cases.append(0.)
    cases[day_count] = data.loc[data[areaLabel] == location].sum()['Confirmed']
    day_count += 1

new_cases = [0]
for i in range(len(cases)):
    if not i == 0:
        new_cases.append(cases[i] - cases[i-1])

new_cases = new_cases[40:]
new_cases = new_cases[:days_to_include]
new_cases[95] = 0
# Google
Google = pd.read_csv('Global_Mobility_Report.csv', low_memory = False)
MI_mobility = Google.loc[Google['country_region'] == 'United States']
MI_mobility = MI_mobility.loc[MI_mobility['sub_region_1'] == 'Michigan']
no_counties_rows = pd.isnull(MI_mobility['sub_region_2'])
MI_mobility = MI_mobility[no_counties_rows]
#print(MI_mobility)
MI_mobility = MI_mobility.loc[2472084:]
#print(MI_mobility)

add_on = '_percent_change_from_baseline'
retail_and_rec = MI_mobility['retail_and_recreation'+add_on].tolist()
groc_and_pharm = MI_mobility['grocery_and_pharmacy'+add_on].tolist()
parks = MI_mobility['parks'+add_on].tolist()
transit = MI_mobility['transit_stations'+add_on].tolist()
work = MI_mobility['workplaces'+add_on].tolist()
residential = MI_mobility['residential'+add_on].tolist()

list_of_locs = [retail_and_rec, groc_and_pharm, parks, transit, work, residential]
j = 0
for loc in list_of_locs:
    for i in range(len(retail_and_rec)):
        if np.isnan(loc[i]):
            loc[i] = 0
            if not i == 0:
                loc[i] = loc[i-1]
    j += 1

retail_and_rec = retail_and_rec[:days_to_include]
groc_and_pharm = groc_and_pharm[:days_to_include]
parks = parks[:days_to_include]
transit = transit[:days_to_include]
work = work[:days_to_include]
residential = residential[:days_to_include]

# Unacast
Unacast = pd.read_csv('gen_csvs/UM_USSDS_MI.csv', low_memory = False)
FIPS_list = Unacast['county_fips'].unique().tolist()
pops_list = Unacast['county_population'].unique().tolist()
dates_list = Unacast['date'].unique().tolist()
MI_pop = np.sum(pops_list)
pop_fracs = pops_list/MI_pop

daily_distance_diffs = []
encounters_rate = []

for date in dates_list:
    rows_today = Unacast.loc[Unacast['date'] == date]
    distance_diff_today = 0
    encounters_rate_today = 0
    for i in range(len(FIPS_list)):
        this_fips = FIPS_list[i]
        weight = pop_fracs[i]
        this_distance_diff = rows_today.loc[rows_today['county_fips'] == this_fips]['daily_distance_diff'].values[0]
        this_encounters_rate = rows_today.loc[rows_today['county_fips'] == this_fips]['encounters_rate'].values[0]
        distance_diff_today += weight * this_distance_diff 
        encounters_rate_today += weight * this_encounters_rate 

    daily_distance_diffs.append(distance_diff_today)
    encounters_rate.append(encounters_rate_today)

#print(encounters_rate)
daily_distance_diffs = daily_distance_diffs[7:]
encounters_rate = encounters_rate[7:]

measurement_list = [daily_distance_diffs, encounters_rate]
for measurement in measurement_list:
    measurement = measurement[:days_to_include]

days_to_include = np.arange(0, len(daily_distance_diffs))

palette = itertools.cycle(sn.color_palette())

c = next(colors)['color']
fig = plt.figure()
#c = next(colors)['color']
ax1 = fig.add_subplot(111)
#ax1.plot(days_to_include, daily_distance_diffs, label = 'Unacast Distances', color = c)
#c = next(colors)['color']
ax1.plot(days_to_include, encounters_rate, label = 'Unacast Encounters', color = c)
ax1.set_ylabel('Unacast Deviations')
ax1.set_xlabel('Day (Starting March 2, 2020)')


c = next(colors)['color']
ax2 = ax1.twinx()
#ax2.set_prop_cycle(sn.color_palette("light:#5A9", 6))
#ax2.plot(days_to_include, retail_and_rec, label = 'Google Retail', color = c)
#c = next(colors)['color']
#ax2.plot(days_to_include, groc_and_pharm, label = 'Google Groc and Pharm', color = c)
#c = next(colors)['color']
#ax2.plot(days_to_include, parks, label = 'Google Parks', color = c)
c = next(colors)['color']
ax2.plot(days_to_include, transit, label = 'Google Transit', color = c)
#c = next(colors)['color']
#ax2.plot(days_to_include, work, label = 'Google Work', color = c)
#c = next(colors)['color']
#ax2.plot(days_to_include, residential, label = 'Google Residential', color = c)
ax2.set_ylabel('Google Deviations')

#c = next(colors)['color']
ax3 = ax1.twinx()
#ax3.plot(days_to_include, daily_distance_diffs, label = 'Unacast Distances', color = c)
c = next(colors)['color']
ax3.plot(days_to_include, new_cases, label = 'JHU Cases', color = c)
#ax3.set_ylabel('JHU Cases')
ax3.get_yaxis().set_visible(False)


plt.title('Dataset Comparison')
fig.legend(loc = 'upper left')
#plt.legend()
plt.show()

# Google corr matrix
Google_df = pd.DataFrame(list(zip(retail_and_rec, groc_and_pharm, parks, transit, work, residential)), columns = ['retail_and_rec', 'groc_and_pharm', 'parks', 'transit', 'work', 'residential'])
corrMatrix = Google_df.corr()
sn.heatmap(corrMatrix, annot=True)
plt.show()
