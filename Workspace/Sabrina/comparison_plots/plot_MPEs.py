import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn

df = pd.read_csv('by_date_MPEs_JHU.csv')
df = df.sort_values('training_days')
training_days = df['training_days']
one_wk_ahead_MPE_conf = df['1wk_ahead_MPE_conf']
two_wk_ahead_MPE_conf = df['2wk_ahead_MPE_conf']
three_wk_ahead_MPE_conf = df['3wk_ahead_MPE_conf']
four_wk_ahead_MPE_conf = df['4wk_ahead_MPE_conf']
#one_wk_ahead_MPE_death = df['1wk_ahead_MPE_death']
#two_wk_ahead_MPE_death = df['2wk_ahead_MPE_death']
#three_wk_ahead_MPE_death = df['3wk_ahead_MPE_death']
#four_wk_ahead_MPE_death = df['4wk_ahead_MPE_death']

#corrMatrix = df.corr()

first_day = min(training_days)
print(first_day)

plt.plot(training_days, one_wk_ahead_MPE_conf, label = '1 Wk Ahead')
plt.plot(training_days, two_wk_ahead_MPE_conf, label = '2 Wks Ahead')
plt.plot(training_days, three_wk_ahead_MPE_conf, label = '3 Wks Ahead')
plt.plot(training_days, four_wk_ahead_MPE_conf, label = '4 Wks Ahead')
plt.yscale('log')
#plt.axvline(x = first_day+38, color = 'k', linestyle = '--', label = 'July Peak')
plt.xlabel('Days Used for Training')
plt.ylabel('Mean Percent Error')
plt.title('JHU Dataset Errors')
plt.legend()
plt.savefig('Google_MPE_confirmed')
plt.show()
'''
plt.plot(training_days, one_wk_ahead_MPE_death, label = '1 Wk Ahead')
plt.plot(training_days, two_wk_ahead_MPE_death, label = '2 Wks Ahead')
plt.plot(training_days, three_wk_ahead_MPE_death, label = '3 Wks Ahead')
plt.plot(training_days, four_wk_ahead_MPE_death, label = '4 Wks Ahead')
plt.yscale('log')
#plt.axvline(x = first_day+38, color = 'k', linestyle = '--', label = 'July Peak')
plt.xlabel('Days Used for Training')
plt.ylabel('Mean Percent Error')
plt.title('Death MPE by Training Set Size')
plt.legend()
plt.savefig('Google_MPE_deaths')
plt.show()
'''
#sn.heatmap(corrMatrix, annot=True)
#plt.show()
