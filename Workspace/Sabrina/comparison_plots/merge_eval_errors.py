import pandas as pd
import os

training_days = []
MPEs_conf_week_one, MPEs_conf_week_two, MPEs_conf_week_three, MPEs_conf_week_four = [], [], [], []
MPEs_death_week_one, MPEs_death_week_two, MPEs_death_week_three, MPEs_death_week_four = [], [], [], []

directory = r'./US/'
for filename in os.listdir(directory):
    if filename.endswith(".txt"):
        sep_name = filename.split('_')
        num_days = int(sep_name[0])

        MPE_file = open('./US/'+filename, "r")
        lines = MPE_file.readlines()
        count = 0
        training_days.append(num_days)
        for line in lines:
            sep_line = line.split(' ')

            val_in_brackets = sep_line[-1]
            val_in_brackets_sep = val_in_brackets.split('[')
            val_in_brackets = val_in_brackets_sep[-1]
            val_in_brackets_sep = val_in_brackets.split(']')
            val = val_in_brackets_sep[0]

            if count == 0:
                MPEs_conf_week_one.append(val)
            if count == 2:
                MPEs_conf_week_two.append(val)
            if count == 4:
                MPEs_conf_week_three.append(val)
            if count == 6:
                MPEs_conf_week_four.append(val)
            
            if count == 1:
                MPEs_death_week_one.append(val)
            if count == 3:
                MPEs_death_week_two.append(val)
            if count == 5:
                MPEs_death_week_three.append(val)
            if count == 7:
                MPEs_death_week_four.append(val)

            count += 1
            
        MPE_file.close()

df = pd.DataFrame(list(zip(training_days, MPEs_conf_week_one, MPEs_conf_week_two, MPEs_conf_week_three, MPEs_conf_week_four, MPEs_death_week_one, MPEs_death_week_two, MPEs_death_week_three, MPEs_death_week_four)), columns = ['training_days', '1wk_ahead_MPE_conf', '2wk_ahead_MPE_conf', '3wk_ahead_MPE_conf', '4wk_ahead_MPE_conf', '1wk_ahead_MPE_death', '2wk_ahead_MPE_death', '3wk_ahead_MPE_death', '4wk_ahead_MPE_death'])

df.to_csv('./US/by_date_MPEs.csv')
