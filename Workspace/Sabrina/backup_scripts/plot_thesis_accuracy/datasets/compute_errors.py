import pandas as pd
import numpy as np

national_df = pd.read_csv('by_date_errors_thesis_merged.csv')
one_week_conf_errors = national_df['1wk_ahead_error_Metro']
two_week_conf_errors = national_df['2wk_ahead_error_Metro']
three_week_conf_errors = national_df['3wk_ahead_error_Metro']
four_week_conf_errors = national_df['4wk_ahead_error_Metro']

one_week_death_errors = national_df['1wk_ahead_error_combined']
two_week_death_errors = national_df['2wk_ahead_error_combined']
three_week_death_errors = national_df['3wk_ahead_error_combined']
four_week_death_errors = national_df['4wk_ahead_error_combined']

one_week_avg_conf_errors = np.mean([abs(error) for error in one_week_conf_errors])
two_week_avg_conf_errors = np.mean([abs(error) for error in two_week_conf_errors])
three_week_avg_conf_errors = np.mean([abs(error) for error in three_week_conf_errors])
four_week_avg_conf_errors = np.mean([abs(error) for error in four_week_conf_errors])

one_week_avg_conf_std = np.std([abs(error) for error in one_week_conf_errors])
two_week_avg_conf_std = np.std([abs(error) for error in two_week_conf_errors])
three_week_avg_conf_std = np.std([abs(error) for error in three_week_conf_errors])
four_week_avg_conf_std = np.std([abs(error) for error in four_week_conf_errors])

one_week_avg_death_errors = np.mean([abs(error) for error in one_week_death_errors])
two_week_avg_death_errors = np.mean([abs(error) for error in two_week_death_errors])
three_week_avg_death_errors = np.mean([abs(error) for error in three_week_death_errors])
four_week_avg_death_errors = np.mean([abs(error) for error in four_week_death_errors])

one_week_avg_death_std = np.std([abs(error) for error in one_week_death_errors])
two_week_avg_death_std = np.std([abs(error) for error in two_week_death_errors])
three_week_avg_death_std = np.std([abs(error) for error in three_week_death_errors])
four_week_avg_death_std = np.std([abs(error) for error in four_week_death_errors])

print('one wk conf errors: '+ str(one_week_avg_conf_errors))
print('two wk conf errors: '+ str(two_week_avg_conf_errors))
print('three wk conf errors: '+ str(three_week_avg_conf_errors))
print('four wk conf errors: '+ str(four_week_avg_conf_errors))

print('one wk conf sd: '+ str(one_week_avg_conf_std))
print('two wk conf sd: '+ str(two_week_avg_conf_std))
print('three wk conf sd: '+ str(three_week_avg_conf_std))
print('four wk conf sd: '+ str(four_week_avg_conf_std))

print('one wk death errors: '+ str(one_week_avg_death_errors))
print('two wk death errors: '+ str(two_week_avg_death_errors))
print('three wk death errors: '+ str(three_week_avg_death_errors))
print('four wk death errors: '+ str(four_week_avg_death_errors))

print('one wk death sd: '+ str(one_week_avg_death_std))
print('two wk death sd: '+ str(two_week_avg_death_std))
print('three wk death sd: '+ str(three_week_avg_death_std))
print('four wk death sd: '+ str(four_week_avg_death_std))

