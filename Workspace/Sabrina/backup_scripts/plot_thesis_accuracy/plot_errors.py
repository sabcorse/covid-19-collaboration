import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn
from matplotlib import gridspec
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from dateutil.parser import parse
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.linear_model import Lasso, LassoCV
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from sklearn import preprocessing
from scipy import signal

# Plotting params
plt.style.use('seaborn-white')
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

# Date-getting function
def Get_First_Date():
    first_day_US = parse('January 22, 2020')
    return (first_day_US + timedelta(days = 100+205)).strftime('%Y-%m-%d')

# Read in data
df = pd.read_csv('datasets/by_date_errors_thesis_merged.csv')

print(df)
df = df.sort_values('training_days')
df.reset_index(inplace = True)
print(df)
training_days = df['training_days']
one_wk_ahead_error_conf = df['1wk_ahead_error_conf']
two_wk_ahead_error_conf = df['2wk_ahead_error_conf']
three_wk_ahead_error_conf = df['3wk_ahead_error_conf']
four_wk_ahead_error_conf = df['4wk_ahead_error_conf']

one_wk_ahead_upper_error_conf = df['1wk_ahead_upper_error_conf']
two_wk_ahead_upper_error_conf = df['2wk_ahead_upper_error_conf']
three_wk_ahead_upper_error_conf = df['3wk_ahead_upper_error_conf']
four_wk_ahead_upper_error_conf = df['4wk_ahead_upper_error_conf']

one_wk_ahead_lower_error_conf = df['1wk_ahead_lower_error_conf']
two_wk_ahead_lower_error_conf = df['2wk_ahead_lower_error_conf']
three_wk_ahead_lower_error_conf = df['3wk_ahead_lower_error_conf']
four_wk_ahead_lower_error_conf = df['4wk_ahead_lower_error_conf']

for i in range(len(one_wk_ahead_error_conf)):
    one_wk_ahead_lower_error_conf[i] = one_wk_ahead_error_conf[i] - one_wk_ahead_lower_error_conf[i]
    one_wk_ahead_upper_error_conf[i] = one_wk_ahead_upper_error_conf[i] - one_wk_ahead_error_conf[i]
    two_wk_ahead_lower_error_conf[i] = two_wk_ahead_error_conf[i] - two_wk_ahead_lower_error_conf[i]
    two_wk_ahead_upper_error_conf[i] = two_wk_ahead_upper_error_conf[i] - two_wk_ahead_error_conf[i]
    three_wk_ahead_lower_error_conf[i] = three_wk_ahead_error_conf[i] - three_wk_ahead_lower_error_conf[i]
    three_wk_ahead_upper_error_conf[i] = three_wk_ahead_upper_error_conf[i] - three_wk_ahead_error_conf[i]
    four_wk_ahead_lower_error_conf[i] = four_wk_ahead_error_conf[i] - four_wk_ahead_lower_error_conf[i]
    four_wk_ahead_upper_error_conf[i] = four_wk_ahead_upper_error_conf[i] - four_wk_ahead_error_conf[i]

one_wk_ahead_bars_conf = [one_wk_ahead_lower_error_conf, one_wk_ahead_upper_error_conf]
two_wk_ahead_bars_conf = [two_wk_ahead_lower_error_conf, two_wk_ahead_upper_error_conf]
three_wk_ahead_bars_conf = [three_wk_ahead_lower_error_conf, three_wk_ahead_upper_error_conf]
four_wk_ahead_bars_conf = [four_wk_ahead_lower_error_conf, four_wk_ahead_upper_error_conf]

one_wk_ahead_error_death = df['1wk_ahead_error_death']
two_wk_ahead_error_death = df['2wk_ahead_error_death']
three_wk_ahead_error_death = df['3wk_ahead_error_death']
four_wk_ahead_error_death = df['4wk_ahead_error_death']

one_wk_ahead_upper_error_death = df['1wk_ahead_upper_error_death']
two_wk_ahead_upper_error_death = df['2wk_ahead_upper_error_death']
three_wk_ahead_upper_error_death = df['3wk_ahead_upper_error_death']
four_wk_ahead_upper_error_death = df['4wk_ahead_upper_error_death']

one_wk_ahead_lower_error_death = df['1wk_ahead_lower_error_death']
two_wk_ahead_lower_error_death = df['2wk_ahead_lower_error_death']
three_wk_ahead_lower_error_death = df['3wk_ahead_lower_error_death']
four_wk_ahead_lower_error_death = df['4wk_ahead_lower_error_death']

#print(four_wk_ahead_lower_error_death[0:86])
for i in range(len(one_wk_ahead_error_death)):
    one_wk_ahead_lower_error_death[i] = one_wk_ahead_error_death[i] - one_wk_ahead_lower_error_death[i]
    one_wk_ahead_upper_error_death[i] = one_wk_ahead_upper_error_death[i] - one_wk_ahead_error_death[i]
    two_wk_ahead_lower_error_death[i] = two_wk_ahead_error_death[i] - two_wk_ahead_lower_error_death[i]
    two_wk_ahead_upper_error_death[i] = two_wk_ahead_upper_error_death[i] - two_wk_ahead_error_death[i]
    three_wk_ahead_lower_error_death[i] = three_wk_ahead_error_death[i] - three_wk_ahead_lower_error_death[i]
    three_wk_ahead_upper_error_death[i] = three_wk_ahead_upper_error_death[i] - three_wk_ahead_error_death[i]
    four_wk_ahead_lower_error_death[i] = four_wk_ahead_error_death[i] - four_wk_ahead_lower_error_death[i]
    four_wk_ahead_upper_error_death[i] = four_wk_ahead_upper_error_death[i] - four_wk_ahead_error_death[i]

one_wk_ahead_bars_death = [one_wk_ahead_lower_error_death, one_wk_ahead_upper_error_death]
two_wk_ahead_bars_death = [two_wk_ahead_lower_error_death, two_wk_ahead_upper_error_death]
three_wk_ahead_bars_death = [three_wk_ahead_lower_error_death, three_wk_ahead_upper_error_death]
four_wk_ahead_bars_death = [four_wk_ahead_lower_error_death, four_wk_ahead_upper_error_death]

#print(four_wk_ahead_bars_death[0])

# Set up dates
start_day = calendar.fromisoformat(Get_First_Date())
dates = [(start_day + timedelta(days=i)).strftime('%Y-%m-%d') for i in range(len(one_wk_ahead_error_conf))]

# Create grid
gs = gridspec.GridSpec(4, 1)
y_pos = np.arange(len(dates))

# Plot accuracy confirmed
fig = plt.figure()
ax1 = plt.subplot(gs[0])
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [-25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [-50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [0 for i in range(len(one_wk_ahead_error_conf))], color = 'r')
ax1.errorbar([i for i in range(len(one_wk_ahead_error_conf))], one_wk_ahead_error_conf, yerr = one_wk_ahead_bars_conf, ecolor = 'k', color = 'k', fmt = '.', label = '1 week ahead')
ax1.set_ylim(-220, 220)
ax1.set_ylabel('% Error (1 Week)')

ax2 = plt.subplot(gs[1], sharex = ax1)
ax2.plot([i for i in range(len(two_wk_ahead_error_conf))], [25 for i in range(len(two_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax2.plot([i for i in range(len(one_wk_ahead_error_conf))], [-25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax2.plot([i for i in range(len(one_wk_ahead_error_conf))], [50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax2.plot([i for i in range(len(one_wk_ahead_error_conf))], [-50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax2.plot([i for i in range(len(one_wk_ahead_error_conf))], [0 for i in range(len(one_wk_ahead_error_conf))], color = 'r')
ax2.errorbar([i for i in range(len(one_wk_ahead_error_conf))], two_wk_ahead_error_conf, yerr = two_wk_ahead_bars_conf, ecolor = 'k', color = 'k', fmt = '.', label = '2 week ahead')
ax2.set_ylim(-280, 280)
ax2.set_ylabel('% Error i(2 Week)')

ax3 = plt.subplot(gs[2], sharex = ax1)
ax3.plot([i for i in range(len(two_wk_ahead_error_conf))], [25 for i in range(len(two_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax3.plot([i for i in range(len(one_wk_ahead_error_conf))], [-25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax3.plot([i for i in range(len(one_wk_ahead_error_conf))], [50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax3.plot([i for i in range(len(one_wk_ahead_error_conf))], [-50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax3.plot([i for i in range(len(one_wk_ahead_error_conf))], [0 for i in range(len(one_wk_ahead_error_conf))], color = 'r')
ax3.errorbar([i for i in range(len(one_wk_ahead_error_conf))], three_wk_ahead_error_conf, yerr = three_wk_ahead_bars_conf, ecolor = 'k', color = 'k', fmt = '.', label = '3 week ahead')
ax3.set_ylim(-370, 370)
ax3.set_ylabel('% Error (3 Week)')

ax4 = plt.subplot(gs[3], sharex = ax1)
ax4.plot([i for i in range(len(two_wk_ahead_error_conf))], [25 for i in range(len(two_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax4.plot([i for i in range(len(one_wk_ahead_error_conf))], [-25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax4.plot([i for i in range(len(one_wk_ahead_error_conf))], [50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax4.plot([i for i in range(len(one_wk_ahead_error_conf))], [-50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax4.plot([i for i in range(len(one_wk_ahead_error_conf))], [0 for i in range(len(one_wk_ahead_error_conf))], color = 'r')
ax4.errorbar([i for i in range(len(one_wk_ahead_error_conf))], four_wk_ahead_error_conf, yerr = four_wk_ahead_bars_conf, ecolor = 'k', color = 'k', fmt = '.', label = '4 week ahead')
ax4.set_ylim(-440, 440)
ax4.set_ylabel('% Error (4 Week)')

ax4.set_xticks(y_pos)
ax4.set_xticklabels(dates, rotation=75, fontsize = 6)
ax4.xaxis.set_major_locator(plt.MultipleLocator(14))
ax4.xaxis.set_minor_locator(plt.MultipleLocator(1))
ax4.set_xlabel('Prediction Date')

ax1.set_title('Case Prediction Errors\nMichigan', fontstyle = 'italic')
#ax1.legend(loc = 'upper right')
#ax2.legend(loc = 'upper right')
#ax3.legend(loc = 'upper right')
#ax4.legend(loc = 'upper right')

plt.subplots_adjust(hspace = .0)
plt.savefig('figs/case_errors_merged')
plt.show()

# Plot accuracy deaths
fig = plt.figure()
ax1 = plt.subplot(gs[0])
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [-25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [-50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax1.plot([i for i in range(len(one_wk_ahead_error_conf))], [0 for i in range(len(one_wk_ahead_error_conf))], color = 'r')
ax1.errorbar([i for i in range(len(one_wk_ahead_error_death))], one_wk_ahead_error_death, yerr = one_wk_ahead_bars_death, ecolor = 'k', color = 'k', fmt = '.', label = '1 week ahead')
ax1.set_ylim(-140, 140)
ax1.set_ylabel('% Error (1 Week)')

ax2 = plt.subplot(gs[1], sharex = ax1)
ax2.plot([i for i in range(len(two_wk_ahead_error_conf))], [25 for i in range(len(two_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax2.plot([i for i in range(len(one_wk_ahead_error_conf))], [-25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax2.plot([i for i in range(len(one_wk_ahead_error_conf))], [50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax2.plot([i for i in range(len(one_wk_ahead_error_conf))], [-50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax2.plot([i for i in range(len(one_wk_ahead_error_conf))], [0 for i in range(len(one_wk_ahead_error_conf))], color = 'r')
ax2.errorbar([i for i in range(len(one_wk_ahead_error_death))], two_wk_ahead_error_death, yerr = two_wk_ahead_bars_death, ecolor = 'k', color = 'k', fmt = '.', label = '2 week ahead')
ax2.set_ylim(-190, 190)
ax2.set_ylabel('% Error (2 Week)')

ax3 = plt.subplot(gs[2], sharex = ax1)
ax3.plot([i for i in range(len(two_wk_ahead_error_conf))], [25 for i in range(len(two_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax3.plot([i for i in range(len(one_wk_ahead_error_conf))], [-25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax3.plot([i for i in range(len(one_wk_ahead_error_conf))], [50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax3.plot([i for i in range(len(one_wk_ahead_error_conf))], [-50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax3.plot([i for i in range(len(one_wk_ahead_error_conf))], [0 for i in range(len(one_wk_ahead_error_conf))], color = 'r')
ax3.errorbar([i for i in range(len(one_wk_ahead_error_conf))], three_wk_ahead_error_death, yerr = three_wk_ahead_bars_death, ecolor = 'k', color = 'k', fmt = '.', label = '3 week ahead')
ax3.set_ylim(-270, 270)
ax3.set_ylabel('% Error (3 Week)')

ax4 = plt.subplot(gs[3], sharex = ax1)
ax4.plot([i for i in range(len(two_wk_ahead_error_conf))], [25 for i in range(len(two_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax4.plot([i for i in range(len(one_wk_ahead_error_conf))], [-25 for i in range(len(one_wk_ahead_error_conf))], color = 'deepskyblue', linestyle = '--')
ax4.plot([i for i in range(len(one_wk_ahead_error_conf))], [50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax4.plot([i for i in range(len(one_wk_ahead_error_conf))], [-50 for i in range(len(one_wk_ahead_error_conf))], color = 'k')
ax4.plot([i for i in range(len(one_wk_ahead_error_conf))], [0 for i in range(len(one_wk_ahead_error_conf))], color = 'r')
ax4.errorbar([i for i in range(len(one_wk_ahead_error_conf))], four_wk_ahead_error_death, yerr = four_wk_ahead_bars_death, ecolor = 'k', color = 'k', fmt = '.', label = '4 week ahead')
ax4.set_ylim(-340, 340)
ax4.set_ylabel('% Error (4 Week)')

ax4.set_xticks(y_pos)
ax4.set_xticklabels(dates, rotation=75, fontsize = 6)
ax4.xaxis.set_major_locator(plt.MultipleLocator(14))
ax4.xaxis.set_minor_locator(plt.MultipleLocator(1))
ax4.set_xlabel('Prediction Date')

ax1.set_title('Death Prediction Errors\nMichigan', fontstyle = 'italic')
#ax1.legend(loc = 'upper right')
#ax2.legend(loc = 'upper right')
#ax3.legend(loc = 'upper right')
#ax4.legend(loc = 'upper right')

plt.subplots_adjust(hspace = .0)
plt.savefig('figs/death_errors_merged')
plt.show()
