import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sn
from matplotlib import gridspec
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from dateutil.parser import parse
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.linear_model import Lasso, LassoCV
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from sklearn import preprocessing
from scipy import signal

# Plotting params
plt.style.use('seaborn-white')
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

# Date-getting function
def Get_First_Date():
    first_day_US = parse('January 22, 2020')
    return (first_day_US + timedelta(days = 100+205)).strftime('%Y-%m-%d')

# Read in data
df = pd.read_csv('datasets/by_date_cumulative_thesis_merged.csv')
df2 = pd.read_csv('datasets/death_errors/by_date_cumulative_thesis_merged.csv')
df = df.sort_values(by = 'training_days')
df2 = df2.sort_values(by = 'training_days')
df.reset_index(inplace = True)
df2.reset_index(inplace = True)
print(df)
training_days = df['training_days']

one_wk_ahead_conf_pred = df['1wk_ahead_cumulative_preds_conf']
two_wk_ahead_conf_pred = df['2wk_ahead_cumulative_preds_conf']
three_wk_ahead_conf_pred = df['3wk_ahead_cumulative_preds_conf']
four_wk_ahead_conf_pred = df['4wk_ahead_cumulative_preds_conf']

one_wk_ahead_death_pred = df2['1wk_ahead_cumulative_preds_death']
two_wk_ahead_death_pred = df2['2wk_ahead_cumulative_preds_death']
three_wk_ahead_death_pred = df2['3wk_ahead_cumulative_preds_death']
four_wk_ahead_death_pred = df2['4wk_ahead_cumulative_preds_death']

current_conf = df['cumulative_truth_baseline_conf']
current_death = df2['cumulative_truth_baseline_death']

# Set up dates
start_day = calendar.fromisoformat(Get_First_Date())
dates = [(start_day + timedelta(days=i)).strftime('%Y-%m-%d') for i in range(len(one_wk_ahead_conf_pred) + 28)]

# Create grid
gs = gridspec.GridSpec(1, 2)
y_pos = np.arange(len(dates))

# Plot confirmed
fig = plt.figure()
ax1 = plt.subplot(gs[0])
for i in range(len(current_conf)):
    if i % 7 == 0:
        ax1.plot([i, i + 7, i + 14, i + 21, i+28], [current_conf[i], one_wk_ahead_conf_pred[i], two_wk_ahead_conf_pred[i], three_wk_ahead_conf_pred[i], four_wk_ahead_conf_pred[i]],  color = 'deepskyblue')
        if i == 0:
            ax1.plot([i, i + 7, i + 14, i + 21, i+28], [current_conf[i], one_wk_ahead_conf_pred[i], two_wk_ahead_conf_pred[i], three_wk_ahead_conf_pred[i], four_wk_ahead_conf_pred[i]],  '.', color = 'deepskyblue', label = 'prediction') 
            ax1.plot([i], current_conf[i], '.', color = 'k', label = 'truth')
        else:
            ax1.plot([i, i + 7, i + 14, i + 21, i+28], [current_conf[i], one_wk_ahead_conf_pred[i], two_wk_ahead_conf_pred[i], three_wk_ahead_conf_pred[i], four_wk_ahead_conf_pred[i]],  '.', color = 'deepskyblue') 
            ax1.plot([i], current_conf[i], '.', color = 'k')
            
ax1.plot([i for i in range(len(current_conf))], current_conf, color = 'k')
ax1.legend(fancybox = True)

ax1.set_title('Cumulative Case Predictions vs. Truth\nMichigan', fontstyle = 'italic')
ax1.set_ylabel('Cases')

ax1.set_xticks(y_pos)
ax1.set_xticklabels(dates, rotation=75, fontsize = 6)
ax1.xaxis.set_major_locator(plt.MultipleLocator(14))
ax1.xaxis.set_minor_locator(plt.MultipleLocator(1))
ax1.set_xlabel('Date')

# Plot death
ax2 = plt.subplot(gs[1])
for i in range(len(current_death)):
    if i % 7 == 0:
        ax2.plot([i, i + 7, i + 14, i + 21, i+28], [current_death[i], one_wk_ahead_death_pred[i], two_wk_ahead_death_pred[i], three_wk_ahead_death_pred[i], four_wk_ahead_death_pred[i]],  color = 'deepskyblue')
        if i == 0:
            ax2.plot([i, i + 7, i + 14, i + 21, i+28], [current_death[i], one_wk_ahead_death_pred[i], two_wk_ahead_death_pred[i], three_wk_ahead_death_pred[i], four_wk_ahead_death_pred[i]],  '.', color = 'deepskyblue', label = 'prediction')
            ax2.plot([i], current_death[i], '.', color = 'k', label = 'truth')
        else:
            ax2.plot([i, i + 7, i + 14, i + 21, i+28], [current_death[i], one_wk_ahead_death_pred[i], two_wk_ahead_death_pred[i], three_wk_ahead_death_pred[i], four_wk_ahead_death_pred[i]],  '.', color = 'deepskyblue')
            ax2.plot([i], current_death[i], '.', color = 'k')

ax2.plot([i for i in range(len(current_death))], current_death, color = 'k')
ax2.legend(fancybox = True)

ax2.set_title('Cumulative Death Predictions vs. Truth\nMichigan', fontstyle = 'italic')
ax2.set_ylabel('Deaths')

ax2.set_xticks(y_pos)
ax2.set_xticklabels(dates, rotation=75, fontsize = 6)
ax2.xaxis.set_major_locator(plt.MultipleLocator(14))
ax2.xaxis.set_minor_locator(plt.MultipleLocator(1))
ax2.set_xlabel('Date')

plt.savefig('figs/cumulative_US_2_2')
plt.show()
