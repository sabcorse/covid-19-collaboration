import pandas as pd
import numpy as np

national_df = pd.read_csv('by_date_errors_thesis_US_merged.csv')
one_week_conf_upper_errors = national_df['1wk_ahead_upper_error_conf']
two_week_conf_upper_errors = national_df['2wk_ahead_upper_error_conf']
three_week_conf_upper_errors = national_df['3wk_ahead_upper_error_conf']
four_week_conf_upper_errors = national_df['4wk_ahead_upper_error_conf']

one_week_conf_lower_errors = national_df['1wk_ahead_lower_error_conf']
two_week_conf_lower_errors = national_df['2wk_ahead_lower_error_conf']
three_week_conf_lower_errors = national_df['3wk_ahead_lower_error_conf']
four_week_conf_lower_errors = national_df['4wk_ahead_lower_error_conf']

one_week_death_upper_errors = national_df['1wk_ahead_upper_error_death']
two_week_death_upper_errors = national_df['2wk_ahead_upper_error_death']
three_week_death_upper_errors = national_df['3wk_ahead_upper_error_death']
four_week_death_upper_errors = national_df['4wk_ahead_upper_error_death']

one_week_death_lower_errors = national_df['1wk_ahead_lower_error_death']
two_week_death_lower_errors = national_df['2wk_ahead_lower_error_death']
three_week_death_lower_errors = national_df['3wk_ahead_lower_error_death']
four_week_death_lower_errors = national_df['4wk_ahead_lower_error_death']
coverage = 0
for i in range(len(one_week_death_upper_errors)):
    if one_week_conf_lower_errors[i] <= 0 <= one_week_conf_upper_errors[i]:
        coverage += 1.
percent_coverage = coverage/len(one_week_conf_lower_errors)
print('one wk conf coverage: ' + str(percent_coverage))

coverage = 0
for i in range(len(one_week_death_upper_errors)):
    if two_week_conf_lower_errors[i] <= 0 <= two_week_conf_upper_errors[i]:
        coverage += 1.
percent_coverage = coverage/len(two_week_conf_lower_errors)
print('two wk conf coverage: ' + str(percent_coverage))

coverage = 0
for i in range(len(one_week_death_upper_errors)):
    if three_week_conf_lower_errors[i] <= 0 <= three_week_conf_upper_errors[i]:
        coverage += 1.
percent_coverage = coverage/len(three_week_conf_lower_errors)
print('three wk conf coverage: ' + str(percent_coverage))

coverage = 0
for i in range(len(one_week_death_upper_errors)):
    if four_week_conf_lower_errors[i] <= 0 <= four_week_conf_upper_errors[i]:
        coverage += 1.
percent_coverage = coverage/len(four_week_conf_lower_errors)
print('four wk conf coverage: ' + str(percent_coverage))


coverage = 0
for i in range(len(one_week_death_upper_errors)):
    if one_week_death_lower_errors[i] <= 0 <= one_week_death_upper_errors[i]:
        coverage += 1.
percent_coverage = coverage/len(one_week_death_lower_errors)
print('one wk death coverage: ' + str(percent_coverage))

coverage = 0
for i in range(len(one_week_death_upper_errors)):
    if two_week_death_lower_errors[i] <= 0 <= two_week_death_upper_errors[i]:
        coverage += 1.
percent_coverage = coverage/len(two_week_death_lower_errors)
print('two wk death coverage: ' + str(percent_coverage))

coverage = 0
for i in range(len(one_week_death_upper_errors)):
    if three_week_death_lower_errors[i] <= 0 <= three_week_death_upper_errors[i]:
        coverage += 1.
percent_coverage = coverage/len(three_week_death_lower_errors)
print('three wk death coverage: ' + str(percent_coverage))

coverage = 0
for i in range(len(one_week_death_upper_errors)):
    if four_week_death_lower_errors[i] <= 0 <= four_week_death_upper_errors[i]:
        coverage += 1.
percent_coverage = coverage/len(four_week_death_lower_errors)
print('four wk death coverage: ' + str(percent_coverage))



