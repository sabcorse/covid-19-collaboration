import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import os
import sys
import operator
import iminuit
import scipy
from datetime import datetime
from pygrok import Grok
from models import SEIR
from sklearn.linear_model import Ridge
from sklearn.metrics import r2_score, mean_squared_error

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

def GetLocType():
    loc_type = input("\'Country\' or \'Province\':\n")
    if loc_type.lower() == "country":
        return 1
    elif loc_type.lower() == "province":
        return 0
    else:
        print("\nInput not valid. Please try again.")
        return GetLocType()

def GetLoc():
    location_r_formatted = input("Region of interest: ")
    '''
    if location_r == "US":
        location_r_formatted = location_r
    else:
        location_r_formatted = location_r[0].upper() + location_r[1:].lower()
    '''
    loc_type_r = GetLocType()
    return (location_r_formatted, loc_type_r)

# Specify
location, countryOrState = GetLoc()
# Save in separate directories
if not os.path.exists(location):
    os.makedirs(location)


###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Initialization
dates = []
confirmedCases = []
deaths = []
recovered = []
active = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
        if (location == 'South Korea') and file == (path + '03-10-2020.csv'):
            location = 'Republic of Korea'
        if (location == 'Republic of Korea') and file == (path + '03-11-2020.csv'):
            location = 'Korea, South'
        filedate = grok.match(file)
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Fill dates from the file into an array
        day = str(filedate["month"])+'-'+str(filedate["day"])
        dates.append(day)

        # 'if' statement to deal with Country or State and JHU data format change
        areaLabel = 'Country/Region'
        if countryOrState == 0:
                areaLabel = 'Province/State'
        if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):
                areaLabel = 'Country_Region'
                if countryOrState == 0:
                        areaLabel = 'Province_State'

        # Grab Confirmed and Deaths
        confirmedCases.append(0)
        deaths.append(0.)
        recovered.append(0.)
        for index, row in data.iterrows():
                if row[areaLabel] == location:
                        if str(row['Confirmed']) != 'nan':
                                confirmedCases[dayCount] = confirmedCases[dayCount] + row['Confirmed']
                        if str(row['Deaths']) != 'nan':
                                deaths[dayCount] = deaths[dayCount] + row['Deaths']
                        if str(row['Recovered']) != 'nan':
                                recovered[dayCount] = recovered[dayCount] + row['Recovered']

        dayCount = dayCount + 1

# print confirmedCases
if location == 'Korea, South':
    location = 'South Korea'

###############################################################
#                                                             #
#            Make and view all-encompassing plot              #
#                                                             #
###############################################################

deathRate = np.divide(deaths, confirmedCases)

# Find the first day of infection and create a grid
day = 0
for cases in confirmedCases:
    if cases > 1000:
        first_day = day
        print ("\nFirst day of count: " + str(first_day))
        inf_init = cases
        print ("Initial case count: " + str(cases) + "\n")
        break
    day += 1

minRange = first_day

float_dates = []
day_count = 0
for date in dates[minRange:]:
    float_dates.append( day_count )
    day_count = day_count + 1                                                                     

active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]

# Total Plots
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
# Temporarily removing recovered[minRange:] to match JHU
ax1.plot(dates[minRange:],confirmedCases[minRange:],'bo',deaths[minRange:],'ro')
ax1.plot(dates[minRange+1:],list(map(operator.sub, confirmedCases[minRange+1:],confirmedCases[minRange:-1])), 'co')
ax2.plot(dates[minRange:],deathRate[minRange:],'m')
ax1.set_xlabel('Date')
ax1.set_ylabel('Cases')
ax2.set_ylabel('Death Rate')
ax2.set_ylim(0,0.1)
plt.title(location)
ax1.legend(['Confirmed','Deaths','New'])
ax2.legend(['Death Rate'],loc='upper center')

if not os.path.exists(location):
    os.makedirs(location)

plt.savefig(location + '/' + location + '.pdf')
plt.show()

##############################################################
#                                                            #
#                      Epidemic Model                        #
#                                                            #
##############################################################

# Find total population:
pop_file = 'gen_csvs/world_pop.csv'
pop_data = pd.read_csv( pop_file, index_col = None, header = 0 )
print(location)
for index, row in pop_data.iterrows():
    if row['Region'] == location:
        N = int(row['Population'])
print("Population = " + str(N))

# Divide timeline into blocks
test_frac = 1
today = float_dates[-1] + 1
t_test = np.linspace(0, int(today/test_frac), int(today/test_frac)*10)
t_out = np.linspace(0, today + 200, (today + 200)*10)
t = np.linspace(0, today, today*10)

# Integrate the SIR equations over the time grid, t
def SEIR_func(test, pop_guess, inf_init, exp_init, b, g, s):
    if test == 0:
        time = t
    elif test == 1:
        time = t_test
    elif test == 2:
        time = t_out
    S, E, I, R = SEIR(time,pop_guess,inf_init,exp_init,b,g,s)
    I_to_ret = []
    for i in range(len(time)):
        if i % 10 == 0:
            I_to_ret.append(I[i])
    return I_to_ret

# Use Minuit to minimize sum of least squares with SIR model
def minimizeMin( pop_guess, inf_init, exp_init, b, g, s ):
    I_to_sum = SEIR_func( 1, pop_guess, inf_init, exp_init, b, g, s )
    return sum( ( x - y )**2 for x,y in zip( I_to_sum, active ) )

# Find standard error of the estimate of Minuit model
def SE( data_arr, SEIR_arr ):
    sq_dist_from_fit = [(data_arr[i] - SEIR_arr[i])**2 for i in range(int(len(data_arr)/test_frac))]
    dist_sum = np.sum(sq_dist_from_fit)
    SE_est = np.sqrt(dist_sum/len(active))
    print( "SE of the Estimate: " + str(SE_est) )
    return SE_est

# Perform fit using Minuit (fixing initial guesses and limits)
m = iminuit.Minuit( minimizeMin, pop_guess = 100000, inf_init = 5, exp_init = 10, b = .1, g = 1/7., s = 1/6., error_pop_guess=5, error_inf_init=1, error_exp_init = 1, error_b=.001, error_g=.001, error_s=.001, limit_pop_guess = (np.amax(confirmedCases) + .001*np.amax(confirmedCases), N), limit_inf_init = (0,1e6), limit_exp_init = (0,1e6), limit_b = (0.,10), limit_g = (0, 10), limit_s = (0, 10) )

m.migrad( ncall = 500000 )
param = m.args
popc = m.covariance

# Plot estimate versus data
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
ax.scatter(float_dates, active, label = 'Data')
I_fit = SEIR_func(0, *param)
ax.plot(float_dates, I_fit, 'r', label = 'SEIR Fit')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Number')
plt.title(str(location) + ' SEIR Fit')
legend = ax.legend()
print('\nFit: pop_est = %5.3f, I0 = %5.3f, E0 = %5.3f, beta = %5.3f, gamma = %5.3f, sigma = %5.3f' % tuple(param) )
SE( active, SEIR_func(0, *param) )
day_to_check = int(len(float_dates)/test_frac) + -1
#print('Two Week Percentage Error: ' + str(100. * abs(active[day_to_check] - I_fit[day_to_check])/active[day_to_check]))
plt.show()
'''
# Plot estimate into the future
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
#ax.scatter(float_dates, active, label = 'Data')
I_pred = SEIR_func(2, *param)
ax.plot(np.linspace(0, today + 200, today + 200), I_pred, 'r', label = 'SIR Fit')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Number')
plt.title(str(location) + ' SEIR Prediction')
ax.yaxis.set_tick_params(length=0)
ax.xaxis.set_tick_params(length=0)
legend = ax.legend()
plt.show()
'''
##############################################################
#                                                            #
#                     Predict Beta, Gamma                    #
#                                                            #
##############################################################

# Get stepwise beta and gamma
r_sum = [recovered[i] + deaths[i] for i in range(len(recovered))]
r_sum_cut = r_sum[minRange:]

beta_arr = []
gamma_arr = []

# Check to see if dividing by 0 anywhere
has_zeros = False
zero_loc = []
for i in range(len(active)):
    if active[i] == 0:
        has_zeros = True
        zero_loc.append(i)

# Quick parameter computation when no division by 0
if has_zeros == False:
    beta_arr = [(active[i+1] - active[i] + r_sum_cut[i+1] - r_sum_cut[i])/active[i] for i in range(len(active)-1)]
    gamma_arr = [(r_sum_cut[i+1] - r_sum_cut[i])/active[i] for i in range(len(active)-1)]

# Fill in 0's if necessary
else:
    for i in range(len(active) - 1):
        if i in zero_loc:
            beta_arr.append(0)
            gamma_arr.append(0)
        else:
            beta_arr.append((active[i+1] - active[i] + r_sum_cut[i+1] - r_sum_cut[i])/active[i])
            gamma_arr.append((r_sum_cut[i+1] - r_sum_cut[i])/active[i])

beta_to_plot = beta_arr
gamma_to_plot = gamma_arr

infected_pred = active
recovered_pred = r_sum_cut

# Optional: impose limit on the amount of data considered
frac = 1/2.5
stop_pt = int(len(beta_arr)*frac)
infected_pred = infected_pred[:stop_pt]
recovered_pred = recovered_pred[:stop_pt]
beta_arr = beta_arr[:stop_pt]
gamma_arr = gamma_arr[:stop_pt]

# Cut off signal to match sums (Eqn. 15 and 16)
order = 5
beta_for_ridge = beta_arr[order:]
beta_pred_matrix = []
gamma_for_ridge = gamma_arr[order:]
gamma_pred_matrix = []

# Backtrack to create X matrices in sklearn notation (dimensions len(beta_for_ridge) x order)
for i in range(len(beta_arr) - order):
    new_row_beta = [1]
    new_row_gamma = [1]
    for j in range(order):
        new_row_beta.append(beta_arr[i+j])
        new_row_gamma.append(gamma_arr[i+j])
    beta_pred_matrix.append(new_row_beta)
    gamma_pred_matrix.append(new_row_gamma)

# Remove sub-0 data from consideration
for i in range(len(beta_for_ridge)):
    this_beta = beta_for_ridge[i]
    this_gamma = gamma_for_ridge[i]

    if this_beta < 0:
        beta_for_ridge[i] = 0
    if this_gamma < 0:
        gamma_for_ridge[i] = 0

# Make constant beta, gamma, I, and R to revert to
beta_arr_const = beta_arr[:]
beta_for_ridge_const = beta_for_ridge[:]
gamma_arr_const = gamma_arr[:]
gamma_for_ridge_const = gamma_for_ridge[:]
infected_pred_const = infected_pred[:]
recovered_pred_const = recovered_pred[:]

# Choose alphas to try
# Was 25 steps
coeff_list = np.linspace(1.01, 9.99, 15)
alpha_list = []
i = 0
while i < 9:
    for j in range(len(coeff_list)):
        coeff = coeff_list[j]
        alpha_list.append(coeff * (10**(-i)))
    i += 1

#print(len(alpha_list))

# Creating optimal value placeholders
min_least_total_beta = 1e9
min_least_total_gamma = 1e9
min_least_total_inf = 1e9
best_alpha_beta = 0
beta_alpha_gamma = 0
best_beta_model = []
best_gamma_model = []
best_infected_pred = []
k = 0
# Loop over alpha pairs
while k < len(alpha_list):
    #print(k)    
    j = 0
    while j < len(alpha_list):
        # Reset to original prepped values
        beta_arr = beta_arr_const[:]
        beta_for_ridge = beta_for_ridge_const[:]
        gamma_arr = gamma_arr_const[:]
        gamma_for_ridge = gamma_for_ridge_const[:]
        infected_pred = infected_pred_const[:]
        recovered_pred = recovered_pred_const[:]

        #print("k = " + str(k) + " j = " + str(j))
        ridge_beta = Ridge(alpha = alpha_list[k])
        ridge_gamma = Ridge(alpha = alpha_list[j])

        # Fit beta and gamma to corresponding matrices
        # Return "w" coefficient vectors (in sklearn notation; a and b vectors in paper)
        ridge_beta.fit(beta_pred_matrix, beta_for_ridge)
        ridge_gamma.fit(gamma_pred_matrix, gamma_for_ridge)

        beta_model = ridge_beta.predict(beta_pred_matrix)
        gamma_model = ridge_gamma.predict(gamma_pred_matrix)

        beta_model = beta_model.tolist()
        gamma_model = gamma_model.tolist()

        # Insert values for plot matching later
        for i in range(order):
            beta_model.insert(i, beta_arr[i])
            gamma_model.insert(i, gamma_arr[i])

        # Predict next beta and gamma
        stop = len(active)
        start = len(beta_arr)
        t = start
        while t < stop:
            sample_beta = [1]
            sample_gamma = [1]
            i = -order
            while i < 0:
                sample_beta.append(beta_for_ridge[i])
                sample_gamma.append(gamma_for_ridge[i])
                i += 1

            # Reshape for prediction (needs 2D array)
            sample_beta = np.asarray(sample_beta)
            sample_gamma = np.asarray(sample_gamma)
            sample_beta = sample_beta.reshape(1,-1)
            sample_gamma = sample_gamma.reshape(1,-1)

            next_beta = ridge_beta.predict(sample_beta)
            next_gamma = ridge_gamma.predict(sample_gamma)

            if next_beta < 0:
                beta_for_ridge.append(0)
                beta_arr.append(0)
                beta_model.append(0)
            else:
                beta_for_ridge.append(next_beta)
                beta_arr.append(next_beta)
                beta_model.append(next_beta)

            if next_gamma < 0:
                gamma_for_ridge.append(0)
                gamma_arr.append(0)
                gamma_model.append(0)
            else:
                gamma_for_ridge.append(next_gamma)
                gamma_arr.append(next_gamma)
                gamma_model.append(next_gamma)

            # Predict infected and recovered:
            infected_pred.append((1 + beta_for_ridge[-1] - gamma_for_ridge[-1])*infected_pred[-1])
            recovered_pred.append(recovered_pred[-1] + gamma_for_ridge[-1] * infected_pred[-2])

            t += 1

        # Determine error
        least_sq_total_beta = 0
        least_sq_total_gamma = 0
        least_sq_total_inf = 0
        for n in range(start, start + 14):
            least_sq_total_beta += (beta_to_plot[n] - beta_model[n])**2
            least_sq_total_gamma += (gamma_to_plot[n] - gamma_model[n])**2
            least_sq_total_inf += (active[n] - infected_pred[n])**2
        
        least_sq_total_beta = np.sqrt(least_sq_total_beta)
        least_sq_total_gamma = np.sqrt(least_sq_total_gamma)
        least_sq_total_inf = np.sqrt(least_sq_total_inf)
        
        #print("Beta penalty = " + str(alpha_list[k]))
        #print("Gamma penalty = " + str(alpha_list[j]))
        #print("Beta least squares = " + str(least_sq_total_beta))
        #print("Gamma least squares = " + str(least_sq_total_gamma))
        
        # Set best active prediction (both parameters optimal)
        #if least_sq_total_beta <= min_least_total_beta and least_sq_total_gamma <= min_least_total_gamma:
        if least_sq_total_inf <= min_least_total_inf:
            best_infected_pred = infected_pred
            
            min_least_total_beta = least_sq_total_beta
            best_alpha_beta = alpha_list[k]
            best_beta_model = beta_model

            min_least_total_gamma = least_sq_total_gamma
            best_alpha_gamma = alpha_list[j]
            best_gamma_model = gamma_model

            min_least_total_inf = least_sq_total_inf

        # Determine optimal values (reset benchmarks)
        '''
        if least_sq_total_beta < min_least_total_beta:
            min_least_total_beta = least_sq_total_beta

        if least_sq_total_gamma < min_least_total_gamma:
            min_least_total_gamma = least_sq_total_gamma
        '''
        j += 1

    k += 1

print("Best beta penalty: " + str(best_alpha_beta))
print("Best gamma penalty: " + str(best_alpha_gamma))

# Plot optimal beta results
plt.plot(best_beta_model[:start+14], 'g', linestyle = '--', label = 'model')
plt.plot(beta_to_plot[:start+14], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Beta')
plt.title('Beta Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_beta.pdf')
plt.show()

# Plot optimal gamma results
plt.plot(best_gamma_model[:start+14], 'g', linestyle = '--', label = 'model')
plt.plot(gamma_to_plot[:start+14], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Gamma')
plt.title('Gamma Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_gamma.pdf')
plt.show()

# Plot optimal active case results
plt.plot(best_infected_pred[:start+28], 'g', linestyle = '--', label = 'model')
plt.plot(active[:start+28], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Active Cases')
plt.title('Active Case Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_active.pdf')
plt.show()

##############################################################
#                                                            #
#                     Estimate Milestones                    #
#                                                            #
##############################################################
'''
# Estimate peak
I_prev = 0
I_now = 0
I_next = 0

peak_date = 0
peak_cases = 0
safe_date = 0
found = False

for i in range(1,len(I_pred)-2):
    if i == 1:
        I_now = I_pred[i]
        I_next = I_pred[i+1]

    else:
        I_prev = I_pred[i-1]
        I_now = I_pred[i]
        I_next = I_pred[i+1]

    if I_now > I_prev and I_now > I_next:
        peak_date = i
        peak_cases = I_now

    if I_now < 1000 and I_next < I_now and found == False:
        safe_date = i
        found = True

print('\nEstimated Peak Date from Start: ' + str(peak_date) + '\nEstimated Peak Infections: ' + str(int(peak_cases)) + '\nEstimated Safe Date: ' + str(safe_date))
'''
