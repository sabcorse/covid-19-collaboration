import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import os
import sys
import operator
from scipy.optimize import curve_fit
from datetime import datetime
from pygrok import Grok
from models import SIR

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

def GetLocType():
    loc_type = raw_input("\'Country\' or \'Province\':\n")
    if loc_type.lower() == "country":
        return 1
    elif loc_type.lower() == "province":
        return 0
    else:
        print("\nInput not valid. Please try again.")
        return GetLocType()

def GetLoc():
    location_r_formatted = raw_input("Region of interest: ")
    '''
    if location_r == "US":
        location_r_formatted = location_r
    else:
        location_r_formatted = location_r[0].upper() + location_r[1:].lower()
    '''
    loc_type_r = GetLocType()
    return (location_r_formatted, loc_type_r)

# Specify
location, countryOrState = GetLoc()
# Save in separate directories
if not os.path.exists(location):
    os.makedirs(location)


###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Initialization
dates = []
confirmedCases = []
deaths = []
recovered = []
active = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
        if (location == 'South Korea') and file == (path + '03-10-2020.csv'):
            location = 'Republic of Korea'
        if (location == 'Republic of Korea') and file == (path + '03-11-2020.csv'):
            location = 'Korea, South'
        filedate = grok.match(file)
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Fill dates from the file into an array
        day = str(filedate["month"])+'-'+str(filedate["day"])
        dates.append(day)

        # 'if' statement to deal with Country or State and JHU data format change
        areaLabel = 'Country/Region'
        if countryOrState == 0:
                areaLabel = 'Province/State'
        if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):
                areaLabel = 'Country_Region'
                if countryOrState == 0:
                        areaLabel = 'Province_State'

        # Grab Confirmed and Deaths
        confirmedCases.append(0)
        deaths.append(0.)
        recovered.append(0.)
        for index, row in data.iterrows():
                if row[areaLabel] == location:
                        if str(row['Confirmed']) != 'nan':
                                confirmedCases[dayCount] = confirmedCases[dayCount] + row['Confirmed']
                        if str(row['Deaths']) != 'nan':
                                deaths[dayCount] = deaths[dayCount] + row['Deaths']
                        if str(row['Recovered']) != 'nan':
                                recovered[dayCount] = recovered[dayCount] + row['Recovered']

        dayCount = dayCount + 1

#print confirmedCases
if location == 'Korea, South':
    location = 'South Korea'

###############################################################
#                                                             #
#            Make and view all-encompassing plot              #
#                                                             #
###############################################################

deathRate = np.divide(deaths, confirmedCases)

# Make my plots
minRange = 20

float_dates = []
day_count = 0
for date in dates[minRange:]:
    float_dates.append( day_count )
    day_count = day_count + 1                                                                     

active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]

# Total Plots
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
# Temporarily removing recovered[minRange:] to match JHU
ax1.plot(dates[minRange:],confirmedCases[minRange:],'bo',deaths[minRange:],'ro')
ax1.plot(dates[minRange+1:],list(map(operator.sub, confirmedCases[minRange+1:],confirmedCases[minRange:-1])), 'co')
ax2.plot(dates[minRange:],deathRate[minRange:],'m')
ax1.set_xlabel('Date')
ax1.set_ylabel('Cases')
ax2.set_ylabel('Death Rate')
ax2.set_ylim(0,0.1)
plt.title(location)
ax1.legend(['Confirmed','Deaths','New'])
ax2.legend(['Death Rate'],loc='upper center')

if not os.path.exists(location):
    os.makedirs(location)

plt.savefig(location + '/' + location + '.pdf')
plt.show()

###############################################################
#                                                             #
#            Pick and view new or confirmed data              #
#                                                             #
###############################################################

case_type = raw_input( "\nCase type? \'New\', \'Confirmed\': " )
function_type = raw_input( "Function type? \'Exp\', \'Log\', \'Pwr\', \'AsymB\': " )

# Choose function type based on user input
def func(x, a, b, c, d = 0):
    if function_type.lower() == 'pwr':
        return a * 10**-5. * np.asarray(x)**b + c
    elif function_type.lower() == 'log':
        return a * np.log( b * np.asarray(x)  + c ) + d
    elif function_type.lower() == 'asymb':
        return a * np.asarray(x)/b**2 * np.exp(-(np.asarray(x) + c)**2/2./b**2) + d
    else:
        return a * np.exp( b * np.asarray(x) ) + c

if case_type.lower() == "confirmed":
    popt, pcov = curve_fit( func, float_dates, confirmedCases[minRange:], p0 = [1, 1, 1, 1], maxfev = 10000 )
elif case_type.lower() == "new":
    popt, pcov = curve_fit( func, float_dates[1:], list(map(operator.sub, confirmedCases[minRange+1:],confirmedCases[minRange:-1])), p0 = [1, 1, 1, 1], maxfev = 10000 )

# Prediction:
today = str(datetime.today().strftime('%Y-%-m-%-d'))
print(today)
print(location + ' Fit: a = %5.3f, b = %5.3f, c = %5.3f, d = %5.3f' % tuple( popt ) )
if case_type.lower() == "confirmed":
    print('Infected Prediction for tomorrow = ' + str( int(func(day_count,popt[0],popt[1],popt[2],popt[3])) - confirmedCases[minRange+day_count-1]))

# Fit Plot
fig1, ax3 = plt.subplots()
ax4 = ax3.twinx()
ax3.plot( float_dates, func(float_dates, *popt), 'r-', label='Fit: a = %5.3f,\n b = %5.3f,\n c = %5.3f,\n d = %5.3f' % tuple( popt ) )
if case_type.lower() == "confirmed":
    ax4.scatter( float_dates, confirmedCases[minRange:], label = 'Data' )
elif case_type.lower() == "new":
    ax4.scatter( float_dates[1:], list(map(operator.sub, confirmedCases[minRange+1:],confirmedCases[minRange:-1])), label = 'Data')
ax4.set_xlabel('Days')
ax4.set_ylabel('Cases')
plt.title(location)
ax3.legend(loc = 'upper left')
ax4.legend(loc = 'upper center')
plt.savefig(location + '/' + location + 'Fit.pdf')
#plt.show()


##############################################################
#                                                            #
#                      Epidemic Model                        #
#                                                            #
##############################################################

# SIR Model
# ** Note: eventually include birth/death rate
# Active = a/b * log(healthy) - healthy + C
# C = I(0) - a/b * log(initial population) + initial population

#susceptible = [population - (active[i] + deaths[i] + recovered[i]) for i in range(len(confirmedCases))]

# Find total population:
pop_file = 'world_pop.csv'
pop_data = pd.read_csv( pop_file, index_col = None, header = 0 )
print location
for index, row in pop_data.iterrows():
    if row['Region'] == location:
        N = int(row['Population'])
print("Population = " + str(N))

# Contact rate, beta, and mean recovery rate, gamma, (in 1/days).
beta, gamma = 0.2, 1./10

# Find the first day of infection and create a grid
day = 1
for cases in confirmedCases[minRange:]:
    if cases != 0:
        first_day = float_dates[minRange+day]
        print ("First day of cases: " + str(first_day))
        inf_init = cases
        print ("Initial case count: " + str(cases))
        break
    day += 1

today = float_dates[-1] + 1
t = np.linspace(0, today, (today)*10)
print len(t)

# Integrate the SIR equations over the time grid, t
def SIR_func(time, inf_init, sus_pop, b, g):
    S, I, R = SIR(t,sus_pop,inf_init,b,g)
    I_to_ret = []
    for i in range(len(t)):
        if i % 10 == 0:
            I_to_ret.append(I[i])
    return I_to_ret

cutoff = int(len(float_dates))/3
popt3, pcov3 = curve_fit( SIR_func, float_dates, active, bounds = ([0, 0, 0, 1./15.], [100, 1e7, 1, .2]), p0 = [5.412, 1e5, .328, .111], maxfev = 1000000 )

# Find SE of the estimate
def SE( data_arr, SIR_arr ):
    sq_dist_from_fit = [(data_arr[i] - SIR_arr[i])**2 for i in range(len(data_arr))]
    dist_sum = np.sum(sq_dist_from_fit)
    SE_est = np.sqrt(dist_sum/len(active))
    print( "\nStandard Error of the Estimate  = " + str(SE_est) )
    return SE_est

SE( active, SIR_func(float_dates, *popt3) )

# Plot the data on three separate curves for S(t), I(t) and R(t)
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
#ax.plot(float_dates, S[minRange:minRange+today+1]/1000, 'b', alpha=0.5, lw=2, label='Susceptible')
#ax.plot(float_dates, I[minRange:minRange+today+1]/1000, 'r', alpha=0.5, lw=2, label='Infected')
#ax.plot(float_dates, R[minRange:minRange+today+1]/1000, 'g', alpha=0.5, lw=2, label='Recovered with immunity')
#ax.plot(float_dates, active/1000, 'c', alpha=0.5, lw=2, label='Data')
ax.scatter(float_dates, active, label = 'Data')
ax.plot(float_dates, SIR_func(float_dates, *popt3), 'r', label = 'SIR Fit')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Number')
plt.title(str(location) + ' SIR Fit')
#ax.set_ylim(0, 6)
ax.yaxis.set_tick_params(length=0)
ax.xaxis.set_tick_params(length=0)
#ax.grid(b=True, which='major', c='w', lw=2, ls='-')
legend = ax.legend()
#legend.get_frame().set_alpha(0.5)
#for spine in ('top', 'right', 'bottom', 'left'):
#    ax.spines[spine].set_visible(True)
print('\nFit: I0 = %5.3f, N = %5.3f, beta = %5.3f, gamma = %5.3f' % tuple(popt3) )
plt.show()
