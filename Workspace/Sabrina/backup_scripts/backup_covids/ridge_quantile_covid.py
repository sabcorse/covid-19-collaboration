import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import os
import sys
import operator
import iminuit
import scipy
from datetime import datetime
from pygrok import Grok
from models import SEIR
from sklearn.linear_model import Ridge
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

def GetLocType():
    loc_type = input("\'Country\' or \'Province\':\n")
    if loc_type.lower() == "country":
        return 1
    elif loc_type.lower() == "province":
        return 0
    else:
        print("\nInput not valid. Please try again.")
        return GetLocType()

def GetLoc():
    location_r_formatted = input("Region of interest: ")
    '''
    if location_r == "US":
        location_r_formatted = location_r
    else:
        location_r_formatted = location_r[0].upper() + location_r[1:].lower()
    '''
    loc_type_r = GetLocType()
    return (location_r_formatted, loc_type_r)

# Specify
location, countryOrState = GetLoc()
# Save in separate directories
if not os.path.exists(location):
    os.makedirs(location)


###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Initialization
dates = []
confirmedCases = []
deaths = []
recovered = []
active = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
        if (location == 'South Korea') and file == (path + '03-10-2020.csv'):
            location = 'Republic of Korea'
        if (location == 'Republic of Korea') and file == (path + '03-11-2020.csv'):
            location = 'Korea, South'
        filedate = grok.match(file)
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Fill dates from the file into an array
        day = str(filedate["month"])+'-'+str(filedate["day"])
        dates.append(day)

        # 'if' statement to deal with Country or State and JHU data format change
        areaLabel = 'Country/Region'
        if countryOrState == 0:
                areaLabel = 'Province/State'
        if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):
                areaLabel = 'Country_Region'
                if countryOrState == 0:
                        areaLabel = 'Province_State'

        # Grab Confirmed and Deaths
        confirmedCases.append(0)
        deaths.append(0.)
        recovered.append(0.)
        for index, row in data.iterrows():
                if row[areaLabel] == location:
                        if str(row['Confirmed']) != 'nan':
                                confirmedCases[dayCount] = confirmedCases[dayCount] + row['Confirmed']
                        if str(row['Deaths']) != 'nan':
                                deaths[dayCount] = deaths[dayCount] + row['Deaths']
                        if str(row['Recovered']) != 'nan':
                                recovered[dayCount] = recovered[dayCount] + row['Recovered']

        dayCount = dayCount + 1

# print confirmedCases
if location == 'Korea, South':
    location = 'South Korea'

###############################################################
#                                                             #
#            Make and view all-encompassing plot              #
#                                                             #
###############################################################

deathRate = np.divide(deaths, confirmedCases)

# Find the first day of infection and create a grid
day = 0
for cases in confirmedCases:
    if cases > 1000:
        first_day = day
        print ("\nFirst day of count: " + str(first_day))
        inf_init = cases
        print ("Initial case count: " + str(cases) + "\n")
        break
    day += 1

minRange = first_day

float_dates = []
day_count = 0
for date in dates[minRange:]:
    float_dates.append( day_count )
    day_count = day_count + 1                                                                     

active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]
# Total Plots
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
# Temporarily removing recovered[minRange:] to match JHU
ax1.plot(dates[minRange:],confirmedCases[minRange:],'bo',deaths[minRange:],'ro')
ax1.plot(dates[minRange+1:],list(map(operator.sub, confirmedCases[minRange+1:],confirmedCases[minRange:-1])), 'co')
ax2.plot(dates[minRange:],deathRate[minRange:],'m')
ax1.set_xlabel('Date')
ax1.set_ylabel('Cases')
ax2.set_ylabel('Death Rate')
ax2.set_ylim(0,0.1)
plt.title(location)
ax1.legend(['Confirmed','Deaths','New'])
ax2.legend(['Death Rate'],loc='upper center')

if not os.path.exists(location):
    os.makedirs(location)

plt.savefig(location + '/' + location + '.pdf')
plt.show()

##############################################################
#                                                            #
#                      Epidemic Model                        #
#                                                            #
##############################################################

# Moved to Ella SEIR_covid.py

##############################################################
#                                                            #
#                     Predict Beta, Gamma                    #
#                                                            #
##############################################################

active_arr_checker = active[:]
print(len(active_arr_checker))

# Get stepwise beta and gamma
r_sum = [recovered[i] + deaths[i] for i in range(len(recovered))]
r_sum_cut = r_sum[minRange:]

deaths = deaths[minRange:]
deaths_arr_checker = deaths[:]

confirmed = confirmedCases[minRange:]
confirmed_arr_checker = confirmed[:]

recovered_arr_checker = r_sum_cut[:]

beta_arr = []
gamma_arr = []

# Check to see if dividing by 0 anywhere
has_zeros = False
zero_loc = []
for i in range(len(active)):
    if active[i] == 0:
        has_zeros = True
        zero_loc.append(i)

active_to_plot = active
recovered_to_plot = r_sum_cut
deaths_to_plot = deaths
confirmed_to_plot = confirmed

active_pred = active
recovered_pred = r_sum_cut
deaths_pred = deaths
confirmed_pred = confirmed

# Optional: impose limit on the amount of data considered
frac = 1.

stop_pt = int(len(active)*frac)
active_pred = active_pred[:stop_pt]
recovered_pred = recovered_pred[:stop_pt]
confirmed_pred = confirmed_pred[:stop_pt]
deaths_pred = deaths_pred[:stop_pt]
active = active[:stop_pt]
r_sum_cut = r_sum_cut[:stop_pt]
deaths = deaths[:stop_pt]
confirmed = confirmed[:stop_pt]


# Cut off signal to match sums (Eqn. 15 and 16)
order = 15
active_for_ridge = active[order:]
active_pred_matrix = []
recovered_for_ridge = r_sum_cut[order:]
recovered_pred_matrix = []
deaths_for_ridge = deaths[order:]
deaths_pred_matrix = []
confirmed_for_ridge = confirmed[order:]
confirmed_pred_matrix = []

# Backtrack to create X matrices in sklearn notation (dimensions len(beta_for_ridge) x order)
for i in range(len(active) - order):
    new_row_active = [1]
    new_row_recovered = [1]
    new_row_deaths = [1]
    new_row_confirmed = [1]
    for j in range(order):
        new_row_active.append(active[i+j])
        new_row_recovered.append(r_sum_cut[i+j])
        new_row_deaths.append(deaths[i+j])
        new_row_confirmed.append(confirmed[i+j])
    active_pred_matrix.append(new_row_active)
    recovered_pred_matrix.append(new_row_recovered)
    deaths_pred_matrix.append(new_row_deaths)
    confirmed_pred_matrix.append(new_row_confirmed)

# Remove sub-0 data from consideration
for i in range(len(active_for_ridge)):
    this_active = active_for_ridge[i]
    this_recovered = recovered_for_ridge[i]
    this_deaths = deaths_for_ridge[i]
    this_confirmed = confirmed_for_ridge[i]

    if this_active < 0:
        active_for_ridge[i] = 0
    if this_recovered < 0:
        recovered_for_ridge[i] = 0
    if this_deaths < 0:
        deaths_for_ridge[i] = 0
    if this_confirmed < 0:
        confirmed_for_ridge[i] = 0

# Make constant beta, gamma, I, and R to revert to
active_arr_const = active[:]
active_for_ridge_const = active_for_ridge[:]
recovered_arr_const = r_sum_cut[:]
recovered_for_ridge_const = recovered_for_ridge[:]
deaths_arr_const = deaths[:]
deaths_for_ridge_const = deaths_for_ridge[:]
confirmed_arr_const = confirmed[:]
confirmed_for_ridge_const = confirmed_for_ridge[:]
#infected_pred_const = infected_pred[:]
#recovered_pred_const = recovered_pred[:]

# Choose alphas to try
# Was 25 steps
coeff_list = [1., 5.]
coeff = 1.
alpha_list = []
i = 4
while i < 9:
    for j in range(len(coeff_list)):
        coeff = coeff_list[j]
        alpha_list.append(coeff * (10**(-i)))
    i += 1

#print(len(alpha_list))

# Creating optimal value placeholders
min_least_total_active = 1e9
min_least_total_recovered = 1e9
min_least_total_deaths = 1e9
min_least_total_confirmed = 1e9
#min_least_total_inf = 1e9
best_alpha_active = 0
best_alpha_recovered = 0
best_alpha_deaths = 0
best_alpha_confirmed = 0
best_active_model = []
best_recovered_model = []
best_deaths_model = []
best_confirmed_model = []
#best_infected_pred = []

confirmed_model_list = []
deaths_model_list = []

k = 0
# Loop over alpha pairs
while k < len(alpha_list):
    #print(k)    
    j = 0
    while j < len(alpha_list):
        # Reset to original prepped values
        active = active_arr_const[:]
        active_for_ridge = active_for_ridge_const[:]
        r_sum_cut = recovered_arr_const[:]
        recovered_for_ridge = recovered_for_ridge_const[:]
        deaths = deaths_arr_const[:]
        deaths_for_ridge = deaths_for_ridge_const[:]
        confirmed = confirmed_arr_const[:]
        confirmed_for_ridge = confirmed_for_ridge_const[:]
        #infected_pred = infected_pred_const[:]
        #recovered_pred = recovered_pred_const[:]

        #print("k = " + str(k) + " j = " + str(j))
        ridge_active = Ridge(alpha = alpha_list[k], normalize = True)
        ridge_recovered = Ridge(alpha = alpha_list[j], normalize = True)
        ridge_deaths = Ridge(alpha = alpha_list[k], normalize = True)
        ridge_confirmed = Ridge(alpha = alpha_list[k], normalize = True)

        # Fit beta and gamma to corresponding matrices
        # Return "w" coefficient vectors (in sklearn notation; a and b vectors in paper)
        ridge_active.fit(active_pred_matrix, active_for_ridge)
        ridge_recovered.fit(recovered_pred_matrix, recovered_for_ridge)
        ridge_deaths.fit(deaths_pred_matrix, deaths_for_ridge)
        ridge_confirmed.fit(confirmed_pred_matrix, confirmed_for_ridge)

        active_model = ridge_active.predict(active_pred_matrix)
        recovered_model = ridge_recovered.predict(recovered_pred_matrix)
        deaths_model = ridge_deaths.predict(deaths_pred_matrix)
        confirmed_model = ridge_confirmed.predict(confirmed_pred_matrix)

        active_model = active_model.tolist()
        recovered_model = recovered_model.tolist()
        deaths_model = deaths_model.tolist()
        confirmed_model = confirmed_model.tolist()

        # Insert values for plot matching later
        for i in range(0, order):
            active_model.insert(i, active[i])
            recovered_model.insert(i, r_sum_cut[i])
            deaths_model.insert(i, deaths[i])
            confirmed_model.insert(i, confirmed[i])

        # Predict next beta and gamma
        stop = int(len(active_arr_checker)) + 40
        start = len(active)
        t = start
        while t < stop:
            sample_active = [1]
            sample_recovered = [1]
            sample_deaths = [1]
            sample_confirmed = [1]
            i = -order
            while i < 0:
                sample_active.append(active_for_ridge[i])
                sample_recovered.append(recovered_for_ridge[i])
                sample_deaths.append(deaths_for_ridge[i])
                sample_confirmed.append(confirmed_for_ridge[i])
                i += 1

            # Reshape for prediction (needs 2D array)
            sample_active = np.asarray(sample_active)
            sample_recovered = np.asarray(sample_recovered)
            sample_deaths = np.asarray(sample_deaths)
            sample_confirmed = np.asarray(sample_confirmed)
            sample_active = sample_active.reshape(1,-1)
            sample_recovered = sample_recovered.reshape(1,-1)
            sample_deaths = sample_deaths.reshape(1,-1)
            sample_confirmed = sample_confirmed.reshape(1,-1)

            next_active = ridge_active.predict(sample_active)
            next_recovered = ridge_recovered.predict(sample_recovered)
            next_deaths = ridge_deaths.predict(sample_deaths)
            next_confirmed = ridge_confirmed.predict(sample_confirmed)

            if next_active < 0:
                active_for_ridge.append(0)
                active.append(0)
                active_model.append(0)
            else:
                active_for_ridge.append(next_active)
                active.append(next_active)
                active_model.append(next_active)

            if next_recovered < 0:
                recovered_for_ridge.append(0)
                r_sum_cut.append(0)
                recovered_model.append(0)
            else:
                recovered_for_ridge.append(next_recovered)
                r_sum_cut.append(next_recovered)
                recovered_model.append(next_recovered)

            if next_deaths < 0:
                deaths_for_ridge.append(0)
                deaths.append(0)
                deaths_model.append(0)

            else:
                deaths_for_ridge.append(next_deaths)
                deaths.append(next_deaths)
                deaths_model.append(next_deaths)

            if next_confirmed < 0:
                confirmed_for_ridge.append(0)
                confirmed.append(0)
                confirmed_model.append(0)

            else:
                confirmed_for_ridge.append(next_confirmed)
                confirmed.append(next_confirmed)
                confirmed_model.append(next_confirmed)

            # Predict infected and recovered:
            #infected_pred.append((1 + beta_for_ridge[-1] - gamma_for_ridge[-1])*infected_pred[-1])
            #recovered_pred.append(recovered_pred[-1] + gamma_for_ridge[-1] * infected_pred[-2])

            t += 1

        # Determine error
        least_sq_total_active = 0
        least_sq_total_recovered = 0
        least_sq_total_deaths = 0
        least_sq_total_confirmed = 0
        #least_sq_total_inf = 0
        for n in range(len(active_arr_checker)):
            #least_sq_total_active += (active_to_plot[n] - beta_model[n])**2
            #least_sq_total_gamma += (gamma_to_plot[n] - gamma_model[n])**2
            #print(len(active))
            #print(len(active_arr_checker))
            least_sq_total_active += (active[n] - active_arr_checker[n])**2
            least_sq_total_recovered += (r_sum_cut[n] - recovered_arr_checker[n])**2
            least_sq_total_deaths += (deaths[n] - deaths_arr_checker[n])**2
            least_sq_total_confirmed += (confirmed[n] - confirmed_arr_checker[n])**2
        
        #least_sq_total_beta = np.sqrt(least_sq_total_beta)
        #least_sq_total_gamma = np.sqrt(least_sq_total_gamma)
        least_sq_total_active = np.sqrt(least_sq_total_active)
        least_sq_total_recovered = np.sqrt(least_sq_total_recovered)
        least_sq_total_deaths = np.sqrt(least_sq_total_deaths)
        least_sq_total_confirmed = np.sqrt(least_sq_total_confirmed)

        #print("Beta penalty = " + str(alpha_list[k]))
        #print("Gamma penalty = " + str(alpha_list[j]))
        #print("Beta least squares = " + str(least_sq_total_beta))
        #print("Gamma least squares = " + str(least_sq_total_gamma))
    
        # Set best active prediction (both parameters optimal)
        #if least_sq_total_beta <= min_least_total_beta and least_sq_total_gamma <= min_least_total_gamma:
        if least_sq_total_active <= min_least_total_active:
            best_active_model = active_model
            min_least_total_active = least_sq_total_active
            best_alpha_active = alpha_list[k]
            #best_active_model = _model

        if least_sq_total_recovered <= min_least_total_recovered:
            best_recovered_model = recovered_model
            min_least_total_recovered = least_sq_total_recovered
            best_alpha_recovered = alpha_list[j]

        if least_sq_total_deaths <= min_least_total_deaths:
            best_deaths_model = deaths_model
            min_least_total_deaths = least_sq_total_deaths
            best_alpha_deaths = alpha_list[k]

        if least_sq_total_confirmed <= min_least_total_confirmed:
            best_confirmed_model = confirmed_model
            min_least_total_confirmed = least_sq_total_confirmed
            best_alpha_confirmed = alpha_list[k]

        for i in range(len(confirmed_model)):
            confirmed_model_list.append((i, confirmed_model[i]))
            deaths_model_list.append((i, deaths_model[i]))

        # Determine optimal values (reset benchmarks)
        '''
        if least_sq_total_beta < min_least_total_beta:
            min_least_total_beta = least_sq_total_beta

        if least_sq_total_gamma < min_least_total_gamma:
            min_least_total_gamma = least_sq_total_gamma
        '''
        j += 1

    k += 1

print("Best active penalty: " + str(best_alpha_active))
print("Best recovered penalty: " + str(best_alpha_recovered))
print("Best deaths penalty: " + str(best_alpha_deaths))
print("Best confirmed penalty: " + str(best_alpha_confirmed))

for i in range(41):
    active_to_plot.append(0)
    recovered_to_plot.append(0)
    deaths_to_plot.append(0)
    confirmed_to_plot.append(0)

# Plot optimal beta results
plt.plot(best_active_model[:start+40], 'g', linestyle = '--', label = 'model')
plt.plot(active_to_plot[:start+40], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Active')
plt.title('Active Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_active.pdf')
plt.show()

# Plot optimal gamma results
plt.plot(best_recovered_model[:start+40], 'g', linestyle = '--', label = 'model')
plt.plot(recovered_to_plot[:start+40], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Recovered')
plt.title('Recovered Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_recovered.pdf')
plt.show()

# Plot optimal active case results
plt.plot(best_deaths_model[:start+40], 'g', linestyle = '--', label = 'model')
plt.plot(deaths_to_plot[:start+40], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Deaths')
plt.title('Deaths Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_deaths.pdf')
plt.show()

plt.plot(best_confirmed_model[:start+40], 'g', linestyle = '--', label = 'model')
plt.plot(confirmed_to_plot[:start+40], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Confirmed')
plt.title('Confirmed Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()

##############################################################
#                                                            #
#                       Build Quantiles                      #
#                                                            #
##############################################################

submission_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'location', 'type', 'quantile', 'value'])
# Edit days here
forecast_date = '2020-07-03'
target_end_date = '2020-07-11'

alpha = 0.975
clf_confirmed = GradientBoostingRegressor(loss = 'quantile', alpha = alpha, n_estimators = 1500, max_depth = 5, learning_rate = .1, min_samples_leaf = 5, min_samples_split = 5)

clf_deaths = GradientBoostingRegressor(loss = 'quantile', alpha = alpha, n_estimators = 1500, max_depth = 3, learning_rate = .1, min_samples_leaf = 9, min_samples_split = 9)


quantile_list = [.01, 0.025]
for i in np.arange(0.05, 1.0, .05):
    quantile_list.append(i)
quantile_list.append(0.975)
quantile_list.append(0.99)

quantile_list_case_inc = [0.025, 0.100, 0.250, 0.500, 0.750, 0.900]

case_count_list = []
case_inc_list = []
death_count_list = []
death_inc_list = []

# Edit days here
days_for_pred = [5, 12, 19, 26]

days_to_fit = len(best_confirmed_model) - len(confirmedCases[minRange:])
day_arr = np.atleast_2d(np.arange(0, days_to_fit, 1)).T
day_arr = day_arr.astype(np.float32)

X_confirmed = []
y_confirmed = []
X_deaths = []
y_deaths = []

for (i, j) in confirmed_model_list:
    if i >= 0: #len(confirmedCases[minRange:]):
        X_confirmed.append(i)
        y_confirmed.append(j)

for (i, j) in deaths_model_list:
    if i >= 0: #len(confirmedCases[minRange:]):
        X_deaths.append(i)
        y_deaths.append(j)

X_confirmed = np.atleast_2d(np.asarray(X_confirmed)).T
X_deaths = np.atleast_2d(np.asarray(X_deaths)).T
clf_confirmed.fit(X_confirmed, y_confirmed)
clf_deaths.fit(X_deaths, y_deaths)

day_arr_fine = np.atleast_2d(np.linspace(X_deaths[0], X_deaths[-1], 100))
day_arr_fine = day_arr_fine.astype(np.float32)

upper = clf_confirmed.predict(day_arr_fine)
upper_d = clf_deaths.predict(day_arr_fine)

clf_confirmed.set_params(alpha = .025)
clf_deaths.set_params(alpha = .025)
clf_confirmed.fit(X_confirmed, y_confirmed)
clf_deaths.fit(X_deaths, y_deaths)
lower = clf_confirmed.predict(day_arr_fine)
lower_d = clf_deaths.predict(day_arr_fine)

clf_confirmed.set_params(loss = 'ls')
clf_deaths.set_params(loss = 'ls')
clf_confirmed.fit(X_confirmed, y_confirmed)
clf_deaths.fit(X_deaths, y_deaths)

confirmed_pred = clf_confirmed.predict(day_arr_fine)
deaths_pred = clf_deaths.predict(day_arr_fine)

print(len(X_confirmed))

fig = plt.figure()
plt.plot(X_confirmed, y_confirmed, 'b.', markersize = 10, label = u'Observations')
plt.plot(day_arr_fine, confirmed_pred, 'r-', label = u'Prediction')
plt.plot(day_arr_fine, lower, 'k-')
plt.plot(day_arr_fine, upper, 'k-')
plt.fill(np.concatenate([day_arr_fine, day_arr_fine[::-1]]), np.concatenate([upper, lower[::-1]]),
        alpha = .5, fc = 'b', ec = 'None', label = '95% Prediction Interval')
plt.xlabel('Days')
plt.ylabel('Confirmed Cases')
plt.title('Confirmed Case Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()

fig = plt.figure()
plt.plot(X_deaths, y_deaths, 'b.', markersize = 10, label = u'Observations')
plt.plot(day_arr_fine, deaths_pred, 'r-', label = u'Prediction')
plt.plot(day_arr_fine, lower_d, 'k-')
plt.plot(day_arr_fine, upper_d, 'k-')
plt.fill(np.concatenate([day_arr_fine, day_arr_fine[::-1]]), np.concatenate([upper_d, lower_d[::-1]]), alpha = .5, fc = 'b', ec = 'None', label = '95% Prediction Interval')
plt.xlabel('Days')
plt.ylabel('Deaths')
plt.title('Death Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()

for i in range(len(days_for_pred)):
    print(i)
    # Edit days here
    if i == 1:
        target_end_date = '2020-07-18'
    if i == 2:
        target_end_date = '2020-07-25'
    if i == 3:
        target_end_date = '2020-08-01'

    for quant in quantile_list:
        clf_confirmed.set_params(loss = 'quantile')
        clf_deaths.set_params(loss = 'quantile')
        clf_confirmed.set_params(alpha = quant)
        clf_deaths.set_params(alpha = quant)
        clf_confirmed.fit(X_confirmed, y_confirmed)
        clf_deaths.fit(X_deaths, y_deaths)
        # This day-setting is intentional; it sets you back to today
        #pred_day = len(confirmedCases[minRange:]) + days_for_pred[i]
        pred_day = np.asarray(len(confirmedCases[minRange:]) + days_for_pred[i] - 1)
        pred_day = pred_day.reshape(1,-1)
        case_pred = clf_confirmed.predict(pred_day)
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum cases'.format(week = i+1), target_end_date, location, 'quantile', quant, case_pred]], columns = submission_frame.columns))
        deaths_pred = clf_deaths.predict(pred_day)
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'quantile', quant, deaths_pred]], columns = submission_frame.columns))

        if i == 0:
            deaths_inc = deaths_pred - deaths_arr_const[-1]
        else:
            deaths_inc = deaths_pred - death_count_list[-1]
        if deaths_inc < 0:
            deaths_inc = 0
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'quantile', quant, deaths_inc]], columns = submission_frame.columns))


        if quant in quantile_list_case_inc:
            if i == 0:
                case_inc = (case_pred - confirmedCases[-1])
            else:
                case_inc = (case_pred - case_count_list[-1])
            if case_inc < 0:
                case_inc = 0
            submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc cases'.format(week = i+1), target_end_date, location, 'quantile', quant, case_inc]], columns = submission_frame.columns))

    clf_confirmed.set_params(loss = 'ls')
    clf_deaths.set_params(loss = 'ls')
    clf_confirmed.fit(X_confirmed, y_confirmed)
    clf_deaths.fit(X_deaths, y_deaths)
    # Again, intentional
    #pred_day = len(confirmedCases[minRange:]) + days_for_pred[i]
    pred_day = np.asarray(len(confirmedCases[minRange:]) + days_for_pred[i] - 1)
    pred_day = pred_day.reshape(1,-1)
    case_pred = clf_confirmed.predict(pred_day)
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum cases'.format(week = i+1), target_end_date, location, 'point', 'NA', case_pred]], columns = submission_frame.columns))

    deaths_pred = clf_deaths.predict(pred_day)
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'point', 'NA', deaths_pred]], columns = submission_frame.columns))

    if i == 0:
        case_inc = (case_pred - confirmedCases[-1])
    else:
        case_inc = (case_pred - case_count_list[-1])
    if case_inc < 0:
        case_inc = 0
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc cases'.format(week = i+1), target_end_date, location, 'point', 'NA', case_inc]], columns = submission_frame.columns))
    
    if i == 0:
        deaths_inc = (deaths_pred - deaths_arr_const[-1])
    else:
        deaths_inc = (deaths_pred - death_count_list[-1])
    if deaths_inc < 0:
        deaths_inc = 0
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'point', 'NA', deaths_inc]], columns = submission_frame.columns))

    case_count_list.append(case_pred)
    death_count_list.append(deaths_pred)

# Edit days here
submission_frame.to_csv('2020-07-03-UM-ridgequantreg.csv')

##############################################################
#                                                            #
#                     Estimate Milestones                    #
#                                                            #
##############################################################
'''
# Estimate peak
I_prev = 0
I_now = 0
I_next = 0

peak_date = 0
peak_cases = 0
safe_date = 0
found = False

for i in range(1,len(I_pred)-2):
    if i == 1:
        I_now = I_pred[i]
        I_next = I_pred[i+1]

    else:
        I_prev = I_pred[i-1]
        I_now = I_pred[i]
        I_next = I_pred[i+1]

    if I_now > I_prev and I_now > I_next:
        peak_date = i
        peak_cases = I_now

    if I_now < 1000 and I_next < I_now and found == False:
        safe_date = i
        found = True

print('\nEstimated Peak Date from Start: ' + str(peak_date) + '\nEstimated Peak Infections: ' + str(int(peak_cases)) + '\nEstimated Safe Date: ' + str(safe_date))
'''
