#import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import os
import sys
import operator
import iminuit
import scipy
import matplotlib.pyplot as plt
#import tensorflow.compat.v1 as tf
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
#from pygrok import Grok
#from models import SEIR
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.metrics import r2_score, mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
#from matplotlib.ticker import MultipleLocator
#import logging
#tf.disable_v2_behavior()
#tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

location = sys.argv[1]
countryOrState = sys.argv[2]

# Split location name arg
if location == 'NewHampshire':
        location = 'New Hampshire'
elif location == 'NewJersey':
        location = 'New Jersey'
elif location == 'NewMexico':
        location = 'New Mexico'
elif location == 'NewYork':
        location = 'New York'
elif location == 'NorthCarolina':
        location = 'North Carolina'
elif location == 'NorthDakota':
        location = 'North Dakota'
elif location == 'RhodeIsland':
        location = 'Rhode Island'
elif location == 'SouthCarolina':
        location = 'South Carolina'
elif location == 'SouthDakota':
        location = 'South Dakota'
elif location == 'WestVirginia':
        location = 'West Virginia'

print("The location is:", location)
print("This is a country/province:", countryOrState)
if countryOrState.lower() == 'province':
    countryOrState = 0
elif countryOrState.lower() == 'country':
    countryOrState = 1
print(countryOrState)
# Save in separate directories
if not os.path.exists(location):
    os.makedirs(location)


###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Initialization
dates = []
confirmedCases = []
deaths = []
recovered = []
active = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
#grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
        if (location == 'South Korea') and file == (path + '03-10-2020.csv'):
            location = 'Republic of Korea'
        if (location == 'Republic of Korea') and file == (path + '03-11-2020.csv'):
            location = 'Korea, South'

        month = file[-14:-12]
        day = file[-11:-9]
        day1 = day
        year = file[-8:-4]
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Break when data reaches previous Sunday
        today = calendar.today()
        idx = (today.weekday() + 1) % 7
        sun = today - timedelta(idx)
        sun = '{:%m/%d/%Y}'.format(sun)
        this_file = '{m}/{d}/{Y}'.format(m = month, d = day1, Y = year)
        if (this_file == sun):
            print('Stopping file this past Sunday: ', str(sun))
            break
        #print('The loop is obedient')

	# Fill dates from the file into an array
        day = month+'-'+day
        dates.append(day)   

        # 'if' statement to deal with Country or State and JHU data format change
        areaLabel = 'Country/Region'
        if countryOrState == 0:
                areaLabel = 'Province/State'
        if (int(month) == 3 and int(day1) > 21 and int(year) > 2019) or (int(month) > 3 and int(year) > 2019):
            areaLabel = 'Country_Region'
            if countryOrState == 0:
                    areaLabel = 'Province_State'

        # Grab Confirmed and Deaths
        confirmedCases.append(0)
        deaths.append(0.)
        recovered.append(0.)
        for index, row in data.iterrows():
                if row[areaLabel] == location:
                        if str(row['Confirmed']) != 'nan':
                                confirmedCases[dayCount] = confirmedCases[dayCount] + row['Confirmed']
                        if str(row['Deaths']) != 'nan':
                                deaths[dayCount] = deaths[dayCount] + row['Deaths']
                        if str(row['Recovered']) != 'nan':
                                recovered[dayCount] = recovered[dayCount] + row['Recovered']

        dayCount = dayCount + 1

# print(max(confirmedCases))
# print confirmedCases
if location == 'Korea, South':
    location = 'South Korea'

###############################################################
#                                                             #
#            Make and view all-encompassing plot              #
#                                                             #
###############################################################

deathRate = np.divide(deaths, confirmedCases)

# Find the first day of infection and create a grid
day = 0
for cases in confirmedCases:
    if max(confirmedCases) < 2500:
        threshold = 0

    elif max(confirmedCases) < 100000:
        threshold = .005 * max(confirmedCases)

    else:
        threshold = 1000

    if cases > threshold:
        first_day = day
        print ("\nFirst day of count: " + str(first_day))
        inf_init = cases
        print ("Initial case count: " + str(cases) + "\n")
        break
    
    day += 1

minRange = first_day

float_dates = []
day_count = 0
for date in dates[minRange:]:
    float_dates.append( day_count )
    day_count = day_count + 1                                                                     

active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]
# Total Plots

if not os.path.exists(location):
    os.makedirs(location)

#plt.savefig(location + '/' + location + '.pdf')
#plt.show()

##############################################################
#                                                            #
#                  Perform Ridge Regression                  #
#                                                            #
##############################################################

# Pick daily or cumulative data for model
dailyOrCumulative = sys.argv[3] if len(sys.argv) > 3 else 'cumulative'
print("The model is daily/cumulative:", dailyOrCumulative)
if dailyOrCumulative.lower() == 'daily':
    dailyOrCumulative = 1
    def cumulativeToDaily(cumulative):
        daily = []
        # one less data point for the differences
        for i in range(len(cumulative) - 1):
            if not i == 0:
                daily.append(cumulative[i] - cumulative[i-1])
        return daily

    def dailyToCumulative(daily, start_val):
        cumulative = [0]
        # one more data point for cumulative
        for i in range(len(daily) + 1):
            if i == 0:
                cumulative[i] = start_val
            else:
                cumulative.append(cumulative[i-1] + daily[i-1])
        return cumulative

elif dailyOrCumulative.lower() == 'weekly':
    dailyOrCumulative = 2

    def cumulativeToWeeklyCumulative(cumulative):
        excess = len(cumulative) % 7
        cumulative = cumulative[excess:]
        tot_weeks = int(len(cumulative)/7)
        weekly = []
        for i in range(tot_weeks):
            weekly.append(cumulative[7*i])
        return weekly

    # Get *new* weekly stats
    def cumulativeToWeekly(cumulative):
        weekly = []
        for i in range(len(cumulative) - 1):
            if not i == 0:
                weekly.append(cumulative[i] - cumulative[i-1])
        return weekly

    def weeklyToCumulative(weekly, start_val):
        cumulative = [0]
        for i in range(len(weekly) + 1):
            if i == 0:
                cumulative[i] = start_val
            else:
                cumulative.append(cumulative[i-1] + weekly[i-1])
        return cumulative

    active = cumulativeToWeeklyCumulative(active)
    r_sum = [recovered[i] + deaths[i] for i in range(len(recovered))]
    r_sum_cut = r_sum[minRange:]
    r_sum = cumulativeToWeeklyCumulative(r_sum)
    r_sum_cut = cumulativeToWeeklyCumulative(r_sum_cut)
    deaths = deaths[minRange:]
    deaths_arr_checker = deaths[:]
    deaths = cumulativeToWeeklyCumulative(deaths)
    deaths_arr_checker = cumulativeToWeeklyCumulative(deaths_arr_checker)
    confirmed = confirmedCases[minRange:]
    confirmed_arr_checker = confirmed[:]
    confirmed = cumulativeToWeeklyCumulative(confirmed)
    confirmed_arr_checker = cumulativeToWeeklyCumulative(confirmed_arr_checker)

else:
    dailyOrCumulative = 0

active_arr_checker = active[:]
print(len(active_arr_checker))

if not dailyOrCumulative == 2:
 
    r_sum = [recovered[i] + deaths[i] for i in range(len(recovered))]
    r_sum_cut = r_sum[minRange:]

    deaths = deaths[minRange:]
    deaths_arr_checker = deaths[:]

    confirmed = confirmedCases[minRange:]
    confirmed_arr_checker = confirmed[:]

recovered_arr_checker = r_sum_cut[:]

# Check to see if dividing by 0 anywhere
has_zeros = False
zero_loc = []
for i in range(len(active)):
    if active[i] == 0:
        has_zeros = True
        zero_loc.append(i)

active_to_plot = active
recovered_to_plot = r_sum_cut
deaths_to_plot = deaths
confirmed_to_plot = confirmed

active_pred = active
recovered_pred = r_sum_cut
deaths_pred = deaths
confirmed_pred = confirmed

# Optional: impose limit on the amount of data considered
frac = 1.

stop_pt = int(len(active)*frac)
active_pred = active_pred[:stop_pt]
recovered_pred = recovered_pred[:stop_pt]
confirmed_pred = confirmed_pred[:stop_pt]
deaths_pred = deaths_pred[:stop_pt]
active = active[:stop_pt]
r_sum_cut = r_sum_cut[:stop_pt]
deaths = deaths[:stop_pt]
confirmed = confirmed[:stop_pt]

# Create daily active, recovered, deaths, and confirmed for use in daily change model
if dailyOrCumulative == 1:
    daily_active = cumulativeToDaily(active)
    daily_active_pred = cumulativeToDaily(active_pred)
    daily_recovered = cumulativeToDaily(r_sum_cut)
    daily_recovered_pred = cumulativeToDaily(recovered_pred)
    daily_confirmed = cumulativeToDaily(confirmed)
    daily_confirmed_pred = cumulativeToDaily(confirmed_pred)
    daily_deaths = cumulativeToDaily(deaths)
    daily_deaths_pred = cumulativeToDaily(deaths_pred)

if dailyOrCumulative == 2:
    weekly_active = cumulativeToWeekly(active)
    weekly_active_pred = cumulativeToWeekly(active_pred)
    weekly_recovered = cumulativeToWeekly(r_sum_cut)
    weekly_recovered_pred = cumulativeToWeekly(recovered_pred)
    weekly_confirmed = cumulativeToWeekly(confirmed)
    weekly_confirmed_pred = cumulativeToWeekly(confirmed_pred)
    weekly_deaths = cumulativeToWeekly(deaths)
    weekly_deaths_pred = cumulativeToWeekly(deaths_pred)

# Cut off signal to match sums (Eqn. 15 and 16)
order = 4
if not dailyOrCumulative == 2:
    order = 22
print('order ', order)
active_for_ridge = active[order:]
recovered_for_ridge = r_sum_cut[order:]
deaths_for_ridge = deaths[order:]
confirmed_for_ridge = confirmed[order:]

# Backtrack to create X matrices in sklearn notation (dimensions len(beta_for_ridge) x order) for both daily and cumulative
if dailyOrCumulative == 1:
    daily_active_pred_matrix = []
    daily_recovered_pred_matrix = []
    daily_deaths_pred_matrix = []
    daily_confirmed_pred_matrix = []
    for i in range(len(daily_active) - order):
        new_row_daily_active = [1]
        new_row_daily_recovered = [1]
        new_row_daily_deaths = [1]
        new_row_daily_confirmed = [1]
        for j in range(order):
            new_row_daily_active.append(daily_active[i+j])
            new_row_daily_recovered.append(daily_recovered[i+j])
            new_row_daily_deaths.append(daily_deaths[i+j])
            new_row_daily_confirmed.append(daily_confirmed[i+j])
        daily_active_pred_matrix.append(new_row_daily_active)
        daily_recovered_pred_matrix.append(new_row_daily_recovered)
        daily_deaths_pred_matrix.append(new_row_daily_deaths)
        daily_confirmed_pred_matrix.append(new_row_daily_confirmed)

if dailyOrCumulative == 2:
    weekly_active_pred_matrix = []
    weekly_recovered_pred_matrix = []
    weekly_deaths_pred_matrix = []
    weekly_confirmed_pred_matrix = []
    for i in range(len(weekly_active) - order):
        new_row_weekly_active = [1]
        new_row_weekly_recovered = [1]
        new_row_weekly_deaths = [1]
        new_row_weekly_confirmed = [1]
        for j in range(order):
            new_row_weekly_active.append(weekly_active[i+j])
            new_row_weekly_recovered.append(weekly_recovered[i+j])
            new_row_weekly_deaths.append(weekly_deaths[i+j])
            new_row_weekly_confirmed.append(weekly_confirmed[i+j])
        weekly_active_pred_matrix.append(new_row_weekly_active)
        weekly_recovered_pred_matrix.append(new_row_weekly_recovered)
        weekly_deaths_pred_matrix.append(new_row_weekly_deaths)
        weekly_confirmed_pred_matrix.append(new_row_weekly_confirmed)


elif dailyOrCumulative == 0:
    active_pred_matrix = []
    recovered_pred_matrix = []
    deaths_pred_matrix = []
    confirmed_pred_matrix = []
    for i in range(len(active) - order):
        new_row_active = [1]
        new_row_recovered = [1]
        new_row_deaths = [1]
        new_row_confirmed = [1]
        for j in range(order):
            new_row_active.append(active[i+j])
            new_row_recovered.append(r_sum_cut[i+j])
            new_row_deaths.append(deaths[i+j])
            new_row_confirmed.append(confirmed[i+j])
        active_pred_matrix.append(new_row_active)
        recovered_pred_matrix.append(new_row_recovered)
        deaths_pred_matrix.append(new_row_deaths)
        confirmed_pred_matrix.append(new_row_confirmed)

# Remove sub-0 data from consideration
for i in range(len(active_for_ridge)):
    this_active = active_for_ridge[i]
    this_recovered = recovered_for_ridge[i]
    this_deaths = deaths_for_ridge[i]
    this_confirmed = confirmed_for_ridge[i]

    if this_active < 0:
        active_for_ridge[i] = 0
    if this_recovered < 0:
        recovered_for_ridge[i] = 0
    if this_deaths < 0:
        deaths_for_ridge[i] = 0
    if this_confirmed < 0:
        confirmed_for_ridge[i] = 0

# Create active, recovered, deaths, and confirmed for ridge for daily model
if dailyOrCumulative == 1:
    daily_active_for_ridge = cumulativeToDaily(active_for_ridge)
    daily_recovered_for_ridge = cumulativeToDaily(recovered_for_ridge)
    daily_deaths_for_ridge = cumulativeToDaily(deaths_for_ridge)
    daily_confirmed_for_ridge = cumulativeToDaily(confirmed_for_ridge)

if dailyOrCumulative == 2:
    weekly_active_for_ridge = cumulativeToWeekly(active_for_ridge)
    weekly_recovered_for_ridge = cumulativeToWeekly(recovered_for_ridge)
    weekly_deaths_for_ridge = cumulativeToWeekly(deaths_for_ridge)
    weekly_confirmed_for_ridge = cumulativeToWeekly(confirmed_for_ridge)

# Make constant values to revert to for both daily and cumulative model
if dailyOrCumulative == 1:
    daily_active_arr_const = daily_active[:]
    daily_active_for_ridge_const = daily_active_for_ridge[:]
    daily_recovered_arr_const = daily_recovered[:]
    daily_recovered_for_ridge_const = daily_recovered_for_ridge[:]
    daily_deaths_arr_const = daily_deaths[:]
    daily_deaths_for_ridge_const = daily_deaths_for_ridge[:]
    daily_confirmed_arr_const = daily_confirmed[:]
    daily_confirmed_for_ridge_const = daily_confirmed_for_ridge[:]
    # The daily model also needs to have cumulative values to revert to
    active_arr_const = active[:]
    active_for_ridge_const = active_for_ridge[:]
    recovered_arr_const = r_sum_cut[:]
    recovered_for_ridge_const = recovered_for_ridge[:]
    deaths_arr_const = deaths[:]
    deaths_for_ridge_const = deaths_for_ridge[:]
    confirmed_arr_const = confirmed[:]
    confirmed_for_ridge_const = confirmed_for_ridge[:]

if dailyOrCumulative == 2:
    weekly_active_arr_const = weekly_active[:]
    weekly_active_for_ridge_const = weekly_active_for_ridge[:]
    weekly_recovered_arr_const = weekly_recovered[:]
    weekly_recovered_for_ridge_const = weekly_recovered_for_ridge[:]
    weekly_deaths_arr_const = weekly_deaths[:]
    weekly_deaths_for_ridge_const = weekly_deaths_for_ridge[:]
    weekly_confirmed_arr_const = weekly_confirmed[:]
    weekly_confirmed_for_ridge_const = weekly_confirmed_for_ridge[:]
    # The daily model also needs to have cumulative values to revert to
    active_arr_const = active[:]
    active_for_ridge_const = active_for_ridge[:]
    recovered_arr_const = r_sum_cut[:]
    recovered_for_ridge_const = recovered_for_ridge[:]
    deaths_arr_const = deaths[:]
    deaths_for_ridge_const = deaths_for_ridge[:]
    confirmed_arr_const = confirmed[:]
    confirmed_for_ridge_const = confirmed_for_ridge[:]

elif dailyOrCumulative == 0:
    active_arr_const = active[:]
    active_for_ridge_const = active_for_ridge[:]
    recovered_arr_const = r_sum_cut[:]
    recovered_for_ridge_const = recovered_for_ridge[:]
    deaths_arr_const = deaths[:]
    deaths_for_ridge_const = deaths_for_ridge[:]
    confirmed_arr_const = confirmed[:]
    confirmed_for_ridge_const = confirmed_for_ridge[:]
    #infected_pred_const = infected_pred[:]
    #recovered_pred_const = recovered_pred[:]

# Choose alphas to try
# Was 25 steps
coeff_list = [1., 5.]
coeff = 1.
alpha_list = []
i = -3
while i < 12:
    for j in range(len(coeff_list)):
        coeff = coeff_list[j]
        alpha_list.append(coeff * (10**(-i)))
    i += 1

parameters = {'alpha': alpha_list}
#print(len(alpha_list))

# Creating optimal value placeholders
min_least_total_active = 1e9
min_least_total_recovered = 1e9
min_least_total_deaths = 1e9
min_least_total_confirmed = 1e9
#min_least_total_inf = 1e9
best_alpha_active = 0
best_alpha_recovered = 0
best_alpha_deaths = 0
best_alpha_confirmed = 0
best_active_model = []
best_recovered_model = []
best_deaths_model = []
best_confirmed_model = []
#best_infected_pred = []

confirmed_model_list = []
deaths_model_list = []

#print("k = " + str(k) + " j = " + str(j))
r_active = Ridge(normalize = True)
r_recovered = Ridge(normalize = True)
r_deaths = Ridge(normalize = True)
r_confirmed = Ridge(normalize = True)
ridge_active_cv = GridSearchCV(r_active, parameters, scoring = 'neg_mean_squared_error', cv = 10)
ridge_recovered_cv = GridSearchCV(r_recovered, parameters, scoring = 'neg_mean_squared_error', cv = 10)
ridge_deaths_cv = GridSearchCV(r_deaths, parameters, scoring = 'neg_mean_squared_error', cv = 10)
ridge_confirmed_cv = GridSearchCV(r_confirmed, parameters, scoring = 'neg_mean_squared_error', cv = 10)

#Fitting for both daily and cumulative
if dailyOrCumulative == 1:
    ridge_active_cv.fit(daily_active_pred_matrix, daily_active_for_ridge)
    ridge_recovered_cv.fit(daily_recovered_pred_matrix, daily_recovered_for_ridge)
    ridge_deaths_cv.fit(daily_deaths_pred_matrix, daily_deaths_for_ridge)
    ridge_confirmed_cv.fit(daily_confirmed_pred_matrix, daily_confirmed_for_ridge)

elif dailyOrCumulative == 2:
    ridge_active_cv.fit(weekly_active_pred_matrix, weekly_active_for_ridge)
    ridge_recovered_cv.fit(weekly_recovered_pred_matrix, weekly_recovered_for_ridge)
    ridge_deaths_cv.fit(weekly_deaths_pred_matrix, weekly_deaths_for_ridge)
    ridge_confirmed_cv.fit(weekly_confirmed_pred_matrix, weekly_confirmed_for_ridge)

elif dailyOrCumulative == 0:
    ridge_active_cv.fit(active_pred_matrix, active_for_ridge)
    ridge_recovered_cv.fit(recovered_pred_matrix, recovered_for_ridge)
    ridge_deaths_cv.fit(deaths_pred_matrix, deaths_for_ridge)
    ridge_confirmed_cv.fit(confirmed_pred_matrix, confirmed_for_ridge)

# Retrieve best alpha
alpha_mid_active = ridge_active_cv.best_params_['alpha']
alpha_mid_recovered = ridge_recovered_cv.best_params_['alpha']
alpha_mid_deaths = ridge_deaths_cv.best_params_['alpha']
alpha_mid_confirmed = ridge_confirmed_cv.best_params_['alpha']

# Retrieve best R^2
score_active = ridge_active_cv.best_score_
score_recovered = ridge_recovered_cv.best_score_
score_deaths = ridge_deaths_cv.best_score_
score_confirmed = ridge_confirmed_cv.best_score_

print(str(alpha_mid_active) + ' ' + str(alpha_mid_recovered) + ' ' + str(alpha_mid_deaths) + ' ' + str(alpha_mid_confirmed))
print('Score Active: ' + str(score_active))
print('Score Recovered: ' + str(score_recovered))
print('Score Deaths: ' + str(score_deaths))
print('Score Confirmed: ' + str(score_confirmed))

# Recreate alpha lists based on baseline
coeff_list = [1., 2.5, 5., 7.5, 9.]
coeff = 1.
alpha_list_active = []
alpha_list_recovered = []
alpha_list_deaths = []
alpha_list_confirmed = []
i = -1
while i < 1:
    for j in range(len(coeff_list)):
        coeff = coeff_list[j]
        alpha_list_active.append(coeff * (10**(-i)) * alpha_mid_active)
        alpha_list_recovered.append(coeff * (10**(-i)) * alpha_mid_recovered)
        alpha_list_deaths.append(coeff * (10**(-i)) * alpha_mid_deaths)
        alpha_list_confirmed.append(coeff * (10**(-i)) * alpha_mid_confirmed)
        
    i += 1

k = 0
# Loop over alpha pairs
while k < len(alpha_list_active):
    #print(k)

    # Reset to original prepped values for both daily and cumulative
    if dailyOrCumulative == 1:
        # Reset daily active, recovered, death, and confirmed values
        daily_active = daily_active_arr_const[:]
        daily_active_for_ridge = daily_active_for_ridge_const[:]
        daily_recovered = daily_recovered_arr_const[:]
        daily_recovered_for_ridge = daily_recovered_for_ridge_const[:]
        daily_deaths = daily_deaths_arr_const[:]
        daily_deaths_for_ridge = daily_deaths_for_ridge_const[:]
        daily_confirmed = daily_confirmed_arr_const[:]
        daily_confirmed_for_ridge = daily_confirmed_for_ridge_const[:]
        # Reset cumulative active, recovered, death, and confirmed values
        active = active_arr_const[:]
        active_for_ridge = active_for_ridge_const[:]
        r_sum_cut = recovered_arr_const[:]
        recovered_for_ridge = recovered_for_ridge_const[:]
        deaths = deaths_arr_const[:]
        deaths_for_ridge = deaths_for_ridge_const[:]
        confirmed = confirmed_arr_const[:]
        confirmed_for_ridge = confirmed_for_ridge_const[:]
    
    if dailyOrCumulative == 2:
        # Reset daily active, recovered, death, and confirmed values
        weekly_active = weekly_active_arr_const[:]
        weekly_active_for_ridge = weekly_active_for_ridge_const[:]
        weekly_recovered = weekly_recovered_arr_const[:]
        weekly_recovered_for_ridge = weekly_recovered_for_ridge_const[:]
        weekly_deaths = weekly_deaths_arr_const[:]
        weekly_deaths_for_ridge = weekly_deaths_for_ridge_const[:]
        weekly_confirmed = weekly_confirmed_arr_const[:]
        weekly_confirmed_for_ridge = weekly_confirmed_for_ridge_const[:]
        # Reset cumulative active, recovered, death, and confirmed values
        active = active_arr_const[:]
        active_for_ridge = active_for_ridge_const[:]
        r_sum_cut = recovered_arr_const[:]
        recovered_for_ridge = recovered_for_ridge_const[:]
        deaths = deaths_arr_const[:]
        deaths_for_ridge = deaths_for_ridge_const[:]
        confirmed = confirmed_arr_const[:]
        confirmed_for_ridge = confirmed_for_ridge_const[:]
    
    elif dailyOrCumulative == 0:
        active = active_arr_const[:]
        active_for_ridge = active_for_ridge_const[:]
        r_sum_cut = recovered_arr_const[:]
        recovered_for_ridge = recovered_for_ridge_const[:]
        deaths = deaths_arr_const[:]
        deaths_for_ridge = deaths_for_ridge_const[:]
        confirmed = confirmed_arr_const[:]
        confirmed_for_ridge = confirmed_for_ridge_const[:]
        #infected_pred = infected_pred_const[:]
        #recovered_pred = recovered_pred_const[:]

    #print("k = " + str(k) + " j = " + str(j))
    ridge_active = Ridge(alpha = alpha_list_active[k], normalize = True)
    ridge_recovered = Ridge(alpha = alpha_list_recovered[k], normalize = True)
    ridge_deaths = Ridge(alpha = alpha_list_deaths[k], normalize = True)
    ridge_confirmed = Ridge(alpha = alpha_list_confirmed[k], normalize = True)

    # Fit beta and gamma to corresponding matrices for both daily and cumulative
    # Return "w" coefficient vectors (in sklearn notation; a and b vectors in paper)
    if dailyOrCumulative == 1:
        ridge_active.fit(daily_active_pred_matrix, daily_active_for_ridge)
        ridge_recovered.fit(daily_recovered_pred_matrix, daily_recovered_for_ridge)
        ridge_deaths.fit(daily_deaths_pred_matrix, daily_deaths_for_ridge)
        ridge_confirmed.fit(daily_confirmed_pred_matrix, daily_confirmed_for_ridge)

        daily_active_model = ridge_active.predict(daily_active_pred_matrix)
        daily_recovered_model = ridge_recovered.predict(daily_recovered_pred_matrix)
        daily_deaths_model = ridge_deaths.predict(daily_deaths_pred_matrix)
        daily_confirmed_model = ridge_confirmed.predict(daily_confirmed_pred_matrix)

        daily_active_model = daily_active_model.tolist()
        daily_recovered_model = daily_recovered_model.tolist()
        daily_deaths_model = daily_deaths_model.tolist()
        daily_confirmed_model = daily_confirmed_model.tolist()
    
    if dailyOrCumulative == 2:
        ridge_active.fit(weekly_active_pred_matrix, weekly_active_for_ridge)
        ridge_recovered.fit(weekly_recovered_pred_matrix, weekly_recovered_for_ridge)
        ridge_deaths.fit(weekly_deaths_pred_matrix, weekly_deaths_for_ridge)
        ridge_confirmed.fit(weekly_confirmed_pred_matrix, weekly_confirmed_for_ridge)

        weekly_active_model = ridge_active.predict(weekly_active_pred_matrix)
        weekly_recovered_model = ridge_recovered.predict(weekly_recovered_pred_matrix)
        weekly_deaths_model = ridge_deaths.predict(weekly_deaths_pred_matrix)
        weekly_confirmed_model = ridge_confirmed.predict(weekly_confirmed_pred_matrix)

        weekly_active_model = weekly_active_model.tolist()
        weekly_recovered_model = weekly_recovered_model.tolist()
        weekly_deaths_model = weekly_deaths_model.tolist()
        weekly_confirmed_model = weekly_confirmed_model.tolist()
    
    elif dailyOrCumulative == 0:
        ridge_active.fit(active_pred_matrix, active_for_ridge)
        ridge_recovered.fit(recovered_pred_matrix, recovered_for_ridge)
        ridge_deaths.fit(deaths_pred_matrix, deaths_for_ridge)
        ridge_confirmed.fit(confirmed_pred_matrix, confirmed_for_ridge)

        active_model = ridge_active.predict(active_pred_matrix)
        recovered_model = ridge_recovered.predict(recovered_pred_matrix)
        deaths_model = ridge_deaths.predict(deaths_pred_matrix)
        confirmed_model = ridge_confirmed.predict(confirmed_pred_matrix)

        active_model = active_model.tolist()
        recovered_model = recovered_model.tolist()
        deaths_model = deaths_model.tolist()
        confirmed_model = confirmed_model.tolist()

    # Insert values for plot matching later for both daily and cumulative
    if dailyOrCumulative == 1:
        for i in range(0, order):
            daily_active_model.insert(i, daily_active[i])
            daily_recovered_model.insert(i, daily_recovered[i])
            daily_deaths_model.insert(i, daily_deaths[i])
            daily_confirmed_model.insert(i, daily_confirmed[i])
    
    if dailyOrCumulative == 2:
        for i in range(0, order):
            weekly_active_model.insert(i, weekly_active[i])
            weekly_recovered_model.insert(i, weekly_recovered[i])
            weekly_deaths_model.insert(i, weekly_deaths[i])
            weekly_confirmed_model.insert(i, weekly_confirmed[i])

    elif dailyOrCumulative == 0:
        for i in range(0, order):
            active_model.insert(i, active[i])
            recovered_model.insert(i, r_sum_cut[i])
            deaths_model.insert(i, deaths[i])
            confirmed_model.insert(i, confirmed[i])

    # Predict next beta and gamma for both daily and cumulative
    if dailyOrCumulative == 1:
        #lose one entry when looking at differences
        stop = int(len(active_arr_checker)) + 40 - 1
        start = len(active) - 1
    
    if dailyOrCumulative == 2:
        #lose one entry when looking at differences
        stop = int(len(active_arr_checker)) + 40 - 1
        start = len(active) - 1
    
    elif dailyOrCumulative ==0:
        stop = int(len(active_arr_checker)) + 40
        start = len(active)
    t = start
    while t < stop:
        sample_active = [1]
        sample_recovered = [1]
        sample_deaths = [1]
        sample_confirmed = [1]
        i = -order
        while i < 0:
            if dailyOrCumulative == 1:
                sample_active.append(daily_active_for_ridge[i])
                sample_recovered.append(daily_recovered_for_ridge[i])
                sample_deaths.append(daily_deaths_for_ridge[i])
                sample_confirmed.append(daily_confirmed_for_ridge[i])
            if dailyOrCumulative == 2:
                sample_active.append(weekly_active_for_ridge[i])
                sample_recovered.append(weekly_recovered_for_ridge[i])
                sample_deaths.append(weekly_deaths_for_ridge[i])
                sample_confirmed.append(weekly_confirmed_for_ridge[i])
            elif dailyOrCumulative == 0:
                sample_active.append(active_for_ridge[i])
                sample_recovered.append(recovered_for_ridge[i])
                sample_deaths.append(deaths_for_ridge[i])
                sample_confirmed.append(confirmed_for_ridge[i])
            i += 1

        # Reshape for prediction (needs 2D array)
        sample_active = np.asarray(sample_active)
        sample_recovered = np.asarray(sample_recovered)
        sample_deaths = np.asarray(sample_deaths)
        sample_confirmed = np.asarray(sample_confirmed)
        sample_active = sample_active.reshape(1,-1)
        sample_recovered = sample_recovered.reshape(1,-1)
        sample_deaths = sample_deaths.reshape(1,-1)
        sample_confirmed = sample_confirmed.reshape(1,-1)

        next_active = ridge_active.predict(sample_active)
        next_recovered = ridge_recovered.predict(sample_recovered)
        next_deaths = ridge_deaths.predict(sample_deaths)
        next_confirmed = ridge_confirmed.predict(sample_confirmed)

        # Append predictions to model
        if dailyOrCumulative == 1:
            daily_active_for_ridge.append(next_active)
            daily_active.append(next_active)
            daily_active_model.append(next_active)

            daily_recovered_for_ridge.append(next_recovered)
            daily_recovered.append(next_recovered)
            daily_recovered_model.append(next_recovered)

            daily_deaths_for_ridge.append(next_deaths)
            daily_deaths.append(next_deaths)
            daily_deaths_model.append(next_deaths)

            daily_confirmed_for_ridge.append(next_confirmed)
            daily_confirmed.append(next_confirmed)
            daily_confirmed_model.append(next_confirmed)
        
        if dailyOrCumulative == 2:
            weekly_active_for_ridge.append(next_active)
            weekly_active.append(next_active)
            weekly_active_model.append(next_active)

            weekly_recovered_for_ridge.append(next_recovered)
            weekly_recovered.append(next_recovered)
            weekly_recovered_model.append(next_recovered)

            weekly_deaths_for_ridge.append(next_deaths)
            weekly_deaths.append(next_deaths)
            weekly_deaths_model.append(next_deaths)

            weekly_confirmed_for_ridge.append(next_confirmed)
            weekly_confirmed.append(next_confirmed)
            weekly_confirmed_model.append(next_confirmed)

        # Remove negative values from cumulative model and append
        elif dailyOrCumulative == 0:
            if next_active < 0:
                active_for_ridge.append(0)
                active.append(0)
                active_model.append(0)
            else:
                active_for_ridge.append(next_active)
                active.append(next_active)
                active_model.append(next_active)

            if next_recovered < 0:
                recovered_for_ridge.append(0)
                r_sum_cut.append(0)
                recovered_model.append(0)
            else:
                recovered_for_ridge.append(next_recovered)
                r_sum_cut.append(next_recovered)
                recovered_model.append(next_recovered)

            if next_deaths < 0:
                deaths_for_ridge.append(0)
                deaths.append(0)
                deaths_model.append(0)

            else:
                deaths_for_ridge.append(next_deaths)
                deaths.append(next_deaths)
                deaths_model.append(next_deaths)

            if next_confirmed < 0:
                confirmed_for_ridge.append(0)
                confirmed.append(0)
                confirmed_model.append(0)

            else:
                confirmed_for_ridge.append(next_confirmed)
                confirmed.append(next_confirmed)
                confirmed_model.append(next_confirmed)


        # Predict infected and recovered:
        #infected_pred.append((1 + beta_for_ridge[-1] - gamma_for_ridge[-1])*infected_pred[-1])
        #recovered_pred.append(recovered_pred[-1] + gamma_for_ridge[-1] * infected_pred[-2])
        t += 1


    # Transform daily model back into cumulative for error and plotting
    if dailyOrCumulative == 1:
        active_for_ridge = dailyToCumulative(daily_active_for_ridge, active_for_ridge[0])
        active = dailyToCumulative(daily_active, active[0])
        active_model = dailyToCumulative(daily_active_model, active[0])

        recovered_for_ridge = dailyToCumulative(daily_recovered_for_ridge, recovered_for_ridge[0])
        r_sum_cut = dailyToCumulative(daily_recovered, r_sum_cut[0])
        recovered_model = dailyToCumulative(daily_recovered_model, r_sum_cut[0])

        deaths_for_ridge = dailyToCumulative(daily_deaths_for_ridge, deaths_for_ridge[0])
        deaths = dailyToCumulative(daily_deaths, deaths[0])
        deaths_model = dailyToCumulative(daily_deaths_model, deaths[0])

        confirmed_for_ridge = dailyToCumulative(daily_confirmed_for_ridge, confirmed_for_ridge[0])
        confirmed = dailyToCumulative(daily_confirmed, confirmed[0])
        confirmed_model = dailyToCumulative(daily_confirmed_model, confirmed[0])
        # Add 1 to the stop and start locations to adjust for the expanded cumulative data set
        stop += 1
        start += 1

    if dailyOrCumulative == 2:
        active_for_ridge = weeklyToCumulative(weekly_active_for_ridge, active_for_ridge[0])
        active = weeklyToCumulative(weekly_active, active[0])
        active_model = weeklyToCumulative(weekly_active_model, active[0])

        recovered_for_ridge = weeklyToCumulative(weekly_recovered_for_ridge, recovered_for_ridge[0])
        r_sum_cut = weeklyToCumulative(weekly_recovered, r_sum_cut[0])
        recovered_model = weeklyToCumulative(weekly_recovered_model, r_sum_cut[0])

        deaths_for_ridge = weeklyToCumulative(weekly_deaths_for_ridge, deaths_for_ridge[0])
        deaths = weeklyToCumulative(weekly_deaths, deaths[0])
        deaths_model = weeklyToCumulative(weekly_deaths_model, deaths[0])

        confirmed_for_ridge = weeklyToCumulative(weekly_confirmed_for_ridge, confirmed_for_ridge[0])
        confirmed = weeklyToCumulative(weekly_confirmed, confirmed[0])
        confirmed_model = weeklyToCumulative(weekly_confirmed_model, confirmed[0])
        # Add 1 to the stop and start locations to adjust for the expanded cumulative data set
        stop += 1
        start += 1
    

    # Determine error
    least_sq_total_active = 0
    least_sq_total_recovered = 0
    least_sq_total_deaths = 0
    least_sq_total_confirmed = 0
    #least_sq_total_inf = 0
    for n in range(len(active_arr_checker)):
        #least_sq_total_active += (active_to_plot[n] - beta_model[n])**2
        #least_sq_total_gamma += (gamma_to_plot[n] - gamma_model[n])**2
        #print(len(active))
        #print(len(active_arr_checker))
        least_sq_total_active += (active[n] - active_arr_checker[n])**2
        least_sq_total_recovered += (r_sum_cut[n] - recovered_arr_checker[n])**2
        least_sq_total_deaths += (deaths[n] - deaths_arr_checker[n])**2
        least_sq_total_confirmed += (confirmed[n] - confirmed_arr_checker[n])**2
    
    #least_sq_total_beta = np.sqrt(least_sq_total_beta)
    #least_sq_total_gamma = np.sqrt(least_sq_total_gamma)
    least_sq_total_active = np.sqrt(least_sq_total_active)
    least_sq_total_recovered = np.sqrt(least_sq_total_recovered)
    least_sq_total_deaths = np.sqrt(least_sq_total_deaths)
    least_sq_total_confirmed = np.sqrt(least_sq_total_confirmed)

    #print("Beta penalty = " + str(alpha_list[k]))
    #print("Gamma penalty = " + str(alpha_list[j]))
    #print("Beta least squares = " + str(least_sq_total_beta))
    #print("Gamma least squares = " + str(least_sq_total_gamma))

    # Set best active prediction (both parameters optimal)
    #if least_sq_total_beta <= min_least_total_beta and least_sq_total_gamma <= min_least_total_gamma:
    # Determine optimal values (reset benchmarks)
    '''
    if least_sq_total_beta < min_least_total_beta:
        min_least_total_beta = least_sq_total_beta

    if least_sq_total_gamma < min_least_total_gamma:
        min_least_total_gamma = least_sq_total_gamma
    '''

    if alpha_list_active[k] == alpha_mid_active:
        best_active_model = active_model

    if alpha_list_recovered[k] == alpha_mid_recovered:
        best_recovered_model = recovered_model
        
    if alpha_list_deaths[k] == alpha_mid_deaths:
        best_deaths_model = deaths_model

    if alpha_list_confirmed[k] == alpha_mid_confirmed:
        best_confirmed_model = confirmed_model

    for i in range(len(confirmed_model)):
        deaths_model_list.append((i, deaths_model[i]))
        confirmed_model_list.append((i, confirmed_model[i]))

    k += 1

if dailyOrCumulative == 0:
    for i in range(41):
        active_to_plot.append(0)
        recovered_to_plot.append(0)
        deaths_to_plot.append(0)
        confirmed_to_plot.append(0)

if dailyOrCumulative == 1:
    daily_active_to_plot = cumulativeToDaily(active_to_plot)
    daily_recovered_to_plot = cumulativeToDaily(recovered_to_plot)
    daily_deaths_to_plot = cumulativeToDaily(deaths_to_plot)
    daily_confirmed_to_plot = cumulativeToDaily(confirmed_to_plot)
    daily_best_active_model = cumulativeToDaily(best_active_model)
    daily_best_recovered_model = cumulativeToDaily(best_recovered_model)
    daily_best_deaths_model = cumulativeToDaily(best_deaths_model)
    daily_best_confirmed_model = cumulativeToDaily(best_confirmed_model)

    for i in range(41):
        daily_active_to_plot.append(0)
        daily_recovered_to_plot.append(0)
        daily_deaths_to_plot.append(0)
        daily_confirmed_to_plot.append(0)

    for i in range(41):
        active_to_plot.append(0)
        recovered_to_plot.append(0)
        deaths_to_plot.append(0)
        confirmed_to_plot.append(0)

if dailyOrCumulative == 2:
    weekly_active_to_plot = cumulativeToWeekly(active_to_plot)
    weekly_recovered_to_plot = cumulativeToWeekly(recovered_to_plot)
    weekly_deaths_to_plot = cumulativeToWeekly(deaths_to_plot)
    weekly_confirmed_to_plot = cumulativeToWeekly(confirmed_to_plot)
    weekly_best_active_model = cumulativeToWeekly(best_active_model)
    weekly_best_recovered_model = cumulativeToWeekly(best_recovered_model)
    weekly_best_deaths_model = cumulativeToWeekly(best_deaths_model)
    weekly_best_confirmed_model = cumulativeToWeekly(best_confirmed_model)

    for i in range(41):
        weekly_active_to_plot.append(0)
        weekly_recovered_to_plot.append(0)
        weekly_deaths_to_plot.append(0)
        weekly_confirmed_to_plot.append(0)

    for i in range(41):
        active_to_plot.append(0)
        recovered_to_plot.append(0)
        deaths_to_plot.append(0)
        confirmed_to_plot.append(0)

if dailyOrCumulative == 1:
    # Plot optimal beta results
    plt.plot(daily_best_active_model[:start+40], 'g', linestyle = '--', label = 'model')
    plt.plot(daily_active_to_plot[:start+40], 'k', label = 'data')
    plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
    plt.xlabel('Days')
    plt.ylabel('Active')
    plt.title('Active Prediction\n' + str(location))
    plt.legend(loc = 'upper left')
    plt.savefig(location + '/' + location + '_active.pdf')
    plt.show()

    # Plot optimal gamma results
    plt.plot(daily_best_recovered_model[:start+40], 'g', linestyle = '--', label = 'model')
    plt.plot(daily_recovered_to_plot[:start+40], 'k', label = 'data')
    plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
    plt.xlabel('Days')
    plt.ylabel('Recovered')
    plt.title('Recovered Prediction\n' + str(location))
    plt.legend(loc = 'upper left')
    plt.savefig(location + '/' + location + '_recovered.pdf')
    plt.show()

    # Plot optimal active case results
    plt.plot(daily_best_deaths_model[:start+40], 'g', linestyle = '--', label = 'model')
    plt.plot(daily_deaths_to_plot[:start+40], 'k', label = 'data')
    plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
    plt.xlabel('Days')
    plt.ylabel('Deaths')
    plt.title('Deaths Prediction\n' + str(location))
    plt.legend(loc = 'upper left')
    plt.savefig(location + '/' + location + '_deaths.pdf')
    plt.show()

    plt.plot(daily_best_confirmed_model[:start+40], 'g', linestyle = '--', label = 'model')
    plt.plot(daily_confirmed_to_plot[:start+40], 'k', label = 'data')
    plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
    plt.xlabel('Days')
    plt.ylabel('Confirmed')
    plt.title('Confirmed Prediction\n' + str(location))
    plt.legend(loc = 'upper left')
    plt.savefig(location + '/' + location + '_confirmed.pdf')
    plt.show()

if dailyOrCumulative == 2:
    # Plot optimal beta results
    plt.plot(weekly_best_active_model[:start+4], 'g', linestyle = '--', label = 'model')
    plt.plot(weekly_active_to_plot[:start+4], 'k', label = 'data')
    plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
    plt.xlabel('Days')
    plt.ylabel('Active')
    plt.title('Active Prediction\n' + str(location))
    plt.legend(loc = 'upper left')
    plt.savefig(location + '/' + location + '_active.pdf')
    plt.show()

    # Plot optimal gamma results
    plt.plot(weekly_best_recovered_model[:start+4], 'g', linestyle = '--', label = 'model')
    plt.plot(weekly_recovered_to_plot[:start+4], 'k', label = 'data')
    plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
    plt.xlabel('Days')
    plt.ylabel('Recovered')
    plt.title('Recovered Prediction\n' + str(location))
    plt.legend(loc = 'upper left')
    plt.savefig(location + '/' + location + '_recovered.pdf')
    plt.show()

    # Plot optimal active case results
    plt.plot(weekly_best_deaths_model[:start+4], 'g', linestyle = '--', label = 'model')
    plt.plot(weekly_deaths_to_plot[:start+4], 'k', label = 'data')
    plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
    plt.xlabel('Days')
    plt.ylabel('Deaths')
    plt.title('Deaths Prediction\n' + str(location))
    plt.legend(loc = 'upper left')
    plt.savefig(location + '/' + location + '_deaths.pdf')
    plt.show()

    plt.plot(weekly_best_confirmed_model[:start+4], 'g', linestyle = '--', label = 'model')
    plt.plot(weekly_confirmed_to_plot[:start+4], 'k', label = 'data')
    plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
    plt.xlabel('Days')
    plt.ylabel('Confirmed')
    plt.title('Confirmed Prediction\n' + str(location))
    plt.legend(loc = 'upper left')
    plt.savefig(location + '/' + location + '_confirmed.pdf')
    plt.show()

##############################################################
#                                                            #
#                       Build Quantiles                      #
#                                                            #
##############################################################

alpha = 0.975
clf_confirmed = GradientBoostingRegressor(loss = 'quantile', alpha = alpha, n_estimators = 1500, max_depth = 5, learning_rate = .1, min_samples_leaf = 5, min_samples_split = 5, random_state = 1)

clf_deaths = GradientBoostingRegressor(loss = 'quantile', alpha = alpha, n_estimators = 1500, max_depth = 3, learning_rate = .1, min_samples_leaf = 9, min_samples_split = 9, random_state = 1)

quantile_list = [.01, 0.025]
for i in np.arange(0.05, 1.0, .05):
    quantile_list.append(i)
quantile_list.append(0.975)
quantile_list.append(0.99)

quantile_list_case_inc = [0.025, 0.100, 0.250, 0.500, 0.750, 0.900]

case_count_list = []
case_inc_list = []
death_count_list = []
death_inc_list = []

# Edit days here
days_for_pred = [7, 14, 21, 28]
if dailyOrCumulative == 2:
    days_for_pred = [1,2,3,4]

days_to_fit = len(best_confirmed_model) - len(confirmedCases[minRange:])
day_arr = np.atleast_2d(np.arange(0, days_to_fit, 1)).T
day_arr = day_arr.astype(np.float32)

X_confirmed = []
y_confirmed = []
X_deaths = []
y_deaths = []

for (i, j) in confirmed_model_list:
    if i >= 0: #len(confirmedCases[minRange:]):
        X_confirmed.append(i)
        y_confirmed.append(j)

for (i, j) in deaths_model_list:
    if i >= 0: #len(confirmedCases[minRange:]):
        X_deaths.append(i)
        y_deaths.append(j)

X_confirmed_OLS = np.atleast_2d(np.asarray(X_confirmed)).T
X_deaths_OLS = np.atleast_2d(np.asarray(X_deaths)).T

y_confirmed_OLS = y_confirmed
y_deaths_OLS = y_deaths

# Changes made for later on
y_confirmed = [float(y_confirmed[i]) for i in range(len(y_confirmed))]
y_deaths = [float(y_deaths[i]) for i in range(len(y_deaths))]
#print(y_confirmed)

day_arr_fine = np.atleast_2d(np.linspace(X_deaths[0], X_deaths[-1], 100))
day_arr_fine = day_arr_fine.astype(np.float32)

# OLS prediction, for point predictions

clf_confirmed.set_params(loss = 'ls')
clf_deaths.set_params(loss = 'ls')

clf_confirmed.fit(X_confirmed_OLS, y_confirmed_OLS)
clf_deaths.fit(X_deaths_OLS, y_deaths_OLS)

##############################################################
#                                                            #  
#                   QuantCalc Implementation                 #
#                                                            #
##############################################################

QUANTILES = [.01, .025, .05, .1, .15, .2, .25, .3, .35, .4, .45, .5, .55, .6, .65, .7, .75, .8, .85, .9, .95, .975, .99]

quantiles_dc1 = []
quantiles_dc2_5 = []
quantiles_dc5 = []
quantiles_dc10 = []
quantiles_dc15 = []
quantiles_dc20 = []
quantiles_dc25 = []
quantiles_dc30 = []
quantiles_dc35 = []
quantiles_dc40 = []
quantiles_dc45 = []
quantiles_dc50 = []
quantiles_dc55 = []
quantiles_dc60 = []
quantiles_dc65 = []
quantiles_dc70 = []
quantiles_dc75 = []
quantiles_dc80 = []
quantiles_dc85 = []
quantiles_dc90 = []
quantiles_dc95 = []
quantiles_dc97_5 = []
quantiles_dc99 = []

quantiles_cc2_5 = []
quantiles_cc10 = []
quantiles_cc25 = []
quantiles_cc50 = []
quantiles_cc75 = []
quantiles_cc90 = []
quantiles_cc97_5 = []

deaths_quant_list = [quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99]

confirmed_quant_list = [quantiles_cc2_5, quantiles_cc10, quantiles_cc25, quantiles_cc50, quantiles_cc75, quantiles_cc90, quantiles_cc97_5]

QUANTILES_CASE_INC = [0.025, 0.100, 0.250, 0.500, 0.750, 0.900, 0.975]

day_pred_deaths = []
day_pred_deaths_matrix = []
day_pred_confirmed = []
day_pred_confirmed_matrix = []
# Quantiles deaths and confirmed
for i in range(X_deaths[-1] + 1):
    day = i
    for j in range(len(y_deaths)):
        if X_deaths[j] == i:
            day_pred_deaths.append(y_deaths[j])
        if X_confirmed[j] == i:
            day_pred_confirmed.append(y_confirmed[j])
    day_pred_deaths_matrix.append(day_pred_deaths)
    day_pred_confirmed_matrix.append(day_pred_confirmed)
    day_pred_deaths = []
    day_pred_confirmed = []

num_preds = len(day_pred_confirmed_matrix[0])
for i in range(len(day_pred_confirmed_matrix)):
    death_preds_ordered = np.sort(day_pred_deaths_matrix[i])
    #print(death_preds_ordered)
    confirmed_preds_ordered = np.sort(day_pred_confirmed_matrix[i])
    print(confirmed_preds_ordered)
    
    for j in range(len(deaths_quant_list)):
        Quantile = QUANTILES[j]
        matching_day_quantile_deaths = np.percentile(death_preds_ordered, Quantile*100.)
        deaths_quant_list[j].append((i, matching_day_quantile_deaths))

    for j in range(len(confirmed_quant_list)):
        Quantile = QUANTILES_CASE_INC[j]
        matching_day_quantile_confirmed = np.percentile(confirmed_preds_ordered, Quantile*100.)
        confirmed_quant_list[j].append((i, matching_day_quantile_confirmed))

upper_confirmed = [j for (i, j) in confirmed_quant_list[-1]]
lower_confirmed = [j for (i, j) in confirmed_quant_list[0]]
upper_confirmed = np.asarray(upper_confirmed)
lower_confirmed = np.asarray(lower_confirmed)
upper_confirmed = upper_confirmed.reshape((-1,1))
lower_confirmed = lower_confirmed.reshape((-1,1))

day_arr = np.atleast_2d(np.arange(0, X_deaths[-1] + 1, 1))
day_arr = day_arr.astype(np.float32)
day_arr = day_arr.reshape((-1,1))

dates = dates[minRange:]
dates = [dates[i] + str('-20') for i in range(len(dates))]
# Edit days here
# for i in range(24, 32):
#     dates.append('08-' + str(i) + '-20')
# for i in range(7, 10):
#     dates.append('09-0' + str(i) + '-20')
# for i in range(10, 29):
#     dates.append('09-' + str(i) + '-20')

#print(dates)

#fig, ax = plt.subplots()
#ax2 = ax.twinx()

'''
days_mod = []
dates_mod = []
modder = int(len(dates)/10)
for i in range(len(dates)):
    if i % modder == 0:
        days_mod.append(day_arr[i])

        #dates_mod.append(dates[i])
        dates_mod.append(dates[i])
'''

########################################################
#                                                      #
#                    Data Writing                      #
#                                                      #
########################################################

# NEW (9-7-2020) -- Use python's datetime library, set the current date

# get today's date
date_today = calendar.today()
# get the next Saturday's date
date_saturday = date_today + timedelta((5 - date_today.weekday()) % 7)

submission_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'location', 'type', 'quantile', 'value'])
# Edit days here
forecast_date = "2020-09-14"
target_end_date = "2020-09-19"

case_count_list = []
death_count_list = []

cumulative_deaths_for_adding = [deaths_arr_const[-1]]

# Edit days here
for i in range(len(days_for_pred)):
    if i == 1:
        target_end_date = "2020-09-26"
    if i == 2:
        target_end_date = "2020-10-03"
    if i == 3:
        target_end_date = "2020-10-10"

    pred_day = len(active_arr_checker) + days_for_pred[i] - 1
    #pred_day = np.asarray(pred_day)
    #pred_day = pred_day.reshape(1,-1)
    
    for j in range(len(confirmed_quant_list)):
        quantile = QUANTILES_CASE_INC[j]
        row = [k for (l, k) in confirmed_quant_list[j]]
        if quantile == 0.975:
            break
        else:
            if i == 0:
                case_inc = row[pred_day] - clf_confirmed.predict(np.asarray(int(len(active_arr_checker) - 1)).reshape(1, -1))
                case_inc = float(case_inc[0])
            else:
                case_inc = row[pred_day] - case_count_list[-1]
                case_inc = float(case_inc[0])
            if case_inc < 0:
                case_inc = 0.
            submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case'.format(week = i+1), target_end_date, location, 'quantile', quantile, case_inc]], columns = submission_frame.columns))

    for j in range(len(deaths_quant_list)):
        quantile = QUANTILES[j]
        row = [k for (l, k) in deaths_quant_list[j]]

        if i == 0:
            deaths_inc = row[pred_day] - clf_deaths.predict(np.asarray(int(len(deaths_arr_const) - 1)).reshape(1, -1))
            deaths_inc = float(deaths_inc[0])
        else:
            deaths_inc = row[pred_day] - death_count_list[-1]
            deaths_inc = float(deaths_inc[0])
        if deaths_inc < 0:
            deaths_inc = 0.
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'quantile', quantile, deaths_inc]], columns = submission_frame.columns))
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'quantile', quantile, deaths_inc + cumulative_deaths_for_adding[-1]]], columns = submission_frame.columns))

    pred_day = np.asarray(pred_day)
    pred_day = pred_day.reshape(1,-1)
 
    if i == 0:
        case_inc = clf_confirmed.predict(pred_day) - clf_confirmed.predict(np.asarray(int(len(active_arr_checker) - 1)).reshape(1, -1))
    else:
        case_inc = clf_confirmed.predict(pred_day) - case_count_list[-1]
    if case_inc < 0:
        case_inc = [0.]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case'.format(week = i+1), target_end_date, location, 'point', 'NA', float(case_inc[0])]], columns = submission_frame.columns))

    if i == 0:
        deaths_inc = clf_deaths.predict(pred_day) - clf_deaths.predict(np.asarray(int(len(deaths_arr_const) - 1)).reshape(1, -1))
    else:
        deaths_inc = clf_deaths.predict(pred_day) - death_count_list[-1]
    if deaths_inc < 0:
        deaths_inc = [0.]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'point', 'NA', float(deaths_inc[0])]], columns = submission_frame.columns))

    this_cumulative = deaths_inc + cumulative_deaths_for_adding[-1]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'point', 'NA', float(this_cumulative[0])]], columns = submission_frame.columns))
    cumulative_deaths_for_adding.append(float(this_cumulative[0])) 
    
    case_count_list.append(clf_confirmed.predict(pred_day))
    death_count_list.append(clf_deaths.predict(pred_day))

# Edit days here
submission_frame.to_csv(location + '/' + "2020-09-14" + '-UMich-RidgeTfReg.csv')
