import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import os
import sys
import operator
import iminuit
from scipy.optimize import curve_fit
from datetime import datetime
from pygrok import Grok
from models import SIR

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

def GetLocType():
    loc_type = raw_input("\'Country\' or \'Province\':\n")
    if loc_type.lower() == "country":
        return 1
    elif loc_type.lower() == "province":
        return 0
    else:
        print("\nInput not valid. Please try again.")
        return GetLocType()

def GetLoc():
    location_r_formatted = raw_input("Region of interest: ")
    '''
    if location_r == "US":
        location_r_formatted = location_r
    else:
        location_r_formatted = location_r[0].upper() + location_r[1:].lower()
    '''
    loc_type_r = GetLocType()
    return (location_r_formatted, loc_type_r)

# Specify
location, countryOrState = GetLoc()
# Save in separate directories
if not os.path.exists(location):
    os.makedirs(location)


###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Initialization
dates = []
confirmedCases = []
deaths = []
recovered = []
active = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
        if (location == 'South Korea') and file == (path + '03-10-2020.csv'):
            location = 'Republic of Korea'
        if (location == 'Republic of Korea') and file == (path + '03-11-2020.csv'):
            location = 'Korea, South'
        filedate = grok.match(file)
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Fill dates from the file into an array
        day = str(filedate["month"])+'-'+str(filedate["day"])
        dates.append(day)

        # 'if' statement to deal with Country or State and JHU data format change
        areaLabel = 'Country/Region'
        if countryOrState == 0:
                areaLabel = 'Province/State'
        if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):
                areaLabel = 'Country_Region'
                if countryOrState == 0:
                        areaLabel = 'Province_State'

        # Grab Confirmed and Deaths
        confirmedCases.append(0)
        deaths.append(0.)
        recovered.append(0.)
        for index, row in data.iterrows():
                if row[areaLabel] == location:
                        if str(row['Confirmed']) != 'nan':
                                confirmedCases[dayCount] = confirmedCases[dayCount] + row['Confirmed']
                        if str(row['Deaths']) != 'nan':
                                deaths[dayCount] = deaths[dayCount] + row['Deaths']
                        if str(row['Recovered']) != 'nan':
                                recovered[dayCount] = recovered[dayCount] + row['Recovered']

        dayCount = dayCount + 1

#print confirmedCases
if location == 'Korea, South':
    location = 'South Korea'

###############################################################
#                                                             #
#            Make and view all-encompassing plot              #
#                                                             #
###############################################################

deathRate = np.divide(deaths, confirmedCases)

# Make my plots
minRange = 20

float_dates = []
day_count = 0
for date in dates[minRange:]:
    float_dates.append( day_count )
    day_count = day_count + 1                                                                     

active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]

# Total Plots
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
# Temporarily removing recovered[minRange:] to match JHU
ax1.plot(dates[minRange:],confirmedCases[minRange:],'bo',deaths[minRange:],'ro')
ax1.plot(dates[minRange+1:],list(map(operator.sub, confirmedCases[minRange+1:],confirmedCases[minRange:-1])), 'co')
ax2.plot(dates[minRange:],deathRate[minRange:],'m')
ax1.set_xlabel('Date')
ax1.set_ylabel('Cases')
ax2.set_ylabel('Death Rate')
ax2.set_ylim(0,0.1)
plt.title(location)
ax1.legend(['Confirmed','Deaths','New'])
ax2.legend(['Death Rate'],loc='upper center')

if not os.path.exists(location):
    os.makedirs(location)

plt.savefig(location + '/' + location + '.pdf')
plt.show()

###############################################################
#                                                             #
#            Pick and view new or confirmed data              #
#                                                             #
###############################################################

# Switched into curvefit_covid.py

##############################################################
#                                                            #
#                      Epidemic Model                        #
#                                                            #
##############################################################

# SIR Model
# ** Note: eventually include birth/death rate
# Active = a/b * log(healthy) - healthy + C
# C = I(0) - a/b * log(initial population) + initial population

#susceptible = [population - (active[i] + deaths[i] + recovered[i]) for i in range(len(confirmedCases))]

# Find total population:
pop_file = 'world_pop.csv'
pop_data = pd.read_csv( pop_file, index_col = None, header = 0 )
print location
for index, row in pop_data.iterrows():
    if row['Region'] == location:
        N = int(row['Population'])
print("Population = " + str(N))

'''
# Find testing data:
test_list = []
test_percent_list = []
test_file = 'covid_test_data.csv'
test_data = pd.read_csv( test_file, index_col = None, header = 0 )
for index, row in test_data.iterrows():
    entity = row['Entity']
    country = entity.split(' - ')[0]
    if country == location:
        test_list.append(row['Cumulative total'])
        test_percent_list.append(row['Cumulative total per thousand'])
'''

# Contact rate, beta, and mean recovery rate, gamma, (in 1/days).
beta, gamma = 0.2, 1./10

# Find the first day of infection and create a grid
day = 1
for cases in confirmedCases[minRange:]:
    if cases != 0:
        first_day = float_dates[minRange+day]
        print ("\nFirst day of cases: " + str(first_day))
        inf_init = cases
        print ("Initial case count: " + str(cases) + "\n")
        break
    day += 1

today = float_dates[-1] + 1
t = np.linspace(0, today, today*10)
t_test = np.linspace(0, int(today/1), int(today/1)*10)
t_out = np.linspace(0, today + 100, (today + 100)*10)

# Integrate the SIR equations over the time grid, t
def SIR_func(test, pop_guess, inf_init, b, g):
    if test == 0:
        time = t
    elif test == 1:
        time = t_test
    elif test == 2:
        time = t_out
    S, I, R = SIR(time,pop_guess,inf_init,b,g)
    I_to_ret = []
    for i in range(len(time)):
        if i % 10 == 0:
            I_to_ret.append(I[i])
    return I_to_ret

# Use Minuit to minimize sum of least squares with SIR model
def minimizeMin( pop_guess, inf_init, b, g ):
    I_to_sum = SIR_func( 1, pop_guess, inf_init, b, g )
    return sum( ( x - y )**2 for x,y in zip( I_to_sum, active ) )

# Find standard error of the estimate of Minuit model
def SE( data_arr, SIR_arr ):
    sq_dist_from_fit = [(data_arr[i] - SIR_arr[i])**2 for i in range(len(data_arr))]
    dist_sum = np.sum(sq_dist_from_fit)
    SE_est = np.sqrt(dist_sum/len(active))
    print( "\nSE of the Estimate: " + str(SE_est) )
    return SE_est

# Perform fit using Minuit (fixing initial guesses and limits)
m = iminuit.Minuit( minimizeMin, pop_guess = 100000, inf_init = 5, b = .2, g = 1/7., limit_pop_guess = (np.amax(confirmedCases) + .001*np.amax(confirmedCases), N), error_pop_guess=5, error_inf_init=1, error_b=.001, error_g=.001, limit_inf_init = (0,1e7), limit_b = (0.,5.), limit_g = (1/43., 1/5.) )
m.migrad( ncall = 500000 )
param = m.args
popc = m.covariance

# Plot estimate versus data
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
ax.scatter(float_dates, active, label = 'Data')
I_fit = SIR_func(0, *param)
ax.plot(float_dates, I_fit, 'r', label = 'SIR Fit')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Number')
plt.title(str(location) + ' SIR Fit')
legend = ax.legend()
print('\nFit: pop_est = %5.3f, I0 = %5.3f, beta = %5.3f, gamma = %5.3f' % tuple(param) )
SE( active, SIR_func(0, *param) )
day_to_check = int(len(float_dates)/3) + 14
print('\nTwo Week Percentage Error: ' + str(100. * abs(active[day_to_check] - I_fit[day_to_check])/active[day_to_check]))
plt.show()

# Plot estimate into the future
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
#ax.scatter(float_dates, active, label = 'Data')
ax.plot(np.linspace(0, today + 100, today + 100), SIR_func(2, *param), 'r', label = 'SIR Fit')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Number')
plt.title(str(location) + ' SIR Prediction')
ax.yaxis.set_tick_params(length=0)
ax.xaxis.set_tick_params(length=0)
legend = ax.legend()
plt.show()
