import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import os
import sys
import operator
import iminuit
from scipy.optimize import curve_fit
from datetime import datetime
from pygrok import Grok
from models import SEIR

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

def GetLocType():
    loc_type = raw_input("\'Country\' or \'Province\':\n")
    if loc_type.lower() == "country":
        return 1
    elif loc_type.lower() == "province":
        return 0
    else:
        print("\nInput not valid. Please try again.")
        return GetLocType()

def GetLoc():
    location_r_formatted = raw_input("Region of interest: ")
    '''
    if location_r == "US":
        location_r_formatted = location_r
    else:
        location_r_formatted = location_r[0].upper() + location_r[1:].lower()
    '''
    loc_type_r = GetLocType()
    return (location_r_formatted, loc_type_r)

# Specify
location, countryOrState = GetLoc()
# Save in separate directories
if not os.path.exists(location):
    os.makedirs(location)


###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Initialization
dates = []
confirmedCases = []
deaths = []
recovered = []
active = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
        if (location == 'South Korea') and file == (path + '03-10-2020.csv'):
            location = 'Republic of Korea'
        if (location == 'Republic of Korea') and file == (path + '03-11-2020.csv'):
            location = 'Korea, South'
        filedate = grok.match(file)
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Fill dates from the file into an array
        day = str(filedate["month"])+'-'+str(filedate["day"])
        dates.append(day)

        # 'if' statement to deal with Country or State and JHU data format change
        areaLabel = 'Country/Region'
        if countryOrState == 0:
                areaLabel = 'Province/State'
        if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):
                areaLabel = 'Country_Region'
                if countryOrState == 0:
                        areaLabel = 'Province_State'

        # Grab Confirmed and Deaths
        confirmedCases.append(0)
        deaths.append(0.)
        recovered.append(0.)
        for index, row in data.iterrows():
                if row[areaLabel] == location:
                        if str(row['Confirmed']) != 'nan':
                                confirmedCases[dayCount] = confirmedCases[dayCount] + row['Confirmed']
                        if str(row['Deaths']) != 'nan':
                                deaths[dayCount] = deaths[dayCount] + row['Deaths']
                        if str(row['Recovered']) != 'nan':
                                recovered[dayCount] = recovered[dayCount] + row['Recovered']

        dayCount = dayCount + 1

# print confirmedCases
if location == 'Korea, South':
    location = 'South Korea'

# Reading in quarantine measures
mitigation_triples = []
quarantine_file = 'quarantine_measures.csv'
quarantine_data = pd.read_csv( quarantine_file, index_col = None, header = 0 )
location_row = quarantine_data[quarantine_data['Region'] == location].index[0]
day_count = 1
for i in range(1,6):

    if i == 1 or i == 3 or i == 5:
        day_max = 31
    elif i == 2:
        day_max = 29
    elif i == 4:
        day_max = 30

    if i == 1:
        day_min = 22
    else:
        day_min = 1
            
    for j in range(day_min,day_max):
        if j < 10:
            date = '0{month}/0{day}/20'.format(month = i, day = j)
        else:
            date = '0{month}/{day}/20'.format(month = i, day = j)

        if quarantine_data.loc[location_row, date] != 0:
            mitigation_triples.append([date, day_count, quarantine_data.loc[location_row, date]])

        day_count += 1

# Log quarantine measures
print('\nQuarantine Measures:')
for [i,j,k] in mitigation_triples:
    if j == 1:
        statement = 'commenced shelter in place.'
    if j == 2:
        statement = 'lifted shelter in place.'
    if j == 3:
        statement = 'commenced hard quarantine.'
    if j == 4:
        statement = 'lifted hard quarantine.'
    if j == 5:
        statement = 'restricted domestic travel.'
    if j == 6:
        statement = 'resumed domestic travel.'
    if j == 7:
        statement = 'restricted international travel.'
    if j == 8:
        statement = 'resumed international travel.'
   
    print('On {date} (day {day}), {l} {s}'.format(date = i, day = j, l = location, s = statement))

###############################################################
#                                                             #
#            Make and view all-encompassing plot              #
#                                                             #
###############################################################

deathRate = np.divide(deaths, confirmedCases)

# Find the first day of infection and create a grid
day = 0
for cases in confirmedCases:
    if cases > 1000:
        first_day = day
        print ("\nFirst day of count: " + str(first_day))
        inf_init = cases
        print ("Initial case count: " + str(cases) + "\n")
        break
    day += 1

minRange = first_day
mitigation_trim = []
# Trim quarantine measures considered
for triple in mitigation_triples:
    if triple[1] >= first_day:
        mitigation_trim.append(triple)

float_dates = []
day_count = 0
for date in dates[minRange:]:
    float_dates.append( day_count )
    day_count = day_count + 1                                                                     

active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]

# Total Plots
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
# Temporarily removing recovered[minRange:] to match JHU
ax1.plot(dates[minRange:],confirmedCases[minRange:],'bo',deaths[minRange:],'ro')
ax1.plot(dates[minRange+1:],list(map(operator.sub, confirmedCases[minRange+1:],confirmedCases[minRange:-1])), 'co')
ax2.plot(dates[minRange:],deathRate[minRange:],'m')
ax1.set_xlabel('Date')
ax1.set_ylabel('Cases')
ax2.set_ylabel('Death Rate')
ax2.set_ylim(0,0.1)
plt.title(location)
ax1.legend(['Confirmed','Deaths','New'])
ax2.legend(['Death Rate'],loc='upper center')

if not os.path.exists(location):
    os.makedirs(location)

plt.savefig(location + '/' + location + '.pdf')
plt.show()

###############################################################
#                                                             #
#            Pick and view new or confirmed data              #
#                                                             #
###############################################################

# Switched into curvefit_covid.py

##############################################################
#                                                            #
#                      Epidemic Model                        #
#                                                            #
##############################################################

# SIR Model
# ** Note: eventually include birth/death rate
# Active = a/b * log(healthy) - healthy + C
# C = I(0) - a/b * log(initial population) + initial population

#susceptible = [population - (active[i] + deaths[i] + recovered[i]) for i in range(len(confirmedCases))]

# Find total population:
pop_file = 'world_pop.csv'
pop_data = pd.read_csv( pop_file, index_col = None, header = 0 )
print location
for index, row in pop_data.iterrows():
    if row['Region'] == location:
        N = int(row['Population'])
print("Population = " + str(N))

'''
# Find testing data:
test_list = []
test_percent_list = []
test_file = 'covid_test_data.csv'
test_data = pd.read_csv( test_file, index_col = None, header = 0 )
for index, row in test_data.iterrows():
    entity = row['Entity']
    country = entity.split(' - ')[0]
    if country == location:
        test_list.append(row['Cumulative total'])
        test_percent_list.append(row['Cumulative total per thousand'])
'''

# Divide timeline into blocks
test_frac = 1
today = float_dates[-1] + 1
'''
t_list = []
prev_stop = 0
for triple in mitigation_trim:
    t_list.append(np.linspace(prev_stop, triple[1], (triple[1] - prev_stop)*10))
    prev_stop = triple[1]
'''
t_test = np.linspace(0, int(today/test_frac), int(today/test_frac)*10)
t_out = np.linspace(0, today + 200, (today + 200)*10)
t = np.linspace(0, today, today*10)

# Integrate the SIR equations over the time grid, t
def SEIR_func(test, pop_guess, inf_init, exp_init, b, g, s):
    if test == 0:
        time = t
    elif test == 1:
        time = t_test
    elif test == 2:
        time = t_out
    S, E, I, R = SEIR(time,pop_guess,inf_init,exp_init,b,g,s)
    I_to_ret = []
    for i in range(len(time)):
        if i % 10 == 0:
            I_to_ret.append(I[i])
    return I_to_ret

# Use Minuit to minimize sum of least squares with SIR model
def minimizeMin( pop_guess, inf_init, exp_init, b, g, s ):
    I_to_sum = SEIR_func( 1, pop_guess, inf_init, exp_init, b, g, s )
    return sum( ( x - y )**2 for x,y in zip( I_to_sum, active ) )

# Find standard error of the estimate of Minuit model
def SE( data_arr, SEIR_arr ):
    sq_dist_from_fit = [(data_arr[i] - SEIR_arr[i])**2 for i in range(int(len(data_arr)/test_frac))]
    dist_sum = np.sum(sq_dist_from_fit)
    SE_est = np.sqrt(dist_sum/len(active))
    print( "SE of the Estimate: " + str(SE_est) )
    return SE_est

# Perform fit using Minuit (fixing initial guesses and limits)
m = iminuit.Minuit( minimizeMin, pop_guess = 100000, inf_init = 5, exp_init = 10, b = .1, g = 1/7., s = 1/6., error_pop_guess=5, error_inf_init=1, error_exp_init = 1, error_b=.001, error_g=.001, error_s=.001, limit_pop_guess = (np.amax(confirmedCases) + .001*np.amax(confirmedCases), N), limit_inf_init = (0,1e6), limit_exp_init = (0,1e6), limit_b = (0.,10), limit_g = (0, 10), limit_s = (0, 10) )

m.migrad( ncall = 500000 )
param = m.args
popc = m.covariance

# Plot estimate versus data
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
ax.scatter(float_dates, active, label = 'Data')
I_fit = SEIR_func(0, *param)
ax.plot(float_dates, I_fit, 'r', label = 'SEIR Fit')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Number')
plt.title(str(location) + ' SEIR Fit')
legend = ax.legend()
print('\nFit: pop_est = %5.3f, I0 = %5.3f, E0 = %5.3f, beta = %5.3f, gamma = %5.3f, sigma = %5.3f' % tuple(param) )
SE( active, SEIR_func(0, *param) )
day_to_check = int(len(float_dates)/test_frac) + -1
#print('Two Week Percentage Error: ' + str(100. * abs(active[day_to_check] - I_fit[day_to_check])/active[day_to_check]))
plt.show()

# Plot estimate into the future
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
#ax.scatter(float_dates, active, label = 'Data')
I_pred = SEIR_func(2, *param)
ax.plot(np.linspace(0, today + 200, today + 200), I_pred, 'r', label = 'SIR Fit')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Number')
plt.title(str(location) + ' SEIR Prediction')
ax.yaxis.set_tick_params(length=0)
ax.xaxis.set_tick_params(length=0)
legend = ax.legend()
#plt.show()

##############################################################
#                                                            #
#                     Estimate Milestones                    #
#                                                            #
##############################################################
'''
# Estimate peak
I_prev = 0
I_now = 0
I_next = 0

peak_date = 0
peak_cases = 0
safe_date = 0
found = False

for i in range(1,len(I_pred)-2):
    if i == 1:
        I_now = I_pred[i]
        I_next = I_pred[i+1]

    else:
        I_prev = I_pred[i-1]
        I_now = I_pred[i]
        I_next = I_pred[i+1]

    if I_now > I_prev and I_now > I_next:
        peak_date = i
        peak_cases = I_now

    if I_now < 1000 and I_next < I_now and found == False:
        safe_date = i
        found = True

print('\nEstimated Peak Date from Start: ' + str(peak_date) + '\nEstimated Peak Infections: ' + str(int(peak_cases)) + '\nEstimated Safe Date: ' + str(safe_date))
'''
