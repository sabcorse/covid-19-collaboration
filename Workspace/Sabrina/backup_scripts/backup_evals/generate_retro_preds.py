import pandas as pd
import os
import sys
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta

Friday_date_list = [datetime.strptime("2020-06-19", "%Y-%m-%d")]
Saturday_date_list = [datetime.strptime("2020-06-20", "%Y-%m-%d")]
Sunday_date_list = [datetime.strptime("2020-06-21", "%Y-%m-%d")]

final_date = "2020-10-04"
is_final_date = False
while not is_final_date:
    next_Friday = Friday_date_list[-1] + timedelta(days = 7)
    next_Saturday = Saturday_date_list[-1] + timedelta(days = 7)
    next_Sunday = Sunday_date_list[-1] + timedelta(days = 7)
    #print(next_Friday)

    Friday_date_list.append(next_Friday)
    Saturday_date_list.append(next_Saturday)
    Sunday_date_list.append(next_Sunday)

    print(str(next_Sunday)[:10])
    if str(next_Sunday)[:10] == final_date:
        break

location = sys.argv[1]
location_type = sys.argv[2]
order = sys.argv[3]

for friday in Friday_date_list[0:1]:
    os.system('python3 retrospective_predictions.py '+location+' '+location_type+' '+str(friday)[:10]+' '+str(order))
for saturday in Saturday_date_list[0:1]:
    os.system('python3 retrospective_predictions.py '+location+' '+location_type+' '+str(saturday)[:10]+' '+str(order))
for sunday in Sunday_date_list[0:1]:
    os.system('python3 retrospective_predictions.py '+location+' '+location_type+' '+str(sunday)[:10]+' '+str(order))
