import numpy as np
import glob
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import seaborn as sn
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from backports.datetime_fromisoformat import MonkeyPatch
from dateutil.parser import parse
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.linear_model import Lasso, LassoCV
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from sklearn import preprocessing
from scipy import signal

MonkeyPatch.patch_fromisoformat()

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

order = int(sys.argv[1])

###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

df = pd.read_csv('gen_csvs/MH_COVID Daily Census_202101180516.csv')
covid_census = df.loc[df['Series_Name'] == 'COVID_CENSUS']['Series_Value'][7:].tolist()
#covid_census = covid_census.loc[7:]
print(covid_census)

# Store most recent deaths and most recent confirmed cases
# Predicted new cases and deaths are later added to these to get aggregates
most_recent_census = covid_census[-1]
#print(most_recent_census)

###############################################################
#                                                             #
#                 Read and Sort JHU Data                      #
#                                                             #
###############################################################

def Get_File_Date(file_name):
    month, day, year = file[-14:-12], file[-11:-9], file[-8:-4]
    return month, day, year

# List and sort JHU data files
path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

confirmedCases, deaths, recovered, active = [], [], [], []
day_count = 0

dates = []
Kent_cases = []
# Loop over JHU data, and update cases, deaths, recoveries
for file in file_set:
        
        file_mo, file_day, file_yr = Get_File_Date(file)

        #if (int(file_mo) > 5) or (int(file_yr) > 2020):
        if (int(file_mo) == 3 and int(file_day) > 29) or (int(file_mo) > 3) or (int(file_yr) > 2020) :
            if (int(file_mo) == 1 and int(file_day) == 19):
                print('Breaking')
                break
            else:
                dates.append(file_mo+'-'+file_day)
            data = pd.read_csv( file, index_col = None, header = 0 )
            data_MI = data.loc[data['Province_State'] == 'Michigan']
            data_Kent = data_MI.loc[data_MI['Admin2'] == 'Kent']

            Kent_cases.append(data_Kent['Confirmed'].values[0])

        day_count += 1

#print(Kent_cases)

Kent_cases_incident = [0]
for i in range(1, len(Kent_cases)):
    Kent_cases_incident.append(Kent_cases[i] - Kent_cases[i-1])

plt.plot(Kent_cases_incident, color = 'k')
plt.xlabel('Days Since 03/30/2020')
plt.ylabel('Kent County Cases')
plt.title('Incident Cases\nKent County')
plt.show()

start_day = (calendar.fromisoformat('2020-03-30'))
dates = [(start_day + timedelta(days = i)).strftime('%Y-%m-%d') for i in range(len(Kent_cases_incident))]
df_for_Metro = pd.DataFrame(list(zip(dates, Kent_cases_incident)), columns = ['date', 'incident Kent cases'])
df_for_Metro.to_csv('MetroCsvs/Kent_cases_training.csv')
print('csv made')

for i in range(9, 32):
    if i < 10:
        dates.append('2021-01-0'+str(i))
    else:
        dates.append('2021-01-'+str(i))
for i in range(1, 7):
    dates.append('2021-02-0'+str(i))

'''
Kent_cases = Kent_cases[:len(covid_hosp_agg)+1]
days_mod_seven = len(Kent_cases) % 7
Kent_cases = Kent_cases[days_mod_seven:]
covid_hosp_agg = covid_hosp_agg[days_mod_seven:]

tot_weeks = int(len(covid_admissions)/7)

Kent_cases_weekly = []
covid_hosp_agg_weekly = []
for i in range(tot_weeks):
    Kent_cases_weekly.append(Kent_cases[7*i])
    covid_hosp_agg_weekly.append(covid_hosp_agg[7*i])

Kent_cases_incident_weekly = [0]
covid_admissions_weekly = [0]
for i in range(1, len(covid_hosp_agg_weekly)):
    Kent_cases_incident_weekly.append(Kent_cases_weekly[i] - Kent_cases_weekly[i-1])
    covid_admissions_weekly.append(covid_hosp_agg_weekly[i] - covid_hosp_agg_weekly[i-1])
'''
#minRange = 175
#covid_census = covid_census[minRange:]

###############################################################
#                                                             #
#                     Read in Case Data                       #
#                                                             #
###############################################################

case_onset_df = pd.read_csv('gen_csvs/onset_cases.csv')
case_onset_Kent = case_onset_df.loc[case_onset_df['COUNTY'] == "Kent"]
case_onset_Kent = case_onset_Kent.loc[case_onset_Kent['CASE_STATUS'] == "Confirmed"]
case_onset_Kent = case_onset_Kent[25:]
#case_onset_Kent = case_onset_Kent[:-13]
#print(case_onset_Kent)
case_onset_Kent = case_onset_Kent["Cases"].values
print(case_onset_Kent)

###############################################################
#                                                             #
#            Trim Data and Create Active Case List            #
#                                                             #
###############################################################
##########################################################
#                                                        #
#           Get Mobility Data, Using First Day           #
#                                                        #
##########################################################
##############################################################
#                                                            #
#                 Prepare Data for Regression                #
#                                                            #
##############################################################

# Convert cumulative data to daily data, since predictions based based off of incident values
def cumulativeToDaily(cumulative_lists):
    daily_list = []
    for list_i in cumulative_lists:
        daily = []
        for i in range(len(list_i)):
            if not i == 0:
                # Subtract each "yesterday's" values from the current day's
                daily.append(list_i[i] - list_i[i-1])
        # For testing, currently unused: smooth case and death data to remove spikes
        #if list_i == confirmed or list_i == deaths:
            #daily = Sav_Gol(daily)
        daily_list.append(daily)
    return daily_list[0]

# Convert daily data to cumulative data
# Can be used to test aggregate predictions based off the sum of incident predictions
def dailyToCumulative(daily_list_start_pairs):
    cumulative_list = []
    for (list_i, start_val) in daily_list_start_pairs:
        cumulative = []
        for i in range(len(list_i) + 1):
            # Start from baseline saved cumulative value
            if i == 0:
                cumulative.append(start_val)
            # Add most recent cumulative value to most recent incident value
            else:
                cumulative.append(cumulative[i-1] + list_i[i-1])
        cumulative_list.append(cumulative)
    return cumulative_list[0]

# Remove first <order> days of data for fitting the ridge model
# Order is the number of days used to predict each upcoming day
# E.g., if 21 days are used to predict each upcoming day, the first 21 should be removed, since they can't be predicted
def Truncate_For_Ridge(data_list):
    truncated_lists = []
    for list_i in data_list:
        truncated_lists.append(list_i[order:])
    return truncated_lists[0]

# Create constant arrays for aggregate model performance checks
covid_census_arr_checker = covid_census[:]

# Create constant arrays for incident model performance checks
daily_covid_census_arr_checker = covid_census_arr_checker#cumulativeToDaily([covid_census_arr_checker])

# Create daily active, recovered, deaths, and confirmed lists for incident value prediction making

daily_covid_census = covid_census#cumulativeToDaily([covid_census])

# Trim first days of data for fitting the model (See function comments)
covid_census_for_ridge = Truncate_For_Ridge([covid_census])

# Create matrices for prediction, as in LaTeX document
def Create_Pred_Matrices(full_data_lists):
    matrices = []
    for list_i in full_data_lists:
        pred_matrix = []
        for i in range(len(list_i) - order):
            #print(i)
            new_row_daily = [1]
            for j in range(order):
                new_row_daily.append(list_i[i+j])
            for j in range(i, order+i):
                #new_row_daily.append(case_onset_Kent[j])
                new_row_daily.append(Kent_cases_incident[j])
            pred_matrix.append(new_row_daily)
        matrices.append(pred_matrix)
    return matrices[0]

# Remove negative case, death, recovery, and active values
# Come from data-reporting anomalies, so don't belong in predictions
def Clean_Sub_Zero(full_data_lists):
    no_zeros_list = []
    for list_i in full_data_lists:
        for i in range(len(list_i)):
            if list_i[i] < 0 and not i == 0 and not i == (len(list_i) - 1):
               list_i[i] = (list_i[i-1]+list_i[i+1])/2.
               print('removing a 0')
               print(list_i[i])
        no_zeros_list.append(list_i)
    return no_zeros_list

# Create matrices, as in LaTeX document, for active cases, recoveries, deaths, and confirmed cases
#daily_covid_census = Clean_Sub_Zero([daily_covid_census])
daily_covid_census_pred_matrix = Create_Pred_Matrices([daily_covid_census])

# Remove 0's
#covid_census_for_ridge = Clean_Sub_Zero([covid_census_for_ridge])

# Trim first days of data for fitting incident data
daily_covid_census_for_ridge = Truncate_For_Ridge([daily_covid_census])

# Make constant values to revert to for both daily and cumulative model
daily_covid_census_arr_const, daily_covid_census_for_ridge_const = daily_covid_census[:], daily_covid_census_for_ridge[:]
covid_census_for_ridge_const = covid_census_for_ridge[:]

# Generate a range of ridge regression penalty alphas to try
def Generate_Alphas(coeff_list):
    alpha_list = []
    #i = -3
    i = -5
    while i < 8:
    #while i < 12:
        for coeff in coeff_list:
            alpha_list.append(coeff * (10**(-i)))
        i += 1
    params = {'alpha': alpha_list}
    return params

parameters = Generate_Alphas([1., 2.5, 5., 7.5])

###################################################
#                                                 #
#            Perform Ridge Regression             #
#                                                 #
###################################################

# Create different types of ridge regressor, based on circumstances
def Make_Ridge(input_list, ridge_type, alpha_list = [0]):
    ridge_list = []

    # Create bare-bones ridge regressors to feed into grid searcher
    if ridge_type == 'no_cv':
        for list_i in input_list:
            ridge_list.append(Ridge(normalize = True))

    # Create ridge regressor with only penalty specified, based on grid search results
    if ridge_type == 'no_cv_alpha_spec':
        for i in range(len(input_list)):
            ridge_list.append(Ridge(normalize = True, alpha = alpha_list[i]))

    # Create grid searcher to find best penalties
    if ridge_type == 'cv':
        for list_i in input_list:
            ridge_list.append(GridSearchCV(list_i, parameters, scoring = 'neg_mean_squared_error', cv = 10))

    return ridge_list[0]

# Create grid searchers to find optimal ridge penalties for active and total cases, deaths, and recoveries
r_covid_census = Make_Ridge([covid_census], 'no_cv')
ridge_covid_census_cv = Make_Ridge([r_covid_census], 'cv')

# Fit ridge regressors, and return necessary values or predictions, depending on circumstances
def Fit_Ridge(prediction_triads, fit_type):
    print('fitting')
    
    # For grid searchers, return optimal penalty and best score
    if fit_type == 'round_1_eval':
        alpha_mid_list, score_list = [], []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            alpha_mid_list.append(regressor.best_params_['alpha'])
            score_list.append(regressor.best_score_)
        return alpha_mid_list, score_list

    # For non-grid searches, create future predictions
    if fit_type == 'make_pred':
        model_list = []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            model = regressor.predict(matrix)
            model_list.append(model.tolist())
        return model_list

# Creating triples of values for fitting
# Triples include the regressor, the prediction matrix, and the vector from the RHS of the LaTeX document
covid_census_triad = (ridge_covid_census_cv, daily_covid_census_pred_matrix, daily_covid_census_for_ridge)
assigned_alphas, assigned_scores = Fit_Ridge([covid_census_triad], 'round_1_eval')
alpha_mid_covid_census = assigned_alphas[0]
score_covid_census = assigned_scores[0]

print(str(alpha_mid_covid_census))
print('Score: ' + str(score_covid_census))

ridge_covid_census = Make_Ridge([covid_census], 'no_cv_alpha_spec', [alpha_mid_covid_census])
#print(ridge_covid_census)

##############################################################
#                                                            #
#                        Bootstrapping                       #
#                                                            #
##############################################################

required_covid_census_quantiles = [.01, .025, .05, .1, .15, .2, .25, .3, .35, .4, .45, .5, .55, .6, .65, .7, .75, .8, .85, .9, .95, .975, .99]

# Fills in missing values from when the "for_ridge" lists were created without the first <order> days
# Useful for plotting, to see the full spectrum of data
def Model_Baseline(unmatched_models, baseline_lists):
    matched_lists = []
    for i in range(len(unmatched_models)):
        list_i = unmatched_models[i]
        baseline_list_i = baseline_lists[i]
        for j in range(0, order):
            list_i.insert(j, baseline_list_i[j])
        matched_lists.append(list_i)
    return matched_lists

def Get_Social_Distancing_Averages(distancing_lists, weeks_to_go_back):
    averages = []
    for list_i in distancing_lists:
        if weeks_to_go_back == 1:
            averages.append(np.sum(list_i[-7*weeks_to_go_back:])/7.)
        else:
            averages.append(np.sum(list_i[-7*weeks_to_go_back:-7*(weeks_to_go_back-1)])/7.)
    return averages

preds_made = 0
#print(data_diff)
def Create_Samples(lists_for_sampling):
    global preds_made

    sample_list = []
    for list_i in lists_for_sampling:
        sample = [1]
        i = -order
        while i < 0:
            sample.append(list_i[i])
            i += 1
        #for avg in avg_list:
            #sample.append(avg)
        
        #case_vals = case_onset_Kent[-order-5:-5]
        
        case_vals = Kent_cases_incident[-order:]
        for i in range(len(case_vals)):
             sample.append(case_vals[i])
        
        '''
        case_vals = Kent_cases_incident[-order:]
        if preds_made < len(case_vals):
            for i in range(len(case_vals) - preds_made):
                sample.append(case_vals[i + preds_made])
            for i in range(preds_made):
                sample.append(case_vals[-1])

        else:
            for i in range(len(case_vals)):
                sample.append(case_vals[-1])
        '''
        print(sample)
        #sample = preprocessing.scale(sample)
        sample_list.append(np.asarray(sample, dtype = 'object'))#.reshape(1,-1))
        preds_made += 1
        if preds_made % 2 == 0:
            preds_made = int(preds_made/2)
        #print(sample_list)
    
    return sample_list

# Create ridge regressor predictions based on samples 
def Predict_Ridge(models_list, samples_list):
    next_list = []
    for i in range(len(models_list)):
        next_val = models_list[i].predict(samples_list[i])
        next_list.append(next_val)
    return next_list

def Append_To_Col_Vect(col_vects, new_vals):
    appended_lists = []
    for i in range(len(col_vects)):
        list_i = col_vects[i]
        new_val = new_vals[i]
        list_i.append(new_val)
        appended_lists.append(list_i)
    return appended_lists

covid_census_cumulative_start = 0
global_preds_Metro = []
# Function adapted from: https://saattrupdan.github.io/2020-03-01-bootstrap-prediction/
def Prediction_Interval(model, X_train, y_train, x0, metro_only):
  global covid_census_cumulative_start
  global global_preds_Metro

  X_train = np.asarray(X_train)
  y_train = np.asarray(y_train)
  # Number of training samples
  n = X_train.shape[0]-1

  # The authors choose the number of bootstrap samples as the square root
  # of the number of samples
  nbootstraps = int(np.sqrt(n))

  # Compute the m_i's and the validation residuals
  bootstrap_preds, val_residuals = np.empty(nbootstraps), []
  for b in range(nbootstraps):
    train_idxs = np.random.choice(range(n), size = n, replace = True)
    val_idxs = np.array([idx for idx in range(n) if idx not in train_idxs])
    model.fit(X_train[train_idxs, :], y_train[train_idxs])
    preds = model.predict(X_train[val_idxs])
    val_residuals.append(y_train[val_idxs] - preds)
    bootstrap_preds[b] = model.predict(x0)
  bootstrap_preds -= np.mean(bootstrap_preds)
  val_residuals = np.concatenate(val_residuals)

  # Compute the prediction and the training residuals
  model.fit(X_train, y_train)
  preds = model.predict(X_train)
  if metro_only:
    global_preds_Metro = preds
  covid_census_cumulative_start = preds[-1]
  train_residuals = y_train - preds

  # Take percentiles of the training- and validation residuals to enable
  # comparisons between them
  val_residuals = np.quantile(val_residuals, q = np.arange(100)/100)
  train_residuals = np.quantile(train_residuals, q = np.arange(100)/100)

  # Compute the .632+ bootstrap estimate for the sample noise and bias
  no_information_error = np.mean(np.abs(np.random.permutation(y_train) - \
    np.random.permutation(preds)))
  generalisation = np.abs(val_residuals - train_residuals)
  no_information_val = np.abs(no_information_error - train_residuals)
  relative_overfitting_rate = np.mean(generalisation / no_information_val)
  weight = .632 / (1 - .368 * relative_overfitting_rate)
  residuals = (1 - weight) * train_residuals + weight * val_residuals

  # Construct the C set and get the percentiles
  C = np.array([m + o for m in bootstrap_preds for o in residuals])

  percentiles = np.quantile(C, q = required_covid_census_quantiles)

  y_hat = model.predict(x0)
  #percentiles = [y_hat + percentile for percentile in percentiles]
  return percentiles, y_hat

stop = int(len(daily_covid_census_arr_checker)) + 40 - 1
start = len(daily_covid_census) - 1
    
quantiles_covid_census = []
points_covid_census = []

daily_covid_census_for_ridge_orig = daily_covid_census_for_ridge[:]

# Make ~6 weeks worth of predictions
t = start
while t < stop:
    # Make predictions
    this_day_quantiles_covid_census, this_day_point_covid_census = Prediction_Interval(ridge_covid_census, daily_covid_census_pred_matrix, daily_covid_census_for_ridge_orig, Create_Samples([daily_covid_census_for_ridge]), True)
 
    # Append predictions to model
    daily_covid_census_for_ridge, daily_covid_census = Append_To_Col_Vect([daily_covid_census_for_ridge, daily_covid_census], [this_day_point_covid_census, this_day_point_covid_census])

     
    # Quantiles are additive - like SDev
    quantiles_covid_census.append(this_day_quantiles_covid_census)
    points_covid_census.append(this_day_point_covid_census)


    t += 1

points_covid_census_aggregate = points_covid_census#dailyToCumulative([(points_covid_census, covid_census_cumulative_start + covid_census[-1])])


########################################################
#                                                      #
#                   Sort Quantiles                     #
#                                                      #
########################################################

# Separate lists for each quantile, for recording data in csv
# "dc" for "death count"
quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99 = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []


# A list of quantile lists for deaths
covid_census_quant_list = [quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99]


covid_census_quant_list_aggregate = []
summed_covid_census_quantiles = []


for i in range(len(quantiles_covid_census)):
    # Get quantiles for each individual day
    daily_quantiles = quantiles_covid_census[i]

    # Add these quantiles to the day's point prediction to get prediction spectrum
    summed_covid_census_quantiles.append((daily_quantiles + points_covid_census_aggregate[i]).tolist())
    

# Index over every day of summed quantiles
for i in range(len(summed_covid_census_quantiles)):
    # Set a single day's quantiles
    this_days_quantiles = summed_covid_census_quantiles[i]

    # Index over each quantile for that day, and append it to the appropriate chronological quantile list
    for j in range(len(this_days_quantiles)):
        covid_census_quant_list[j].append(this_days_quantiles[j])


################################################
#                                              #
#               Plot Quantiles                 #
#                                              #
################################################

Model_Baseline([points_covid_census], [daily_covid_census])

#for i in range(41):
#    daily_covid_census_arr_checker.append(0)

#for i in range(start):
#    covid_census_quant_list[2].insert(0, 0)
#    covid_census_quant_list[-3].insert(0, 0)

#print(deaths_quant_list_aggregate[2])

upper_bounds = []
lower_bounds = []

percent = .5
for i in range(start, start+28):
    if i == start + 14:
        percent = 1.
    if i > 0:
        upper_bounds.append((covid_census[i] + percent*covid_census[i]))
        lower_bounds.append((covid_census[i] - percent*covid_census[i]))

    else:
        upper_bounds.append(covid_census[i] + percent*covid_census[i])
        lower_bounds.append(covid_census[i] - percent*covid_census[i])


for i in range(1, len(upper_bounds)):
    upper_bounds[i] = upper_bounds[i][0]
    lower_bounds[i] = lower_bounds[i][0]

#print(upper_bounds)
#print(type(upper_bounds[0]))
x = [i for i in range(start, start+28)]

for i in range(len(global_preds_Metro)):
    covid_census[order+i-1] = global_preds_Metro[i]
'''
x_dates = []
start_date = datetime.strptime('03/30/2020', '%m/%d/%Y').date()
for i in range(start+28):
    x_dates.append(start_date+timedelta(days=i))
#print(x_dates)
'''

fig, ax1 = plt.subplots()

y_pos = np.arange(len(dates))

ax1.plot(covid_census[:start+28], color = 'g', label = 'model')
ax1.fill_between(x, upper_bounds, lower_bounds, facecolor='.45', label = 'error bounds', alpha = .25)
ax1.plot([i for i in range(len(covid_census_arr_checker))], covid_census_arr_checker, 'k', label = 'data')
ax1.plot([i for i in range(start, start+28)], covid_census_quant_list[1][:28], c = '.35', linestyle = '--', label = '95% prediction bounds')#start+28])
ax1.plot([i for i in range(start, start+28)], covid_census_quant_list[-2][:28], c = '.35', linestyle = '--')#[:start+28])
ax1.plot(covid_census_arr_checker[:start+28], 'k', label = 'data')
ax1.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
ax1.set_xlabel('Dates')
ax1.set_ylabel('COVID Census')
ax1.set_title('COVID Hospitalization Census\nMetro Health')
ax1.legend(loc = 'upper left')

ax1.set_xticks(y_pos)
ax1.set_xticklabels(dates, rotation=75, fontsize = 6)
ax1.xaxis.set_major_locator(plt.MultipleLocator(14))
ax1.xaxis.set_minor_locator(plt.MultipleLocator(1))
#plt.savefig('GR_covid_census_quantile.pdf')
plt.show()

########################################################
'''
fig, ax1 = plt.subplots()

y_pos = np.arange(len(dates))

ax1.plot(combined_covid_census[:start+28], color = 'g', label = 'model')
ax1.fill_between(x, combined_upper_bounds, combined_lower_bounds, facecolor='.45', label = 'error bounds', alpha = .25)
ax1.plot([i for i in range(start, start+28)], combined_covid_census_quant_list[1][:28], c = '.35', linestyle = '--', label = '95% prediction bounds')#start+28])
ax1.plot([i for i in range(start, start+28)], combined_covid_census_quant_list[-2][:28], c = '.35', linestyle = '--')#[:start+28])
ax1.plot(combined_covid_census_arr_checker[:start+28], 'k', label = 'data')
ax1.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
ax1.set_xlabel('Dates')
ax1.set_ylabel('COVID Census')
ax1.set_title('COVID Hospitalization Census\nCombined')
ax1.legend(loc = 'upper left')

ax1.set_xticks(y_pos)
ax1.set_xticklabels(dates, rotation=75, fontsize = 6)
ax1.xaxis.set_major_locator(plt.MultipleLocator(14))
ax1.xaxis.set_minor_locator(plt.MultipleLocator(1))
#plt.savefig('GR_covid_census_quantile.pdf')
plt.show()
'''
########################################################
#                                                      #
#                    Data Writing                      #
#                                                      #
########################################################

covid_census = covid_census[:start+28]
covid_census = covid_census[-28:]

date_today = calendar.today()
forecast_date = date_today# - timedelta(days = 3)
date_saturday = (date_today)# - timedelta(days = 3))
hosp_count_list = []

daily_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'type', 'value'])

for i in range(28):
    target_end_date = (date_saturday + timedelta(days = i+1)).strftime("%Y-%m-%d")
    pred_day = i

    #if i == 0:
    #    hosp_inc = points_covid_census_aggregate[pred_day] - points_covid_census_aggregate[0]
    #else: 
    #    hosp_inc = points_covid_census_aggregate[pred_day] - hosp_count_list[-1]
    #if hosp_inc < 0:
    #    hosp_inc = [0.]
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'lower 95% quantile bound', covid_census_quant_list[1][pred_day]]], columns = daily_frame.columns))
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'upper 95% quantile bound', covid_census_quant_list[-2][pred_day]]], columns = daily_frame.columns))
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'lower error bound', lower_bounds[pred_day]]], columns = daily_frame.columns))
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'upper error bound', upper_bounds[pred_day]]], columns = daily_frame.columns))
    if i == 0:
        daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'point prediction', covid_census[pred_day]]], columns = daily_frame.columns))
    else:
        daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'point prediction', covid_census[pred_day][0]]], columns = daily_frame.columns))

daily_frame.to_csv('MetroCsvs/Metro_prediction_updated' + date_today.strftime("%Y-%m-%d") + '.csv')
'''
#######################################################
#                                                     #
#                Cumulative Writing                   #
#                                                     #
#######################################################

combined_covid_census = combined_covid_census[:start+28]
combined_covid_census = combined_covid_census[-28:]

cumulative_hosp_count_list = []

cumulative_daily_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'type', 'value'])

for i in range(28):
    target_end_date = (date_saturday + timedelta(days = i+1)).strftime("%Y-%m-%d")
    pred_day = i

    #if i == 0:
    #    hosp_inc = points_covid_census_aggregate[pred_day] - points_covid_census_aggregate[0]
    #else: 
    #    hosp_inc = points_covid_census_aggregate[pred_day] - hosp_count_list[-1]
    #if hosp_inc < 0:
    #    hosp_inc = [0.]
    cumulative_daily_frame = cumulative_daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'lower 95% quantile bound', combined_covid_census_quant_list[1][pred_day]]], columns = cumulative_daily_frame.columns))
    cumulative_daily_frame = cumulative_daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'upper 95% quantile bound', combined_covid_census_quant_list[-2][pred_day]]], columns = cumulative_daily_frame.columns))
    cumulative_daily_frame = cumulative_daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'lower error bound', combined_lower_bounds[pred_day]]], columns = cumulative_daily_frame.columns))
    cumulative_daily_frame = cumulative_daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'upper error bound', combined_upper_bounds[pred_day]]], columns = cumulative_daily_frame.columns))
    if i == 0:
        cumulative_daily_frame = cumulative_daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'point prediction', combined_covid_census[pred_day]]], columns = cumulative_daily_frame.columns))
    else:
        cumulative_daily_frame = cumulative_daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead COVID census'.format(day = i+1), target_end_date, 'point prediction', combined_covid_census[pred_day][0]]], columns = cumulative_daily_frame.columns))

cumulative_daily_frame.to_csv('MetroCsvs/cumulative_prediction_updated' + date_today.strftime("%Y-%m-%d") + '.csv')
'''
