import matplotlib.pyplot as plt
import seaborn as sn
import pandas as pd
import glob
import numpy as np
from datetime import datetime

df = pd.read_csv('gen_csvs/GrandRapids_Covid_Aggregated_20201204.csv')

print(df)
covid_admissions = df['Metro_Covid_Admissions'].loc[7:]
covid_admissions = covid_admissions.tolist()
covid_hosp_agg = [0]

case_onset_df = pd.read_csv('gen_csvs/onset_cases.csv')
case_onset_Kent = case_onset_df.loc[case_onset_df['COUNTY'] == "Kent"]
case_onset_Kent = case_onset_Kent.loc[case_onset_Kent['CASE_STATUS'] == "Confirmed"]
case_onset_Kent = case_onset_Kent[30:]
case_onset_Kent = case_onset_Kent["Cases"].values
#print(case_onset_Kent)

case_onset_Kent = case_onset_Kent[:len(covid_admissions)+1]

fig, ax1 = plt.subplots()
ax1.plot([i for i in range(len(covid_admissions))], covid_admissions, color = 'k', label = 'hosp')
ax1.set_ylabel('Hospital Admissions')
ax2 = ax1.twinx()
ax2.plot([i for i in range(len(case_onset_Kent))], case_onset_Kent, color = 'c', label = 'cases')
ax1.set_xlabel('Days since March 30')
ax2.set_ylabel('Cases by Onset')
ax2.legend()
plt.show()

excess = len(case_onset_Kent) % 7
covid_admissions = covid_admissions[excess:]
case_onset_Kent = case_onset_Kent[excess:]
#ratio_list = ratio_list[excess:]

covid_admissions_wkly = []
percent_wkly = []
case_onset_Kent_wkly = []

n_weeks = int(len(covid_admissions)/7)
for i in range(n_weeks):
    covid_admissions_wkly.append(np.sum(covid_admissions[7*i:7*(i+1)])/7)
    case_onset_Kent_wkly.append(np.sum(case_onset_Kent[7*i:7*(i+1)])/7)
    #ratio_list_wkly.append(np.sum(ratio_list[7*i:7*(i+1)])/7)

fig, ax3 = plt.subplots()
ax3.plot([i for i in range(len(covid_admissions_wkly))], covid_admissions_wkly, color = 'k', label = 'hosp')
ax3.set_ylabel('Hospital Admissions')
ax4 = ax3.twinx()
ax4.plot([i for i in range(len(case_onset_Kent_wkly))], case_onset_Kent_wkly, color = 'c', label = 'cases')
ax3.set_xlabel('Weeks since March 30')
ax4.set_ylabel('Cases by Onset Date')
ax4.legend()
plt.show()
'''
def Get_File_Date(file_name):
    month, day, year = file[-14:-12], file[-11:-9], file[-8:-4]
    return month, day, year

# List and sort JHU data files

path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

confirmedCases, deaths, recovered, active = [], [], [], []
day_count = 0

Kent_cases = []
# Loop over JHU data, and update cases, deaths, recoveries
for file in file_set:

        file_mo, file_day, file_yr = Get_File_Date(file)

        if (int(file_mo) == 3 and int(file_day) > 29) or (int(file_mo) > 3):
            data = pd.read_csv( file, index_col = None, header = 0 )
            data_MI = data.loc[data['Province_State'] == 'Michigan']
            data_Kent = data_MI.loc[data_MI['Admin2'] == 'Kent']

            Kent_cases.append(data_Kent['Confirmed'].values[0])

        day_count += 1

Kent_cases_incident = [0]
for i in range(1, len(Kent_cases)):
    Kent_cases_incident.append(Kent_cases[i] - Kent_cases[i-1])
covid_admissions.insert(0,0)

#sample_df = pd.DataFrame(list(zip(Kent_cases_incident[:len(covid_admissions)+1], covid_admissions)), columns = ['cases', 'hosps'])
#corrMatrix = sample_df.corr()
#sn.heatmap(corrMatrix, annot=True)
#plt.show()

testing_df = pd.read_csv('gen_csvs/County_Testing.csv')
Kent_df = testing_df.loc[testing_df['COUNTY'] == 'Kent']
Kent_df = Kent_df.iloc[7:]
#print(Kent_df)
cases = Kent_df['Positive'].tolist()
total = Kent_df['Total'].tolist()
percent = [cases[i]/total[i] for i in range(len(total))]
#print(percent)

MI_total = []
for i in range(len(cases)):
    this_date = Kent_df.iloc[i]['MessageDate']
    tests_on_this_date = testing_df.loc[testing_df['MessageDate'] == this_date]['Total'].values
    MI_total.append(np.sum(tests_on_this_date))

ratio_list = [total[i]/MI_total[i] for i in range(len(total))]

fig, ax1 = plt.subplots()
ax1.plot([i for i in range(len(covid_admissions))], covid_admissions, color = 'k', label = 'hosp')
ax1.set_ylabel('Hospital Admissions')
ax2 = ax1.twinx()
ax2.plot([i for i in range(len(ratio_list[:len(covid_admissions)+1]))], ratio_list[:len(covid_admissions)+1], color = 'c', label = 'test')
ax1.set_xlabel('Days since March 30')
ax2.set_ylabel('Kent/State Test Ratio')
ax2.legend()
plt.show()

fig, ax1 = plt.subplots()
ax1.plot([i for i in range(len(covid_admissions))], covid_admissions, color = 'k', label = 'hosp')
ax1.set_ylabel('Hospital Admissions')
ax2 = ax1.twinx()
ax2.plot([i for i in range(len(percent[:len(covid_admissions)+1]))], percent[:len(covid_admissions)+1], color = 'c', label = 'test')
ax1.set_xlabel('Days since March 30')
ax2.set_ylabel('Positive Test Percentage')
ax2.legend()
plt.show()

percent = percent[:len(covid_admissions)+1]
excess = len(percent) % 7
covid_admissions = covid_admissions[excess:]
percent = percent[excess:]
ratio_list = ratio_list[excess:]

covid_admissions_wkly = []
percent_wkly = []
ratio_list_wkly = []

n_weeks = int(len(covid_admissions)/7)
for i in range(n_weeks):
    covid_admissions_wkly.append(np.sum(covid_admissions[7*i:7*(i+1)])/7)
    percent_wkly.append(np.sum(percent[7*i:7*(i+1)])/7)
    ratio_list_wkly.append(np.sum(ratio_list[7*i:7*(i+1)])/7)

fig, ax3 = plt.subplots()
ax3.plot([i for i in range(len(covid_admissions_wkly))], covid_admissions_wkly, color = 'k', label = 'hosp')
ax3.set_ylabel('Hospital Admissions')
ax4 = ax3.twinx()
ax4.plot([i for i in range(len(ratio_list_wkly))], ratio_list_wkly, color = 'c', label = 'test')
ax3.set_xlabel('Weeks since March 30')
ax4.set_ylabel('Kent/State Test Ratio')
ax4.legend()
plt.show()

fig, ax3 = plt.subplots()
ax3.plot([i for i in range(len(covid_admissions_wkly))], covid_admissions_wkly, color = 'k', label = 'hosp')
ax3.set_ylabel('Hospital Admissions')
ax4 = ax3.twinx()
ax4.plot([i for i in range(len(percent_wkly))], percent_wkly, color = 'c', label = 'test')
ax3.set_xlabel('Weeks since March 30')
ax4.set_ylabel('Positive Test Percentage')
ax4.legend()
plt.show()
'''
