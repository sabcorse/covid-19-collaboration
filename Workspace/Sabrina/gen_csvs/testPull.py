#i-*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import requests
import os
import glob
import urllib2

# Read in the URL
testdata = urllib2.urlopen('https://raw.githubusercontent.com/owid/covid-19-data/master/public/data/testing/covid-testing-all-observations.csv')
datatowrite = testdata.read()

with open('covid_test_data.csv', 'wb') as f:
    f.write(datatowrite)
