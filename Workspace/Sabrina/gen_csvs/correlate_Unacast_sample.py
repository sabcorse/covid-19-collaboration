import pandas as pd
import seaborn as sn
import numpy as np
import matplotlib.pyplot as plt 

mobility_data = pd.read_csv('UM_USSDS_MI.csv')
FIPS_list = mobility_data['county_fips'].unique().tolist()
pops_list = mobility_data['county_population'].unique().tolist()
dates_list = mobility_data['date'].unique().tolist()

MI_pop = np.sum(pops_list)
pop_fracs = pops_list/MI_pop

daily_distance_diffs_state = []
# Commenting out for now - too many NaNs, but probably correlated anyway
#daily_visitation_diffs_state = []
encounters_rate_state = []

for date in dates_list:
    rows_today = mobility_data.loc[mobility_data['date'] == date]
    #print(rows_today)
    distance_diff_today = 0
    encounters_rate_today = 0
    for i in range(len(FIPS_list)):
        this_fips = FIPS_list[i]
        weight = pop_fracs[i]
        this_distance_diff = rows_today.loc[rows_today['county_fips'] == this_fips]['daily_distance_diff'].values[0]
        this_encounters_rate = rows_today.loc[rows_today['county_fips'] == this_fips]['encounters_rate'].values[0]
        distance_diff_today += weight * this_distance_diff
        encounters_rate_today += weight * this_encounters_rate

    daily_distance_diffs_state.append(distance_diff_today)
    encounters_rate_state.append(encounters_rate_today)

count = 0
for date in dates_list:
    if date == '3/22/20':
        first_date_row = count
        daily_distance_diffs_state = daily_distance_diffs_state[count:]
        encounters_rate_state = encounters_rate_state[count:]
    count += 1

# Social distancing data not updated every day. Find difference b/w today & most recent update
most_recent_date = dates_list[-1]
most_recent_month = int(most_recent_date[0:1])
most_recent_day = int(most_recent_date[2:4])
most_recent_year = int(most_recent_date[5:]) + 2000
social_distancing_reach_back = 0
#social_distancing_reach_back = (calendar.today() - calendar(most_recent_year, most_recent_month, most_recent_day)).days
#print('Will reach ' + str(social_distancing_reach_back) + ' days back for social distancing')

print(daily_distance_diffs_state)

df_new = pd.DataFrame(list(zip(daily_distance_diffs_state, encounters_rate_state)), columns = ['Distances', 'Encounters'])

print(df_new)

corrMatrix = df_new.corr()
print(corrMatrix)
sn.heatmap(corrMatrix, annot=True)
plt.show()
