#i-*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import requests
import os
import glob

# Read in the URLs (states covered under province URL)
url_country = "https://www.worldometers.info/world-population/population-by-country/"
#url_state = "https://www.citypopulation.de/php/usa-states-admin.php"
url_province = "https://en.wikipedia.org/wiki/List_of_country_subdivisions_by_population"

# Pretend to be a browser
header = {
  "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
  "X-Requested-With": "XMLHttpRequest"
}

# As browser, request read access
r_country = requests.get(url_country, headers = header)
#r_state = requests.get(url_state, headers = header)
r_province = requests.get(url_province, headers = header)

# Read HTML files as text
pop_df_country = pd.read_html(r_country.text)
#pop_df_state = pd.read_html(r_state.text)
pop_df_province = pd.read_html(r_province.text)

# Create data frames using index of data in df lists
data_country = pop_df_country[0]
#data_state = pop_df_state[0]
data_province = pop_df_province[1]

# Delete unnecessary data
for column in data_country.columns:
    if not (column == 'Country (or dependency)') and not (column == 'Population (2020)'):
        del data_country[column]

#for column in data_state.columns:
#    if not (column == 'Name') and not (column == 'PopulationEstimate2019-07-01'):
#        del data_state[column]

for column in data_province.columns:
    if not (column == 'Subdivision') and not (column == 'Population'):
        del data_province[column]

# Rename columns
#data_state.columns = ['Region', 'Population']
data_country.columns = ['Region', 'Population']
data_province.columns = ['Region', 'Population']

# Rename values to match JHU
#data_state = data_state[data_state.index != 51]
data_country.set_value(2, 'Region', 'US')
data_country.set_value(20, 'Region', 'UK')

# Create separate CSVs
#data_state.to_csv('state_pop.csv', index = False, encoding = 'utf-8')
data_country.to_csv('country_pop.csv', index = False, encoding = 'utf-8')
data_province.to_csv('province_pop.csv', index = False, encoding = 'utf-8')

# Concatenate CSVs
files = ['country_pop.csv', 'province_pop.csv']
data = pd.concat([pd.read_csv(f) for f in files])
data.to_csv('world_pop.csv', index = False, encoding = 'utf-8')
