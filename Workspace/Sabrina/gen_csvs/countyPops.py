#i-*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import requests
import os
import glob

# Read in the URLs (states covered under province URL)
url_country = "https://www.michigan-demographics.com/counties_by_population"

# Pretend to be a browser
header = {
  "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
  "X-Requested-With": "XMLHttpRequest"
}

# As browser, request read access
req = requests.get(url_country, headers = header)

# Read HTML files as text
pop_df = pd.read_html(req.text)

# Create data frames using index of data in df lists
data = pop_df[0]

# Create separate CSVs
#data_state.to_csv('state_pop.csv', index = False, encoding = 'utf-8')
data.to_csv('MI_counties_pop.csv', index = False, encoding = 'utf-8')
