# Plot ridge regression model output
# MCE, 11-30-20 - update 1-5-21

library(readr)
library(lhs)
library(SobolSequence)
library(deSolve)
library(tidyverse)
library(sf)
library(zoo)
library(tidycensus)
library(ggplot2)
library(gridExtra)
library(compiler)
library(dplyr)
options(scipen=999) # increased penalty on scientific notation for ggplot 
enableJIT(3)        # enables just-in-time compiling

year = "2021"
monthday = "05-17"

# ======= Load data ======= #

### Ridge regression training data & forecasts ###
trainingData = read_csv(paste0("JHU_csvs/Agg-JHU_training-",monthday,"-",year,"_MERC.csv"),col_types = cols())
modelDaily = read_csv(paste0("daily_and_weekly_csvs/",year,"-",monthday,"-agg_daily_MERC.csv"),col_types = cols())
modelWeekly = read_csv(paste0("daily_and_weekly_csvs/", year,"-",monthday,"-agg_wkly_MERC.csv"),col_types = cols())

trainingData$date <- as.Date(trainingData$date, '%m/%d/%Y')
modelDaily$target_end_date <- as.Date(modelDaily$target_end_date, '%m/%d/%Y')
modelWeekly$target_end_date <- as.Date(modelWeekly$target_end_date, '%m/%d/%Y')

# Pops from MISafeStart_companioncode_conf_prob_v2.R
MERCregionpops = c("Detroit" = 5242200, "Grand Rapids" = 1510695, "Jackson" = 303217, "Kalamazoo" = 957373, "Lansing" = 586175, 
                   "Saginaw" = 612392, "Traverse City" = 441634+303802, "Upper Peninsula" = 303802)/1000000
MERCregionpops = MERCregionpops[-8] #drop the last since we aren't using the UP currently

# Note the model data has a different order of the regions!
MERCregionpopsModel = MERCregionpops[c(1,2,4,6,5,7,3)]

# ======= Set up data for plotting ======= #

# Daily 
dailyCasesData = spread(trainingData[,c("date","cases","location")], location, cases)
dailyCasesData = cbind(tail(dailyCasesData$date,-1), as.data.frame(diff(as.matrix(dailyCasesData[,2:8]))) )
names(dailyCasesData)[1]="date"
dailyCasesData = gather(dailyCasesData, location, cases, Detroit:`Traverse City`)

dailyDeathsData = spread(trainingData[,c("date","deaths","location")], location, deaths)
dailyDeathsData = cbind(tail(dailyDeathsData$date,-1), as.data.frame(diff(as.matrix(dailyDeathsData[,2:8]))) )
names(dailyDeathsData)[1]="date"
dailyDeathsData = gather(dailyDeathsData, location, deaths, Detroit:`Traverse City`)

modelDailyCases = modelDaily[grep("case", modelDaily$target), ]
modelDailyDeaths = modelDaily[grep("death", modelDaily$target), ]


# Weekly 

weeklyCasesData = spread(trainingData[,c("date","cases","location")], location, cases)
weeklyCasesData = cbind(tail(weeklyCasesData$date,-7), as.data.frame(rollsum(diff(as.matrix(weeklyCasesData[,2:8])),7)) )
names(weeklyCasesData)[1]="date"
len = length(weeklyCasesData$date)
weeklyCasesData = gather(weeklyCasesData, location, cases, Detroit:`Traverse City`)
weeklyCasesData = cbind(weeklyCasesData, "permil" = weeklyCasesData$cases/rep(MERCregionpops,each = len) )

weeklyDeathsData = spread(trainingData[,c("date","deaths","location")], location, deaths)
weeklyDeathsData = cbind(tail(weeklyDeathsData$date,-7), as.data.frame(rollsum(diff(as.matrix(weeklyDeathsData[,2:8])),7)) )
names(weeklyDeathsData)[1]="date"
len = length(weeklyDeathsData$date)
weeklyDeathsData = gather(weeklyDeathsData, location, deaths, Detroit:`Traverse City`)
weeklyDeathsData = cbind(weeklyDeathsData, "permil" = weeklyDeathsData$deaths/rep(MERCregionpops,each = len) )

modelWkQuantiles = spread(modelWeekly[grep("quantile", modelWeekly$type), ], quantile, value)
modelWkPoint = modelWeekly[grep("point", modelWeekly$type), ]

modelWkCaseQuantiles = modelWkQuantiles[grep("case", modelWkQuantiles$target),]
modelWkDeathQuantiles = modelWkQuantiles[grep("inc death", modelWkQuantiles$target),]
modelWkCasePoint = modelWkPoint[grep("case", modelWkPoint$target), ]
modelWkDeathPoint = modelWkPoint[grep("inc death", modelWkPoint$target), ]

modelWkCasePoint = mutate(modelWkCasePoint, permil = value/rep(MERCregionpopsModel,each = 4) )
modelWkDeathPoint = mutate(modelWkDeathPoint, permil = value/rep(MERCregionpopsModel,each = 4) )


# ======= Plot stuff ======= #

# Daily Cases
ggplot() +	
  geom_point(aes(x = dailyCasesData$date, y = dailyCasesData$cases, colour = dailyCasesData$location), size=1, alpha=0.5) +
  # geom_line(aes(x = dailyCasesData$date, y = c(rep(NA,6),rollmean(dailyCasesData$cases, 7)), colour = dailyCasesData$location), size=1) +
  geom_smooth(aes(x = dailyCasesData$date, y = dailyCasesData$cases, colour = dailyCasesData$location), size=1, method = "loess", span = 0.25, se=FALSE) +
  geom_vline(aes(xintercept = tail(dailyCasesData$date,1))) + 
  geom_label(aes(x = tail(dailyCasesData$date,1), y = 10000), label = "Forecast", hjust = "left", size = 3) + 
  geom_point(aes(x = modelDailyCases$target_end_date, y = modelDailyCases$value, colour = modelDailyCases$location), size = 1) + 
  geom_smooth(aes(x = modelDailyCases$target_end_date, y = modelDailyCases$value, colour = modelDailyCases$location), size = 1, method = "loess", se=FALSE) +
  # geom_smooth(aes(x = modelDailyCases$target_end_date, y = modelDailyCases$value, colour = modelDailyCases$location), size=1, method = "loess", span = 0.25, se=FALSE) +
  scale_color_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"), values=c("#800080","#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  labs(x = "", y="Daily Cases", colour="State") + 
  scale_x_date(date_breaks = "1 month", date_labels = "%b") + 
  theme_light(base_size = 15) +
  theme(aspect.ratio=9/16)
ggsave("Rplots_MERC/daily_cases.pdf")

# Daily Deaths
ggplot() + 
  geom_point(aes(x = dailyDeathsData$date, y = dailyDeathsData$deaths, colour = dailyDeathsData$location), size=1, alpha=0.5) +
  # geom_line(aes(x = dailyDeathsData$date, y = c(rep(NA,6),rollmean(dailyDeathsData$cdeaths, 7)), colour = dailyDeathsData$location), size=1) +
  geom_smooth(aes(x = dailyDeathsData$date, y = dailyDeathsData$deaths, colour = dailyDeathsData$location), size=1, alpha=0.5, method = "loess", span = 0.25, se=FALSE) +
  geom_vline(aes(xintercept = tail(dailyCasesData$date,1))) + 
  geom_label(aes(x = tail(dailyCasesData$date,1), y = 150), label = "Forecast", hjust = "left", size = 3) + 
  geom_point(aes(x = modelDailyDeaths$target_end_date, y = modelDailyDeaths$value, colour = modelDailyDeaths$location), size = 1) + 
  geom_smooth(aes(x = modelDailyDeaths$target_end_date, y = modelDailyDeaths$value, colour = modelDailyDeaths$location), size=1, method = "loess", se=FALSE) + 
  scale_color_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  labs(x = "", y="Daily Deaths", colour="State") + 
  ylim(0,150) + 
  scale_x_date(date_breaks = "1 month", date_labels = "%b") + 
  theme_light(base_size = 15) +
  theme(aspect.ratio=9/16)
ggsave("Rplots_MERC/daily_deaths.pdf")

# Weekly Cases
ggplot() + 
  geom_point(aes(x = weeklyCasesData$date, y = weeklyCasesData$cases, colour = weeklyCasesData$location), size=1) +
  # geom_smooth(aes(x = weeklyCasesData$date, y = weeklyCasesData$cases, colour = weeklyCasesData$location), size=1, alpha=0.5, method = "loess", span = 0.1, se=FALSE) +
  geom_line(aes(x = modelWkCasePoint$target_end_date, y = modelWkCasePoint$value, colour = modelWkCasePoint$location), size = 1) + 
  # geom_point(aes(x = modelWkCasePoint$target_end_date, y = modelWkCasePoint$value, colour = modelWkCasePoint$location), size = 1) + 
  # geom_ribbon(aes(x = modelWkCaseQuantiles$target_end_date, ymin = modelWkCaseQuantiles$`0.25`, ymax = modelWkCaseQuantiles$`0.75`, fill = modelWkCaseQuantiles$location), alpha = 0.5) +
  geom_ribbon(aes(x = modelWkCaseQuantiles$target_end_date, ymin = modelWkCaseQuantiles$`0.025`, ymax = modelWkCaseQuantiles$`0.975`, fill = modelWkCaseQuantiles$location), alpha = 0.25) + 
  scale_color_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  scale_fill_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  labs(x = "", y="Weekly Cases (rolling)", colour="State", fill = "State") + 
  scale_x_date(date_breaks = "1 month", date_labels = "%b") + 
  theme_light(base_size = 15) +
  theme(aspect.ratio = 9/16)
ggsave("Rplots_MERC/weekly_cases.pdf")

# Weekly Deaths
ggplot() + 
  geom_point(aes(x = weeklyDeathsData$date, y = weeklyDeathsData$deaths, colour = weeklyDeathsData$location), size=1) +
  #geom_smooth(aes(x = weeklyDeathsData$date, y = weeklyDeathsData$cases, colour = weeklyDeathsData$location), size=1, alpha=0.5, method = "loess", span = 0.1, se=FALSE) +
  geom_line(aes(x = modelWkDeathPoint$target_end_date, y = modelWkDeathPoint$value, colour = modelWkDeathPoint$location), size = 1) + 
  # geom_ribbon(aes(x = modelWkDeathQuantiles$target_end_date, ymin = modelWkDeathQuantiles$`0.25`, ymax = modelWkDeathQuantiles$`0.75`, fill = modelWkDeathQuantiles$location), alpha = 0.5) +
  geom_ribbon(aes(x = modelWkDeathQuantiles$target_end_date, ymin = modelWkDeathQuantiles$`0.025`, ymax = modelWkDeathQuantiles$`0.975`, fill = modelWkDeathQuantiles$location), alpha = 0.25) + 
  scale_color_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  scale_fill_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  labs(x = "", y="Weekly Deaths (rolling)", colour="State", fill = "State") + 
  scale_x_date(date_breaks = "1 month", date_labels = "%b") + 
  theme_light(base_size = 15) +
  theme(aspect.ratio = 9/16)
ggsave("Rplots_MERC/weekly_deaths.pdf")

# 7-day Average Cases
ggplot() + 
  geom_point(aes(x = weeklyCasesData$date, y = weeklyCasesData$cases/7, colour = weeklyCasesData$location), size=1) +
  # geom_smooth(aes(x = weeklyCasesData$date, y = weeklyCasesData$cases, colour = weeklyCasesData$location), size=1, alpha=0.5, method = "loess", span = 0.1, se=FALSE) +
  geom_line(aes(x = modelWkCasePoint$target_end_date, y = modelWkCasePoint$value/7, colour = modelWkCasePoint$location), size = 1) + 
  # geom_point(aes(x = modelWkCasePoint$target_end_date, y = modelWkCasePoint$value, colour = modelWkCasePoint$location), size = 1) + 
  # geom_ribbon(aes(x = modelWkCaseQuantiles$target_end_date, ymin = modelWkCaseQuantiles$`0.25`, ymax = modelWkCaseQuantiles$`0.75`, fill = modelWkCaseQuantiles$location), alpha = 0.5) +
  # geom_ribbon(aes(x = modelWkCaseQuantiles$target_end_date, ymin = modelWkCaseQuantiles$`0.025`/7, ymax = modelWkCaseQuantiles$`0.975`/7, fill = modelWkCaseQuantiles$location), alpha = 0.25) + 
  scale_color_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  scale_fill_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  labs(x = "", y="7-day Avg. Daily Cases", colour="State", fill = "State") + 
  scale_x_date(date_breaks = "1 month", date_labels = "%b") + 
  theme_light(base_size = 15) +
  theme(aspect.ratio = 9/16)
ggsave("Rplots_MERC/seven_day_avg_cases.pdf")

# 7-day Average Deaths
ggplot() + 
  geom_point(aes(x = weeklyDeathsData$date, y = weeklyDeathsData$deaths/7, colour = weeklyDeathsData$location), size=1) +
  # geom_smooth(aes(x = weeklyDeathsData$date, y = weeklyDeathsData$cases/7, colour = weeklyDeathsData$location), size=1, alpha=0.5, method = "loess", span = 0.1, se=FALSE) +
  geom_line(aes(x = modelWkDeathPoint$target_end_date, y = modelWkDeathPoint$value/7, colour = modelWkDeathPoint$location), size = 1) + 
  # geom_ribbon(aes(x = modelWkDeathQuantiles$target_end_date, ymin = modelWkDeathQuantiles$`0.25`, ymax = modelWkDeathQuantiles$`0.75`, fill = modelWkDeathQuantiles$location), alpha = 0.5) +
  # geom_ribbon(aes(x = modelWkDeathQuantiles$target_end_date, ymin = modelWkDeathQuantiles$`0.025`/7, ymax = modelWkDeathQuantiles$`0.975`/7, fill = modelWkDeathQuantiles$location), alpha = 0.25) + 
  scale_color_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  scale_fill_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  labs(x = "", y="7-day Avg. Daily Deaths", colour="State", fill = "State") + 
  scale_x_date(date_breaks = "1 month", date_labels = "%b") + 
  theme_light(base_size = 15) +
  theme(aspect.ratio = 9/16)
ggsave("Rplots_MERC/seven_day_avg_deaths.pdf")

# 7-day average cases/million
ggplot() + 
  geom_point(aes(x = weeklyCasesData$date, y = weeklyCasesData$permil/7, colour = weeklyCasesData$location), size=1) +
  # geom_smooth(aes(x = weeklyCasesData$date, y = weeklyCasesData$cases, colour = weeklyCasesData$location), size=1, alpha=0.5, method = "loess", span = 0.1, se=FALSE) +
  geom_line(aes(x = modelWkCasePoint$target_end_date, y = modelWkCasePoint$permil/7, colour = modelWkCasePoint$location), size = 1) + 
  # geom_point(aes(x = modelWkCasePoint$target_end_date, y = modelWkCasePoint$value, colour = modelWkCasePoint$location), size = 1) + 
  # geom_ribbon(aes(x = modelWkCaseQuantiles$target_end_date, ymin = modelWkCaseQuantiles$`0.25`, ymax = modelWkCaseQuantiles$`0.75`, fill = modelWkCaseQuantiles$location), alpha = 0.5) +
  # geom_ribbon(aes(x = modelWkCaseQuantiles$target_end_date, ymin = modelWkCaseQuantiles$`0.025`/7, ymax = modelWkCaseQuantiles$`0.975`/7, fill = modelWkCaseQuantiles$location), alpha = 0.25) +
  scale_color_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  scale_fill_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  labs(x = "", y="7-day Avg. Daily Cases per Million", colour="State", fill = "State") + 
  scale_x_date(date_breaks = "1 month", date_labels = "%b") + 
  theme_light(base_size = 15)
ggsave("Rplots_MERC/seven_day_avg_cases_per_mil.pdf")

# 7-day average deaths/million
ggplot() + 
  geom_point(aes(x = weeklyDeathsData$date, y = weeklyDeathsData$permil/7, colour = weeklyDeathsData$location), size=1) +
  # geom_smooth(aes(x = weeklyDeathsData$date, y = weeklyDeathsData$deaths/7, colour = weeklyDeathsData$location), size=1, alpha=0.5, method = "loess", span = 0.1, se=FALSE) +
  geom_line(aes(x = modelWkDeathPoint$target_end_date, y = modelWkDeathPoint$permil/7, colour = modelWkDeathPoint$location), size = 1) + 
  # geom_ribbon(aes(x = modelWkDeathQuantiles$target_end_date, ymin = modelWkDeathQuantiles$`0.25`, ymax = modelWkDeathQuantiles$`0.75`, fill = modelWkDeathQuantiles$location), alpha = 0.5) +
  # geom_ribbon(aes(x = modelWkDeathQuantiles$target_end_date, ymin = modelWkDeathQuantiles$`0.025`/7, ymax = modelWkDeathQuantiles$`0.975`/7, fill = modelWkDeathQuantiles$location), alpha = 0.25) + 
  scale_color_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  scale_fill_manual(labels = c("Detroit", "Grand Rapids", "Jackson", "Kalamazoo", "Lansing", "Saginaw", "Traverse City + \nUpper Peninsula"),values=c("#800080", "#0077BB", "#33BBEE", "#009988","#F6C141", "#EE7733", "#CC3311")) +
  labs(x = "", y="7-day Avg. Daily Deaths per Million", colour="State", fill = "State") + 
  scale_x_date(date_breaks = "1 month", date_labels = "%b") + 
  theme_light(base_size = 15)
ggsave("Rplots_MERC/seven_day_avg_deaths_per_mil.pdf")
