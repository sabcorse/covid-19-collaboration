import matplotlib.pyplot as plt
import pandas as pd

df = pd.read_csv('Detroit/2021-02-15-daily.csv')
df = df.loc[df['target'].str.contains('case')]
#df_combined = pd.read_csv('cumulative_prediction_updated2021-01-25.csv')
#df_Kent = pd.read_csv('Kent_cases_training.csv')

#df_u_nf = df.loc[df['type'] == 'upper 95% quantile bound']['value'].tolist()
#df_l_nf = df.loc[df['type'] == 'lower 95% quantile bound']['value'].tolist()
df_u_e = df.loc[df['type'] == 'upper bound']['value'].tolist()
df_l_e = df.loc[df['type'] == 'lower bound']['value'].tolist()
df_pt = df.loc[df['type'] == 'point']['value'].tolist()

#df_combined_u_nf = df_combined.loc[df_combined['type'] == 'upper 95% quantile bound']['value'].tolist()
#df_combined_l_nf = df_combined.loc[df_combined['type'] == 'lower 95% quantile bound']['value'].tolist()
#df_combined_u_e = df_combined.loc[df_combined['type'] == 'upper error bound']['value'].tolist()
#df_combined_l_e = df_combined.loc[df_combined['type'] == 'lower error bound']['value'].tolist()
#df_combined_pt = df_combined.loc[df_combined['type'] == 'point prediction']['value'].tolist()

#df_Kent_cases = df_Kent['incident Kent cases'].tolist()

#plt.plot(df_u_nf)
#plt.plot(df_l_nf)
plt.plot(df_u_e)
plt.plot(df_l_e)
plt.plot(df_pt)
plt.show()

#plt.plot(df_combined_u_nf)
#plt.plot(df_combined_l_nf)
#plt.plot(df_combined_u_e)
#plt.plot(df_combined_l_e)
#plt.plot(df_combined_pt)
#plt.show()

#plt.plot(df_Kent_cases)
#plt.show()
