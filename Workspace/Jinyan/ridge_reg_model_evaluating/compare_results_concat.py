## Jinyan Miao
## University of Michigan
## March 2022

import numpy as np
import pandas as pd
import plotly.graph_objects as go
import datetime
import os

## ask for the date of the JHU data set
today_date = input("Actual Data's date: (format: MMDDYYYY)   ")
today_month = today_date[:2]
today_day = today_date[2:4]
today_year = today_date[4:]

## read the JHU data set
actual_data_df = pd.read_csv('results_csvs/Agg-JHU_training-'+today_month+'-'+today_day+'-'+today_year+'_MERC.csv')
actual_data_df.index = actual_data_df.date

## get the dates in the JHU data set
dates = actual_data_df[actual_data_df.location == "Detroit"].date.values.tolist()

## indices for extracting data
cum_death_pt_idx = ["1 wk ahead cum death",
                    "2 wk ahead cum death",
                    "3 wk ahead cum death",
                    "4 wk ahead cum death"]

inc_case_pt_dx = ["1 wk ahead inc case",
                  "2 wk ahead inc case",
                  "3 wk ahead inc case",
                  "4 wk ahead inc case"]


def get_actual_data_for_region(region, data_type, dates):
    """
    input:
        region: string, region name
        data_type: string, cases or deaths
        dates: list, a list of datetime
    output:
        a list of actual data of this region by dates
    """
    this_location_df = actual_data_df[actual_data_df.location == region]
    actual_this_region_data_list = []
    for date in dates:
        today = datetime.datetime.strptime(date, "%Y-%m-%d")
        today = str(today)[:10]
        try:
            actual_this_region_data_list.append(this_location_df.loc[today,data_type])
        except:
            actual_this_region_data_list.append(np.NaN)
    return actual_this_region_data_list

def save_actual_data_all_regions(regions):
    """
    input:
        regions: list, a list of the names of all regions
    output:
        a set of .csv files containing the actual data for each region is created
    """
    for region in regions:
        this_region_actual_data_df = pd.DataFrame()
        this_region_actual_data_df["dates"] = [date.replace('/','-') for date in dates]
        this_region_actual_data_df["actual_cases"] = get_actual_data_for_region(region, "cases", dates)
        this_region_actual_data_df["actual_deaths"] = get_actual_data_for_region(region, "deaths", dates)
        if not os.path.exists('actual_regions_csvs'):
            os.makedirs('actual_regions_csvs')
            print('\nNew directory created for generating .csv for actual covid data')
        this_region_actual_data_df.to_csv('actual_regions_csvs/'+region+'.csv')

def get_prediction_data_list(df, region, point_type, quantile_num, point_index):
    """
    input:
        df: dataframe, a prediction dataframe
        region: string, name of region, ex:"Detroit", "Lansing"
        point_type: string, type of this point, either "quantile", "point", "upper bound", or "lower bound"
        quantile_num: string, quantile number, ex:"0.05", "0.95", if not a quantile: "N"
        point_index: list of strings, name of the index, ex: cum_death_pt_idx
    output:
        a list of desired data, and a list of dates
    """
    this_df = df.loc[(df.location == region)
                      & (df.type == point_type)
                      & (df.target.isin(point_index))]
    if (quantile_num != "N"):
        this_df = this_df.loc[this_df["quantile"] == quantile_num]
    dates = this_df.target_end_date.values.tolist()
    data = this_df.value.values.tolist()
    return dates, data

def get_base_case(df, region):
    """
    input:
        df: dataframe, a prediction dataframe
        region: string, name of the region
    output:
        get the number of actual cases on the last day before the prediction
    """
    date = df.forecast_date.values.tolist()[0].replace('-','/')
    date = str(datetime.datetime.strptime(date, "%Y/%m/%d"))[:10]
    region_actual_df = pd.read_csv('actual_regions_csvs/'+region+'.csv')
    base_case = region_actual_df.loc[region_actual_df.dates == date].actual_cases.values.tolist()[0]
    return base_case

def generate_weekly_case_prediction_quantiles(df, region, this_quantile):
    """
    input:
        df: dataframe, a prediction dataframe
        region: string, name of the region
        quantile: float, the quantile that we want to extract
    output:
        a list of accumulative prediction quantiles for four predictions weeks
    """
    base_case = get_base_case(df, region)
    dates, pts = get_prediction_data_list(df, region, "point", "N", inc_case_pt_dx)
    dates, quantiles = get_prediction_data_list(df, region, "quantile", this_quantile, inc_case_pt_dx)
    accum_quantiles = [float(base_case)+float(quantiles[0]),
                       float(pts[0])+float(base_case)+float(quantiles[1]),
                       float(pts[0])+float(pts[1])+float(base_case)+float(quantiles[2]),
                       float(pts[0])+float(pts[1])+float(pts[2])+float(base_case)+float(quantiles[3])]
    return accum_quantiles

def generate_weekly_death_prediction_quantiles_and_dates(df, region, this_quantile):
    """
    input:
        df: dataframe, a prediction dataframe
        region: string, name of the region
        quantile: float, the quantile that we want to extract
    output:
        a list of prediction dates
        a list of accumulative prediction quantiles for four predictions weeks
    """
    dates, accum_quantiles = get_prediction_data_list(df, region, "quantile", this_quantile, cum_death_pt_idx)
    return [date.replace('/','-') for date in dates], [float(i) for i in accum_quantiles]

def concat_all_quantiles_into_df(df, region):
    """
    input:
        df: dataframe, a prediction dataframe
        region: string, name of the region
    output:
        create a concatinated dataframe that stores dates, accumulative 0.025 case/death quantiles,
        and accumulative 0.975 case/death quantiles for this region, for this prediction dataframe
    """
    df_concat = pd.DataFrame()
    df_concat["dates"], df_concat["death_025_quantiles"] = generate_weekly_death_prediction_quantiles_and_dates(df, region, 0.025)
    null, df_concat["death_975_quantiles"] = generate_weekly_death_prediction_quantiles_and_dates(df, region, 0.975)
    df_concat["case_025_quantiles"] = generate_weekly_case_prediction_quantiles(df, region, 0.025)
    df_concat["case_975_quantiles"] = generate_weekly_case_prediction_quantiles(df, region, 0.975)
    return df_concat

def process_all_prediction_dfs(dfs, regions):
    """
    input:
        dfs: a list a dataframes, a list that contains all the prediction dataframes
             each of which is a previous prediction model\
        regions: a list of regions, a list containing the name of the regions
    output:
        for each model, a .csv file is saved in the directory, containing the 4 quantiles and the dates
    """
    for df in dfs:
        for region in regions:
            try:
                processed_df = concat_all_quantiles_into_df(df, region)
                date = processed_df.dates.values.tolist()[0]
                if not os.path.exists('prediction_regions_csvs/'+region):
                    os.makedirs('prediction_regions_csvs/'+region)
                    print('\nNew directory created for generating .csv for prediction covid data for '+region)
                processed_df.to_csv('prediction_regions_csvs/'+region+'/'+date+'.csv')
            except:
                print("No data for this region on this date: "+region+" "+ df.forecast_date.values.tolist()[0])

## ask for the dates of the prediction dataframes
prediction_dated_dfs = []
prediction_date = "R"
while prediction_date != "N":
    prediction_date = input("Date of previous prediction: (format: MMDDYYYY)   ")
    if prediction_date == "N":
        break
    ## check if there exists such prediction dataframe,
    ## if yes, append it to the list
    try:
        prediction_month = prediction_date[:2]
        prediction_day = prediction_date[2:4]
        prediction_year = prediction_date[4:]
        prediction_df = pd.read_csv('results_csvs/'
                                    + prediction_year
                                    + '-'
                                    + prediction_month+ '-'
                                    + prediction_day
                                    + '-agg_wkly_MERC.csv')
        prediction_dated_dfs.append(prediction_df)
    except:
        print("No such file. Try again or enter N to exit.")

## name of the regions
regions = ['Detroit', 'Grand Rapids', 'Kalamazoo', 'Saginaw', 'Lansing', 'Traverse City', 'Jackson', 'Upper Peninsula']

save_actual_data_all_regions(regions)
process_all_prediction_dfs(prediction_dated_dfs, regions)
