## Jinyan Miao
## University of Michigan
## March 2022


import pandas as pd
import numpy as np
import plotly.graph_objects as go
import datetime
from datetime import timedelta
import os

def read_prediction_df(region, model_date):
    """
    input:
        date: string, indicate the date of the model
        region: string, indicate the region name
    output:
        return a dataframe that store the prediction data for this region this model
    """
    path = "prediction_regions_csvs/"+region+"/"+model_date.replace('/','-')+'.csv'
    df = pd.read_csv(path)
    return df

def get_date_region_actual_data(date, region, type):
    """
    input:
        date: string, the date of this model
        region: string, name of the region
        type: string, either "case" or "death"
    output:
        get the actual data point (number of cases or number of death)
        on this date, for this region
    """
    df = pd.read_csv('actual_regions_csvs/'+region+'.csv')
    pt = df.loc[df.dates == date]['actual_'+type+'s'].values.tolist()[0]
    return pt

def append_start_day(dates, lower_bounds, upper_bounds, model_date, region, type):
    """
    input:
        dates: list, containing the prediction dates
        lower_bounds: list, containing the 0.025 prediction quantiles
        upper_bounds: list, containing the 0.975 prediction quantiles
        model_date: string, the date of the prediction model (starting date)
        region: string, name of the region
        type: string, either "case" or "death"
    output:
        append the start date to dates
        appedn the initial case to lower_bounds and upper_bounds
    """
    model_date = datetime.datetime.strptime(model_date, "%Y-%m-%d")
    six_days = timedelta(days = 6)
    start_date = model_date - six_days
    start_date = str(start_date)[:10]
    pt = get_date_region_actual_data(start_date, region, type)
    dates.insert(0, start_date)
    lower_bounds.insert(0, pt)
    upper_bounds.insert(0, pt)


def plot_prediction_df(region, model_date, type, fig, i):
    """
    input:
        region: string, indicate the name of the region
        model_date: string, indicate the date of model
        type: string, indicate case or death
        fig: plotly graphic object, go.Figure()
        i: integer, represents the i-th model in this set
    output:
        add a two traces on fig, showing upper and lower bounds
    """
    df = read_prediction_df(region, model_date)
    dates = df.dates.values.tolist()
    type_025_quantiles = type+'_025_quantiles'
    type_975_quantiles = type+'_975_quantiles'
    upper_bounds = df[type_975_quantiles].values.tolist()
    lower_bounds = df[type_025_quantiles].values.tolist()
    append_start_day(dates, lower_bounds, upper_bounds, model_date, region, type)
    colorRGB = 'rgb('+str(120)+','+str(200+(i*5))+','+str(250-(i*5))+')'
    fig.add_trace(go.Scatter(x=dates, y=lower_bounds, mode='lines',
                             name=type+', ' + model_date, fill=None,
                             line_color=colorRGB,
                             legendgroup = type+' bounds for ' + model_date + ' model'))
    fig.add_trace(go.Scatter(x=dates, y=upper_bounds, mode='lines',
                             showlegend = False, name='0.975 '+type+' quantile', fill='tonexty',
                             line_color=colorRGB, legendgroup = type+' bounds for ' + model_date + ' model'))

def plot_actual_data(region, type, fig):
    """
    input:
        region: string, indicate the name of the region
        type: string, indicate case or death
        fig: plotly graphic object, go.Figure()
    output:
        add a trace on fig, scatter plotting the actual data point for this region
    """
    df = pd.read_csv('actual_regions_csvs/'+region+'.csv')
    dates = df.dates
    pts_col_name = 'actual_'+type+'s'
    pts = df[pts_col_name]
    fig.add_trace(go.Scatter(x=dates, y=pts,
                             mode='lines',line_color='rgb(255,30,30)',
                             name='actual '+type+'s'))

def get_model_dates():
    """
    output:
        return a list that contains the date of the models
    """
    model_dates = []
    entry = "R"
    while (entry != "N"):
        entry = input("Date of previous prediction: (format: MMDDYYYY)   ")
        if entry == "N":
            break
        try:
            prediction_month = entry[:2]
            prediction_day = entry[2:4]
            prediction_year = entry[4:]
            date = prediction_year+'-'+prediction_month+'-'+prediction_day
            null = read_prediction_df('Detroit', date)
            model_dates.append(date)
        except:
            print("No such file， try entering another date, or enter N to exit")
    return model_dates

def plot_all_date(type):
    """
    input:
        type: string, either "case" or "death"
    output:
        plots of case/death are saved in the directory
        for each of the regions separately
    """
    model_dates = get_model_dates()
    regions = ['Detroit', 'Grand Rapids',
               'Kalamazoo', 'Saginaw',
               'Lansing', 'Traverse City',
               'Jackson', 'Upper Peninsula']
    plot_y_print = "Number of "+type+'s'
    plot_x_print = "Dates"
    for region in regions:
        fig = go.Figure()
        title_plot = plot_y_print + ' for ' + region + ' region'
        model_count = 0
        for model_date in model_dates:
            try:
                plot_prediction_df(region, model_date, type, fig, model_count)
                fig.update_layout(title=title_plot,
                                  xaxis_title=plot_x_print,
                                  yaxis_title=plot_y_print,
                                  showlegend=True)
                model_count=model_count+1
            except:
                model_count=model_count+1
            # Create path to save the plot
        plot_actual_data(region, type, fig)
        if not os.path.exists('scatter_plots'):
            os.makedirs('scatter_plots')
            print('\nNew directory created for plotting scatter plots')
        fig.write_html('scatter_plots/'+region.replace(' ','_')+'_models.html',
                       auto_open=False)


plot_all_date("case")
