import numpy as np
import glob
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from scipy import signal

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

# Split country/state name input into separate words if necessary
def Split_Loc_Name(loc):
    i = 0
    while i < len(loc):
        # Scan over name, and split when capital letters found
        if loc[i].isupper() and not i == 0 and not loc == 'US':
            loc = loc[:i]+' '+loc[i:]
            i += 2
        else:
            i += 1
    return loc

# Convert country or state/province to integers
# Country -> 1, State/province -> 0
def Loc_Type_to_Num(loc_type):
    loc_type_num = 1
    if loc_type.lower() == 'province':
        loc_type_num = 0
    return loc_type_num

# Initialize analysis, using location name splitter and location type -> number functions
def Init_Analysis(given_loc, given_loc_type):
    # Split name if necessary, and convert type (country or state/province) to 1 or 0
    location = Split_Loc_Name(given_loc)
    location_type = Loc_Type_to_Num(given_loc_type)
    print('Location: ', location, '\nType: ', str(location_type))
    # Make a directory for saving the location's data, if necessary
    if not os.path.exists(location):
        os.makedirs(location)
        print('New directory created for', location)
    return location, location_type

# Run initialization
# Arg 1 <- location name, arg 2 <- country or province
location, countryOrState = Init_Analysis(sys.argv[1], sys.argv[2])

###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Handle JHU South Korea name changes
def Handle_South_Korea(file_name):
    if file_name == path + '03-10-2020.csv':
        location = 'Republic of Korea'
    if file_name == path + '03-11-2020.csv':
        location = 'Korea, South'
    return location

# Work-around, since Grok library not often available
# Convert JHU file names to usable dates
def Get_File_Date(file_name):
    month, day, year = file[-14:-12], file[-11:-9], file[-8:-4]
    if '{m}/{d}/{Y}'.format(m = month, d = day, Y = year) == sunday_stop:
        month, day, year = 'stop', 'stop', 'stop'
    return month, day, year

# Handle JHU region type changes
def Get_Loc_Type_JHU(month, day, year):
    areaLabel = 'Country/Region'
    if countryOrState == 0:
        areaLabel = 'Province/State'
    if (int(month) == 3 and int(day) > 21 and int(year) > 2019) or (int(month) > 3 and int(year) > 2019):
        # After certain date, "/" changed to "_"
        areaLabel = 'Country_Region'
        if countryOrState == 0:
                areaLabel = 'Province_State'
    return areaLabel

# Append data from JHU 
def Append_Data(daily_data):
    confirmedCases.append(0.)
    deaths.append(0.)
    recovered.append(0.)
    
    # Use global variables to update relevant values
    confirmedCases[day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Confirmed']
    deaths        [day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Deaths']
    recovered     [day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Recovered']
    
    # Related sources on making this function efficient:
    # https://engineering.upside.com/a-beginners-guide-to-optimizing-pandas-code-for-speed-c09ef2c6a4d6
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/missing_data.html

# Tells the data when to stop reading in
# Predictions made Saturday -> Saturday, but are created on Monday, so Sunday data dropped
def Get_Last_Sunday():
    today = calendar.today()
    idx = (today.weekday() + 1) % 7
    sun = today - timedelta(idx)
    sun = '{:%m/%d/%Y}'.format(sun)
    return sun

# Prepare most recent Sunday
sunday_stop = Get_Last_Sunday()

# List and sort JHU data files
path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

confirmedCases, deaths, recovered, active = [], [], [], []
day_count = 0

# Loop over JHU data, and update cases, deaths, recoveries
for file in file_set:
        # Handles South Korea name changes, if necessary
        if location == 'South Korea' or location == 'Republic of Korea':
            location = Handle_South_Korea(file)

        # Break the loop once most recent Sunday hit; do not read in
        # Predictions made Saturday -> Saturday
        month, day_index, year = Get_File_Date(file)
        if month == 'stop':
            print('Stopping data this past Sunday: ', str(sunday_stop))
            break
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Deal with JHU region type label changes
        areaLabel = Get_Loc_Type_JHU(month, day_index, year)

        # Update confirmed, death, and recovery counts
        Append_Data(data)

        day_count += 1

# Revert South Korea name, if location is South Korea
if location == 'Korea, South':
    location = 'South Korea'

# Store most recent deaths and most recent confirmed cases
# Predicted new cases and deaths are later added to these to get aggregates
most_recent_deaths = deaths[-1]
most_recent_confirmed = confirmedCases[-1]

###############################################################
#                                                             #
#            Trim Data and Create Active Case List            #
#                                                             #
###############################################################

# Create cutoff to remove early days with too few cases
def Get_First_Day(untrimmed_list):
    # If cases in a region never above 2500, include all days
    if max(untrimmed_list) < 2500:
        threshold = 0
    # Else, if cases never above 100000, include all days after .5% of max reached
    elif max(untrimmed_list) < 100000:
        threshold = .005 * max(confirmedCases)
    # Else, include all days after first 1000 cases
    else:
        threshold = 1000
    
    # Loop over days, and see when cases get high enough to include
    day = 0
    for val in untrimmed_list:
        # If cases high enough, return given day and stop loop
        if val > threshold:
            first_day = day
            print('\nFirst day of count: ' + str(first_day))
            return first_day, [i for i in range(day_count - first_day)]
        day += 1

# Cut off first few days, based on thresholds in Get_First_Day
def Truncate_From_Min(untrimmed_lists):
    trimmed_lists = []
    for list_i in untrimmed_lists:
        trimmed_lists.append(list_i[minRange:])
    return trimmed_lists

# Set first day for inclusion, as well as a list of day indices
minRange, float_dates = Get_First_Day(confirmedCases)

# Active cases are total cases - deaths - recoveries
active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]
# Total "recoveries" include both recoveries and deaths
r_sum = [recovered[i] + deaths[i] for i in range(len(recovered))]
# Cut off days with too few cases
r_sum_cut, deaths, confirmed = Truncate_From_Min([r_sum, deaths, confirmedCases])

##############################################################
#                                                            #
#                 Prepare Data for Regression                #
#                                                            #
##############################################################

# For testing: using a savitzky-golay filter to smooth the data and improve predictions
def Sav_Gol(unsmooth_data):
    daily_data = signal.savgol_filter(unsmooth_data, 21, 4).tolist()
    x_ax = [i for i in range(len(unsmooth_data))]
    plt.plot(x_ax, unsmooth_data)
    plt.plot(x_ax, daily_data)
    plt.show()
    return daily_data

# Convert cumulative data to daily data, since predictions based based off of incident values
def cumulativeToDaily(cumulative_lists):
    daily_list = []
    for list_i in cumulative_lists:
        daily = []
        for i in range(len(list_i)):
            if not i == 0:
                # Subtract each "yesterday's" values from the current day's
                daily.append(list_i[i] - list_i[i-1])
        # For testing, currently unused: smooth case and death data to remove spikes
        #if list_i == confirmed or list_i == deaths:
            #daily = Sav_Gol(daily)
        daily_list.append(daily)
    return daily_list

# Convert daily data to cumulative data
# Can be used to test aggregate predictions based off the sum of incident predictions
def dailyToCumulative(daily_list_start_pairs):
    cumulative_list = []
    for (list_i, start_val) in daily_list_start_pairs:
        cumulative = []
        for i in range(len(list_i) + 1):
            # Start from baseline saved cumulative value
            if i == 0:
                cumulative.append(start_val)
            # Add most recent cumulative value to most recent incident value
            else:
                cumulative.append(cumulative[i-1] + list_i[i-1])
        cumulative_list.append(cumulative)
    return cumulative_list

# Remove first <order> days of data for fitting the ridge model
# Order is the number of days used to predict each upcoming day
# E.g., if 21 days are used to predict each upcoming day, the first 21 should be removed, since they can't be predicted
def Truncate_For_Ridge(data_list, order):
    truncated_lists = []
    for list_i in data_list:
        truncated_lists.append(list_i[order:])
    return truncated_lists

# Create constant arrays for aggregate model performance checks
active_arr_checker, deaths_arr_checker, confirmed_arr_checker, recovered_arr_checker = active[:], deaths[:], confirmed[:], r_sum_cut[:]
# Create constant arrays for incident model performance checks
daily_active_arr_checker, daily_recovered_arr_checker, daily_deaths_arr_checker, daily_confirmed_arr_checker = cumulativeToDaily([active_arr_checker, recovered_arr_checker, deaths_arr_checker, confirmed_arr_checker])

# Create daily active, recovered, deaths, and confirmed lists for incident value prediction making
daily_active, daily_recovered, daily_confirmed, daily_deaths = cumulativeToDaily([active, r_sum_cut, confirmed, deaths])

# Trim first days of data for fitting the model (See function comments)
active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge = Truncate_For_Ridge([active, r_sum_cut, deaths, confirmed], 36)

# Create prediction matrix, as in LaTeX model description
def Create_Pred_Matrices(full_data_lists, order):
    matrices = []
    for list_i in full_data_lists:
        pred_matrix = []
        for i in range(len(list_i) - order):
            new_row_daily = [1]
            for j in range(order):
                new_row_daily.append(list_i[i+j])
            pred_matrix.append(new_row_daily)
        matrices.append(pred_matrix)
    return matrices

# Remove negative case, death, recovery, and active values
# Come from data-reporting anomalies, so don't belong in predictions
def Clean_Sub_Zero(full_data_lists):
    no_zeros_list = []
    for list_i in full_data_lists:
        for i in range(len(list_i)):
            if list_i[i] < 0:
                list_i[i] = 0
        no_zeros_list.append(list_i)
    return no_zeros_list

# Create matrices, as in LaTeX document, for active cases, recoveries, deaths, and confirmed cases
daily_active_pred_matrix, daily_recovered_pred_matrix, daily_deaths_pred_matrix, daily_confirmed_pred_matrix = Create_Pred_Matrices([daily_active, daily_recovered, daily_deaths, daily_confirmed], 36)
# Remove 0's
active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge = Clean_Sub_Zero([active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge])
# Trim first days of data for fitting incident data
daily_active_for_ridge, daily_recovered_for_ridge, daily_deaths_for_ridge, daily_confirmed_for_ridge = Truncate_For_Ridge([daily_active, daily_recovered, daily_deaths, daily_confirmed], 36)

# Make constant values to revert to for both daily and cumulative model
daily_active_arr_const, daily_active_for_ridge_const = daily_active[:], daily_active_for_ridge[:]
daily_recovered_arr_const, daily_recovered_for_ridge_const = daily_recovered[:], daily_recovered_for_ridge[:]
daily_deaths_arr_const, daily_deaths_for_ridge_const = daily_deaths[:], daily_deaths_for_ridge[:]
daily_confirmed_arr_const, daily_confirmed_for_ridge_const = daily_confirmed[:], daily_confirmed_for_ridge[:]
active_for_ridge_const, recovered_for_ridge_const, deaths_for_ridge_const, confirmed_for_ridge_const = active_for_ridge[:], recovered_for_ridge[:], deaths_for_ridge[:], confirmed_for_ridge[:]

# Generate a range of ridge regression penalty alphas to try
def Generate_Alphas(coeff_list):
    alpha_list = []
    i = -3
    while i < 12:
        for coeff in coeff_list:
            alpha_list.append(coeff * (10**(-i)))
        i += 1
    params = {'alpha': alpha_list}
    return params

parameters = Generate_Alphas([1., 5.])

# Placeholders for the best models
best_active_model, best_recovered_model, best_deaths_model, best_confirmed_model = [], [], [], []
# Empty lists for all models created around the best one
confirmed_model_list, deaths_model_list = [], []

###################################################
#                                                 #
#            Perform Ridge Regression             #
#                                                 #
###################################################

# Create different types of ridge regressor, based on circumstances
def Make_Ridge(input_list, ridge_type, alpha_list = [0]):
    ridge_list = []

    # Create bare-bones ridge regressors to feed into grid searcher
    if ridge_type == 'no_cv':
        for list_i in input_list:
            ridge_list.append(Ridge(normalize = True))

    # Create ridge regressor with only penalty specified, based on grid search results
    if ridge_type == 'no_cv_alpha_spec':
        for i in range(len(input_list)):
            ridge_list.append(Ridge(normalize = True, alpha = alpha_list[i]))

    # Create grid searcher to find best penalties
    if ridge_type == 'cv':
        for list_i in input_list:
            ridge_list.append(GridSearchCV(list_i, parameters, scoring = 'neg_mean_squared_error', cv = 10))

    return ridge_list

# Create grid searchers to find optimal ridge penalties for active and total cases, deaths, and recoveries
r_active, r_recovered, r_deaths, r_confirmed = Make_Ridge([active, recovered, deaths, confirmed], 'no_cv')
ridge_active_cv, ridge_recovered_cv, ridge_deaths_cv, ridge_confirmed_cv = Make_Ridge([r_active, r_recovered, r_deaths, r_confirmed], 'cv')

# Fit ridge regressors, and return necessary values or predictions, depending on circumstances
def Fit_Ridge(prediction_triads, fit_type):
    # For grid searchers, return optimal penalty and best score
    if fit_type == 'round_1_eval':
        alpha_mid_list, score_list = [], []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            alpha_mid_list.append(regressor.best_params_['alpha'])
            score_list.append(regressor.best_score_)
        return alpha_mid_list, score_list

    # For non-grid searches, create future predictions
    if fit_type == 'make_pred':
        model_list = []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            model = regressor.predict(matrix)
            model_list.append(model.tolist())
        return model_list

# Creating triples of values for fitting
# Triples include the regressor, the prediction matrix, and the vector from the RHS of the LaTeX document
active_triad, recovered_triad, deaths_triad, confirmed_triad = (ridge_active_cv, daily_active_pred_matrix, daily_active_for_ridge), (ridge_recovered_cv, daily_recovered_pred_matrix, daily_recovered_for_ridge), (ridge_deaths_cv, daily_deaths_pred_matrix, daily_deaths_for_ridge), (ridge_confirmed_cv, daily_confirmed_pred_matrix, daily_confirmed_for_ridge)
# Get best penalties and performance metrics
assigned_alphas, assigned_scores = Fit_Ridge([active_triad, recovered_triad, deaths_triad, confirmed_triad], 'round_1_eval')
alpha_mid_active, alpha_mid_recovered, alpha_mid_deaths, alpha_mid_confirmed = assigned_alphas[0], assigned_alphas[1], assigned_alphas[2], assigned_alphas[3]
score_active, score_recovered, score_deaths, score_confirmed = assigned_scores[0], assigned_scores[1], assigned_scores[2], assigned_scores[3]

print(str(alpha_mid_active) + ' ' + str(alpha_mid_recovered) + ' ' + str(alpha_mid_deaths) + ' ' + str(alpha_mid_confirmed))
print('Score Active: ' + str(score_active) + '\nScore Recovered: ' + str(score_recovered) + '\nScore Deaths: ' + str(score_deaths) + '\nScore Confirmed: ' + str(score_confirmed))

# Create spectrum of penalties around each of the best
# Used to make a spectrum of predictions for quantile evaluation
def Generate_Alphas_From_Middle(middle_list, coeff_list):
    alpha_lists = []
    for middle in middle_list:
        indiv_alphas = []
        i = -1
        while i < 2:
            for coeff in coeff_list:
                indiv_alphas.append(coeff * (10**(-i)) * middle)
            i += 1
        alpha_lists.append(indiv_alphas)
    return alpha_lists

# Create penalty spectrum
alpha_list_active, alpha_list_recovered, alpha_list_deaths, alpha_list_confirmed = Generate_Alphas_From_Middle([alpha_mid_active, alpha_mid_recovered, alpha_mid_deaths, alpha_mid_confirmed], [1., 2.5, 5., 7.5, 9.])

# Fills in missing values from when the "for_ridge" lists were created without the first <order> days
# Useful for plotting, to see the full spectrum of data
def Model_Baseline(unmatched_models, baseline_lists, order):
    matched_lists = []
    for i in range(len(unmatched_models)):
        list_i = unmatched_models[i]
        baseline_list_i = baseline_lists[i]
        for j in range(0, order):
            list_i.insert(j, baseline_list_i[j])
        matched_lists.append(list_i)
    return matched_lists

# Create samples of length <order> to be used for predictions
# Like the final prediction-generation example in the LaTeX doc
def Create_Samples(lists_for_sampling, order):
    sample_list = []
    for list_i in lists_for_sampling:
        sample = [1]
        i = -order
        while i < 0:
            sample.append(list_i[i])
            i += 1
        sample_list.append(np.asarray(sample).reshape(1,-1))
    return sample_list

# Create ridge regressor predictions based on samples 
def Predict_Ridge(models_list, samples_list):
    next_list = []
    for i in range(len(models_list)):
        next_val = models_list[i].predict(samples_list[i])
        next_list.append(next_val)
    return next_list

# Append regressor predictions to each model's data
def Append_To_Col_Vect(col_vects, new_vals):
    appended_lists = []
    for i in range(len(col_vects)):
        list_i = col_vects[i]
        new_val = new_vals[i]
        list_i.append(new_val)
        appended_lists.append(list_i)
    return appended_lists

k = 0
# Loop over penalty list
while k < len(alpha_list_active):
    # Reset all values to default "checker" values
    daily_active, daily_active_for_ridge = daily_active_arr_const[:], daily_active_for_ridge_const[:]
    daily_recovered, daily_recovered_for_ridge = daily_recovered_arr_const[:], daily_recovered_for_ridge_const[:]
    daily_deaths, daily_deaths_for_ridge = daily_deaths_arr_const[:], daily_deaths_for_ridge_const[:]
    daily_confirmed, daily_confirmed_for_ridge = daily_confirmed_arr_const[:], daily_confirmed_for_ridge_const[:]
    active, active_for_ridge = active_arr_checker[:], active_for_ridge_const[:]
    r_sum_cut, recovered_for_ridge = recovered_arr_checker[:], recovered_for_ridge_const[:]
    deaths, deaths_for_ridge = deaths_arr_checker[:], deaths_for_ridge_const[:]
    confirmed, confirmed_for_ridge = confirmed_arr_checker[:], confirmed_for_ridge_const[:]

    # Create ridge regressors with penalty spectra surrounding the best penalties
    ridge_active, ridge_recovered, ridge_deaths, ridge_confirmed = Make_Ridge([active, recovered, deaths, confirmed], 'no_cv_alpha_spec', [alpha_list_active[k], alpha_list_recovered[k], alpha_list_deaths[k], alpha_list_confirmed[k]])
    
    # Create triples of regressors, matrices, and column vectors for regressor training
    active_triad, recovered_triad, deaths_triad, confirmed_triad = (ridge_active, daily_active_pred_matrix, daily_active_for_ridge), (ridge_recovered, daily_recovered_pred_matrix, daily_recovered_for_ridge), (ridge_deaths, daily_deaths_pred_matrix, daily_deaths_for_ridge), (ridge_confirmed, daily_confirmed_pred_matrix, daily_confirmed_for_ridge)
    # Train all of the regressors on accumulated data to create models
    daily_active_model, daily_recovered_model, daily_deaths_model, daily_confirmed_model = Fit_Ridge([active_triad, recovered_triad, deaths_triad, confirmed_triad], 'make_pred')

    # Append earliest values to lists for plotting (See function description)
    Model_Baseline([daily_active_model, daily_recovered_model, daily_deaths_model, daily_confirmed_model], [daily_active, daily_recovered, daily_deaths, daily_confirmed], 36)
    
    # Tell how long to make forecasts (stops about 40 days out)
    stop = int(len(daily_active_arr_checker)) + 40 - 1
    start = len(daily_active) - 1
    
    # Make ~6 weeks worth of predictions
    t = start
    while t < stop:
        # Create samples with most recent data (including accumulated predictions)
        sample_active, sample_recovered, sample_deaths, sample_confirmed = Create_Samples([daily_active_for_ridge, daily_recovered_for_ridge, daily_deaths_for_ridge, daily_confirmed_for_ridge], 36)
        # Make predictions
        next_active, next_recovered, next_deaths, next_confirmed = Predict_Ridge([ridge_active, ridge_recovered, ridge_deaths, ridge_confirmed], [sample_active, sample_recovered, sample_deaths, sample_confirmed])
        # Append predictions to model
        daily_active_for_ridge, daily_active, daily_active_model, daily_recovered_for_ridge, daily_recovered, daily_recovered_model, daily_deaths_for_ridge, daily_deaths, daily_deaths_model, daily_confirmed_for_ridge, daily_confirmed, daily_confirmed_model = Append_To_Col_Vect([daily_active_for_ridge, daily_active, daily_active_model, daily_recovered_for_ridge, daily_recovered, daily_recovered_model, daily_deaths_for_ridge, daily_deaths, daily_deaths_model, daily_confirmed_for_ridge, daily_confirmed, daily_confirmed_model], [next_active, next_active, next_active, next_recovered, next_recovered, next_recovered, next_deaths, next_deaths, next_deaths, next_confirmed, next_confirmed, next_confirmed])
        
        t += 1


    # Transform daily model into cumulative for error and plotting
    active_for_ridge, active, active_model = dailyToCumulative([(daily_active_for_ridge, active_for_ridge[0]), (daily_active, active[0]), (daily_active_model, active[0])])
    recovered_for_ridge, r_sum_cut, recovered_model = dailyToCumulative([(daily_recovered_for_ridge, recovered_for_ridge[0]), (daily_recovered, r_sum_cut[0]), (daily_recovered_model, r_sum_cut[0])])
    deaths_for_ridge, deaths, deaths_model = dailyToCumulative([(daily_deaths_for_ridge, deaths_for_ridge[0]), (daily_deaths, deaths[0]), (daily_deaths_model, deaths[0])])
    confirmed_for_ridge, confirmed, confirmed_model = dailyToCumulative([(daily_confirmed_for_ridge, confirmed_for_ridge[0]), (daily_confirmed, confirmed[0]), (daily_confirmed_model, confirmed[0])])
    
    # Add 1 to the stop and start locations to adjust for the expanded cumulative data set
    # (Always 1 more cumulative point than incident)
    stop += 1
    start += 1

    # Set best models for plotting
    if alpha_list_active[k] == alpha_mid_active: best_active_model = active_model
    if alpha_list_recovered[k] == alpha_mid_recovered: best_recovered_model = recovered_model
    if alpha_list_deaths[k] == alpha_mid_deaths: best_deaths_model = deaths_model
    if alpha_list_confirmed[k] == alpha_mid_confirmed: best_confirmed_model = confirmed_model

    # Create spectrum of predictions for each day (used for finding quantiles)
    for i in range(len(confirmed_model)):
        deaths_model_list.append((i, deaths_model[i]))
        confirmed_model_list.append((i, confirmed_model[i]))

    k += 1

# Retrieve best incident models
daily_best_active_model, daily_best_recovered_model, daily_best_deaths_model, daily_best_confirmed_model = cumulativeToDaily([best_active_model, best_recovered_model, best_deaths_model, best_confirmed_model])

# Appending 0's to baseline data for plotting into the future
for i in range(41):
    daily_active_arr_checker.append(0)
    daily_recovered_arr_checker.append(0)
    daily_deaths_arr_checker.append(0)
    daily_confirmed_arr_checker.append(0)

# Plot deaths and confirmed case results
stop_pt = int(len(active_arr_checker))

plt.plot(daily_best_deaths_model[:start+28], 'g', linestyle = '--', label = 'model')
plt.plot(daily_deaths_arr_checker[:start+28], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Deaths')
plt.title('Deaths Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_deaths.pdf')
plt.show()

plt.plot(daily_best_confirmed_model[:start+28], 'g', linestyle = '--', label = 'model')
plt.plot(daily_confirmed_arr_checker[:start+28], 'k', label = 'data')
plt.axvline(x = stop_pt - 1, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Confirmed')
plt.title('Confirmed Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()

##############################################################
#                                                            #
#                       Build Quantiles                      #
#                                                            #
##############################################################

# Convert prediction spectra into (X, y) pairs, with several predictions for each date
def Create_Pred_Pairs(model_lists):
    X_lists, y_lists = [], []
    for list_i in model_lists:
        X_vals, y_vals = [], []
        for (i, j) in list_i:
            X_vals.append(i)
            y_vals.append(j)
        X_lists.append(X_vals)
        y_lists.append(y_vals)
    return X_lists[0], X_lists[1], y_lists[0], y_lists[1]

# Prepare list of required case quantiles for the forecasting hub

case_count_list, case_inc_list, death_count_list, death_inc_list = [], [], [], []
X_confirmed, y_confirmed, X_deaths, y_deaths = [], [], [], []

# Pair up prediction spectra with each day, and transform data as required by numpy
X_confirmed, X_deaths, y_confirmed, y_deaths = Create_Pred_Pairs([confirmed_model_list, deaths_model_list])
X_confirmed_OLS, X_deaths_OLS = np.atleast_2d(np.asarray(X_confirmed)).T, np.atleast_2d(np.asarray(X_deaths)).T
y_confirmed_OLS, y_deaths_OLS = y_confirmed, y_deaths

# Transform data as required by later libraries
y_confirmed, y_deaths = [float(y_confirmed[i]) for i in range(len(y_confirmed))], [float(y_deaths[i]) for i in range(len(y_deaths))]

# Create an Ordinary Least Squares fit of full prediction spectrum to get "points" for hub
alpha = 0.975
clf_confirmed = GradientBoostingRegressor(loss = 'ls', alpha = alpha, n_estimators = 1500, max_depth = 5, learning_rate = .1, min_samples_leaf = 5, min_samples_split = 5, random_state = 1)
clf_deaths = GradientBoostingRegressor(loss = 'ls', alpha = alpha, n_estimators = 1500, max_depth = 3, learning_rate = .1, min_samples_leaf = 9, min_samples_split = 9, random_state = 1)

# Fit OLS over spectrum
clf_confirmed.fit(X_confirmed_OLS, y_confirmed_OLS)
clf_deaths.fit(X_deaths_OLS, y_deaths_OLS)

##############################################################
#                                                            #  
#                   QuantCalc Implementation                 #
#                                                            #
##############################################################

# List of required death quantiles for the forecasting hub
QUANTILES = [.01, .025, .05, .1, .15, .2, .25, .3, .35, .4, .45, .5, .55, .6, .65, .7, .75, .8, .85, .9, .95, .975, .99]

# Separate lists for each quantile, for recording data in csv
# "dc" for "death count"
quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99 = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

# Separate lists for each quantile
# "cc" for "case count"
quantiles_cc2_5, quantiles_cc10, quantiles_cc25, quantiles_cc50, quantiles_cc75, quantiles_cc90, quantiles_cc97_5 = [], [], [], [], [], [], []

# A list of quantile lists for deaths
deaths_quant_list = [quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99]

# A list of quantile lists for cases
confirmed_quant_list = [quantiles_cc2_5, quantiles_cc10, quantiles_cc25, quantiles_cc50, quantiles_cc75, quantiles_cc90, quantiles_cc97_5]

# List of required case quantiles for the forecasting hub
QUANTILES_CASE_INC = [0.025, 0.100, 0.250, 0.500, 0.750, 0.900, 0.975]

day_pred_deaths, day_pred_deaths_matrix, day_pred_confirmed, day_pred_confirmed_matrix = [], [], [], []

# Sort data and retrieve quantiles
for i in range(X_deaths[-1] + 1):
    day = i
    for j in range(len(y_deaths)):
        if X_deaths[j] == i:
            day_pred_deaths.append(y_deaths[j])
        if X_confirmed[j] == i:
            day_pred_confirmed.append(y_confirmed[j])
    day_pred_deaths_matrix.append(day_pred_deaths)
    day_pred_confirmed_matrix.append(day_pred_confirmed)
    day_pred_deaths, day_pred_confirmed = [], []

num_preds = len(day_pred_confirmed_matrix[0])
for i in range(len(day_pred_confirmed_matrix)):
    death_preds_ordered = np.sort(day_pred_deaths_matrix[i])
    confirmed_preds_ordered = np.sort(day_pred_confirmed_matrix[i])
    print(confirmed_preds_ordered)
    
    for j in range(len(deaths_quant_list)):
        Quantile = QUANTILES[j]
        matching_day_quantile_deaths = np.percentile(death_preds_ordered, Quantile*100.)
        deaths_quant_list[j].append((i, matching_day_quantile_deaths))

    for j in range(len(confirmed_quant_list)):
        Quantile = QUANTILES_CASE_INC[j]
        matching_day_quantile_confirmed = np.percentile(confirmed_preds_ordered, Quantile*100.)
        confirmed_quant_list[j].append((i, matching_day_quantile_confirmed))

########################################################
#                                                      #
#                    Data Writing                      #
#                                                      #
########################################################

# NEW (9-7-2020) -- Use python's datetime library, set the current date

# get today's date
date_today = calendar.today()
# get the next Saturday's date
date_saturday = date_today + timedelta((5 - date_today.weekday()) % 7)

submission_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'location', 'type', 'quantile', 'value'])
# Edit days here
forecast_date = date_today.strftime("%Y-%m-%d")
target_end_date = date_saturday.strftime("%Y-%m-%d")

case_count_list = []
death_count_list = []

cumulative_deaths_for_adding = [most_recent_deaths]

# Edit days here
days_for_pred = [7, 14, 21, 28]

# Edit days here
for i in range(len(days_for_pred)):
    if i == 1:
        target_end_date = (date_saturday + timedelta(days = 7)).strftime("%Y-%m-%d")
    if i == 2:
        target_end_date = (date_saturday + timedelta(days = 14)).strftime("%Y-%m-%d")
    if i == 3:
        target_end_date = (date_saturday + timedelta(days = 21)).strftime("%Y-%m-%d")

    pred_day = len(active_arr_checker) + days_for_pred[i] - 1
    
    for j in range(len(confirmed_quant_list)):
        quantile = QUANTILES_CASE_INC[j]
        row = [k for (l, k) in confirmed_quant_list[j]]
        if quantile == 0.975:
            break
        else:
            if i == 0:
                case_inc = row[pred_day] - clf_confirmed.predict(np.asarray(int(len(active_arr_checker) - 1)).reshape(1, -1))
                case_inc = float(case_inc[0])
            else:
                case_inc = row[pred_day] - case_count_list[-1]
                case_inc = float(case_inc[0])
            if case_inc < 0:
                case_inc = 0.
            submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case'.format(week = i+1), target_end_date, location, 'quantile', quantile, case_inc]], columns = submission_frame.columns))

    for j in range(len(deaths_quant_list)):
        quantile = QUANTILES[j]
        row = [k for (l, k) in deaths_quant_list[j]]

        if i == 0:
            deaths_inc = row[pred_day] - clf_deaths.predict(np.asarray(int(len(deaths_arr_checker) - 1)).reshape(1, -1))
            deaths_inc = float(deaths_inc[0])
        else:
            deaths_inc = row[pred_day] - death_count_list[-1]
            deaths_inc = float(deaths_inc[0])
        if deaths_inc < 0:
            deaths_inc = 0.
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'quantile', quantile, deaths_inc]], columns = submission_frame.columns))
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'quantile', quantile, deaths_inc + cumulative_deaths_for_adding[-1]]], columns = submission_frame.columns))

    pred_day = np.asarray(pred_day)
    pred_day = pred_day.reshape(1,-1)
 
    if i == 0:
        case_inc = clf_confirmed.predict(pred_day) - clf_confirmed.predict(np.asarray(int(len(active_arr_checker) - 1)).reshape(1, -1))
    else:
        case_inc = clf_confirmed.predict(pred_day) - case_count_list[-1]
    if case_inc < 0:
        case_inc = [0.]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case'.format(week = i+1), target_end_date, location, 'point', 'NA', float(case_inc[0])]], columns = submission_frame.columns))

    if i == 0:
        deaths_inc = clf_deaths.predict(pred_day) - clf_deaths.predict(np.asarray(int(len(deaths_arr_checker) - 1)).reshape(1, -1))
    else:
        deaths_inc = clf_deaths.predict(pred_day) - death_count_list[-1]
    if deaths_inc < 0:
        deaths_inc = [0.]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'point', 'NA', float(deaths_inc[0])]], columns = submission_frame.columns))

    this_cumulative = deaths_inc + cumulative_deaths_for_adding[-1]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'point', 'NA', float(this_cumulative[0])]], columns = submission_frame.columns))
    cumulative_deaths_for_adding.append(float(this_cumulative[0])) 
    
    case_count_list.append(clf_confirmed.predict(pred_day))
    death_count_list.append(clf_deaths.predict(pred_day))

# Edit days here
submission_frame.to_csv(location + '/' + date_today.strftime("%Y-%m-%d") + '-UMich-RidgeTfReg.csv')
