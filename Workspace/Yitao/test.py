import unittest
import numpy as np
from cluster_version_test import *

# Test written by Rob to show how unit test works
class Test_Loc_Type_to_Num(unittest.TestCase):
    
    def test_province_norm(self):
        self.assertEqual(Loc_Type_to_Num('province'), 0)
        
    def test_country_norm(self):
        self.assertEqual(Loc_Type_to_Num('country'), 1)
        
    def test_province__first_letter_capitalized(self):
        self.assertEqual(Loc_Type_to_Num('Province'), 0)
        
    def test_country_first_letter_capitalized(self):
        self.assertEqual(Loc_Type_to_Num('Country'), 1)
        
    # Loc_Type_to_Num doesn't handle bad input very well right now.
    # We can change this later if we want to make the script
    # more user-proof, but for our purposes this isn't important.

    # def test_bad_input(self):
        # self.assertRaises(ValueError, Loc_Type_to_Num, 'adadfl@#`a25sl')

# Testing pre ridge regression operations
class Test_Pre_Ridge(unittest.TestCase):
    
    # testing functionality of Truncate_For_Ridge
    def test_truncate_for_ridge(self):
        data = [[1,3,5,7,9],[2,4,6,8,10],[0,0,0,0,0]]
        revised_data = Truncate_For_Ridge(data, 2)
        self.assertEqual(revised_data, [[5,7,9],[6,8,10],[0,0,0]])

    # testing functionality of Create_Pred_Matrix
    def test_create_pred_matrix(self):
        data = [[1,3,5,7,9],[2,4,6,8,10],[0,0,0,0,0]]
        pred_matrix = Create_Pred_Matrices(data, 2)
        self.assertEqual(pred_matrix, [[[1,1,3],[1,3,5],[1,5,7]], 
                                       [[1,2,4],[1,4,6],[1,6,8]], 
                                       [[1,0,0],[1,0,0],[1,0,0]]])

# unit test for ridge regression
class Test_Predict_Ridge(unittest.TestCase):
    
    # testing trivial case of no_cv
    def test_ridge_no_cv_small(self):
        # pre setup
        triads = []
        data = [[1,3,5,7,9,11,13,15]]
        vec = Truncate_For_Ridge(data, 6)
        pred_matrix = Create_Pred_Matrices(data, 6)

        # make ridge
        ridge_list = Make_Ridge(data, 'no_cv')
        triads.append([ridge_list[0], pred_matrix[0], vec[0]])

        # fit ridge
        model_list = Fit_Ridge(triads, 'make_pred')

        # predict
        self.assertAlmostEqual(model_list[0][len(model_list[0])-1], 15)

    # test with out-liers
    def test_ridge_no_cv_large(self):
        # pre setup
        triads = []
        data = [[1,3,5,7,9,11,13,15,140,19,21,23,25,27,29,31,33,35,37,39]]
        vec = Truncate_For_Ridge(data, 15)
        pred_matrix = Create_Pred_Matrices(data, 15)

        # make ridge
        ridge_list = Make_Ridge(data, 'no_cv')
        triads.append([ridge_list[0], pred_matrix[0], vec[0]])

        # fit ridge
        model_list = Fit_Ridge(triads, 'make_pred')

        # predict
        self.assertAlmostEqual(model_list[0][len(model_list[0])-1], 39)

    # testing trivial case of cv
    def test_simple_ridge_cv(self):
        # pre setup
        triads = []
        data = [[1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39]]
        vec = Truncate_For_Ridge(data, 8)
        pred_matrix = Create_Pred_Matrices(data, 8)

        # make ridge
        ridge_list = Make_Ridge(data, 'no_cv')
        ridge_list_cv = Make_Ridge(ridge_list, 'cv')
        triads.append([ridge_list_cv[0], pred_matrix[0], vec[0]])

        # fit ridge
        alpha_list, score_list = Fit_Ridge(triads, 'round_1_eval')

        # make ridge again
        ridge_list = Make_Ridge(data, 'no_cv')
        ridge_list_cv = Make_Ridge(ridge_list, 'cv')
        ridge_list = Make_Ridge(ridge_list_cv, 'no_cv_alpha_spec', alpha_list)
        triads.append([ridge_list[0], pred_matrix[0], vec[0]])
        model_list = Fit_Ridge(triads, 'make_pred')

        # check value
        self.assertAlmostEqual(model_list[0][len(model_list[0])-1], 39)

    def test_prediction(self):
        # pre setup
        triads = []
        data = [[1,3,5,7,9,11,13,15]]
        vec = Truncate_For_Ridge(data, 4)
        pred_matrix = Create_Pred_Matrices(data, 4)

        # make ridge
        ridge_list = Make_Ridge(data, 'no_cv')
        triads.append([ridge_list[0], pred_matrix[0], vec[0]])

        # fit ridge
        Fit_Ridge(triads, 'make_pred')

        # make prediction
        sample_list = Create_Samples(data, 4)
        prediction_list = Predict_Ridge(ridge_list, sample_list)

        # assert
        self.assertEqual(prediction_list, [np.array([17.])])

    def test_ridge_cv_predict(self):
        # pre setup
        triads = []
        data = [[1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31,33,35,37,39]]
        vec = Truncate_For_Ridge(data, 8)
        pred_matrix = Create_Pred_Matrices(data, 8)

        # make ridge
        ridge_list = Make_Ridge(data, 'no_cv')
        ridge_list_cv = Make_Ridge(ridge_list, 'cv')
        triads.append([ridge_list_cv[0], pred_matrix[0], vec[0]])

        # fit ridge
        alpha_list, score_list = Fit_Ridge(triads, 'round_1_eval')

        # make ridge again
        ridge_list = Make_Ridge(data, 'no_cv')
        ridge_list_cv = Make_Ridge(ridge_list, 'cv')
        ridge_list = Make_Ridge(ridge_list_cv, 'no_cv_alpha_spec', alpha_list)
        triads.append([ridge_list[0], pred_matrix[0], vec[0]])
        Fit_Ridge(triads, 'make_pred')

        sample_list = Create_Samples(data, 8)
        prediction_list = Predict_Ridge(ridge_list, sample_list)

        # check value
        self.assertEqual(prediction_list, [np.array([41.])])

if __name__ == "__main__":
    unittest.main()