import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import os
import sys
import operator
import iminuit
from scipy.optimize import curve_fit
from datetime import datetime
from pygrok import Grok

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

def GetLocType():
    loc_type = input("\'Country\' or \'Province\':\n")
    if loc_type.lower() == "country":
        return 1
    elif loc_type.lower() == "province":
        return 0
    else:
        print("\nInput not valid. Please try again.")
        return GetLocType()

def GetLoc():
    location_r_formatted = input("Region of interest: ")
    '''
    if location_r == "US":
        location_r_formatted = location_r
    else:
        location_r_formatted = location_r[0].upper() + location_r[1:].lower()
    '''
    loc_type_r = GetLocType()
    return (location_r_formatted, loc_type_r)

# Specify
location, countryOrState = GetLoc()
# Save in separate directories
if not os.path.exists(location):
    os.makedirs(location)


###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Initialization
dates = []
confirmedCases = []
deaths = []
recovered = []
active = []
tested = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports_us/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
        if (location == 'South Korea') and file == (path + '03-10-2020.csv'):
            location = 'Republic of Korea'
        if (location == 'Republic of Korea') and file == (path + '03-11-2020.csv'):
            location = 'Korea, South'
        filedate = grok.match(file)
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Fill dates from the file into an array
        day = str(filedate["month"])+'-'+str(filedate["day"])
        dates.append(day)

        # 'if' statement to deal with Country or State and JHU data format change
        areaLabel = 'Country/Region'
        if countryOrState == 0:
                areaLabel = 'Province/State'
        if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):
                areaLabel = 'Country_Region'
                if countryOrState == 0:
                        areaLabel = 'Province_State'

        # Grab Confirmed and Deaths
        confirmedCases.append(0)
        deaths.append(0.)
        recovered.append(0.)
        tested.append(0.)

        for index, row in data.iterrows():
                if row[areaLabel] == location:
                        if str(row['Confirmed']) != 'nan':
                                confirmedCases[dayCount] = confirmedCases[dayCount] + row['Confirmed']
                        if str(row['Deaths']) != 'nan':
                                deaths[dayCount] = deaths[dayCount] + row['Deaths']
                        if str(row['Recovered']) != 'nan':
                                recovered[dayCount] = recovered[dayCount] + row['Recovered']
                        if str(row['People_Tested']) != 'nan':
                                tested[dayCount] = tested[dayCount] + row['People_Tested']

        dayCount = dayCount + 1

# print confirmedCases
if location == 'Korea, South':
    location = 'South Korea'

###############################################################
#                                                             #
#            Make and view all-encompassing plot              #
#                                                             #
###############################################################

deathRate = np.divide(deaths, confirmedCases) * 100

# Find the first day of infection and create a grid
day = 1
for cases in confirmedCases:
    if cases > 1000:
        first_day = day
        print ("\nFirst day of count: " + str(first_day))
        inf_init = cases
        print ("Initial case count: " + str(cases) + "\n")
        break
    day += 1

minRange = first_day

float_dates = []
day_count = 0
for date in dates[minRange:]:
    float_dates.append( day_count )
    day_count = day_count + 1                                                                     

active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]

new_case = [confirmedCases[minRange+i] - confirmedCases[minRange+i-1] for i in range(len(float_dates))]

new_test = [tested[minRange+i] - tested[minRange+i-1] for i in range(len(float_dates))] 
for i in range(len(new_test)):
    if new_test[i] < 0:
        new_test[i] = 0
'''
posPercent = np.divide(new_case, new_test) * 100
'''

week_case = []
for i in range(0, len(float_dates), 7):
    week_case.append(sum(new_case[i:i+7]))

week_test = []
for i in range(0, len(float_dates), 7):
    week_test.append(sum(new_test[i:i+7]))

posPercent = np.divide(week_case, week_test) * 100

expected_pos = posPercent[-6] / 100

expected_cases = []
expected_cases = week_case[0:-6]
for i in range(-6,0):
    expected_cases.append(week_test[i] * expected_pos)

print('Current Total Cases:', confirmedCases[-1])
print('Current Total Deaths:', deaths[-1])
print('Amount of Tests Done Yesterday:', new_test[-1])
print('Amount of New Cases Yesterday:', new_case[-1])
print('Current Death Rate:', deathRate[-1], '%')
print('Current Positivity Rate:', posPercent[-1], '%')
print('Positivity Rate 6 weeks ago:', expected_pos * 100, '%', '\n')

# Total Plots
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
# Temporarily removing recovered[minRange:] to match JHU
ax1.plot(dates[minRange:],confirmedCases[minRange:],'go',deaths[minRange:],'ro')
ax1.plot(dates[minRange:],new_test,'co')
ax1.plot(dates[minRange:],list(map(operator.sub, confirmedCases[minRange:],confirmedCases[minRange-1:-1])), 'bo')
ax2.plot(dates[minRange:],deathRate[minRange:],'k')
ax1.set_xticklabels(dates[minRange:],rotation=90, fontsize=5)
ax1.set_xlabel('Date')
ax1.set_ylabel('Cases')
ax2.set_ylabel('Death Rate (%)')
ax2.set_ylim(0,10)
plt.title(str(location) + ' Overview')
ax1.legend(['Confirmed','Deaths','New Tests','New Cases'])
ax2.legend(['Death Rate'],loc='upper center')

if not os.path.exists(location):
    os.makedirs(location)

plt.savefig(location + '/' + location + '.pdf')
plt.show()

###############################################################
#                                                             #
#            Pick and view new or confirmed data              #
#                                                             #
###############################################################


# Scatter plot of daily data
'''
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
ax.scatter(float_dates, new_case, label = 'Data')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Cases')
plt.title(str(location) + ' New Cases')
legend = ax.legend()
plt.show()
'''

# Daily Cases Bar Plot & Positive Test Percent Line Plot
'''
fig, ax3 = plt.subplots()
ax4 = ax3.twinx()
y_pos = np.arange(len(dates[minRange:]))
ax3.bar(y_pos, new_test, align='center', color='c')
ax3.bar(y_pos, new_case, align='center', color='b', width=0.7, alpha=0.7)
ax3.set_xticks(y_pos)
ax3.set_xticklabels(dates[minRange:], rotation=90, fontsize=5)

ax3.xaxis.set_major_locator(plt.MultipleLocator(7))
ax3.xaxis.set_minor_locator(plt.MultipleLocator(1))

ax3.set_xlabel('Date')
ax3.set_ylabel('Number of New Cases and Tests')

ax4.plot(dates[minRange:], posPercent[minRange-1:],'m')
ax4.set_ylabel('Percent of Tests That Were Positive')
ax4.set_ylim(0,100)
ax4.set_xticks(y_pos)
ax4.set_xticklabels(dates[minRange:], rotation=90, fontsize=5)

ax4.xaxis.set_major_locator(plt.MultipleLocator(7))
ax4.xaxis.set_minor_locator(plt.MultipleLocator(1))

ax3.set_title(str(location) + ' New Cases & Testing')
ax3.legend(['Tests','New Cases'], loc='upper left')
ax4.legend(['% Positive'], loc='upper right')
plt.show()
'''

#Weekly Cases Bar Plot & Positive Test Percent Line Plot
'''
fig, ax5 = plt.subplots()
ax6 = ax5.twinx()

#y_pos = len(week_case)

#ax6.bar(range(len(week_test)), week_test, align='center', color='c')
ax5.bar(range(len(week_case)), week_case, align='center', color='b', width=0.7, alpha=0.7)
ax5.bar(range(len(expected_cases)), expected_cases, align='center', color='r', width=0.5, alpha=0.7)

#ax5.set_xticks(y_pos)
#ax5.set_xticklabels()

ax5.set_xlabel('Week')
ax5.set_ylabel('Number of Expected and Actual New Cases')

ax6.plot(range(len(week_test)), posPercent, 'm')
ax6.set_ylabel('Percent of Tests That Were Positive')
#ax6.set_label('Amount of Tests')
ax6.set_ylim(0, 100)

#ax6.set_xticks(y_pos)
#ax6.set_xticklables()

ax5.set_title(str(location) + ' New Cases & Testing')
ax5.legend(['New Cases','Expected Cases'], loc='upper left')

ax6.legend(['% Positive'], loc='upper right')
#ax6.legend(['Tests'], loc='upper right')
plt.show()
'''

#Expected vs Actual line plot
fig, ax7 = plt.subplots()
ax8 = ax7.twinx()

ax8.bar(range(len(week_test)), week_test, align='center', color='c', width=0.7, alpha=0.4)
ax7.plot(range(len(expected_cases)), expected_cases, 'r')
ax7.plot(range(len(week_case)), week_case, 'b')

ax7.set_xticks(range(len(week_test)))
ax7.set_xticklabels(range(len(week_test)+1)[1:])
ax8.set_xticks(range(len(week_test)))
ax8.set_xticklabels(range(len(week_test)+1)[1:])

ax7.set_xlabel('Week (04-13 to Present)')
ax7.set_ylabel('Number of Expected and Actual New Cases')
ax8.set_ylabel('Number of Tests')

ax7.set_title(str(location) + ' Actual vs. Expected Cases (based on positivity rate 6 weeks ago)')
ax7.legend(['Expected Cases', 'New Cases'], loc='upper left')
ax8.legend(['Tests'], loc='upper right')

plt.show()
