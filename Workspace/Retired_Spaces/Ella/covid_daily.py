import matplotlib.pyplot as plt
import numpy as np
import glob
import pandas as pd
import os
import sys
import operator
import iminuit
from scipy.optimize import curve_fit
from datetime import datetime
from pygrok import Grok

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

def GetLocType():
    loc_type = input("\'Country\' or \'Province\':\n")
    if loc_type.lower() == "country":
        return 1
    elif loc_type.lower() == "province":
        return 0
    else:
        print("\nInput not valid. Please try again.")
        return GetLocType()

def GetLoc():
    location_r_formatted = input("Region of interest: ")
    '''
    if location_r == "US":
        location_r_formatted = location_r
    else:
        location_r_formatted = location_r[0].upper() + location_r[1:].lower()
    '''
    loc_type_r = GetLocType()
    return (location_r_formatted, loc_type_r)

# Specify
location, countryOrState = GetLoc()
# Save in separate directories
if not os.path.exists(location):
    os.makedirs(location)


###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Initialization
dates = []
confirmedCases = []
deaths = []
recovered = []
active = []
tested = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports_us/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
        if (location == 'South Korea') and file == (path + '03-10-2020.csv'):
            location = 'Republic of Korea'
        if (location == 'Republic of Korea') and file == (path + '03-11-2020.csv'):
            location = 'Korea, South'
        filedate = grok.match(file)
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Fill dates from the file into an array
        day = str(filedate["month"])+'-'+str(filedate["day"])
        dates.append(day)

        # 'if' statement to deal with Country or State and JHU data format change
        areaLabel = 'Country/Region'
        if countryOrState == 0:
                areaLabel = 'Province/State'
        if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):
                areaLabel = 'Country_Region'
                if countryOrState == 0:
                        areaLabel = 'Province_State'

        # Grab Confirmed and Deaths
        confirmedCases.append(0)
        deaths.append(0.)
        recovered.append(0.)
        tested.append(0.)

        for index, row in data.iterrows():
                if row[areaLabel] == location:
                        if str(row['Confirmed']) != 'nan':
                                confirmedCases[dayCount] = confirmedCases[dayCount] + row['Confirmed']
                        if str(row['Deaths']) != 'nan':
                                deaths[dayCount] = deaths[dayCount] + row['Deaths']
                        if str(row['Recovered']) != 'nan':
                                recovered[dayCount] = recovered[dayCount] + row['Recovered']
                        if str(row['People_Tested']) != 'nan':
                                tested[dayCount] = tested[dayCount] + row['People_Tested']

        dayCount = dayCount + 1

# print confirmedCases
if location == 'Korea, South':
    location = 'South Korea'

# Reading in quarantine measures
'''
mitigation_pairs = []
quarantine_file = 'quarantine_measures.csv'
quarantine_data = pd.read_csv( quarantine_file, index_col = None, header = 0 )
location_row = quarantine_data[quarantine_data['Region'] == location].index[0]
day_count = 0
for i in range(1,6):

    if i == 1 or i == 3 or i == 5:
        day_max = 31
    elif i == 2:
        day_max = 29
    elif i == 4:
        day_max = 30

    if i == 1:
        day_min = 22
    else:
        day_min = 1
            
    for j in range(day_min,day_max):
        if j < 10:
            date = '0{month}/0{day}/20'.format(month = i, day = j)
        else:
            date = '0{month}/{day}/20'.format(month = i, day = j)

        if quarantine_data.loc[location_row, date] != 0:
            mitigation_pairs.append([date, day_count, quarantine_data.loc[location_row, date]])

        day_count += 1

# Log quarantine measures
print('\nQuarantine Measures:')
for [i,j,k] in mitigation_pairs:
    if j == 1:
        statement = 'commenced shelter in place.'
    if j == 2:
        statement = 'lifted shelter in place.'
    if j == 3:
        statement = 'commenced hard quarantine.'
    if j == 4:
        statement = 'lifted hard quarantine.'
    if j == 5:
        statement = 'restricted domestic travel.'
    if j == 6:
        statement = 'resumed domestic travel.'
    if j == 7:
        statement = 'restricted international travel.'
    if j == 8:
        statement = 'resumed international travel.'
   
    print('On {date} (day {day}), {l} {s}'.format(date = i, day = j, l = location, s = statement))
'''
###############################################################
#                                                             #
#            Make and view all-encompassing plot              #
#                                                             #
###############################################################

deathRate = np.divide(deaths, confirmedCases) * 100

# Find the first day of infection and create a grid
day = 1
for cases in confirmedCases:
    if cases > 1000:
        first_day = day
        print ("\nFirst day of count: " + str(first_day))
        inf_init = cases
        print ("Initial case count: " + str(cases) + "\n")
        break
    day += 1

minRange = first_day

float_dates = []
day_count = 0
for date in dates[minRange:]:
    float_dates.append( day_count )
    day_count = day_count + 1                                                                     

active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]

new_case = [confirmedCases[minRange+i] - confirmedCases[minRange+i-1] for i in range(len(float_dates))]

new_test = [tested[minRange+i] - tested[minRange+i-1] for i in range(len(float_dates))] 
for i in range(len(new_test)):
    if new_test[i] < 0:
        new_test[i] = 0

posPercent = np.divide(new_case, new_test) * 100

print('Current Total Cases:', confirmedCases[-1])
print('Current Total Deaths:', deaths[-1])
print('Amount of Tests Done Yesterday:', new_test[-1])
print('Amount of New Cases Yesterday:', new_case[-1])
print('Current Death Rate:', deathRate[-1], '%')
print('Current Positivity Rate:', posPercent[-1], '%', '\n')

# Total Plots
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
# Temporarily removing recovered[minRange:] to match JHU
ax1.plot(dates[minRange:],confirmedCases[minRange:],'go',deaths[minRange:],'ro')
ax1.plot(dates[minRange:],new_test,'co')
ax1.plot(dates[minRange:],list(map(operator.sub, confirmedCases[minRange:],confirmedCases[minRange-1:-1])), 'bo')
ax2.plot(dates[minRange:],deathRate[minRange:],'k')
ax1.set_xticklabels(dates[minRange:],rotation=90, fontsize=5)
ax1.set_xlabel('Date')
ax1.set_ylabel('Cases')
ax2.set_ylabel('Death Rate (%)')
ax2.set_ylim(0,10)
plt.title(str(location) + ' Overview')
ax1.legend(['Confirmed','Deaths','New Tests','New Cases'])
ax2.legend(['Death Rate'],loc='upper center')

if not os.path.exists(location):
    os.makedirs(location)

plt.savefig(location + '/' + location + '.pdf')
plt.show()

###############################################################
#                                                             #
#            Pick and view new or confirmed data              #
#                                                             #
###############################################################

'''
# Plot scatter of daily data
fig = plt.figure(facecolor='w')
ax = fig.add_subplot(111, axisbelow=True)
ax.scatter(float_dates, new_case, label = 'Data')
ax.set_xlabel('Time (days)')
ax.set_ylabel('Cases')
plt.title(str(location) + ' New Cases')
legend = ax.legend()
plt.show()
'''

# Daily Cases Bar Plot & Positive Test Percent Line Plot
fig, ax3 = plt.subplots()
ax4 = ax3.twinx()
y_pos = np.arange(len(dates[minRange:]))
ax3.bar(y_pos, new_test, align='center', color='c')
ax3.bar(y_pos, new_case, align='center', color='b', width=0.7, alpha=0.7)
ax3.set_xticks(y_pos)
ax3.set_xticklabels(dates[minRange:], rotation=90, fontsize=5)
'''
ax3.xaxis.set_major_locator(plt.MultipleLocator(7))
ax3.xaxis.set_minor_locator(plt.MultipleLocator(1))
'''
ax3.set_xlabel('Date')
ax3.set_ylabel('Number of New Cases and Tests')
ax4.plot(dates[minRange:], posPercent[minRange-1:],'m')
ax4.set_ylabel('Percent of Tests That Were Positive')
ax4.set_ylim(0,100)
ax4.set_xticks(y_pos)
ax4.set_xticklabels(dates[minRange:], rotation=90, fontsize=5)
'''
ax4.xaxis.set_major_locator(plt.MultipleLocator(7))
ax4.xaxis.set_minor_locator(plt.MultipleLocator(1))
'''
ax3.set_title(str(location) + ' New Cases & Testing')
ax3.legend(['Tests','New Cases'], loc='upper left')
ax4.legend(['% Positive'], loc='upper right')
plt.show()

##############################################################
#                                                            #
#                     Estimate Milestones                    #
#                                                            #
##############################################################
'''
# Estimate peak
I_prev = 0
I_now = 0
I_next = 0

peak_date = 0
peak_cases = 0
safe_date = 0
found = False

for i in range(1,len(I_pred)-2):
    if i == 1:
        I_now = I_pred[i]
        I_next = I_pred[i+1]

    else:
        I_prev = I_pred[i-1]
        I_now = I_pred[i]
        I_next = I_pred[i+1]

    if I_now > I_prev and I_now > I_next:
        peak_date = i
        peak_cases = I_now

    if I_now < 1000 and I_next < I_now and found == False:
        safe_date = i
        found = True

print('\nEstimated Peak Date from Start: ' + str(peak_date) + '\nEstimated Peak Infections: ' + str(int(peak_cases)) + '\nEstimated Safe Date: ' + str(safe_date))
'''

