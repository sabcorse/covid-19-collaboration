# myModels.py
import numpy as np
from scipy.integrate import odeint

def powerlaw(t, a, b, c): # power law
	return a * (np.asarray(t)**b)*np.exp(-c*np.asarray(t))

def asymB(t,a,b,c,d): #Rayleigh - skewed bell
    return a * np.asarray(t)/b**2 * np.exp(-(np.asarray(t) + c)**2/2./b**2) + d

def SIR_Infected(t, N, beta, gamma): # SIR model Single Output
	#alpha = 500
	#t_shift = [x-alpha for x in t]
	S, I, R = SIR(t, N, beta, gamma)
	return I

def SIR_Recovered(t, N, beta, gamma): # SIR model Single Output
	S, I, R = SIR(t, N, beta, gamma)
	return R

def SIR_Susceptible(t, N, beta, gamma): # SIR model Single Output
    S, I, R = SIR(t, N, beta, gamma)
    return S

def SIR(t, N, I0, beta, gamma): # SIR model
	# Initial number of infected and recovered individuals, I0 and R0.
	R0 = 0
	# Everyone else, S0, is susceptible to infection initially.
	S0 = N - I0 - R0

	# Initial conditions vector
	y0 = S0, I0, R0

	# Integrate the SIR equations over the time grid, t.
	ret = odeint(deriv, y0, t, args=(N, beta, gamma))
	S, I, R = ret.T
	return S,I,R 

def SEIR(t, N, I0, E0, beta, gamma, sigma):
    R0 = 0
    S0 = N - I0 - R0 - E0
    
    y0 = S0, E0, I0, R0

    ret = odeint(derivE, y0, t, args=(N, beta, gamma, sigma))
    S, E, I, R = ret.T
    return S,E,I,R

def deriv(y, t, N, beta, gamma): # 1st order DE's for SIR
    S, I, R = y
    dSdt = -beta * S * I / N
    dIdt = beta * S * I / N - gamma * I
    dRdt = gamma * I
    return dSdt, dIdt, dRdt

def derivE(y, t, N, beta, gamma, sigma):
    S, E, I, R = y
    dSdt = -beta * S * I / N
    dEdt = beta * S * I / N - sigma * E
    dIdt = sigma * E - gamma * I
    dRdt = gamma * I
    return dSdt, dEdt, dIdt, dRdt
