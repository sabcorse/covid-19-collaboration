#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import glob
import sys
import os
from datetime import datetime
from pygrok import Grok


# Initialize
fips = []
dates = []
confirmed = []
deaths = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
	filedate = grok.match(file)
	data = pd.read_csv( file, index_col = None, header = 0 )

	# Grab data for each FIPS
	if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):

		# Get day
		day = str(filedate["year"]+'-'+filedate["month"])+'-'+str(filedate["day"])

		# Grab County Data by Date
		for index, row in data.iterrows():
			if str(row['FIPS']) != 'nan' and str(row['Country_Region']) == 'US':
				fips.append(int(row['FIPS']))
				dates.append(day)
				confirmed.append(int(row['Confirmed']))
				deaths.append(int(row['Deaths']))


# Write to CSV file for fitting
a = np.stack((fips,dates,confirmed,deaths),axis=1)
np.savetxt('./Output/data_county.csv',a,delimiter=",",newline='\n',fmt='%s',header="fips,dates,confirmed,deaths",comments='')
