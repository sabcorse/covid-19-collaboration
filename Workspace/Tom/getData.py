#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import glob
import sys
import os
from datetime import datetime
from pygrok import Grok


# Bring In Parameters
if len(sys.argv) == 1:
	location = 'US'
	countryOrState = 1 # 0 State, 1 Country
else:
	location = sys.argv[1] 
	countryOrState = int(sys.argv[2]) # 0 State, 1 Country

# Initialize
dates = []
confirmed = []
newconfirmed = []
deaths = []
newdeaths = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
path = r'../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
	filedate = grok.match(file)
	data = pd.read_csv( file, index_col = None, header = 0 )

	# Fill dates from the file into an array
	day = str(filedate["year"]+'-'+filedate["month"])+'-'+str(filedate["day"])
	dates.append(day)

	# 'if' statement to deal with Country or State and JHU data format change
	areaLabel = 'Country/Region'
	if countryOrState == 0:
		areaLabel = 'Province/State'
	if (int(filedate["month"]) == 3 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 3 and int(filedate["year"]) > 2019):
		areaLabel = 'Country_Region'
		if countryOrState == 0:
			areaLabel = 'Province_State'

	# Grab Confirmed and Deaths
	confirmed.append(0)
	deaths.append(0)
	newconfirmed.append(0)
	newdeaths.append(0)
	for index, row in data.iterrows():
		if row[areaLabel] == location:
			if str(row['Confirmed']) != 'nan':
				confirmed[dayCount] = confirmed[dayCount] + int(row['Confirmed'])
				if dayCount != 0:
					newconfirmed[dayCount] = confirmed[dayCount] - confirmed[dayCount-1]
			if str(row['Deaths']) != 'nan':
				deaths[dayCount] = deaths[dayCount] + int(row['Deaths'])
				if dayCount != 0:
					newdeaths[dayCount] = deaths[dayCount] - deaths[dayCount-1]

	dayCount = dayCount + 1

# Find first confirmed case
minRange = 0
if location != 'Mainland China': #dealing with name change in JHU data
	for i in confirmed:
		if i == 0:
			minRange = minRange +1

# Write to CSV file for fitting
a = np.stack((dates[minRange:],confirmed[minRange:],deaths[minRange:],newconfirmed[minRange:],newdeaths[minRange:]),axis=1)
np.savetxt('./Output/data_'+location+'.csv',a,delimiter=",",newline='\n',fmt='%s',header="dates,confirmed,deaths,newconfirmed,newdeaths",comments='')
