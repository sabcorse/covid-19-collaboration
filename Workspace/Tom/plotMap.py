#!/usr/bin/env python3

import json
import plotly.express as px
import pandas as pd

with open("geojson-counties-fips.json") as response:
	counties = json.load(response)

#print(counties["features"][2]["properties"]["GEOID"])

df = pd.read_csv("Output/mapdata.csv",dtype={"fips":str})

fig = px.choropleth(df, geojson=counties, locations='fips', color='confirmed',
                           color_continuous_scale="Viridis",
                           range_color=(0, 5000),
                           featureidkey='id',
                           scope="usa"
                          )

fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.show()
