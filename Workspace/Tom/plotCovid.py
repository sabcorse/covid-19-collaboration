#!/usr/bin/env python3

import matplotlib.pyplot as plt
import pandas as pd
import csv
import sys

# Set name of plots
name = str(sys.argv[1])
name = name.replace('Output/data_','')
name = name.replace('.csv','')
print("Plotting "+name)

# Bring In Data
data = pd.read_csv(sys.argv[1])
y1 = data["newconfirmed"]
y2 = data["newdeaths"]
y1label = 'Confirmed Cases'
y2label = 'Deaths'

# Make my plots
fig, ax1 = plt.subplots(figsize=(20,10))
ax2 = ax1.twinx()
ax1.bar(data["dates"],y1,color='#154360',width=0.9)
ax2.bar(data["dates"],y2,color='#893128',width=0.9,alpha=0.7)
#ax1.set_xlabel('Date',fontsize='xx-large',fontweight='bold')
ax1.set_ylabel(y1label,fontsize='xx-large',color='#154360',fontweight='bold')
ax2.set_ylabel(y2label,fontsize='xx-large',color='#893128',fontweight='bold')
ax1.set_ylim(top=y1.max(axis=0)*1.1)
ax2.set_ylim(top=y2.max(axis=0)*1.5)
plt.title(name,fontsize='xx-large',fontweight='bold')
plt.setp(ax1.get_xticklabels(), rotation=45, ha='right')
#ax1.legend(['Confirmed'],loc='upper left', edgecolor='none',fontsize='xx-large')
#ax2.legend(['Deaths'],loc='upper left',bbox_to_anchor=(0, 0.9, 0., 0.),edgecolor='none',fontsize='xx-large')
ax1.xaxis.set_major_locator(plt.MultipleLocator(7.00))
ax1.xaxis.set_minor_locator(plt.MultipleLocator(1.00))
plt.savefig('./Output/' + name + '.pdf')
plt.show()