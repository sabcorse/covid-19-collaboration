#!/usr/bin/env python3

import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import csv
import sys

# Bring In Data
data = pd.read_csv(sys.argv[1])
fips = pd.read_csv(sys.argv[2],dtype={'fips':str})

# Get Dates (for the future)
dates = data["dates"]

# Get Data for each fips and fill dataframe
df = pd.DataFrame(columns=('fips', 'confirmed','deaths'))
for index, row in fips.iterrows():
	mycounty = data[data['fips'] == int(row['fips'])]

	confirmed = int(mycounty["confirmed"].diff().sum()+mycounty["confirmed"].iat[0])
	deaths = int(mycounty["deaths"].diff().sum()+mycounty["deaths"].iat[0])

	df.loc[index] = [row['fips'],confirmed,deaths]


df.to_csv('Output/mapdata.csv',index=False,encoding='utf-8')


