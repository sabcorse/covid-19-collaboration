import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from myModels import powerlaw
from myModels import asymB
import csv
import sys
from datetime import datetime
from scipy.optimize import curve_fit

# Bring In Parameters
name = 'US'
if len(sys.argv) > 2:
 	name = sys.argv[2] 

# Initialize
dates = []
confirmed = []
newconfirmed = []
deaths = []
newdeaths = []
day_count = 0
filename = sys.argv[1] 

# Read in Fitting Data
with open(filename) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
    	dates.append(row[0])
    	confirmed.append(int(row[1]))
    	deaths.append(int(row[2]))
    	newconfirmed.append(int(row[3])) 
    	newdeaths.append(int(row[4]))
    	day_count = day_count + 1


# Make my plots
fig, ax1 = plt.subplots(figsize=(20,10))
ax2 = ax1.twinx()
ax1.plot.hist(dates,newconfirmed,'bo')
ax2.plot.hist(dates,newdeaths,'rx')
ax1.set_xlabel('Date',fontsize='xx-large')
ax1.set_ylabel('Confirmed Cases',fontsize='xx-large')
ax2.set_ylabel('Deaths',fontsize='xx-large')
plt.title(name,fontsize='xx-large')
plt.setp(ax1.get_xticklabels(), rotation=45, ha='right')
ax1.legend(['Confirmed'],loc='upper left', edgecolor='none',fontsize='xx-large')
ax2.legend(['Deaths'],loc='upper left',bbox_to_anchor=(0, 0.9, 0., 0.),edgecolor='none',fontsize='xx-large')
ax1.xaxis.set_major_locator(plt.MultipleLocator(7.00))
ax1.xaxis.set_minor_locator(plt.MultipleLocator(1.00))
plt.savefig('./Output/' + name + '.pdf')
plt.show()