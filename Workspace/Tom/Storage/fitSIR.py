#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from myModels import SIR_Infected
import csv
import sys
from datetime import datetime
from scipy.optimize import curve_fit

# Bring In Parameters
name = 'my'
numDays = 0
if len(sys.argv) > 2:
 	name = sys.argv[2] 
if len(sys.argv) > 3:
 	numDays = int(sys.argv[3])

# Initialize
dates = []
totalcases = []
newcases = []
totaldeaths = []
newdeaths = []
day_count = 0
filename = sys.argv[1] 

# Read in Fitting Data
with open(filename) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    for row in csv_reader:
    	dates.append(row[0])
    	totalcases.append(int(row[1]))
    	totaldeaths.append(int(row[2]))

    	newcases.append(int(row[1]) - int(totalcases[day_count-1]))
    	newdeaths.append(int(row[2]) - int(totaldeaths[day_count-1]))

    	day_count = day_count + 1


# Perform Fit
if numDays == 0: numDays = day_count
predRange = list(range(0,numDays))

popt, pcov = curve_fit( SIR_Infected, predRange[0:day_count], newcases, p0 = [1000, 0.2, 0.1],bounds=(0,[100000000,10,10]))
print(*popt)

# Make a nice list of dates
datelist = pd.date_range(dates[0], periods=numDays).tolist()
nicedates = []
for i in datelist: nicedates.append(str(i.date()))


# Make my plots
fig, ax1 = plt.subplots(figsize=(20,10))
ax2 = ax1.twinx()
ax2.plot(nicedates[:day_count],newdeaths,'rx')
ax1.plot(nicedates[:numDays],SIR_Infected(predRange[:numDays], *popt),'b',nicedates[:day_count],newcases,'go')
ax1.set_xlim(0,numDays)
#ax1.set_ylim(0,6000)
ax1.set_xlabel('Date',fontsize='xx-large')
ax1.set_ylabel('New Cases',fontsize='xx-large')
ax2.set_ylabel('New Deaths',fontsize='xx-large')
ax1.legend(['Fit','Cases'],loc='upper left', edgecolor='none',fontsize='xx-large')
ax2.legend(['Deaths'],loc='upper left',bbox_to_anchor=(0, 0.9, 0., 0.),edgecolor='none',fontsize='xx-large')
ax1.xaxis.set_major_locator(plt.MultipleLocator(7.00))
ax1.xaxis.set_minor_locator(plt.MultipleLocator(1.00))
ax1.tick_params(length=10)
fig.autofmt_xdate()
plt.savefig('./Output/'+ name + '_fit.pdf')
plt.show()


