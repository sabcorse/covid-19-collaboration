PROVINCE             = 'province'
NONE                 = ''

CONFIRMED_JHU_LABEL  = 'Confirmed'
DEATHS_JHU_LABEL     = 'Deaths'
RECOVERED_JHU_LABEL  = 'Recovered'

FIRST_DAY_OF_US_DATA = 'January 22, 2020'

CASES                = 'cases'
DEATHS               = 'deaths'
HOSPS                = 'hosps'