import importlib

module_names = ['module1', 'module2']

modules = []
for module in module_names:
    
    modules.append(importlib.import_module(module))
    
print(modules[0].foo())
print(modules[1].foo2())