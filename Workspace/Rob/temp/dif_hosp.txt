# Prepare most recent Sunday
sunday_stop = Get_Last_Sunday()

# List and sort JHU data files
JHU_path = data_pathname_cfg_parser.get('data_pathnames', 'dataset_path')
file_set = glob.glob(JHU_path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,JHU_path+"%m-%d-%Y.csv"))

confirmedCases, deaths, recovered, active = [], [], [], []
day_count = 0

loc_regex = location

# Handles South Korea name changes, if necessary
if location == 'South Korea' or location == 'Republic of Korea':
    loc_regex = Handle_South_Korea()

# Loop over JHU data, and update cases, deaths, recoveries
for file in file_set:

        # Break the loop once most recent Sunday hit; do not read in
        # Predictions made Saturday -> Saturday
        month, day_index, year = Get_File_Date(file, sunday_stop)
        if month == 'stop':
            print('Stopping data this past Sunday: ', str(sunday_stop))
            break
        #if month == 'stop':
            #print('disobedient')
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Update confirmed, death, and recovery counts
        Append_Data(confirmedCases, deaths, recovered, region_is_country, day_count, data, loc_regex)

        day_count += 1

# Store most recent deaths and most recent confirmed cases
# Predicted new cases and deaths are later added to these to get aggregates
most_recent_deaths = deaths[-1]
most_recent_confirmed = confirmedCases[-1]

#deaths = preprocessing.scale(deaths)
#confirmedCases = preprocessing.scale(confirmedCases)