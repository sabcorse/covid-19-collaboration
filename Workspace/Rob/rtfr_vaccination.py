import pandas          as     pd
from   dateutil.parser import parse

from   rtfr_common     import *

##########################################################
#                                                        #
#                     Get Vaccine Data                   #
#                                                        #
##########################################################

def Get_Vaccine_Data_New(path, region, stop_date, minRange) :
    # file
    unprocessed_df = pd.read_csv(path)
    if region != 'US' :
        # selects state data
        unprocessed_df = unprocessed_df.loc[unprocessed_df['location'] == region]
    # sorts by date
    unprocessed_df = unprocessed_df.sort_values('date', axis=0)
    # sets up iteration
    start_date = parse('January 12, 2021')
    time_difference = (parse(stop_date) - start_date).days
    pre_start_date = parse(Get_First_Date(minRange))
    unfilled_days = (start_date-pre_start_date).days
    vaccine_data = []
    for day in range(unfilled_days) :
        # days before data is available or days before vaccines, fill 0
        vaccine_data.append(0)
    for day in range(time_difference) :
        selected_data = unprocessed_df.loc[unprocessed_df['date'] == (start_date + timedelta(day)).strftime('%Y-%m-%d')]
        if selected_data.empty :
            vaccine_data.append(vaccine_data[-1])
        else :
            vaccines_admin = selected_data['people_vaccinated'].sum()
            vaccine_data.append(vaccines_admin)

    # patches '2021-01-16','2021-01-17','2021-01-18','2021-02-15' - all missing from the data
    # averages the increase over the gap and gives each day the average increase per day
    three_day_jan_patch_daily = (vaccine_data[7+unfilled_days]-vaccine_data[3+unfilled_days])/4
    for i in range(3) :
        i += 1
        vaccine_data[3+i+unfilled_days] = vaccine_data[3+unfilled_days] + three_day_jan_patch_daily*i
    vaccine_data[34+unfilled_days] = (vaccine_data[35+unfilled_days] + vaccine_data[33+unfilled_days])/2

    return vaccine_data
