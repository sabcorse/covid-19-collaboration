import numpy       as     np

from   rtfr_common import *

def Get_Mobility(path, region, minRange, is_country):
    
    # Get first day to include
    start_date = Get_First_Date(minRange)

    # For use in Forecasting Hub - automatically set country to US
    country = 'United States'
    # Read in all data, and narrow down to US-only for both states and national level
    mobility_data = pd.read_csv(path, low_memory = False)
    this_region_mobility = mobility_data.loc[mobility_data['country_region'] == 'United States']
    # If want data for national level, find rows with "NaN" states, and omit others
    if is_country:
        no_states_rows = pd.isnull(this_region_mobility['sub_region_1'])
        this_region_mobility = this_region_mobility[no_states_rows]
    # If want data for a state, find rows with that state and "NaN" counties, and omit others
    else:
        this_region_mobility = this_region_mobility.loc[this_region_mobility['sub_region_1'] == region]
        no_counties_rows = pd.isnull(this_region_mobility['sub_region_2'])
        this_region_mobility = this_region_mobility[no_counties_rows]

    # Find row index where "start_date" occurs, and cut out all previous data
    first_date_row = this_region_mobility.loc[this_region_mobility['date'] == start_date].index.item()
    #print(this_region_mobility.loc[first_date_row])
    this_region_mobility = this_region_mobility.loc[first_date_row:]

    # Social distancing data not updated every day. Find difference b/w today & most recent update
    most_recent_date = this_region_mobility['date'].iloc[-1]
    #print(most_recent_date)
    most_recent_year = int(most_recent_date[0:4])
    most_recent_month = int(most_recent_date[5:7])
    most_recent_day = int(most_recent_date[8:])
    social_distancing_reach_back = (calendar.today() - calendar(most_recent_year, most_recent_month, most_recent_day)).days - 1
    print('Will reach ' + str(social_distancing_reach_back) + ' days back for social distancing')

    # get data for each type of social outing, and convert to lists
    add_on = '_percent_change_from_baseline'
    this_retail_and_rec = this_region_mobility['retail_and_recreation'+add_on].tolist()
    this_groc_and_pharm = this_region_mobility['grocery_and_pharmacy'+add_on].tolist()
    this_parks = this_region_mobility['parks'+add_on].tolist()
    this_transit = this_region_mobility['transit_stations'+add_on].tolist()
    this_work = this_region_mobility['workplaces'+add_on].tolist()
    this_residential = this_region_mobility['residential'+add_on].tolist()

    list_of_locs = [this_retail_and_rec, this_groc_and_pharm, this_parks, this_transit, this_work, this_residential]
    loc_strings = ['retail and recreation', 'grocery and pharmacy', 'parks', 'transit', 'work', 'residences']

    # Eliminate any NaN values in the social distancing data, by using previous value
    # TODO: Can do this with pandas more elegantly -- https://www.geeksforgeeks.org/working-with-missing-data-in-pandas/#:~:text=In%20order%20to%20check%20missing,null%20values%20in%20a%20series.
    j = 0
    for loc in list_of_locs:
        #print(loc)
        num_nan = 0
        for i in range(len(this_retail_and_rec)):
            if np.isnan(loc[i]):
                num_nan += 1
                loc[i] = 0
                if not i == 0:
                    loc[i] = loc[i-1]
        print('Number of NaN values converted for ' + loc_strings[j] + ': ' + str(num_nan))
        j += 1

    return this_retail_and_rec, this_groc_and_pharm, this_parks, this_transit, this_work, this_residential, social_distancing_reach_back