import glob
import sys
import random

import numpy                            as     np
import pandas                           as     pd
import matplotlib.pyplot                as     plt

from   datetime                         import date                      as calendar
from   datetime                         import timedelta
from   backports.datetime_fromisoformat import MonkeyPatch

from   rtfr_constants                   import *
from   rtfr_common                      import *
from   rtfr_regressor                   import *

MonkeyPatch.patch_fromisoformat()

settings_parser = make_cfg_parser('settings.cfg')

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

# Run initialization and read command line
# Arg 1 <- location name, arg 2 <- order
location = Split_Loc_Name(sys.argv[1])
location_for_reading_hosps = sys.argv[1].lower()
print(location_for_reading_hosps)
order = int(sys.argv[2])

check_and_make_dir(location)

case_order = 35
if location == 'Detroit':
    case_order = 7
if location == 'Grand Rapids':
    case_order = 7#56
if location == 'Saginaw':
    case_order = 35#49
if location == 'Lansing':
    case_order = 7
if location == 'Jackson':
    case_order = 7#5
if location == 'Traverse City':
    case_order = 7#56
if location == 'Kalamazoo':
    case_order = 7#56
if location == 'Traverse+up':
    print('setting custom order')
    case_order = 7#77

hosp_order = 7#35
if location == 'Saginaw':
    hosp_order = 60
if location == 'Detroit':
    hosp_order = 72#49
if location == 'Grand Rapids':
    hosp_order = 56#21
if location == 'Lansing':
    hosp_order = 7
if location == 'Jackson':
    hosp_order = 35#7
if location == 'Kalamazoo':
    hosp_order = 60

###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Handle JHU region type changes
# TODO: Do the regex thing
def Get_Loc_Type_JHU(month, day, year):
    areaLabel = 'Province/State'
    if (int(month) == 3 and int(day) > 21 and int(year) > 2019) or (int(month) > 3 and int(year) > 2019) or (int(year) > 2020):
        areaLabel = 'Province_State'
    return areaLabel

# Append data from JHU 
# TODO: Find a way to combine with the other version
# TODO: Make sure this still works
def Append_Data(daily_data, this_county):
    if len(confirmedCases) <= day_count:
    	confirmedCases.append(0.)
    	deaths.append(0.)
    	recovered.append(0.)
    
    #print(daily_data.head())
    # Use global variables to update relevant values
    if day_count > 100:
        
        label = Get_Loc_Type_JHU(False)
        
        summed_daily_data = daily_data.loc[daily_data[daily_data.filter(regex = label).columns[0] == 'Michigan']].loc[daily_data['Admin2'] == this_county].sum()
        
        confirmedCases[day_count] += summed_daily_data[CONFIRMED_JHU_LABEL]
        deaths        [day_count] += summed_daily_data[DEATHS_JHU_LABEL   ]
        recovered     [day_count] += summed_daily_data[RECOVERED_JHU_LABEL]

    # Related sources on making this function efficient:
    # https://engineering.upside.com/a-beginners-guide-to-optimizing-pandas-code-for-speed-c09ef2c6a4d6
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/missing_data.html

# Prepare most recent Sunday
sunday_stop = Get_Last_Sunday()

# List and sort JHU data files
JHU_path = settings_parser.get('data_pathnames', 'dataset_path')
file_set = glob.glob(JHU_path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,JHU_path+"%m-%d-%Y.csv"))

confirmedCases, deaths, recovered, active = [], [], [], []
day_count = 0

reg_county_dict = {'Detroit':['genesee', 'lapeer', 'livingston', 'macomb', 'monroe', 'oakland', 'st. clair', 'washtenaw', 'wayne'], 'Grand Rapids':['clare', 'ionia', 'isabella', 'kent', 'lake', 'mason', 'mecosta', 'montcalm', 'muskegon', 'newaygo', 'oceana', 'ottawa', 'osceola'], 'Kalamazoo':['allegan', 'barry', 'berrien', 'branch', 'calhoun', 'cass', 'kalamazoo', 'st. joseph', 'van buren'], 'Saginaw':['alcona', 'arenac', 'bay', 'gladwin', 'huron', 'iosco', 'midland', 'ogemaw', 'oscoda', 'saginaw', 'sanilac', 'tuscola'], 'Lansing':['clinton', 'eaton', 'gratiot', 'ingham', 'shiawassee'], 'Traverse City':['antrim', 'alpena', 'benzie', 'charlevoix', 'cheboygan', 'crawford', 'emmet', 'grand traverse', 'kalkaska', 'leelanau', 'manistee', 'missaukee', 'montmorency', 'otsego', 'presque isle', 'roscommon', 'wexford'], 'Jackson':['hillsdale', 'jackson', 'lenawee'], 'Upper Peninsula':['alger', 'baraga', 'chippewa', 'delta', 'dickinson', 'gogebic', 'houghton', 'iron', 'luce', 'mackinac', 'marquette', 'menominee', 'ontonagon', 'schoolcraft'], 'Traverse+up':['antrim', 'alpena', 'benzie', 'charlevoix', 'cheboygan', 'crawford', 'emmet', 'grand traverse', 'kalkaska', 'leelanau', 'manistee', 'missaukee', 'montmorency', 'otsego', 'presque isle', 'roscommon', 'wexford', 'alger', 'baraga', 'chippewa', 'delta', 'dickinson', 'gogebic', 'houghton', 'iron', 'luce', 'mackinac', 'marquette', 'menominee', 'ontonagon', 'schoolcraft']}

# TODO: Adapt to the version of this code in read_JHU_data
# Loop over JHU data, and update cases, deaths, recoveries
for county in reg_county_dict[location]:
	county_formatted = county.title()

	day_count = 0
	for file in file_set:
		# Break the loop once most recent Sunday hit; do not read in
		# Predictions made Saturday -> Saturday
		month, day_index, year = Get_File_Date(file, sunday_stop)
        #if month == 1:
        #    print('date'+str(month)+str(day_index))
		if month == 'stop':
		    print('Stopping data this past Sunday: ', str(sunday_stop))
		    break
		data = pd.read_csv( file, index_col = None, header = 0 )

		# Update confirmed, death, and recovery counts
		Append_Data(data, county_formatted)

		day_count += 1

# Store most recent deaths and most recent confirmed cases
# Predicted new cases and deaths are later added to these to get aggregates
most_recent_deaths = deaths[-1]
most_recent_confirmed = confirmedCases[-1]

#deaths = preprocessing.scale(deaths)
#confirmedCases = preprocessing.scale(confirmedCases)

###############################################################
#                                                             #
#            Trim Data and Create Active Case List            #
#                                                             #
###############################################################

# Create cutoff to remove early days with too few cases
def Get_First_Day(untrimmed_list):
    # If cases in a region never above 2500, include all days
    if max(untrimmed_list) < 2500:
        threshold = 0
    # Else, if cases never above 100000, include all days after .5% of max reached
    elif max(untrimmed_list) < 100000:
        threshold = .005 * max(confirmedCases)
    # Else, include all days after first 1000 cases
    else:
        threshold = 1000
    
    if location == 'Detroit':
        return 335, [i for i in range(day_count - 335)]
    if location == 'Grand Rapids':
        return 355, [i for i in range(day_count - 355)]
    if location == 'Lansing':
        return 335, [i for i in range(day_count - 335)]
    if location == 'Traverse City':
        return 315, [i for i in range(day_count - 315)]
    if location == 'Saginaw':
        return 335, [i for i in range(day_count - 335)]
    if location == 'Jackson':
        return 335, [i for i in range(day_count - 335)]
    if location == 'Kalamazoo':
        return 335, [i for i in range(day_count - 335)]
    if location == 'Traverse+up':
        return 315, [i for i in range(day_count - 315)]
    return 205, [i for i in range(day_count - 205)]

# Set first day for inclusion, as well as a list of day indices
minRange, float_dates = Get_First_Day(confirmedCases)

# Active cases are total cases - deaths - recoveries
active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]
# Total "recoveries" include both recoveries and deaths
r_sum = [recovered[i] + deaths[i] for i in range(len(recovered))]
# Cut off days with too few cases
r_sum_cut, deaths, confirmed = Truncate_From_Min([r_sum, deaths, confirmedCases], minRange)

print('length of confirmed: ' + str(len(confirmed)))

start_day = (calendar.fromisoformat('2020-01-22') + timedelta(days = minRange))
dates = [(start_day + timedelta(days = i)).strftime('%Y-%m-%d') for i in range(len(deaths))]
print(dates)
df_forMarisa = pd.DataFrame(list(zip(dates, deaths, confirmed)), columns = ['date', 'deaths', 'cases'])
df_forMarisa.to_csv(location + '-JHU_training.csv', index = False)
print('csv made')

##########################################################
#                                                        #
#                  Get Hospital Data                     #
#                                                        #
##########################################################

if not location_for_reading_hosps == 'traverse+up':
    hosp_csv = pd.read_csv('gen_csvs/covid-19-hospitalization-'+location_for_reading_hosps+'.csv')
else: 
    hosp_csv_traverse = pd.read_csv('gen_csvs/covid-19-hospitalization-traversecity.csv')
    hosp_csv_traverse['COVID-19 Hospitalizations per Million'] = hosp_csv_traverse['COVID-19 Hospitalizations per Million'].fillna(0)
    print(hosp_csv_traverse)
    hosp_csv_up = pd.read_csv('gen_csvs/covid-19-hospitalization-upperpeninsula.csv')
    hosp_csv_up['COVID-19 Hospitalizations per Million'] = hosp_csv_up['COVID-19 Hospitalizations per Million'].fillna(0)
    hosp_csv_traverse['COVID-19 Hospitalizations per Million'] = hosp_csv_traverse['COVID-19 Hospitalizations per Million'] + hosp_csv_up['COVID-19 Hospitalizations per Million']
    hosp_csv = hosp_csv_traverse

print(hosp_csv)

dates_hosps = hosp_csv['DateTime'].tolist()
dates_hosps = [date.split(' ')[0] for date in dates_hosps]

#print(dates_hosps)

start_date = Get_First_Date(minRange)
for i in range(len(dates_hosps)):
    print(str(dates_hosps[i]) + ' ' + str(start_date))
    if dates_hosps[i] == start_date:
        first_date_row = i
        print('first row: ' + str(first_date_row))

hosps_per_mil = hosp_csv.iloc[first_date_row:]['COVID-19 Hospitalizations per Million'].tolist()

start_day = (calendar.fromisoformat('2020-01-22') + timedelta(days = minRange))
dates = [(start_day + timedelta(days = i)).strftime('%Y-%m-%d') for i in range(len(hosps_per_mil))]
print(dates)
df_forMarisa = pd.DataFrame(list(zip(dates, hosps_per_mil)), columns = ['date', 'hosps per mil'])
df_forMarisa.to_csv(location + '-hosp_training.csv', index = False)
print('csv made')

##########################################################
#                                                        #
#           Get Mobility Data, Using First Day           #
#                                                        #
##########################################################

social_distancing_reach_back = 0
# Get Google's mobility data for a given region
def Get_Mobility(path, region):
    print(region)
    global social_distancing_reach_back
    # Get first day to include
    start_date = Get_First_Date(minRange)
    print(start_date)

    # For use in Forecasting Hub - automatically set country to US
    country = 'United States'
    # Read in all data, and narrow down to US-only for both states and national level
    mobility_data = pd.read_csv(path, low_memory = False)
    this_region_mobility = mobility_data.loc[mobility_data['country_region'] == 'United States']
    # If want data for national level, find rows with "NaN" states, and omit others
    
    # If want data for a county, find rows with correct county, in MI
    this_region_mobility = this_region_mobility.loc[this_region_mobility['sub_region_1'] == 'Michigan'].loc[this_region_mobility['sub_region_2'] == region]

    # Find row index where "start_date" occurs, and cut out all previous data
    first_date_row = this_region_mobility.loc[this_region_mobility['date'] == start_date].index.item()
    #print(this_region_mobility.loc[first_date_row])
    this_region_mobility = this_region_mobility.loc[first_date_row:]

    # Social distancing data not updated every day. Find difference b/w today & most recent update
    most_recent_date = this_region_mobility['date'].iloc[-1]
    #print(most_recent_date)
    most_recent_year = int(most_recent_date[0:4])
    most_recent_month = int(most_recent_date[5:7])
    most_recent_day = int(most_recent_date[8:])
    social_distancing_reach_back = (calendar.today() - calendar(most_recent_year, most_recent_month, most_recent_day)).days - 1
    #social_distancing_reach_back = 3
    print('Will reach ' + str(social_distancing_reach_back) + ' days back for social distancing')

    # get data for each type of social outing, and convert to lists
    add_on = '_percent_change_from_baseline'
    this_retail_and_rec = this_region_mobility['retail_and_recreation'+add_on].tolist()
    this_groc_and_pharm = this_region_mobility['grocery_and_pharmacy'+add_on].tolist()
    this_parks = this_region_mobility['parks'+add_on].tolist()
    this_transit = this_region_mobility['transit_stations'+add_on].tolist()
    this_work = this_region_mobility['workplaces'+add_on].tolist()
    this_residential = this_region_mobility['residential'+add_on].tolist()
    
    list_of_locs = [this_retail_and_rec, this_groc_and_pharm, this_parks, this_transit, this_work, this_residential]
    loc_strings = ['retail and recreation', 'grocery and pharmacy', 'parks', 'transit', 'work', 'residences']

    # Eliminate any NaN values in the social distancing data, by using previous value
    j = 0
    for loc in list_of_locs:
        #print(loc)
        num_nan = 0
        for i in range(len(this_retail_and_rec)):
            if np.isnan(loc[i]):
                num_nan += 1
                loc[i] = 0
                if not i == 0:
                    loc[i] = loc[i-1]
        print('Number of NaN values converted for ' + loc_strings[j] + ': ' + str(num_nan))
        j += 1

    print(this_retail_and_rec)

    return this_retail_and_rec, this_groc_and_pharm, this_parks, this_transit, this_work, this_residential    

# Loop over Google file to get social distancing data
for i in range(len(reg_county_dict[location])):
        county = reg_county_dict[location][i]
        if not county == 'luce' and not county == 'ontonagon':
            county = county.title() + " County"
            
            mobility_path = settings_parser.get('data_pathnames', 'mobility_path')
            if i == 0:
                    retail_rec_list, groc_pharm_list, parks_list, transit_list, work_list, residential_list = Get_Mobility(mobility_path, county)
            else:
                    temp_retail_rec_list, temp_groc_pharm_list, temp_parks_list, temp_transit_list, temp_work_list, temp_residential_list = Get_Mobility(mobility_path, county)

                    if len(temp_residential_list) > len(residential_list):
                        for i in range(len(temp_residential_list) - len(residential_list)):
                            residential_list.insert(0,0)
                    if len(temp_transit_list) > len(transit_list):
                        for i in range(len(temp_transit_list) - len(transit_list)):
                            transit_list.insert(0,0)

                    if len(residential_list) > len(temp_residential_list):
                        for i in range(len(residential_list) - len(temp_residential_list)):
                            temp_residential_list.insert(0,0)
                    if len(transit_list) > len(temp_transit_list):
                        for i in range(len(transit_list) - len(temp_transit_list)):
                            temp_transit_list.insert(0,0)

                    for j in range(len(residential_list)):
                        residential_list[j] += temp_residential_list[j]
                    for j in range(len(transit_list)):
                        transit_list[j] += temp_transit_list[j]

            num_counties = i

loc_list = [retail_rec_list, groc_pharm_list, parks_list, transit_list, work_list, residential_list]
# temporary fix attempt
loc_list = [transit_list]
if location == 'Michigan':
    loc_list = [transit_list]
print('length of Google data: ' + str(len(loc_list[0])))

for i in range(len(loc_list[0])):
	loc_list[0][i] = loc_list[0][i]/num_counties

#plt.plot(loc_list[0])
#plt.show()
print(loc_list)

##############################################################
#                                                            #
#                      Get Vaccine Data                      #
#                                                            #
##############################################################

def Get_Vaccine_Data(region):
    location = region
    today_dmy = calendar.today().strftime('%m-%d-%Y')
    path_to_file = './gen_csvs/Covid_Vaccine_Coverage_by_County_'+today_dmy+'.csv'
    vacc_df = pd.read_csv(path_to_file)

    print('df found')
    for i in range(len(reg_county_dict[location])):
        print('opening loop')
        county = reg_county_dict[location][i].title()
        county_vaccines = vacc_df.loc[vacc_df["Person's Residence in County"] == county]
        print(county_vaccines)
        county_vaccines_dates = county_vaccines['Week Ending Date'].unique()
        print('dates'+str(county_vaccines_dates))
    
        vaccine_weekly_dates = []
        vaccine_weekly_sums = []
        for date in county_vaccines_dates:
            vaccine_weekly_dates.append(date)
           
            vaccines_this_week = county_vaccines.loc[county_vaccines['Week Ending Date'] == date]['Number of Doses']
            vaccine_weekly_sums.append(np.sum(vaccines_this_week))

        #print('unsorted_dates'+str(vaccine_weekly_dates))
        #print('unsorted_vals'+str(vaccine_weekly_sums))

        # Clear nans
        vaccine_weekly_dates, vaccine_weekly_sums = vaccine_weekly_dates[1:], vaccine_weekly_sums[1:]

        zipped_lists = zip(vaccine_weekly_dates, vaccine_weekly_sums)
        sorted_zipped_lists = sorted(zipped_lists,key = lambda this_pair: datetime.strptime(this_pair[0], "%m/%d/%Y"))

        sorted_weekly_dates, sorted_weekly_vacc_sums = zip(*sorted_zipped_lists)
        #print('sorted_dates'+str(sorted_weekly_dates))
        #print('sorted_vals'+str(sorted_weekly_vacc_sums))

        sorted_weekly_vacc_sums = [float(val) for val in sorted_weekly_vacc_sums]

        vacc_sum = sorted_weekly_vacc_sums[0]
        sorted_weekly_vacc_totals = [vacc_sum]
        for j in range(1, len(sorted_weekly_vacc_sums)):
            vacc_sum += sorted_weekly_vacc_sums[i]
            sorted_weekly_vacc_totals.append(vacc_sum)

        daily_vacc_totals = []
        daily_dates = []
        for j in range(len(sorted_weekly_vacc_totals)-1):
            week_start_date = sorted_weekly_dates[j]
            #print('start date'+str(week_start_date))
            for k in range(7):
                daily_vacc_totals.append((sorted_weekly_vacc_totals[j+1]-sorted_weekly_vacc_totals[j])/7*k + sorted_weekly_vacc_totals[j])
                daily_dates.append(datetime.strptime(week_start_date, "%m/%d/%Y") + timedelta(k))

        daily_vacc_totals.append(sorted_weekly_vacc_totals[-1])
        daily_dates.append(datetime.strptime(sorted_weekly_dates[-1], "%m/%d/%Y"))

        print('full dates'+str(daily_dates))
        #print('wkly vacc'+str(sorted_weekly_vacc_totals))
        #print('full vacc'+str(daily_vacc_totals))
        
        #print(len(daily_vacc_totals))
        return daily_vacc_totals

print('getting vaccine data')
vaccination_data = Get_Vaccine_Data(location)
vacc_list = [vaccination_data]

##############################################################
#                                                            #
#                 Prepare Data for Regression                #
#                                                            #
##############################################################

# Create constant arrays for aggregate model performance checks
hosps_per_mil_arr_checker = hosps_per_mil[:]
daily_hosps_per_mil_arr_checker = hosps_per_mil_arr_checker[:]
daily_hosps_per_mil = hosps_per_mil

active_arr_checker, deaths_arr_checker, confirmed_arr_checker, recovered_arr_checker, hosps_per_mil_arr_checker = active[:], deaths[:], confirmed[:], r_sum_cut[:], hosps_per_mil[:]
# Create constant arrays for incident model performance checks
daily_active_arr_checker, daily_recovered_arr_checker, daily_deaths_arr_checker, daily_confirmed_arr_checker, daily_hosps_per_mil_arr_checker = cumulativeToDaily([active_arr_checker, recovered_arr_checker, deaths_arr_checker, confirmed_arr_checker, hosps_per_mil_arr_checker])

# Create daily active, recovered, deaths, and confirmed lists for incident value prediction making
daily_active, daily_recovered, daily_confirmed, daily_deaths, daily_hosps_per_mil = cumulativeToDaily([active, r_sum_cut, confirmed, deaths, hosps_per_mil])

if location == 'Illinois':
    #daily_confirmed[106] = (daily_confirmed[105] + daily_confirmed[107])/2.
    daily_deaths[31] = (daily_deaths[30] + daily_deaths[32])/2.
    daily_confirmed[90] = (daily_confirmed[89] + daily_confirmed[91])/2.
    daily_confirmed[209] = daily_confirmed[210]*.45
    daily_confirmed[210] = daily_confirmed[210]*.55

if location == 'Ohio':
    daily_confirmed[173] = (daily_confirmed[174])/2.
    daily_confirmed[174] = (daily_confirmed[174])/2.
    daily_confirmed[185] = (daily_confirmed[184]+daily_confirmed[186])/2.

if location == 'Arkansas':
    daily_deaths[101] = (daily_deaths[100] + daily_deaths[102])/2.

if location == 'New York':
    daily_deaths[24] = (daily_deaths[23] + daily_deaths[25])/2.

if location == 'Wisconsin':
    daily_confirmed[133] = daily_confirmed[132]
    daily_confirmed[134] = daily_confirmed[133]
    daily_confirmed[135] = (daily_confirmed[134] + daily_confirmed[136])/2.
    daily_deaths[159] = (daily_deaths[157] + daily_deaths[159])/2.

if location == 'US':
    daily_deaths[9] = (daily_deaths[8] + daily_deaths[10])/2.
    daily_deaths[164] = daily_deaths[162]
    daily_deaths[165] = daily_deaths[164]
    daily_confirmed[163] = (daily_confirmed[162] + daily_confirmed[164])/2.

if location == 'Indiana':
    daily_deaths[243] = daily_deaths[244]*.55

if location == 'Michigan':
    for i in range(len(daily_confirmed)):
        if daily_deaths[i] == 0:
            daily_deaths[i] = .45 * daily_deaths[i+1]
            daily_deaths[i+1] = .55 * daily_deaths[i+1]

# Trim first days of data for fitting the model (See function comments)
active_for_ridge        = Truncate_For_Ridge([active       ], order     )
recovered_for_ridge     = Truncate_For_Ridge([r_sum_cut    ], order     )
deaths_for_ridge        = Truncate_For_Ridge([deaths       ], order     )
confirmed_for_ridge     = Truncate_For_Ridge([confirmed    ], case_order)
hosps_per_mil_for_ridge = Truncate_For_Ridge([hosps_per_mil], hosp_order)

if len(daily_hosps_per_mil) > len(daily_confirmed):
    daily_hosps_per_mil = daily_hosps_per_mil[:-(len(daily_hosps_per_mil)-len(daily_confirmed))]

for i in range(len(vacc_list)):
    print(i)
    if len(vacc_list[i]) < len(daily_confirmed):
        print('was too short')
        for j in range(len(daily_confirmed) - len(vacc_list[i])):
            vacc_list[i].insert(0,0)

    elif len(vacc_list[i]) > len(daily_confirmed):
        print('was too long'+str(len(vacc_list[i]) - len(daily_confirmed)))
        print('before'+str(len(vacc_list[i])))
        leng_diff = len(vacc_list[i]) - len(daily_confirmed)
        vacc_list[i] = vacc_list[i][leng_diff:]
        print('after'+str(len(vacc_list[i])))

print(len(vacc_list[0]))
print(len(daily_confirmed))

# Create matrices for prediction, as in LaTeX document
def Create_Pred_Matrices(full_data_lists, this_order = order, pred_type = CASES):
    matrices = []
    for list_i in full_data_lists:
        pred_matrix = []
        for i in range(len(list_i) - this_order):
            #print(i)
            new_row_daily = [1]
            for j in range(this_order):
                new_row_daily.append(list_i[i+j])
                #print('data'+str(len(list_i)))
            for vacc in vacc_list:
                for j in range(this_order):
                   #print('appending'+str(vacc[i+j]))
                    new_row_daily.append(vacc[i+j])
                    #print('vacc'+str(len(vacc)))
            # Append the social distancing averages for days -(7+difference) through -difference    
            # E.g. for order = 21 and reach_back = 5, i = 0 really gives day 22
            # To reach back 5 days, want days (11,...,17) = (21 + 1 + i - 6 - 5, 21 + 1 + i - 5)
            if pred_type == CASES:
                for loc in loc_list:
                    #print('list length ' + str(len(loc)))
                    #print((len(list_i)-order+order-social_distancing_reach_back-1-1))
                    # Not doing averages: reach back and do -order+social_distancing_reach_back amount of values
                    # E.g. for order = 21 and reach_back = 5, i = 0 really gives day 22
                    # To reach back 5 days, want (0,...,17) = i, 21 + 1 + i - 5
                    # If 40 total days, eventually want (15,...,35) = ()
                    #print(social_distancing_reach_back)
                    #print(order+i-social_distancing_reach_back)
                    for j in range(i, this_order+i-social_distancing_reach_back):
                        new_row_daily.append(loc[j])

            #elif pred_type == HOSPS:
            #    for positivity in positivity_list:
            #        for j in range(i, this_order+i):
            #            new_row_daily.append(positivity[j])

            else:
                for j in range(this_order):
                    new_row_daily.append(daily_confirmed[i+j])
            # Test of scaling
            #new_row_daily = preprocessing.scale(new_row_daily)
            pred_matrix.append(new_row_daily)
        # Test of scaling
        #pred_matrix = preprocessing.scale(pred_matrix)
        matrices.append(pred_matrix)
        #print(len(matrices))
        #print(len(matrices[0][0]))
        #print(len(matrices[0][0]))
        #print(len(matrices[0][-1]))
    return matrices[0]

# Create matrices, as in LaTeX document, for active cases, recoveries, deaths, and confirmed cases
daily_active, daily_recovered, daily_deaths, daily_confirmed = Clean_Sub_Zero([daily_active, daily_recovered, daily_deaths, daily_confirmed])
daily_active_pred_matrix = Create_Pred_Matrices([daily_active])
daily_recovered_pred_matrix = Create_Pred_Matrices([daily_recovered])
daily_deaths_pred_matrix = Create_Pred_Matrices([daily_deaths], order, DEATHS)
daily_confirmed_pred_matrix = Create_Pred_Matrices([daily_confirmed], case_order, CASES)

daily_hosps_per_mil_pred_matrix = Create_Pred_Matrices([daily_hosps_per_mil], hosp_order, HOSPS)

# Remove 0's
active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge = Clean_Sub_Zero([active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge])
# Trim first days of data for fitting incident data
daily_active_for_ridge        = Truncate_For_Ridge([daily_active       ], order     )
daily_recovered_for_ridge     = Truncate_For_Ridge([daily_recovered    ], order     )
daily_deaths_for_ridge        = Truncate_For_Ridge([daily_deaths       ], order     )
daily_confirmed_for_ridge     = Truncate_For_Ridge([daily_confirmed    ], case_order)
#print('daily_hospital: ' + str(daily_hospital))
daily_hosps_per_mil_for_ridge = Truncate_For_Ridge([daily_hosps_per_mil], hosp_order)

# Make constant values to revert to for both daily and cumulative model
daily_active_arr_const, daily_active_for_ridge_const = daily_active[:], daily_active_for_ridge[:]
daily_recovered_arr_const, daily_recovered_for_ridge_const = daily_recovered[:], daily_recovered_for_ridge[:]
daily_deaths_arr_const, daily_deaths_for_ridge_const = daily_deaths[:], daily_deaths_for_ridge[:]
daily_confirmed_arr_const, daily_confirmed_for_ridge_const = daily_confirmed[:], daily_confirmed_for_ridge[:]
daily_hosps_per_mil_arr_const, daily_hosps_per_mil_for_ridge_const = daily_hosps_per_mil[:], daily_hosps_per_mil_for_ridge[:]

active_for_ridge_const, recovered_for_ridge_const, deaths_for_ridge_const, confirmed_for_ridge_const = active_for_ridge[:], recovered_for_ridge[:], deaths_for_ridge[:], confirmed_for_ridge[:]

parameters = Generate_Alphas([1., 2.5, 5., 7.5])

###################################################
#                                                 #
#            Perform Ridge Regression             #
#                                                 #
###################################################

# Create grid searchers to find optimal ridge penalties for active and total cases, deaths, and recoveries
r_active, r_recovered, r_deaths, r_confirmed, r_hosps_per_mil = Make_Ridge([active, recovered, deaths, confirmed, hosps_per_mil], parameters, 'no_cv')
ridge_active_cv, ridge_recovered_cv, ridge_deaths_cv, ridge_confirmed_cv, ridge_hosps_per_mil_cv = Make_Ridge([r_active, r_recovered, r_deaths, r_confirmed, r_hosps_per_mil], parameters, 'cv')

# Creating triples of values for fitting
# Triples include the regressor, the prediction matrix, and the vector from the RHS of the LaTeX document
active_triad, recovered_triad, deaths_triad, confirmed_triad, hosps_per_mil_triad = (ridge_active_cv, daily_active_pred_matrix, daily_active_for_ridge), (ridge_recovered_cv, daily_recovered_pred_matrix, daily_recovered_for_ridge), (ridge_deaths_cv, daily_deaths_pred_matrix, daily_deaths_for_ridge), (ridge_confirmed_cv, daily_confirmed_pred_matrix, daily_confirmed_for_ridge), (ridge_hosps_per_mil_cv, daily_hosps_per_mil_pred_matrix, daily_hosps_per_mil_for_ridge)
#print('matrix: ' + str(daily_confirmed_pred_matrix))
# Get best penalties and performance metrics
assigned_alphas, assigned_scores = Fit_Ridge([active_triad, recovered_triad, deaths_triad, confirmed_triad, hosps_per_mil_triad], 'round_1_eval')
alpha_mid_active, alpha_mid_recovered, alpha_mid_deaths, alpha_mid_confirmed, alpha_mid_hospital = assigned_alphas[0], assigned_alphas[1], assigned_alphas[2], assigned_alphas[3], assigned_alphas[4]
score_active, score_recovered, score_deaths, score_confirmed, score_hospital = assigned_scores[0], assigned_scores[1], assigned_scores[2], assigned_scores[3], assigned_scores[4]

print(str(alpha_mid_active) + ' ' + str(alpha_mid_recovered) + ' ' + str(alpha_mid_deaths) + ' ' + str(alpha_mid_confirmed) + ' ' + str(alpha_mid_hospital))
print('Score Active: ' + str(score_active) + '\nScore Recovered: ' + str(score_recovered) + '\nScore Deaths: ' + str(score_deaths) + '\nScore Confirmed: ' + str(score_confirmed) + '\nScore Hospital: ' + str(score_hospital))

ridge_deaths, ridge_confirmed, ridge_hosps_per_mil = Make_Ridge([deaths, confirmed, hosps_per_mil], parameters, 'no_cv_alpha_spec', [alpha_mid_deaths, alpha_mid_confirmed, alpha_mid_hospital])

##############################################################
#                                                            #
#                        Bootstrapping                       #
#                                                            #
##############################################################

required_death_quantiles = [.01, .025, .05, .1, .15, .2, .25, .3, .35, .4, .45, .5, .55, .6, .65, .7, .75, .8, .85, .9, .95, .975, .99]
required_conf_quantiles = [0.025, 0.100, 0.250, 0.500, 0.750, 0.900, 0.975]
required_hosps_per_mil_quantiles = required_death_quantiles[:]
#required_conf_quantiles = required_death_quantiles[:]

# Create samples of length <order> to be used for predictions
# Like the final prediction-generation example in the LaTeX doc
# Append regressor predictions to each model's data
#data_diff = social_distancing_reach_back-(len(confirmedCases) - len(loc_list[0]))

preds_made = 0
def Create_Samples(lists_for_sampling, this_order = order, pred_type = CASES):
    global preds_made
    sample_list = []
    for list_i in lists_for_sampling:
        sample = [1]
        i = -this_order
        while i < 0:
            sample.append(list_i[i])
            i += 1

        for vacc in vacc_list:
            for val in vacc[-this_order:]:
                sample.append(val)

        if pred_type == CASES:
            for loc in loc_list:
                loc_vals = loc[-this_order+social_distancing_reach_back:]
                #print(len(loc_vals))
                for i in range(len(loc_vals)):
                    #if preds_made > 5 and (i == (len(loc_vals) - 8)):# or i == (len(loc_vals) - 6)):
                    #    sample.append(loc_vals[-7])
                    #elif preds_made < 6:
                    #    sample.append(loc_vals[(i+preds_made) % len(loc_vals)])
                    #else:
                    sample.append(loc_vals[i])

        #elif pred_type == HOSPS:
        #    for positivity in positivity_list:
        #        for val in positivity[-this_order:]:
        #            sample.append(val)

        else:
            i = -this_order
            while i < 0:
                sample.append(daily_confirmed_for_ridge[i])
                i += 1
        #sample = preprocessing.scale(sample)
        sample_list.append(np.asarray(sample, dtype = 'object'))#.reshape(1,-1))
        preds_made += 1
        #print(sample_list)
        #print(preds_made)
    return sample_list

confirmed_cumulative_start = 0
deaths_cumulative_start = 0
hosps_per_mil_cumulative_start = 0
# Function adapted from: https://saattrupdan.github.io/2020-03-01-bootstrap-prediction/
def Prediction_Interval(model, X_train, y_train, x0, pred_type = CASES):
  X_train = np.asarray(X_train)
  y_train = np.asarray(y_train)
  # Number of training samples
  n = X_train.shape[0]

  # The authors choose the number of bootstrap samples as the square root
  # of the number of samples
  nbootstraps = int(np.sqrt(n))

  # Compute the m_i's and the validation residuals
  bootstrap_preds, val_residuals = np.empty(nbootstraps), []
  for b in range(nbootstraps):
    train_idxs = np.random.choice(range(n), size = n, replace = True)
    val_idxs = np.array([idx for idx in range(n) if idx not in train_idxs])
    model.fit(X_train[train_idxs, :], y_train[train_idxs])
    preds = model.predict(X_train[val_idxs])
    val_residuals.append(y_train[val_idxs] - preds)
    bootstrap_preds[b] = model.predict(x0)
  bootstrap_preds -= np.mean(bootstrap_preds)
  val_residuals = np.concatenate(val_residuals)

  # Compute the prediction and the training residuals
  model.fit(X_train, y_train)
  preds = model.predict(X_train)
  if pred_type == CASES:
      confirmed_cumulative_start = preds[-1]
  elif pred_type == HOSPS:
      hosps_per_mil_cumulative_start = preds[-1]
  else:
      deaths_cumulative_start = preds[-1]
  train_residuals = y_train - preds

  # Take percentiles of the training- and validation residuals to enable
  # comparisons between them
  val_residuals = np.quantile(val_residuals, q = np.arange(100)/100)
  train_residuals = np.quantile(train_residuals, q = np.arange(100)/100)

  # Compute the .632+ bootstrap estimate for the sample noise and bias
  no_information_error = np.mean(np.abs(np.random.permutation(y_train) - \
    np.random.permutation(preds)))
  generalisation = np.abs(val_residuals - train_residuals)
  no_information_val = np.abs(no_information_error - train_residuals)
  relative_overfitting_rate = np.mean(generalisation / no_information_val)
  weight = .632 / (1 - .368 * relative_overfitting_rate)
  residuals = (1 - weight) * train_residuals + weight * val_residuals

  # Construct the C set and get the percentiles
  C = np.array([m + o for m in bootstrap_preds for o in residuals])

  if pred_type == CASES:
    percentiles = np.quantile(C, q = required_conf_quantiles)
  elif pred_type == HOSPS:
    percentiles = np.quantile(C, q = required_hosps_per_mil_quantiles)
  else:
    percentiles = np.quantile(C, q = required_death_quantiles)

  y_hat = model.predict(x0)
  #percentiles = [y_hat + percentile for percentile in percentiles]
  return percentiles, y_hat

stop = int(len(daily_active_arr_checker)) + 40 - 1
start = len(daily_active) - 1
    
quantiles_confirmed, quantiles_deaths, quantiles_hosps_per_mil = [], [], []
points_confirmed, points_deaths, points_hosps_per_mil = [], [], []

daily_confirmed_for_ridge_orig = daily_confirmed_for_ridge[:]
daily_deaths_for_ridge_orig = daily_deaths_for_ridge[:]
#print(daily_hospital_for_ridge)
daily_hosps_per_mil_for_ridge_orig = daily_hosps_per_mil_for_ridge[:]

# Make ~6 weeks worth of predictions
t = start
while t < stop:
    # Make predictions
    this_day_quantiles_confirmed, this_day_point_confirmed = Prediction_Interval(ridge_confirmed, daily_confirmed_pred_matrix, daily_confirmed_for_ridge_orig, Create_Samples([daily_confirmed_for_ridge], case_order, CASES), CASES)
    this_day_quantiles_deaths, this_day_point_deaths = Prediction_Interval(ridge_deaths, daily_deaths_pred_matrix, daily_deaths_for_ridge_orig, Create_Samples([daily_deaths_for_ridge], order, DEATHS), DEATHS)
    #print(daily_hospital_for_ridge_orig)
    this_day_quantiles_hosps_per_mil, this_day_point_hosps_per_mil = Prediction_Interval(ridge_hosps_per_mil, daily_hosps_per_mil_pred_matrix, daily_hosps_per_mil_for_ridge_orig, Create_Samples([daily_hosps_per_mil_for_ridge], hosp_order, HOSPS), HOSPS)
    # Append predictions to model
    daily_deaths_for_ridge, daily_deaths, daily_confirmed_for_ridge, daily_confirmed, daily_hosps_per_mil_for_ridge, daily_hosps_per_mil = Append_To_Col_Vect([daily_deaths_for_ridge, daily_deaths, daily_confirmed_for_ridge, daily_confirmed, daily_hosps_per_mil_for_ridge, daily_hosps_per_mil], [this_day_point_deaths, this_day_point_deaths, this_day_point_confirmed, this_day_point_confirmed, this_day_point_hosps_per_mil, this_day_point_hosps_per_mil])

    # Quantiles are additive - like SDev
    quantiles_confirmed.append(this_day_quantiles_confirmed)
    points_confirmed.append(this_day_point_confirmed)
    quantiles_deaths.append(this_day_quantiles_deaths)
    points_deaths.append(this_day_point_deaths)
    quantiles_hosps_per_mil.append(this_day_quantiles_hosps_per_mil)
    points_hosps_per_mil.append(this_day_point_hosps_per_mil)

    t += 1

points_confirmed_aggregate, points_deaths_aggregate, points_hosps_per_mil_aggregate = dailyToCumulative([(points_confirmed, confirmed_cumulative_start + confirmedCases[-1]), (points_deaths, deaths_cumulative_start + deaths[-1]), (points_hosps_per_mil, hosps_per_mil_cumulative_start + hosps_per_mil[-1])])

########################################################
#                                                      #
#                   Sort Quantiles                     #
#                                                      #
########################################################

# Separate lists for each quantile, for recording data in csv
# "dc" for "death count"
quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99 = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

# Separate lists for each quantile
# "cc" for "case count"
quantiles_cc2_5, quantiles_cc10, quantiles_cc25, quantiles_cc50, quantiles_cc75, quantiles_cc90, quantiles_cc97_5 = [], [], [], [], [], [], []
#quantiles_cc1, quantiles_cc2_5, quantiles_cc5, quantiles_cc10, quantiles_cc15, quantiles_cc20, quantiles_cc25, quantiles_cc30, quantiles_cc35, quantiles_cc40, quantiles_cc45, quantiles_cc50, quantiles_cc55, quantiles_cc60, quantiles_cc65, quantiles_cc70, quantiles_cc75, quantiles_cc80, quantiles_cc85, quantiles_cc90, quantiles_cc95, quantiles_cc97_5, quantiles_cc99 = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []
quantiles_h1, quantiles_h2_5, quantiles_h5, quantiles_h10, quantiles_h15, quantiles_h20, quantiles_h25, quantiles_h30, quantiles_h35, quantiles_h40, quantiles_h45, quantiles_h50, quantiles_h55, quantiles_h60, quantiles_h65, quantiles_h70, quantiles_h75, quantiles_h80, quantiles_h85, quantiles_h90, quantiles_h95, quantiles_h97_5, quantiles_h99 = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

# A list of quantile lists for deathsi
deaths_quant_list = [quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99]

# A list of quantile lists for cases
confirmed_quant_list = [quantiles_cc2_5, quantiles_cc10, quantiles_cc25, quantiles_cc50, quantiles_cc75, quantiles_cc90, quantiles_cc97_5]
#confirmed_quant_list = [quantiles_cc1, quantiles_cc2_5, quantiles_cc5, quantiles_cc10, quantiles_cc15, quantiles_cc20, quantiles_cc25, quantiles_cc30, quantiles_cc35, quantiles_cc40, quantiles_cc45, quantiles_cc50, quantiles_cc55, quantiles_cc60, quantiles_cc65, quantiles_cc70, quantiles_cc75, quantiles_cc80, quantiles_cc85, quantiles_cc90, quantiles_cc95, quantiles_cc97_5, quantiles_cc99]

hosps_per_mil_quant_list = [quantiles_h1, quantiles_h2_5, quantiles_h5, quantiles_h10, quantiles_h15, quantiles_h20, quantiles_h25, quantiles_h30, quantiles_h35, quantiles_h40, quantiles_h45, quantiles_h50, quantiles_h55, quantiles_h60, quantiles_h65, quantiles_h70, quantiles_h75, quantiles_h80, quantiles_h85, quantiles_h90, quantiles_h95, quantiles_h97_5, quantiles_h99]

# Total number of points to have generated by end of each quantile bracket
points_to_include_deaths = [2, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 195, 198, 200]

points_to_include_confirmed = [5, 20, 50, 100, 150, 180, 195, 200]

points_to_include_hosps_per_mil = points_to_include_deaths[:]

##################################################
#                                                #
#          Additive Quantile Creation            #
#                                                #
##################################################

expanded_quantiles_confirmed = [quantiles_confirmed[0]]
expanded_quantiles_deaths = [quantiles_deaths[0]]
expanded_quantiles_hosps_per_mil = [quantiles_hosps_per_mil[0]]

for i in range(1, len(quantiles_confirmed)):
    previous_quantiles_confirmed = []
    previous_quantiles_deaths = []
    previous_quantiles_hosps_per_mil = []

    this_day_quantile_sums_confirmed = []
    this_day_quantile_sums_deaths = []
    this_day_quantile_sums_hosps_per_mil = []

    for j in range(i):
        previous_quantiles_confirmed.append(quantiles_confirmed[j])
        previous_quantiles_deaths.append(quantiles_deaths[j])
        previous_quantiles_hosps_per_mil.append(quantiles_hosps_per_mil[j])

    for j in range(100):
        random_pulls_quantiles_confirmed = []
        random_pulls_quantiles_deaths = []
        random_pulls_quantiles_hosps_per_mil = []

        for quantile_set in previous_quantiles_confirmed:
            representative_set_confirmed = []

            for k in range(0, len(points_to_include_confirmed)):
                if k == 0:
                    samples_to_take = 2
                    representative_set_list = np.random.uniform(0, quantile_set[0], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_confirmed.append(rep)

                elif k < len(points_to_include_confirmed) - 1:

                    samples_to_take = points_to_include_confirmed[k] - points_to_include_confirmed[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], quantile_set[k], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_confirmed.append(rep)

                else:
                    samples_to_take = points_to_include_confirmed[k] - points_to_include_confirmed[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], 1.2*quantile_set[k-1], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_confirmed.append(rep)

            random_pulls_quantiles_confirmed.append(random.choice(representative_set_confirmed))

        this_day_quantile_sums_confirmed.append(np.sum(random_pulls_quantiles_confirmed))

        for quantile_set in previous_quantiles_deaths:
            representative_set_deaths = []

            for k in range(0, len(points_to_include_deaths)):
                if k == 0:
                    samples_to_take = 2
                    representative_set_list = np.random.uniform(0, quantile_set[0], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_deaths.append(rep)

                elif k < len(points_to_include_deaths) - 1:
                    samples_to_take = points_to_include_deaths[k] - points_to_include_deaths[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], quantile_set[k], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_deaths.append(rep)

                else:
                    samples_to_take = points_to_include_deaths[k] - points_to_include_deaths[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], 1.2*quantile_set[k-1], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_deaths.append(rep)

            random_pulls_quantiles_deaths.append(random.choice(representative_set_deaths))

        this_day_quantile_sums_deaths.append(np.sum(random_pulls_quantiles_deaths))

        for quantile_set in previous_quantiles_hosps_per_mil:
            representative_set_hosps_per_mil = []

            for k in range(0, len(points_to_include_hosps_per_mil)):
                if k == 0:
                    samples_to_take = 2
                    representative_set_list = np.random.uniform(0, quantile_set[0], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_hosps_per_mil.append(rep)

                elif k < len(points_to_include_hosps_per_mil) - 1:
                    samples_to_take = points_to_include_hosps_per_mil[k] - points_to_include_hosps_per_mil[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], quantile_set[k], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_hosps_per_mil.append(rep)

                else:
                    samples_to_take = points_to_include_hosps_per_mil[k] - points_to_include_hosps_per_mil[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], 1.2*quantile_set[k-1], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_hosps_per_mil.append(rep)

            random_pulls_quantiles_hosps_per_mil.append(random.choice(representative_set_hosps_per_mil))

        this_day_quantile_sums_hosps_per_mil.append(np.sum(random_pulls_quantiles_hosps_per_mil))

    expanded_quantiles_confirmed.append(np.quantile(this_day_quantile_sums_confirmed, q = required_conf_quantiles))
    expanded_quantiles_deaths.append(np.quantile(this_day_quantile_sums_deaths, q = required_death_quantiles))
    expanded_quantiles_hosps_per_mil.append(np.quantile(this_day_quantile_sums_hosps_per_mil, q = required_hosps_per_mil_quantiles))

quantiles_confirmed = expanded_quantiles_confirmed
quantiles_deaths = expanded_quantiles_deaths
quantiles_hosps_per_mil = expanded_quantiles_hosps_per_mil

##################################################
#                                                #
#          Adding Quantiles to Points            #
#                                                #
##################################################

deaths_quant_list_aggregate = []
confirmed_quant_list_aggregate = []
hosps_per_mil_quant_list_aggregate = []
summed_confirmed_quantiles = []
summed_deaths_quantiles = []
summed_hosps_per_mil_quantiles = []

for i in range(len(quantiles_confirmed)):
    # Get quantiles for each individual day
    daily_quantiles = quantiles_confirmed[i]
    # Add these quantiles to the day's point prediction to get prediction spectrum
    summed_confirmed_quantiles.append((daily_quantiles + points_confirmed_aggregate[i]).tolist())
    
# Index over every day of summed quantiles
for i in range(len(summed_confirmed_quantiles)):
    # Set a single day's quantiles
    this_days_quantiles = summed_confirmed_quantiles[i]
    # Index over each quantile for that day, and append it to the appropriate chronological quantile list
    for j in range(len(this_days_quantiles)):
        confirmed_quant_list[j].append(this_days_quantiles[j])

for i in range(len(quantiles_deaths)):
    # Get quantiles for each individual day
    daily_quantiles = quantiles_deaths[i]
    # Add these quantiles to the day's point prediction to get prediction spectrum
    summed_deaths_quantiles.append((daily_quantiles + points_deaths_aggregate[i]).tolist())
    
# Index over every day of summed quantiles
for i in range(len(summed_deaths_quantiles)):
    # Set a single day's quantiles
    this_days_quantiles = summed_deaths_quantiles[i]
    # Index over each quantile for that day, and append it to the appropriate chronological quantile list
    for j in range(len(this_days_quantiles)):
        deaths_quant_list[j].append(this_days_quantiles[j])

for i in range(len(quantiles_hosps_per_mil)):
    # Get quantiles for each individual day
    daily_quantiles = quantiles_hosps_per_mil[i]
    # Add these quantiles to the day's point prediction to get prediction spectrum
    summed_hosps_per_mil_quantiles.append((daily_quantiles + points_hosps_per_mil_aggregate[i]).tolist())

# Index over every day of summed quantiles
for i in range(len(summed_hosps_per_mil_quantiles)):
    # Set a single day's quantiles
    this_days_quantiles = summed_hosps_per_mil_quantiles[i]
    # Index over each quantile for that day, and append it to the appropriate chronological quantile list
    for j in range(len(this_days_quantiles)):
        hosps_per_mil_quant_list[j].append(this_days_quantiles[j])

################################################
#                                              #
#               Plot Quantiles                 #
#                                              #
################################################

fig, ax1 = plt.subplots()

ax1.plot(deaths[:start+28], 'g', linestyle = '--', label = 'model')
ax1.plot([i for i in range(start, start+28)], deaths_quant_list[2][:28])
ax1.plot([i for i in range(start, start+28)], deaths_quant_list[-3][:28])
ax1.plot(deaths_arr_checker[:start+28], 'k', label = 'data')
ax1.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
ax1.set_xlabel('Days')
ax1.set_ylabel('Deaths')
ax1.set_title('Deaths Prediction\n' + str(location))
ax1.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_deaths.pdf')
plt.show()

fig, ax1 = plt.subplots()

ax1.plot(hosps_per_mil[:start+28], 'g', linestyle = '--', label = 'model')
ax1.plot([i for i in range(start, start+28)], hosps_per_mil_quant_list[2][:28])
ax1.plot([i for i in range(start, start+28)], hosps_per_mil_quant_list[-3][:28])
ax1.plot(hosps_per_mil_arr_checker[:start+28], 'k', label = 'data')
ax1.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
ax1.set_xlabel('Days')
ax1.set_ylabel('Deaths')
ax1.set_title('Deaths Prediction\n' + str(location))
ax1.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_deaths.pdf')
plt.show()

fig, ax1 = plt.subplots()

ax1.plot(daily_deaths[:start+28], 'g', linestyle = '--', label = 'model')
ax1.plot(daily_deaths_arr_checker[:start+28], 'k', label = 'data')
ax1.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
ax1.set_xlabel('Days')
ax1.set_ylabel('Confirmed')
ax1.set_title('Confirmed Prediction\n' + str(location))
ax1.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()

fig, ax1 = plt.subplots()

ax1.plot(daily_confirmed[:start+28], 'g', linestyle = '--', label = 'model')
ax1.plot(daily_confirmed_arr_checker[:start+28], 'k', label = 'data')
ax1.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
ax1.set_xlabel('Days')
ax1.set_ylabel('Confirmed')
ax1.set_title('Confirmed Prediction\n' + str(location))
ax1.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()

########################################################
#                                                      #
#                    Data Writing                      #
#                                                      #
########################################################

# NEW (9-7-2020) -- Use python's datetime library, set the current date

# get today's date
date_today = calendar.today()
# get the next Saturday's date
date_saturday = date_today + timedelta(-2)
print(date_saturday)

submission_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'location', 'type', 'quantile', 'value'])
# Edit days here
forecast_date = (date_saturday + timedelta(days = 1)).strftime('%Y-%m-%d')
target_end_date = (date_saturday + timedelta(days = 7)).strftime("%Y-%m-%d")

case_count_list = []
death_count_list = []

cumulative_deaths_for_adding = [most_recent_deaths]

# Edit days here
days_for_pred = [7, 14, 21, 28]

# Edit days here
for i in range(len(days_for_pred)):
    if i == 1:
        target_end_date = (date_saturday + timedelta(days = 14)).strftime("%Y-%m-%d")
    if i == 2:
        target_end_date = (date_saturday + timedelta(days = 21)).strftime("%Y-%m-%d")
    if i == 3:
        target_end_date = (date_saturday + timedelta(days = 28)).strftime("%Y-%m-%d")

    # No -1, since aggregates include most recent data point
    pred_day = days_for_pred[i]
    
    for j in range(len(confirmed_quant_list)):
        quantile = required_conf_quantiles[j]
        row = confirmed_quant_list[j]
        #if quantile == 0.975:
        #    break
        #else:
        if i == 0:
            case_inc = row[pred_day] - points_confirmed_aggregate[0]
            case_inc = case_inc
        else:
            case_inc = row[pred_day] - case_count_list[-1]
            case_inc = case_inc[0]
        if case_inc < 0:
            case_inc = 0.
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case'.format(week = i+1), target_end_date, location, 'quantile', quantile, case_inc]], columns = submission_frame.columns))

    for j in range(len(deaths_quant_list)):
        quantile = required_death_quantiles[j]
        row = deaths_quant_list[j]
        if i == 0:
            deaths_inc = row[pred_day] - points_deaths_aggregate[0]
            deaths_inc = float(deaths_inc)
        else:
            deaths_inc = row[pred_day] - death_count_list[-1]
            deaths_inc = float(deaths_inc)
        if deaths_inc < 0:
            deaths_inc = 0.
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'quantile', quantile, deaths_inc]], columns = submission_frame.columns))
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'quantile', quantile, row[pred_day]]], columns = submission_frame.columns))

    #pred_day = np.asarray(pred_day)
    #pred_day = pred_day.reshape(1,-1)
 
    if i == 0:
        case_inc = points_confirmed_aggregate[pred_day] - points_confirmed_aggregate[0]
    else:
        case_inc = points_confirmed_aggregate[pred_day] - case_count_list[-1]
    if case_inc < 0:
        case_inc = [0.]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case'.format(week = i+1), target_end_date, location, 'point', 'NA', float(case_inc[0])]], columns = submission_frame.columns))
    if i < 2:
        upper_bound_case = 1.5*float(case_inc[0])
        lower_bound_case = .5*float(case_inc[0])
    else:
        upper_bound_case = 2.*float(case_inc[0])
        lower_bound_case = 0.
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case bound'.format(week = i+1), target_end_date, location, 'upper bound', 'NA', upper_bound_case]], columns = submission_frame.columns))
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case bound'.format(week = i+1), target_end_date, location, 'lower bound', 'NA', lower_bound_case]], columns = submission_frame.columns))


    if i == 0:
        deaths_inc = points_deaths_aggregate[pred_day] - points_deaths_aggregate[0]
    else:
        deaths_inc = points_deaths_aggregate[pred_day] - death_count_list[-1]
    if deaths_inc < 0:
        deaths_inc = [0.]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'point', 'NA', float(deaths_inc[0])]], columns = submission_frame.columns))
    if i < 2:
        upper_bound_deaths = 1.5*float(deaths_inc[0])
        lower_bound_deaths = .5*float(deaths_inc[0])
    else:
        upper_bound_deaths = 2.*float(deaths_inc[0])
        lower_bound_deaths = 0.
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death bound'.format(week = i+1), target_end_date, location, 'upper bound', 'NA', upper_bound_deaths]], columns = submission_frame.columns))
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death bound'.format(week = i+1), target_end_date, location, 'lower bound', 'NA', lower_bound_deaths]], columns = submission_frame.columns))

    #this_cumulative = deaths_inc + cumulative_deaths_for_adding[-1]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'point', 'NA', float(points_deaths_aggregate[pred_day][0])]], columns = submission_frame.columns))
    if i < 2:
        upper_bound_cum_deaths = float(points_deaths_aggregate[pred_day][0]) + .5*float(deaths_inc[0])
        lower_bound_cum_deaths = float(points_deaths_aggregate[pred_day][0]) - .5*float(deaths_inc[0])
    else:
        upper_bound_cum_deaths = float(points_deaths_aggregate[pred_day][0]) + float(deaths_inc[0])
        lower_bound_cum_deaths = float(points_deaths_aggregate[pred_day][0]) - float(deaths_inc[0])
    #cumulative_deaths_for_adding.append(float(this_cumulative[0]))
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death bound'.format(week = i+1), target_end_date, location, 'upper bound', 'NA', upper_bound_cum_deaths]], columns = submission_frame.columns))
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death bound'.format(week = i+1), target_end_date, location, 'lower bound', 'NA', lower_bound_cum_deaths]], columns = submission_frame.columns))
    
    case_count_list.append(points_confirmed_aggregate[pred_day])
    death_count_list.append(points_deaths_aggregate[pred_day])

# Edit days here
submission_frame.to_csv(location + '/' + date_today.strftime("%Y-%m-%d") + '-weekly.csv', index = False)

##############################################
#                                            #
#           Daily Quantile Writing           #
#                                            #
##############################################

case_count_list = []
death_count_list = []

daily_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'location', 'type', 'quantile', 'value'])

for i in range(28):
    target_end_date = (date_saturday + timedelta(days = i + 1)).strftime("%Y-%m-%d")
    pred_day = i + 1

    for j in range(len(confirmed_quant_list)):
        quantile = required_conf_quantiles[j]
        row = confirmed_quant_list[j]
        if i == 0:
            case_inc = row[pred_day] - points_confirmed_aggregate[0]
            case_inc = float(case_inc)
        else:
            case_inc = row[pred_day] - case_count_list[-1]
            case_inc = float(case_inc)
        if case_inc < 0:
            case_inc = 0.
        #daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead inc case'.format(day = i+1), target_end_date, location, 'quantile', quantile, case_inc]], columns = daily_frame.columns))

    for j in range(len(deaths_quant_list)):
        quantile = required_death_quantiles[j]
        row = deaths_quant_list[j]
        if i == 0:
            deaths_inc = row[pred_day] - points_deaths_aggregate[0]
            deaths_inc = float(deaths_inc)
        else:
            deaths_inc = row[pred_day] - death_count_list[-1]
            deaths_inc = float(deaths_inc)
        if deaths_inc < 0:
            deaths_inc = 0.
        #daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead inc death'.format(day = i+1), target_end_date, location, 'quantile', quantile, deaths_inc]], columns = daily_frame.columns))

    if i == 0:
        case_inc = points_confirmed_aggregate[pred_day] - points_confirmed_aggregate[0]
    else:
        case_inc = points_confirmed_aggregate[pred_day] - case_count_list[-1]
    if case_inc < 0:
        case_inc = [0.]
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead inc case'.format(day = i+1), target_end_date, location, 'point', 'NA', float(case_inc[0])]], columns = daily_frame.columns))
    if i < 14:
        upper_bound_case = float(case_inc[0])*1.5
        lower_bound_case = float(case_inc[0])*.5
    else:
        upper_bound_case = float(case_inc[0])*2.
        lower_bound_case = 0.
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead inc case bound'.format(day = i+1), target_end_date, location, 'upper bound', 'NA', upper_bound_case]], columns = daily_frame.columns))
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead inc case bound'.format(day = i+1), target_end_date, location, 'lower bound', 'NA', lower_bound_case]], columns = daily_frame.columns))

    if i == 0:        
        deaths_inc = points_deaths_aggregate[pred_day] - points_deaths_aggregate[0]
    else:
        deaths_inc = points_deaths_aggregate[pred_day] - death_count_list[-1]
    if deaths_inc < 0:
        deaths_inc = [0.]
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead inc death'.format(day = i + 1), target_end_date, location, 'point', 'NA', float(deaths_inc[0])]], columns = daily_frame.columns))
    if i < 14:
        upper_bound_deaths = float(deaths_inc[0])*1.5
        lower_bound_deaths = float(deaths_inc[0])*.5
    else:
        upper_bound_deaths = float(deaths_inc[0])*2.
        lower_bound_deaths = 0.
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead inc death bound'.format(day = i + 1), target_end_date, location, 'upper bound', 'NA', upper_bound_deaths]], columns = daily_frame.columns))
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead inc death bound'.format(day = i + 1), target_end_date, location, 'lower bound', 'NA', lower_bound_deaths]], columns = daily_frame.columns))

    case_count_list.append(points_confirmed_aggregate[pred_day])
    death_count_list.append(points_deaths_aggregate[pred_day])

daily_frame.to_csv(location + '/' + date_today.strftime("%Y-%m-%d") + '-daily.csv', index = False)

#####################################################
#                                                   #
#               Hospital Data Writing               #
#                                                   #
#####################################################

#print(points_hosps_per_mil_aggregate)
#points_hosps_per_mil_aggregate = points_hosps_per_mil_aggregate[:start+28]
#points_hosps_per_mil_aggregate = points_hosps_per_mil_aggregate[-29:]
#print(points_hosps_per_mil_aggregate)

date_today = calendar.today()
forecast_date = date_today - timedelta(days = 1)
date_saturday = date_today - timedelta(days = 2)
hosps_per_mil_count_list = []

daily_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'type', 'value'])

for i in range(28):
    target_end_date = (date_saturday + timedelta(days = i+1)).strftime("%Y-%m-%d")
    pred_day = i+1

    #if i == 0:
    #    hosp_inc = points_covid_census_aggregate[pred_day] - points_covid_census_aggregate[0]
    #else:
    #    hosp_inc = points_covid_census_aggregate[pred_day] - hosp_count_list[-1]
    #if hosp_inc < 0:
    #    hosp_inc = [0.]
    if hosps_per_mil_quant_list[1][pred_day] < 0:
        daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead hosps per mil'.format(day = i+1), target_end_date, 'lower 95% quantile bound', 0.]], columns = daily_frame.columns))
    else:
        daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead hosps per mil'.format(day = i+1), target_end_date, 'lower 95% quantile bound', hosps_per_mil_quant_list[1][pred_day]]], columns = daily_frame.columns))
    daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead hosps per mil'.format(day = i+1), target_end_date, 'upper 95% quantile bound', hosps_per_mil_quant_list[-2][pred_day]]], columns = daily_frame.columns))
    if i == 0:
        daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead hosps per mil'.format(day = i+1), target_end_date, 'point prediction', points_hosps_per_mil_aggregate[pred_day][0]]], columns = daily_frame.columns))      
    else:
        daily_frame = daily_frame.append(pd.DataFrame([[forecast_date, '{day} day ahead hosps per mil'.format(day = i+1), target_end_date, 'point prediction', points_hosps_per_mil_aggregate[pred_day][0]]], columns = daily_frame.columns))

daily_frame.to_csv(location + '/' + location + date_today.strftime("%Y-%m-%d") + '-daily_hosps_per_mil.csv', index = False)

