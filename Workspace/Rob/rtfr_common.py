import re
import os
import configparser

import pandas          as     pd

from   datetime        import date      as calendar
from   datetime        import timedelta
from   dateutil.parser import parse

from   rtfr_constants  import *

# TODO: Check python version on great lakes and see if we need ConfigParser instead
def make_cfg_parser(path):
    
    parser = configparser.RawConfigParser()
    parser.read(path)
    
    return parser

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

# Split country/state name input into separate words if necessary
# e.g. NewJersey -> New Jersey
# REQUIRES: loc does not already have spaces between words
#           Each new word begins with a capital letter (i.e. camelCase)
#           Capital letters only occur at the beginning of a word
# TODO: Clean this up -- eliminate need by handling multi-word command line arguments?
def Split_Loc_Name(location):
    
    loc = location
    
    re_outer = re.compile(r'([^A-Z ])([A-Z])')
    re_inner = re.compile(r'(?<!^)([A-Z])([^A-Z])')
    
    loc = re_outer.sub(r'\1 \2', re_inner.sub(r' \1\2', loc))
    
    return loc

# Convert country or state/province to boolean
# Country -> true, State/province -> false
# TODO: MERC doesn't use this
def Loc_Type_To_Bool(loc_type):
    
    return loc_type.lower() != PROVINCE

def check_and_make_dir(loc):
    
    if not os.path.exists(loc):
        os.makedirs(loc)
        print('New directory created for', loc)

# Handle JHU South Korea name changes
# TODO: Anything I can do about this?
# TODO: Rename this function to something more descriptive
def Handle_South_Korea():
    
    location = r'South Korea|Korea, South|Republic of Korea'
    
    # if file_name == path_name + '03-10-2020.csv':
    #     location = 'Republic of Korea'
    # if file_name == path_name + '03-11-2020.csv':
    #     location = 'Korea, South'
    
    return location

# Handle JHU region type changes
def Get_Loc_Type_JHU(region_is_country):
    # We do this with regex now to eliminate the need to check the date
    areaLabel = 'Province(/|_)State'
    if region_is_country:
        areaLabel = 'Country(/|_)Region'
    return areaLabel

###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Append data from JHU
def Append_Data(confirmedCases, deaths, recovered, region_is_country, day_count, daily_data, loc):
    
    confirmedCases.append(0.)
    deaths.append(0.)
    recovered.append(0.)

    # Use global variables to update relevant values
    
    label = Get_Loc_Type_JHU(region_is_country)
    
    # Search daily_data for entries corresponding to 'loc' and sum them all up
    # summed_daily_data = daily_data[daily_data[daily_data.filter(regex = label).columns[0]] == loc].sum()
    # '... == True' is explicit here to deal with NaN values
    summed_daily_data = daily_data[daily_data[daily_data.filter(regex = label).columns[0]].str.match(loc) == True].sum()
    
    confirmedCases[day_count] = summed_daily_data[CONFIRMED_JHU_LABEL]
    deaths        [day_count] = summed_daily_data[DEATHS_JHU_LABEL   ]
    recovered     [day_count] = summed_daily_data[RECOVERED_JHU_LABEL]

    # Related sources on making this function efficient:
    # https://engineering.upside.com/a-beginners-guide-to-optimizing-pandas-code-for-speed-c09ef2c6a4d6
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/missing_data.html
    
def read_JHU_data(file_set, loc_regex, stop_date, region_is_country):
    
    confirmedCases, deaths, recovered, active = [], [], [], []
    day_count = 0
    
    # Loop over JHU data, and update cases, deaths, recoveries
    for file in file_set:

        # Break the loop once most recent Sunday hit; do not read in
        # Predictions made Saturday -> Saturday
        month, day_index, year = Get_File_Date(file, stop_date)
        if month == 'stop':
            print('Stopping data this past Sunday: ', str(stop_date))
            break
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Update confirmed, death, and recovery counts
        # TODO: Package all these arguments better
        Append_Data(confirmedCases, deaths, recovered, region_is_country, day_count, data, loc_regex)

        day_count += 1
        
    return day_count, confirmedCases, deaths, recovered, active
    
# Tells the data when to stop reading in
# Predictions made Saturday -> Saturday, but are created on Monday, so Sunday data dropped
# TODO: Generalize this so user can set the stop date/time (or a prediction duration)
def Get_Last_Sunday():
    today = calendar.today()
    idx = (today.weekday() + 1) % 7
    #idx = 0
    sun = today - timedelta(idx)
    sun = '{:%m/%d/%Y}'.format(sun)
    return sun

# Work-around, since Grok library not often available
# Convert JHU file names to usable dates
# TODO: Handle stop date better
# TODO: Can we use datetime to streamline things?
def Get_File_Date(file_name, stop_date):
    month, day, year = file_name[-14:-12], file_name[-11:-9], file_name[-8:-4]
    # TODO: Cleaner way to do this?
    if '{m}/{d}/{Y}'.format(m = month, d = day, Y = year) == stop_date:
        month, day, year = 'stop', 'stop', 'stop'
    return month, day, year

# Get date of day corresponding to minRange
# JHU data starts on January 22, so add minRange to January 22
# TODO: Make it possible to change first date from cfg, cmd line, etc
def Get_First_Date(minRange):
    first_day_US = parse(FIRST_DAY_OF_US_DATA)
    return (first_day_US + timedelta(days = minRange)).strftime('%Y-%m-%d')

###############################################################
#                                                             #
#            Trim Data and Create Active Case List            #
#                                                             #
###############################################################

# Cut off first few days, based on thresholds in Get_First_Day
def Truncate_From_Min(untrimmed_lists, minRange):
    trimmed_lists = []
    for list_i in untrimmed_lists:
        trimmed_lists.append(list_i[minRange:])
    return trimmed_lists



###############################################################
#                                                             #
#                          Unit Tests                         #
#                                                             #
###############################################################
    
if __name__ == '__main__':
    
    import unittest
    
    class test_split_loc_name(unittest.TestCase):
        
        def test_one_word(self):
            
            self.assertEqual(Split_Loc_Name('helloimafriend'), 'helloimafriend')
            self.assertEqual(Split_Loc_Name('a'), 'a')
            
        # This one fails right now, but it's not too big a deal
        # TODO: Pass this test
        def test_already_has_spaces(self):
            
            self.assertEqual(Split_Loc_Name('hello world'), 'hello world')
            self.assertEqual(Split_Loc_Name('Hello World'), 'Hello World')
            self.assertEqual(Split_Loc_Name(' '), ' ')
            
        def test_acronym(self):
            
            self.assertEqual(Split_Loc_Name('US'), 'US')
            self.assertEqual(Split_Loc_Name('ImInTheUSRightNow'), 'Im In The US Right Now')
            self.assertEqual(Split_Loc_Name('ImInTheUS'), 'Im In The US')
            
        def test_norm(self):
            
            self.assertEqual(Split_Loc_Name('NewJersey') , 'New Jersey')
            self.assertEqual(Split_Loc_Name('SouthKorea'), 'South Korea')
            
    unittest.main()