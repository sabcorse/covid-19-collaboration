#!/usr/bin/env python3

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import glob
import sys
import os
from datetime import datetime
from datetime import timedelta
from pygrok import Grok
from datetime import date
from time import sleep
import gspread
from oauth2client.service_account import ServiceAccountCredentials
from bs4 import BeautifulSoup
import requests
from selenium import webdriver

url = "https://www.mistartmap.info/compare"
html_content = requests.get(url).text
#soup = BeautifulSoup(html_content, "lxml")

def parse(url_h):
    response = webdriver.Firefox()
    response.get(url_h)
    sleep(5)
    sourceCode=response.page_source
    return sourceCode

soup = BeautifulSoup(parse(url), 'lxml')
div = soup.find('table', id='metrics-overview')
#print(div)

data = []
table_body = div.find('tbody')
rows = table_body.find_all('tr')
for row in rows:
    cols = row.find_all('td')
    cols = [ele.text.strip() for ele in cols]
    data.append([ele for ele in cols if ele]) # Get rid of empty values
#print(data)

county_list = ['genesee', 'lapeer', 'livingston', 'macomb', 'monroe', 'oakland', 'st. clair', 'washtenaw', 'wayne', 'clare', 'ionia', 'isabella', 'kent', 'lake', 'mason', 'mecosta', 'montcalm', 'muskegon', 'newaygo', 'oceana', 'ottawa', 'osceola', 'allegan', 'barry', 'berrien', 'branch', 'calhoun', 'cass', 'kalamazoo', 'st. joseph', 'van buren', 'alcona', 'arenac', 'bay', 'gladwin', 'huron', 'iosco', 'midland', 'ogemaw', 'oscoda', 'saginaw', 'sanilac', 'tuscola', 'clinton', 'eaton', 'gratiot', 'ingham', 'shiawassee', 'antrim', 'alpena', 'benzie', 'charlevoix', 'cheboygan', 'crawford', 'emmet', 'grand traverse', 'kalkaska', 'leelanau', 'manistee', 'missaukee', 'montmorency', 'otsego', 'presque isle', 'roscommon', 'wexford', 'hillsdale', 'jackson', 'lenawee', 'alger', 'baraga', 'chippewa', 'delta', 'dickinson', 'gogebic', 'houghton', 'iron', 'keweenaw', 'luce', 'mackinac', 'marquette', 'menominee', 'ontonagon', 'schoolcraft']

reg_threat_dict = {}

for county in county_list:
    for row in data:
        if row[0] == county.title():
            threat = 'Risk Level ' + row[1]
            reg_threat_dict[county] = threat

print(reg_threat_dict)

# Change directory to JHU repo and pull
os.chdir('/home/sabrina/Desktop/COVID-19')
os.system('git pull')
# Change back to directory location of this file
os.chdir('/home/sabrina/Desktop/covid-19-collaboration/Workspace/Sabrina')

# Initialize
fips = []
counties = []
dates = []
confirmed = []
deaths = []
active = []
dayCount = 0

# List, sort, and open CSV Files.  Fill arrays
# JHU daily reports path
path = r'/home/sabrina/Desktop/COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
grok = Grok('%{MONTHNUM:month}-%{MONTHDAY:day}-%{YEAR:year}')
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

for file in file_set:
	filedate = grok.match(file)
	data = pd.read_csv( file, index_col = None, header = 0 )

	# Grab data for each FIPS
	if (int(filedate["month"]) == 7 and int(filedate["day"]) > 21 and int(filedate["year"]) > 2019) or (int(filedate["month"]) > 7 and int(filedate["year"]) > 2019) or (int(filedate["year"]) > 2020):

		# Get day
		day = str(filedate["year"]+'-'+filedate["month"])+'-'+str(filedate["day"])

		# Grab County Data by Date
		for index, row in data.iterrows():
			if str(row['FIPS']) != 'nan' and row['FIPS'] != 80026 and row['FIPS'] != 90026 and str(row['Province_State']) == 'Michigan' and str(row['Country_Region']) == 'US':
                                fips.append(int(row['FIPS']))
                                counties.append(row['Admin2'])
                                dates.append(day)
                                confirmed.append(int(row['Confirmed']))
                                deaths.append(int(row['Deaths']))
                                active.append(int(row['Active']))

# Get locations
df = pd.DataFrame({'fips':fips, 'counties':counties, 'dates':dates, 'confirmed':confirmed, 'deaths':deaths, 'active':active})
locs = df['fips'].unique().tolist()

# Prep dates to average
today = date.today()
dayback1 = today - timedelta(days=1) 
dayback1 = dayback1.strftime("%Y-%m-%d")
dayback2 = today - timedelta(days=2) 
dayback2 = dayback2.strftime("%Y-%m-%d")
dayback3 = today - timedelta(days=3) 
dayback3 = dayback3.strftime("%Y-%m-%d")
dayback4 = today - timedelta(days=4)
dayback4 = dayback4.strftime("%Y-%m-%d")
dayback5 = today - timedelta(days=5)
dayback5 = dayback5.strftime("%Y-%m-%d")
dayback6 = today - timedelta(days=6)
dayback6 = dayback6.strftime("%Y-%m-%d")
dayback7 = today - timedelta(days=7)
dayback7 = dayback7.strftime("%Y-%m-%d")
dayback8 = today - timedelta(days=8)
dayback8 = dayback8.strftime("%Y-%m-%d")

avg_new_confirmed = []
avg_new_deaths = []
recent_confirmed = []
recent_deaths = []
recent_active = []
recent_active_per_cap = []
loc_threats = []

# Location of the population CSV
pop_df = pd.read_csv('gen_csvs/MI_counties_pop.csv')
population_list = pop_df['Population'].values
#print(pop_df['FIPS'].values)

#print(df['dates'].values)

for loc in locs:
    #print(loc)
    this_loc = pop_df.loc[pop_df['FIPS'] == loc]['County'].values[0]
    this_loc = str(this_loc).lower()
    this_loc_pop = pop_df.loc[pop_df['FIPS'] == loc]['Population'].values[0]

    this_loc_confirmed_1dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback1)]['confirmed'].values[0]
    this_loc_deaths_1dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback1)]['deaths'].values[0]
    this_loc_confirmed_2dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback2)]['confirmed'].values[0]
    this_loc_deaths_2dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback2)]['deaths'].values[0]
    this_loc_confirmed_3dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback3)]['confirmed'].values[0]
    this_loc_deaths_3dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback3)]['deaths'].values[0]
    this_loc_confirmed_4dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback4)]['confirmed'].values[0]
    this_loc_deaths_4dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback4)]['deaths'].values[0]
    this_loc_confirmed_5dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback5)]['confirmed'].values[0]
    this_loc_deaths_5dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback5)]['deaths'].values[0]
    this_loc_confirmed_6dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback6)]['confirmed'].values[0]
    this_loc_deaths_6dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback6)]['deaths'].values[0]
    this_loc_confirmed_7dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback7)]['confirmed'].values[0]
    this_loc_deaths_7dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback7)]['deaths'].values[0]
    this_loc_confirmed_8dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback8)]['confirmed'].values[0]
    this_loc_deaths_8dayback = df.loc[(df['fips'] == loc) & (df['dates'] == dayback8)]['deaths'].values[0]

    this_loc_new_confirmed_7dayback = this_loc_confirmed_7dayback - this_loc_confirmed_8dayback
    this_loc_new_confirmed_6dayback = this_loc_confirmed_6dayback - this_loc_confirmed_7dayback
    this_loc_new_confirmed_5dayback = this_loc_confirmed_5dayback - this_loc_confirmed_6dayback
    this_loc_new_confirmed_4dayback = this_loc_confirmed_4dayback - this_loc_confirmed_5dayback
    this_loc_new_confirmed_3dayback = this_loc_confirmed_3dayback - this_loc_confirmed_4dayback
    this_loc_new_confirmed_2dayback = this_loc_confirmed_2dayback - this_loc_confirmed_3dayback
    this_loc_new_confirmed_1dayback = this_loc_confirmed_1dayback - this_loc_confirmed_2dayback

    this_loc_new_deaths_7dayback = this_loc_deaths_7dayback - this_loc_deaths_8dayback
    this_loc_new_deaths_6dayback = this_loc_deaths_6dayback - this_loc_deaths_7dayback
    this_loc_new_deaths_5dayback = this_loc_deaths_5dayback - this_loc_deaths_6dayback
    this_loc_new_deaths_4dayback = this_loc_deaths_4dayback - this_loc_deaths_5dayback
    this_loc_new_deaths_3dayback = this_loc_deaths_3dayback - this_loc_deaths_4dayback
    this_loc_new_deaths_2dayback = this_loc_deaths_2dayback - this_loc_deaths_3dayback
    this_loc_new_deaths_1dayback = this_loc_deaths_1dayback - this_loc_deaths_2dayback

    total_new_confirmed = this_loc_new_confirmed_7dayback + this_loc_new_confirmed_6dayback + this_loc_new_confirmed_5dayback + this_loc_new_confirmed_4dayback + this_loc_new_confirmed_3dayback + this_loc_new_confirmed_2dayback + this_loc_new_confirmed_1dayback
    total_new_deaths = this_loc_new_deaths_7dayback + this_loc_new_deaths_6dayback + this_loc_new_deaths_5dayback + this_loc_new_deaths_4dayback + this_loc_new_deaths_3dayback + this_loc_new_deaths_2dayback + this_loc_new_deaths_1dayback

    if total_new_confirmed < 0:
        print(str(loc) + ' 7-day confirmed total < 0. Setting to 0.')
        total_new_confirmed = 0
        #print(total_new_confirmed)

    if total_new_deaths < 0:
        print(str(loc) + ' 7-day deaths total < 0. Setting to 0.')
        total_new_deaths = 0

    avg_new_confirmed.append(round(total_new_confirmed/7.))
    avg_new_deaths.append(round(total_new_deaths/7.))

    recent_confirmed_int = df.loc[(df['fips'] == loc) & (df['dates'] == dayback1)]['confirmed'].values[0]
    recent_deaths_int = df.loc[(df['fips'] == loc) & (df['dates'] == dayback1)]['deaths'].values[0]
    recent_active_int = df.loc[(df['fips'] == loc) & (df['dates'] == dayback1)]['active'].values[0]

    recent_confirmed.append(recent_confirmed_int)
    recent_deaths.append(recent_deaths_int)
    recent_active.append(recent_active_int)
    recent_active_per_cap.append(round(float(recent_active_int)/float(this_loc_pop), 2))
    loc_threats.append(reg_threat_dict[this_loc[:-7]])

# Adding Michigan total
locs.append("Michigan")
recent_confirmed.append(sum(recent_confirmed))
recent_deaths.append(sum(recent_deaths))
avg_new_confirmed.append(sum(avg_new_confirmed))
avg_new_deaths.append(sum(avg_new_deaths))
loc_threats.append('NA')

MI_pop = sum(population_list)
recent_active_per_cap.append(round(float(sum(recent_active))/float(MI_pop), 2))
recent_active.append(sum(recent_active))

df2 = pd.DataFrame({'fips':locs, 'confirmed':recent_confirmed, 'deaths':recent_deaths, 'avg_new_confirmed':avg_new_confirmed, 'avg_new_deaths':avg_new_deaths, 'active':recent_active, 'active_per_capita':recent_active_per_cap, 'threat_level':loc_threats})
#df2.to_csv('countyData.csv')

# use creds to create a client to interact with the Google Drive API
scope = ['https://www.googleapis.com/auth/drive']
#scope = ['https://spreadsheets.google.com/feeds']
creds = ServiceAccountCredentials.from_json_keyfile_name('security_files/client_secret.json', scope)
client = gspread.authorize(creds)

# Find a workbook by name and open the first sheet
# Make sure you use the right name here.
sheet = client.open("CountyData").sheet1

# Extract and print all of the values
list_of_hashes = sheet.get_all_records()
#print(list_of_hashes)
'''
for i in range(sheet.row_count-1):
    sheet.delete_row(2)
'''
sheet.update([df2.columns.values.tolist()] + df2.values.tolist())

'''
for i in range(0, len(df2)):
    row = df2.iloc[i].values.tolist()
    row_string = [str(row[i]) for i in range(len(row))]
    sheet.insert_row(row_string, i+1)
    '''
