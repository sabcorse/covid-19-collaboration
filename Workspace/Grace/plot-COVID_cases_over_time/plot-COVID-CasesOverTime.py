## plot-COVID-CasesOverTime
# python3 script by Jinyan Miao
# 5/16/2021 University of Michigan
# This scripte needs to be run under Linux environment

import numpy as np
import glob
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
from datetime import datetime

# using the month, day, year, and loc_type to get an areaLabel which will be used in the Get_File_Data() function
# loc_type: either "country" or "province", specified by the user
# areaLabel: an index needed to extract some rows from the datafiles, used in the Get_File_Data() function
def Get_Loc_Label(month, day, year, loc_type):
    if loc_type == 'country':
        areaLabel = 'Country/Region'
        # In the JHU data, after a certain date, the label name changes from "/" to "_"
        if (int(month) == 3 and int(day) > 21 and int(year) > 2019) or (int(month) > 3 and int(year) > 2019) or int(year) > 2020:
            areaLabel = 'Country_Region'
    elif loc_type == 'province':
        areaLabel = 'Province/State'
        if (int(month) == 3 and int(day) > 21 and int(year) > 2019) or (int(month) > 3 and int(year) > 2019) or int(year) > 2020:
            areaLabel = 'Province_State'
    else:
        print('Invalid country/state type, for country, enter country, for province/state, enter province, use lower case letters only. The program stops.')
        exit()
    return areaLabel

# get the date of the data from the file name
def Get_File_Date(file_name):
    month, day, year = file_name[-14:-12], file_name[-11:-9], file_name[-8:-4]
    return month, day, year

# read the data from the file, using file name, location, areaLabel from the Get_Loc_Label() function, and date
def Get_File_Data(file_name, loc, area_label, month, day, year):
    # read the data from .csv file to pandas dataframe
    df = pd.read_csv(file_name)
    # extract the rows corresponds to the desired location
    df_L = df.loc[df[area_label] == loc]
    # extract the data
    ConfirmedAllDates.append(df_L.Confirmed.sum())
    DeathsAllDates.append(df_L.Deaths.sum())
    RecoveredAllDates.append(df_L.Recovered.sum())
    Dates.append(pd.Timestamp(year=int(year),month=int(month),day=int(day)))

## Ask for location
loc = input("Enter location (for the United States, enter US): ")
loc_type = input("Is that a country or a province/state? For country, enter country, for province/state, enter province: ")

## Get directory
path = r'../../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

## Main algorithm
# Define lists to store data
Dates, ConfirmedAllDates, DeathsAllDates, RecoveredAllDates, ActiveAllDates = [], [], [], [], []
# Run through all files in the directory
for file_name in file_set:
     name_check = os.fsdecode(file_name)
     if name_check.endswith(".csv"):
        month, day, year = Get_File_Date(file_name)
        area_label =  Get_Loc_Label(month, day, year, loc_type)
        Get_File_Data(file_name, loc, area_label, month, day, year)
     else:
         continue
# Convert lists to arrays
ArrayConfirmed = np.array(ConfirmedAllDates)
ArrayDeaths = np.array(DeathsAllDates)
ArrayRecovered = np.array(RecoveredAllDates)
ArrayActive = np.array(ActiveAllDates)
# Clear the zero values
for M in [ArrayConfirmed, ArrayDeaths, ArrayRecovered, ArrayActive]:
    I = M.size
    i = 0
    while i < I:
        if M[i] == 0.0:
            M[i] = np.nan
            i = i+1
        else:
            i = i+1


## Combine arrays into a single data frame
Final_data = pd.DataFrame({'Cumulative Confirmed': ArrayConfirmed,
                    'Cumulative Deaths': ArrayDeaths, 'Cumulative Recovered': ArrayRecovered,},
                    index = Dates,)


## Make sure the region name is correct
if np.all(Final_data.isnull()) == True :
    print('This region is not included in our dataset, please enter another region. The program stops.')
    exit()

## Plot the final data
# Make a title
title = 'Covid Cases in ' + loc
# Define the plot
fig, (ax1, ax2, ax3) = plt.subplots(3,1, figsize=(15,15))
fig.suptitle(title, fontsize=28)
fig.tight_layout(pad=5.0)
# Make Subplots
ax1.plot(Final_data.index, Final_data['Cumulative Confirmed'], label='Cumulative Confirmed', color='b')
ax1.set_title('Cumulative Confirmed Cases',  fontsize='xx-large')
ax1.set_ylabel('Number of cases', fontsize='xx-large')
ax1.grid()

ax2.plot(Final_data.index, Final_data['Cumulative Deaths'], label='Cumulative Deaths', color='r')
ax2.set_title('Cumulative Deaths',  fontsize='xx-large')
ax2.set_ylabel('Number of cases', fontsize='xx-large')
ax2.grid()

ax3.plot(Final_data.index, Final_data['Cumulative Recovered'], label='Cumulative Recovered', color='g')
ax3.set_title('Cumulative Recovered',  fontsize='xx-large')
ax3.set_xlabel('Date', fontsize='xx-large')
ax3.set_ylabel('Number of cases', fontsize='xx-large')
ax3.grid()
# Output the plot into a single file
Plot_file_name = 'COVID-data-plot-for-' + loc + '.png'
plt.savefig(Plot_file_name)
plt.show()

print('Plot generated successfully, the plot is saved in the current directory, the program stops.')
