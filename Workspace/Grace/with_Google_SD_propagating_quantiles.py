import numpy as np
import glob
import pandas as pd
import os
import sys
import matplotlib.pyplot as plt
import random
from matplotlib import gridspec
from datetime import datetime
from datetime import date as calendar
from datetime import timedelta
from dateutil.parser import parse
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.linear_model import Lasso, LassoCV
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from sklearn import preprocessing
from scipy import signal

plt.style.use('seaborn-white')
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.monospace'] = 'Ubuntu Mono'
plt.rcParams['font.size'] = 10
plt.rcParams['axes.labelsize'] = 10
plt.rcParams['axes.labelweight'] = 'bold'
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['legend.fontsize'] = 10
plt.rcParams['figure.titlesize'] = 12

###############################################################
#                                                             #
#            Picking location and location type               #
#                                                             #
###############################################################

# Split country/state name input into separate words if necessary
def Split_Loc_Name(loc):
    i = 0
    while i < len(loc):
        # Scan over name, and split when capital letters found
        if loc[i].isupper() and not i == 0 and not loc == 'US':
            loc = loc[:i]+' '+loc[i:]
            i += 2
        else:
            i += 1
    return loc

# Convert country or state/province to integers
# Country -> 1, State/province -> 0
def Loc_Type_to_Num(loc_type):
    loc_type_num = 1
    if loc_type.lower() == 'province':
        loc_type_num = 0
    return loc_type_num

# Initialize analysis, using location name splitter and location type -> number functions
def Init_Analysis(given_loc, given_loc_type):
    # Split name if necessary, and convert type (country or state/province) to 1 or 0
    location = Split_Loc_Name(given_loc)
    location_type = Loc_Type_to_Num(given_loc_type)
    print('Location: ', location, '\nType: ', str(location_type))
    # Make a directory for saving the location's data, if necessary
    if not os.path.exists(location):
        os.makedirs(location)
        print('New directory created for', location)
    return location, location_type

# Run initialization
# Arg 1 <- location name, arg 2 <- country or province
location, countryOrState = Init_Analysis(sys.argv[1], sys.argv[2])
order = int(sys.argv[3])

case_order = 7
if location == 'Massachusetts':
    case_order = 49
if location == 'Pennsylvania':
    case_order = 7#49
if location == 'Indiana':
    case_order = 70#14
if location == 'US':
    case_order = 7#7
if location == 'Michigan':
    case_order = 70#110
if location == 'Washington':
    case_order = 7
if location == 'Oregon':
    case_order = 49
if location == 'New York':
    case_order = 14#49
if location == 'Wisconsin':
    case_order = 14#49

###############################################################
#                                                             #
#                  Reading and sorting data                   #
#                                                             #
###############################################################

# Handle JHU South Korea name changes
def Handle_South_Korea(file_name):
    if file_name == path + '03-10-2020.csv':
        location = 'Republic of Korea'
    if file_name == path + '03-11-2020.csv':
        location = 'Korea, South'
    return location

# Work-around, since Grok library not often available
# Convert JHU file names to usable dates
def Get_File_Date(file_name):
    month, day, year = file[-14:-12], file[-11:-9], file[-8:-4]
    if '{m}/{d}/{Y}'.format(m = month, d = day, Y = year) == sunday_stop:
        month, day, year = 'stop', 'stop', 'stop'
    return month, day, year

# Handle JHU region type changes
def Get_Loc_Type_JHU(month, day, year):
    areaLabel = 'Country/Region'
    if countryOrState == 0:
        areaLabel = 'Province/State'
    if (int(month) == 3 and int(day) > 21 and int(year) > 2019) or (int(month) > 3 and int(year) > 2019) or int(year) > 2020:
        # After certain date, "/" changed to "_"
        areaLabel = 'Country_Region'
        if countryOrState == 0:
                areaLabel = 'Province_State'
    return areaLabel

# Append data from JHU 
def Append_Data(daily_data):
    confirmedCases.append(0.)
    deaths.append(0.)
    recovered.append(0.)
    
    # Use global variables to update relevant values
    confirmedCases[day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Confirmed']
    deaths        [day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Deaths']
    recovered     [day_count] = daily_data.loc[daily_data[areaLabel] == location].sum()['Recovered']

    # Related sources on making this function efficient:
    # https://engineering.upside.com/a-beginners-guide-to-optimizing-pandas-code-for-speed-c09ef2c6a4d6
    # https://pandas.pydata.org/pandas-docs/stable/user_guide/missing_data.html

# Tells the data when to stop reading in
# Predictions made Saturday -> Saturday, but are created on Monday, so Sunday data dropped
def Get_Last_Sunday():
    today = calendar.today()
    idx = (today.weekday() + 1) % 7 
    #idx = 0
    sun = today - timedelta(idx)
    sun = '{:%m/%d/%Y}'.format(sun)
    return sun

# Prepare most recent Sunday
sunday_stop = Get_Last_Sunday()

# List and sort JHU data files
path = r'../../../COVID-19/csse_covid_19_data/csse_covid_19_daily_reports/'
file_set = glob.glob(path + "/*.csv" )
file_set.sort(key=lambda date: datetime.strptime(date,path+"%m-%d-%Y.csv"))

confirmedCases, deaths, recovered, active = [], [], [], []
day_count = 0

# Loop over JHU data, and update cases, deaths, recoveries
for file in file_set:
        # Handles South Korea name changes, if necessary
        if location == 'South Korea' or location == 'Republic of Korea':
            location = Handle_South_Korea(file)

        # Break the loop once most recent Sunday hit; do not read in
        # Predictions made Saturday -> Saturday
        month, day_index, year = Get_File_Date(file)
        if month == 'stop':
            print('Stopping data this past Sunday: ', str(sunday_stop))
            break
        data = pd.read_csv( file, index_col = None, header = 0 )

        # Deal with JHU region type label changes
        areaLabel = Get_Loc_Type_JHU(month, day_index, year)

        # Update confirmed, death, and recovery counts
        Append_Data(data)

        day_count += 1

# Revert South Korea name, if location is South Korea
if location == 'Korea, South':
    location = 'South Korea'

# Store most recent deaths and most recent confirmed cases
# Predicted new cases and deaths are later added to these to get aggregates
most_recent_deaths = deaths[-1]
most_recent_confirmed = confirmedCases[-1]

#deaths = preprocessing.scale(deaths)
#confirmedCases = preprocessing.scale(confirmedCases)

###############################################################
#                                                             #
#            Trim Data and Create Active Case List            #
#                                                             #
###############################################################

# Create cutoff to remove early days with too few cases
def Get_First_Day(untrimmed_list):
    # If cases in a region never above 2500, include all days
    if max(untrimmed_list) < 2500:
        threshold = 0
    # Else, if cases never above 100000, include all days after .5% of max reached
    elif max(untrimmed_list) < 100000:
        threshold = .005 * max(confirmedCases)
    # Else, include all days after first 1000 cases
    else:
        threshold = 1000
    
    # Loop over days, and see when cases get high enough to include
    day = 0
    for val in untrimmed_list:
        # If cases high enough, return given day and stop loop
        if val > threshold:
            first_day = day
            print('\nFirst day of count: ' + str(first_day))
            if location == 'US':
                return 315, [i for i in range(day_count - 315)]
            if location == 'Illinois' or location == 'Indiana' or location == 'Massachusetts':# or location == 'Ohio':
                return 295, [i for i in range(day_count - 295)]
            if location == 'Michigan':
                return 315, [i for i in range(day_count - 315)]
            if location == 'Wisconsin':
                return 295, [i for i in range(day_count - 295)]
            if location == 'New York':
                return 295, [i for i in range(day_count - 295)]
            return 295, [i for i in range(day_count - 295)]
            return first_day, [i for i in range(day_count - first_day)]
        day += 1

# Cut off first few days, based on thresholds in Get_First_Day
def Truncate_From_Min(untrimmed_lists):
    trimmed_lists = []
    for list_i in untrimmed_lists:
        trimmed_lists.append(list_i[minRange:])
    return trimmed_lists

# Set first day for inclusion, as well as a list of day indices
minRange, float_dates = Get_First_Day(confirmedCases)

# Active cases are total cases - deaths - recoveries
active = [confirmedCases[minRange+i] - deaths[minRange+i] - recovered[minRange+i] for i in range(len(float_dates))]
# Total "recoveries" include both recoveries and deaths
r_sum = [recovered[i] + deaths[i] for i in range(len(recovered))]
# Cut off days with too few cases
r_sum_cut, deaths, confirmed = Truncate_From_Min([r_sum, deaths, confirmedCases])

print('length of confirmed: ' + str(len(confirmed)))

##########################################################
#                                                        #
#           Get Mobility Data, Using First Day           #
#                                                        #
##########################################################

# Get date of day corresponding to minRange
# JHU data starts on January 22, so add minRange to January 22
def Get_First_Date():
    first_day_US = parse('January 22, 2020')
    return (first_day_US + timedelta(days = minRange)).strftime('%Y-%m-%d')

social_distancing_reach_back = 0
# Get Google's mobility data for a given region
def Get_Mobility(region, is_country):
    global social_distancing_reach_back
    # Get first day to include
    start_date = Get_First_Date()

    # For use in Forecasting Hub - automatically set country to US
    country = 'United States'
    # Read in all data, and narrow down to US-only for both states and national level
    mobility_data = pd.read_csv('gen_csvs/Global_Mobility_Report.csv', low_memory = False)
    this_region_mobility = mobility_data.loc[mobility_data['country_region'] == 'United States']
    # If want data for national level, find rows with "NaN" states, and omit others
    if is_country:
        no_states_rows = pd.isnull(this_region_mobility['sub_region_1'])
        this_region_mobility = this_region_mobility[no_states_rows]
    # If want data for a state, find rows with that state and "NaN" counties, and omit others
    else:
        this_region_mobility = this_region_mobility.loc[this_region_mobility['sub_region_1'] == region]
        no_counties_rows = pd.isnull(this_region_mobility['sub_region_2'])
        this_region_mobility = this_region_mobility[no_counties_rows]

    # Find row index where "start_date" occurs, and cut out all previous data
    first_date_row = this_region_mobility.loc[this_region_mobility['date'] == start_date].index.item()
    #print(this_region_mobility.loc[first_date_row])
    this_region_mobility = this_region_mobility.loc[first_date_row:]

    # Social distancing data not updated every day. Find difference b/w today & most recent update
    most_recent_date = this_region_mobility['date'].iloc[-1]
    #print(most_recent_date)
    most_recent_year = int(most_recent_date[0:4])
    most_recent_month = int(most_recent_date[5:7])
    most_recent_day = int(most_recent_date[8:])
    social_distancing_reach_back = (calendar.today() - calendar(most_recent_year, most_recent_month, most_recent_day)).days - 1
    print('Will reach ' + str(social_distancing_reach_back) + ' days back for social distancing')

    # get data for each type of social outing, and convert to lists
    add_on = '_percent_change_from_baseline'
    this_retail_and_rec = this_region_mobility['retail_and_recreation'+add_on].tolist()
    this_groc_and_pharm = this_region_mobility['grocery_and_pharmacy'+add_on].tolist()
    this_parks = this_region_mobility['parks'+add_on].tolist()
    this_transit = this_region_mobility['transit_stations'+add_on].tolist()
    this_work = this_region_mobility['workplaces'+add_on].tolist()
    this_residential = this_region_mobility['residential'+add_on].tolist()
    
    list_of_locs = [this_retail_and_rec, this_groc_and_pharm, this_parks, this_transit, this_work, this_residential]
    loc_strings = ['retail and recreation', 'grocery and pharmacy', 'parks', 'transit', 'work', 'residences']

    # Eliminate any NaN values in the social distancing data, by using previous value
    j = 0
    for loc in list_of_locs:
        #print(loc)
        num_nan = 0
        for i in range(len(this_retail_and_rec)):
            if np.isnan(loc[i]):
                num_nan += 1
                loc[i] = 0
                if not i == 0:
                    loc[i] = loc[i-1]
        print('Number of NaN values converted for ' + loc_strings[j] + ': ' + str(num_nan))
        j += 1

    return this_retail_and_rec, this_groc_and_pharm, this_parks, this_transit, this_work, this_residential    

# Loop over Google file to get social distancing data
retail_rec_list, groc_pharm_list, parks_list, transit_list, work_list, residential_list = Get_Mobility(location, countryOrState == 1)

#if location == 'Indiana':
#    transit_list[174] = transit_list[173]
#    transit_list[175] = transit_list[174]

#transit_list = preprocessing.scale(transit_list)

start_day = calendar.fromisoformat(Get_First_Date())
dates_preds = [(start_day + timedelta(days=i)).strftime('%Y-%m-%d') for i in range(len(confirmed)+28)]
dates_SD = [(start_day + timedelta(days=i)).strftime('%Y-%m-%d') for i in range(len(residential_list))]

##############################################################
#                                                            #
#                 Prepare Data for Regression                #
#                                                            #
##############################################################

# For testing: using a savitzky-golay filter to smooth the data and improve predictions
def Sav_Gol(unsmooth_data):
    daily_data = signal.savgol_filter(unsmooth_data, 51, 4).tolist()
    x_ax = [i for i in range(len(unsmooth_data))]
    plt.plot(x_ax, unsmooth_data)
    plt.plot(x_ax, daily_data)
    plt.show()
    return daily_data

# Convert cumulative data to daily data, since predictions based based off of incident values
def cumulativeToDaily(cumulative_lists):
    daily_list = []
    for list_i in cumulative_lists:
        daily = []
        for i in range(len(list_i)):
            if not i == 0:
                # Subtract each "yesterday's" values from the current day's
                daily.append(list_i[i] - list_i[i-1])
        # For testing, currently unused: smooth case and death data to remove spikes
        #if list_i == confirmed or list_i == deaths:
            #daily = Sav_Gol(daily)
        daily_list.append(daily)
    return daily_list

# Convert daily data to cumulative data
# Can be used to test aggregate predictions based off the sum of incident predictions
def dailyToCumulative(daily_list_start_pairs):
    cumulative_list = []
    for (list_i, start_val) in daily_list_start_pairs:
        cumulative = []
        for i in range(len(list_i) + 1):
            # Start from baseline saved cumulative value
            if i == 0:
                cumulative.append(start_val)
            # Add most recent cumulative value to most recent incident value
            else:
                cumulative.append(cumulative[i-1] + list_i[i-1])
        cumulative_list.append(cumulative)
    return cumulative_list

# Remove first <order> days of data for fitting the ridge model
# Order is the number of days used to predict each upcoming day
# E.g., if 21 days are used to predict each upcoming day, the first 21 should be removed, since they can't be predicted
def Truncate_For_Ridge(data_list, this_order = order):
    truncated_lists = []
    for list_i in data_list:
        truncated_lists.append(list_i[this_order:])
    return truncated_lists[0]

# Create constant arrays for aggregate model performance checks
active_arr_checker, deaths_arr_checker, confirmed_arr_checker, recovered_arr_checker = active[:], deaths[:], confirmed[:], r_sum_cut[:]
# Create constant arrays for incident model performance checks
daily_active_arr_checker, daily_recovered_arr_checker, daily_deaths_arr_checker, daily_confirmed_arr_checker = cumulativeToDaily([active_arr_checker, recovered_arr_checker, deaths_arr_checker, confirmed_arr_checker])

# Create daily active, recovered, deaths, and confirmed lists for incident value prediction making
daily_active, daily_recovered, daily_confirmed, daily_deaths = cumulativeToDaily([active, r_sum_cut, confirmed, deaths])
'''
if location == 'Illinois':
    #daily_confirmed[106] = (daily_confirmed[105] + daily_confirmed[107])/2.
    daily_deaths[31] = (daily_deaths[30] + daily_deaths[32])/2.
    daily_confirmed[90] = (daily_confirmed[89] + daily_confirmed[91])/2.
    daily_confirmed[209] = daily_confirmed[210]*.45
    daily_confirmed[210] = daily_confirmed[210]*.55
'''
if location == 'Illinois':
    daily_confirmed[49] = daily_confirmed[48]*.95
    daily_confirmed[50] = daily_confirmed[51]*1.05

if location == 'Massachusetts':
    daily_confirmed[132] = (daily_confirmed[131] + daily_confirmed[133])/2.
    daily_confirmed[139] = (daily_confirmed[138] + daily_confirmed[140])/2.
'''
if location == 'Ohio':
    #daily_confirmed[173] = (daily_confirmed[174])/2.
    #daily_confirmed[174] = (daily_confirmed[174])/2.
    #daily_confirmed[185] = (daily_confirmed[184]+daily_confirmed[186])/2.
    daily_deaths[97] = (daily_deaths[94])
    daily_deaths[98] = daily_deaths[97]*.95
    daily_deaths[99] = daily_deaths[98]*.96
    for i in range(100, 107):
        daily_deaths[i] = daily_deaths[i-1]*.97
'''
if location == 'Arkansas':
    daily_deaths[31] = (daily_deaths[30] + daily_deaths[32])/2.

if location == 'Wisconsin':
    daily_confirmed[63] = daily_confirmed[62]
    daily_confirmed[64] = daily_confirmed[63]
    daily_confirmed[65] = (daily_confirmed[64] + daily_confirmed[66])/2.
    #daily_deaths[159] = (daily_deaths[157] + daily_deaths[159])/2.
'''
if location == 'US':
    daily_deaths[9] = (daily_deaths[8] + daily_deaths[10])/2.
    daily_deaths[164] = daily_deaths[162]
    daily_deaths[165] = daily_deaths[164]
    daily_confirmed[163] = (daily_confirmed[162] + daily_confirmed[164])/2.
'''
if location == 'Indiana':
    for i in range(83, 93):
        daily_deaths[i] = daily_deaths[i-1]*.99
    daily_confirmed[-1] = daily_confirmed[-2]*.95
    daily_confirmed[-2] = daily_confirmed[-3]*.95

if location == 'Michigan':
    for i in range(len(daily_confirmed)):
        if daily_deaths[i] == 0:
            daily_deaths[i] = daily_deaths[i+1]*.45
            daily_deaths[i+1] = daily_deaths[i+1]*.55

# Trim first days of data for fitting the model (See function comments)
active_for_ridge = Truncate_For_Ridge([active])
recovered_for_ridge = Truncate_For_Ridge([r_sum_cut])
deaths_for_ridge = Truncate_For_Ridge([deaths])
confirmed_for_ridge = Truncate_For_Ridge([confirmed], case_order)

# Create matrices for prediction, as in LaTeX document
def Create_Pred_Matrices(full_data_lists, this_order = order, is_cases = True):
    matrices = []
    for list_i in full_data_lists:
        pred_matrix = []
        for i in range(len(list_i) - this_order):
            #print(i)
            new_row_daily = [1]
            for j in range(this_order):
                new_row_daily.append(list_i[i+j])
            # Append the social distancing averages for days -(7+difference) through -difference    
            # E.g. for order = 21 and reach_back = 5, i = 0 really gives day 22
            # To reach back 5 days, want days (11,...,17) = (21 + 1 + i - 6 - 5, 21 + 1 + i - 5)
            if is_cases:
                for loc in loc_list:
                    # Not doing averages: reach back and do -order+social_distancing_reach_back amount of values
                    # E.g. for order = 21 and reach_back = 5, i = 0 really gives day 22
                    # To reach back 5 days, want (0,...,17) = i, 21 + 1 + i - 5
                    # If 40 total days, eventually want (15,...,35) = ()
                    #print(social_distancing_reach_back)
                    #print(order+i-social_distancing_reach_back)
                    for j in range(i, this_order+i-social_distancing_reach_back):
                        new_row_daily.append(loc[j])
            
            else:
                print('deaths_matrices')
                for j in range(this_order):
                    new_row_daily.append(daily_confirmed[i+j])
            # Test of scaling
            #new_row_daily = preprocessing.scale(new_row_daily)
            pred_matrix.append(new_row_daily)
        # Test of scaling
        #pred_matrix = preprocessing.scale(pred_matrix)
        matrices.append(pred_matrix)
        #print(len(matrices))
        #print(len(matrices[0][0]))
        #print(len(matrices[0][0]))
        #print(len(matrices[0][-1]))
    return matrices[0]

# Remove negative case, death, recovery, and active values
# Come from data-reporting anomalies, so don't belong in predictions
def Clean_Sub_Zero(full_data_lists):
    no_zeros_list = []
    for list_i in full_data_lists:
        for i in range(len(list_i)):
            if list_i[i] < 0 and not i == 0 and not i == (len(list_i) - 1):
               list_i[i] = (list_i[i-1]+list_i[i+1])/2.
               print('removing a 0')
               print(list_i[i])
        no_zeros_list.append(list_i)
    return no_zeros_list

###########################
#                         #
#     Thesis Plotting     #
#                         #
###########################
'''
daily_conf_plot = [float('nan') if x == 0 else x for x in daily_confirmed]

fig, ax1 = plt.subplots()
gs = gridspec.GridSpec(2, 1, height_ratios = [2,1])
y_pos = np.arange(len(dates_SD))

ax1.bar([i for i in range(len(residential_list))], [-transit_list[i] for i in range(len(transit_list))], label = 'transit visits', width = 1.0, color = 'lightskyblue')#'mediumaquamarine')
#ax1.bar([i for i in range(len(residential_list))], [residential_list[i] for i in range(len(residential_list))], width = 1.0, color = 'lightskyblue')#'mediumaquamarine')
ax1.set_xlabel('Dates')
ax1.set_ylabel('Percent Decrease in Transit Station Visits')
#ax1.set_title('Relative Decrease in Transit Station Visits\nMichigan', fontstyle = 'italic')

ax1.set_xticks(y_pos)
ax1.set_xticklabels(dates_SD, rotation=75, fontsize = 6)
ax1.xaxis.set_major_locator(plt.MultipleLocator(14))
ax1.xaxis.set_minor_locator(plt.MultipleLocator(1))
#plt.bar([i for i in range(len(retail_rec_list))], residential_list)

ax2 = ax1.twinx()
ax2.set_ylabel('Incident COVID-19 Cases')
ax2.plot([i for i in range(len(daily_conf_plot) - 1)], daily_conf_plot[:-1], '.', markersize = 1.5, label = 'cases', color = 'k')

lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()
ax2.legend(lines + lines2, labels + labels2, fancybox = True)
#plt.legend(fancybox = True)
plt.title('Social Distancing and COVID-19 Cases\nMichigan', fontstyle = 'italic')
plt.show()

daily_conf_plot = [float('nan') if x == 0 else x for x in daily_confirmed]

fig = plt.figure()
gs = gridspec.GridSpec(2, 1, height_ratios = [2,1])
y_pos = np.arange(len(dates_SD))

ax1 = plt.subplot(gs[0])

ax1.bar([i for i in range(len(residential_list))], [-transit_list[i] for i in range(len(transit_list))], label = 'transit visits', width = 1.0, color = 'lightskyblue')#'mediumaquamarine')
#ax1.bar([i for i in range(len(residential_list))], [residential_list[i] for i in range(len(residential_list))], width = 1.0, color = 'lightskyblue')#'mediumaquamarine')
ax1.set_ylabel('Decrease in Transit Station Visits (%)')
#ax1.set_title('Relative Decrease in Transit Station Visits\nMichigan', fontstyle = 'italic')

#plt.bar([i for i in range(len(retail_rec_list))], residential_list)
#ax2 = plt.subplot(gs[0], sharex = ax1)
ax2 = ax1.twinx()
ax2.set_ylabel('Incident COVID-19 Cases')
ax2.plot([i for i in range(len(daily_conf_plot) - 1)], daily_conf_plot[:-1], '.', markersize = 1.5, label = 'cases', color = 'k')
#ax2.axvline(53, linestyle = '--', color = 'k', ymax = transit_list[53]/min(transit_list))
#ax2.axvline(104, linestyle = '--', color = 'k', ymax = transit_list[104]/min(transit_list))
#ax2.axvline(133, linestyle = '--', color = 'k', ymax = transit_list[133]/min(transit_list))
#ax2.axvline(140, linestyle = '--', color = 'k', ymax = transit_list[140]/min(transit_list))
#ax2.axvline(171, linestyle = '--', color = 'k', ymax = transit_list[171]/min(transit_list))

ax3 = plt.subplot(gs[1], sharex = ax1)
ax3.bar([i for i in range(len(residential_list))], [-transit_list[i] for i in range(len(transit_list))], label = 'transit visits', width = 1.0, color = 'lightskyblue')#'mediumaquamarine')
ax3.axvline(53, linestyle = '--', color = 'k')
ax3.axvline(104, linestyle = '--', color = 'k')
ax3.axvline(133, linestyle = '--', color = 'k')
ax3.axvline(140, linestyle = '--', color = 'k')
ax3.axvline(171, linestyle = '--', color = 'k')
ax3.text(48.5, 5, 'MDHHS emergency order', rotation = 90, fontsize = 8, weight = 'bold')
ax3.text(99.5, 5, 'Thanksgiving', rotation = 90, fontsize = 8, weight = 'bold')
ax3.text(128.5, 5, 'Christmas', rotation = 90, fontsize = 8, weight = 'bold')
ax3.text(135.5, 5, "New Year's Day", rotation = 90, fontsize = 8, weight = 'bold')
ax3.text(166.5, 5, 'Indoor dining resumes', rotation = 90, fontsize = 8, weight = 'bold')

lines, labels = ax1.get_legend_handles_labels()
lines2, labels2 = ax2.get_legend_handles_labels()

ax3.set_xticks(y_pos)
ax3.set_xticklabels(dates_SD, rotation=75, fontsize = 6)
ax3.xaxis.set_major_locator(plt.MultipleLocator(14))
ax3.xaxis.set_minor_locator(plt.MultipleLocator(1))
ax3.set_xlabel('Date')

ax1.legend(lines + lines2, labels + labels2, fancybox = True)
#plt.legend(fancybox = True)
ax1.set_title('Social Distancing and COVID-19 Cases\nMichigan', fontstyle = 'italic')
plt.subplots_adjust(hspace = .0)
plt.show()
'''
##############################
#                            #
#        Usual Code          #
#                            #
##############################

loc_list = [retail_rec_list, groc_pharm_list, parks_list, transit_list, work_list, residential_list]
# temporary fix attempt
loc_list = [residential_list]
#loc_list = [work_list]
if location == 'Michigan' or location == 'Wisconsin' or location == 'Indiana' or location == 'Pennsylvania' or location == 'California':
    loc_list = [transit_list]
print('length of Google data: ' + str(len(loc_list[0])))

# Create matrices, as in LaTeX document, for active cases, recoveries, deaths, and confirmed cases
daily_active, daily_recovered, daily_deaths, daily_confirmed = Clean_Sub_Zero([daily_active, daily_recovered, daily_deaths, daily_confirmed])
daily_active_pred_matrix = Create_Pred_Matrices([daily_active])
daily_recovered_pred_matrix = Create_Pred_Matrices([daily_recovered])
daily_deaths_pred_matrix = Create_Pred_Matrices([daily_deaths], order, False)
daily_confirmed_pred_matrix = Create_Pred_Matrices([daily_confirmed], case_order)

# Remove 0's
active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge = Clean_Sub_Zero([active_for_ridge, recovered_for_ridge, deaths_for_ridge, confirmed_for_ridge])
# Trim first days of data for fitting incident data
daily_active_for_ridge = Truncate_For_Ridge([daily_active])
daily_recovered_for_ridge = Truncate_For_Ridge([daily_recovered])
daily_deaths_for_ridge = Truncate_For_Ridge([daily_deaths])
daily_confirmed_for_ridge = Truncate_For_Ridge([daily_confirmed], case_order)

# Make constant values to revert to for both daily and cumulative model
daily_active_arr_const, daily_active_for_ridge_const = daily_active[:], daily_active_for_ridge[:]
daily_recovered_arr_const, daily_recovered_for_ridge_const = daily_recovered[:], daily_recovered_for_ridge[:]
daily_deaths_arr_const, daily_deaths_for_ridge_const = daily_deaths[:], daily_deaths_for_ridge[:]
daily_confirmed_arr_const, daily_confirmed_for_ridge_const = daily_confirmed[:], daily_confirmed_for_ridge[:]
active_for_ridge_const, recovered_for_ridge_const, deaths_for_ridge_const, confirmed_for_ridge_const = active_for_ridge[:], recovered_for_ridge[:], deaths_for_ridge[:], confirmed_for_ridge[:]

# Generate a range of ridge regression penalty alphas to try
def Generate_Alphas(coeff_list):
    alpha_list = []
    #i = -3
    i = -5
    while i < 8:
    #while i < 12:
        for coeff in coeff_list:
            alpha_list.append(coeff * (10**(-i)))
        i += 1
    params = {'alpha': alpha_list}
    return params

parameters = Generate_Alphas([1., 2.5, 5., 7.5])

###################################################
#                                                 #
#            Perform Ridge Regression             #
#                                                 #
###################################################

# Create different types of ridge regressor, based on circumstances
def Make_Ridge(input_list, ridge_type, alpha_list = [0]):
    ridge_list = []

    # Create bare-bones ridge regressors to feed into grid searcher
    if ridge_type == 'no_cv':
        for list_i in input_list:
            ridge_list.append(Ridge(normalize = True))

    # Create ridge regressor with only penalty specified, based on grid search results
    if ridge_type == 'no_cv_alpha_spec':
        for i in range(len(input_list)):
            ridge_list.append(Ridge(normalize = True, alpha = alpha_list[i]))

    # Create grid searcher to find best penalties
    if ridge_type == 'cv':
        for list_i in input_list:
            ridge_list.append(GridSearchCV(list_i, parameters, scoring = 'neg_mean_squared_error', cv = 10))

    return ridge_list

# Create grid searchers to find optimal ridge penalties for active and total cases, deaths, and recoveries
r_active, r_recovered, r_deaths, r_confirmed = Make_Ridge([active, recovered, deaths, confirmed], 'no_cv')
ridge_active_cv, ridge_recovered_cv, ridge_deaths_cv, ridge_confirmed_cv = Make_Ridge([r_active, r_recovered, r_deaths, r_confirmed], 'cv')

# Fit ridge regressors, and return necessary values or predictions, depending on circumstances
def Fit_Ridge(prediction_triads, fit_type):
    print('fitting')
    
    # For grid searchers, return optimal penalty and best score
    if fit_type == 'round_1_eval':
        alpha_mid_list, score_list = [], []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            alpha_mid_list.append(regressor.best_params_['alpha'])
            score_list.append(regressor.best_score_)
        return alpha_mid_list, score_list

    # For non-grid searches, create future predictions
    if fit_type == 'make_pred':
        model_list = []
        for (regressor, matrix, col_vec) in prediction_triads:
            regressor.fit(matrix, col_vec)
            model = regressor.predict(matrix)
            model_list.append(model.tolist())
        return model_list

# Creating triples of values for fitting
# Triples include the regressor, the prediction matrix, and the vector from the RHS of the LaTeX document
active_triad, recovered_triad, deaths_triad, confirmed_triad = (ridge_active_cv, daily_active_pred_matrix, daily_active_for_ridge), (ridge_recovered_cv, daily_recovered_pred_matrix, daily_recovered_for_ridge), (ridge_deaths_cv, daily_deaths_pred_matrix, daily_deaths_for_ridge), (ridge_confirmed_cv, daily_confirmed_pred_matrix, daily_confirmed_for_ridge)
#print('matrix: ' + str(daily_confirmed_pred_matrix))
# Get best penalties and performance metrics
assigned_alphas, assigned_scores = Fit_Ridge([active_triad, recovered_triad, deaths_triad, confirmed_triad], 'round_1_eval')
alpha_mid_active, alpha_mid_recovered, alpha_mid_deaths, alpha_mid_confirmed = assigned_alphas[0], assigned_alphas[1], assigned_alphas[2], assigned_alphas[3]
score_active, score_recovered, score_deaths, score_confirmed = assigned_scores[0], assigned_scores[1], assigned_scores[2], assigned_scores[3]

print(str(alpha_mid_active) + ' ' + str(alpha_mid_recovered) + ' ' + str(alpha_mid_deaths) + ' ' + str(alpha_mid_confirmed))
print('Score Active: ' + str(score_active) + '\nScore Recovered: ' + str(score_recovered) + '\nScore Deaths: ' + str(score_deaths) + '\nScore Confirmed: ' + str(score_confirmed))

ridge_deaths, ridge_confirmed = Make_Ridge([deaths, confirmed], 'no_cv_alpha_spec', [alpha_mid_deaths, alpha_mid_confirmed])

##############################################################
#                                                            #
#                        Bootstrapping                       #
#                                                            #
##############################################################

required_death_quantiles = [.01, .025, .05, .1, .15, .2, .25, .3, .35, .4, .45, .5, .55, .6, .65, .7, .75, .8, .85, .9, .95, .975, .99]
required_conf_quantiles = [0.025, 0.100, 0.250, 0.500, 0.750, 0.900, 0.975]
#required_conf_quantiles = required_death_quantiles[:]

# Fills in missing values from when the "for_ridge" lists were created without the first <order> days
# Useful for plotting, to see the full spectrum of data
def Model_Baseline(unmatched_models, baseline_lists):
    matched_lists = []
    for i in range(len(unmatched_models)):
        list_i = unmatched_models[i]
        baseline_list_i = baseline_lists[i]
        for j in range(0, order):
            list_i.insert(j, baseline_list_i[j])
        matched_lists.append(list_i)
    return matched_lists

def Get_Social_Distancing_Averages(distancing_lists, weeks_to_go_back):
    averages = []
    for list_i in distancing_lists:
        if weeks_to_go_back == 1:
            averages.append(np.sum(list_i[-7*weeks_to_go_back:])/7.)
        else:
            averages.append(np.sum(list_i[-7*weeks_to_go_back:-7*(weeks_to_go_back-1)])/7.)
    return averages

retail_rec_average1, groc_pharm_average1, parks_average1, transit_average1, work_average1, residential_average1 = Get_Social_Distancing_Averages([retail_rec_list, groc_pharm_list, parks_list, transit_list, work_list, residential_list], 1)
retail_rec_average2, groc_pharm_average2, parks_average2, transit_average2, work_average2, residential_average2 = Get_Social_Distancing_Averages([retail_rec_list, groc_pharm_list, parks_list, transit_list, work_list, residential_list], 2)
retail_rec_average3, groc_pharm_average3, parks_average3, transit_average3, work_average3, residential_average3 = Get_Social_Distancing_Averages([retail_rec_list, groc_pharm_list, parks_list, transit_list, work_list, residential_list], 3)
avg_list = [retail_rec_average1, retail_rec_average2, retail_rec_average3, groc_pharm_average1, groc_pharm_average2, groc_pharm_average3, parks_average1, parks_average2, parks_average3, transit_average1, transit_average2, transit_average3, work_average1, work_average2, work_average3, residential_average1, residential_average2, residential_average3]
# Create samples of length <order> to be used for predictions
# Like the final prediction-generation example in the LaTeX doc
# Append regressor predictions to each model's data

preds_made = 0
#print(data_diff)
def Create_Samples(lists_for_sampling, this_order = order, is_cases = True):
    global preds_made

    sample_list = []
    for list_i in lists_for_sampling:
        sample = [1]
        i = -this_order
        while i < 0:
            sample.append(list_i[i])
            i += 1
        #for avg in avg_list:
            #sample.append(avg)
        
        if is_cases:
            for loc in loc_list:
                loc_vals = loc[-this_order+social_distancing_reach_back:]
                #print(len(loc_vals))
                for i in range(len(loc_vals)):
                    #if preds_made > 5 and (i == (len(loc_vals) - 8)):# or i == (len(loc_vals) - 6)):
                    #    sample.append(loc_vals[-7])
                    #elif preds_made < 6:
                    #    sample.append(loc_vals[(i+preds_made) % len(loc_vals)])
                    #else:
                    sample.append(loc_vals[i])

        else:
            print('death_samples')
            i = -this_order
            while i < 0:
                sample.append(daily_confirmed_for_ridge[i])
                i += 1
        #sample = preprocessing.scale(sample)
        sample_list.append(np.asarray(sample, dtype = 'object'))#.reshape(1,-1))
        preds_made += 1
        #print(sample_list)
    
    return sample_list

# Create ridge regressor predictions based on samples 
def Predict_Ridge(models_list, samples_list):
    next_list = []
    for i in range(len(models_list)):
        next_val = models_list[i].predict(samples_list[i])
        next_list.append(next_val)
    return next_list

def Append_To_Col_Vect(col_vects, new_vals):
    appended_lists = []
    for i in range(len(col_vects)):
        list_i = col_vects[i]
        new_val = new_vals[i]
        list_i.append(new_val)
        appended_lists.append(list_i)
    return appended_lists

confirmed_cumulative_start = 0
deaths_cumulative_start = 0
# Function adapted from: https://saattrupdan.github.io/2020-03-01-bootstrap-prediction/
def Prediction_Interval(model, X_train, y_train, x0, confirmed = False):
  X_train = np.asarray(X_train)
  y_train = np.asarray(y_train)
  # Number of training samples
  n = X_train.shape[0]

  # The authors choose the number of bootstrap samples as the square root
  # of the number of samples
  nbootstraps = int(np.sqrt(n))

  # Compute the m_i's and the validation residuals
  bootstrap_preds, val_residuals = np.empty(nbootstraps), []
  for b in range(nbootstraps):
    train_idxs = np.random.choice(range(n), size = n, replace = True)
    val_idxs = np.array([idx for idx in range(n) if idx not in train_idxs])
    model.fit(X_train[train_idxs, :], y_train[train_idxs])
    preds = model.predict(X_train[val_idxs])
    val_residuals.append(y_train[val_idxs] - preds)
    bootstrap_preds[b] = model.predict(x0)
  bootstrap_preds -= np.mean(bootstrap_preds)
  val_residuals = np.concatenate(val_residuals)

  # Compute the prediction and the training residuals
  model.fit(X_train, y_train)
  preds = model.predict(X_train)
  if confirmed:
      confirmed_cumulative_start = preds[-1]
  else:
      deaths_cumulative_start = preds[-1]
  train_residuals = y_train - preds

  # Take percentiles of the training- and validation residuals to enable
  # comparisons between them
  val_residuals = np.quantile(val_residuals, q = np.arange(100)/100)
  train_residuals = np.quantile(train_residuals, q = np.arange(100)/100)

  # Compute the .632+ bootstrap estimate for the sample noise and bias
  no_information_error = np.mean(np.abs(np.random.permutation(y_train) - \
    np.random.permutation(preds)))
  generalisation = np.abs(val_residuals - train_residuals)
  no_information_val = np.abs(no_information_error - train_residuals)
  relative_overfitting_rate = np.mean(generalisation / no_information_val)
  weight = .632 / (1 - .368 * relative_overfitting_rate)
  residuals = (1 - weight) * train_residuals + weight * val_residuals

  # Construct the C set and get the percentiles
  C = np.array([m + o for m in bootstrap_preds for o in residuals])
  #plt.scatter([i for i in range(len(C))], C)
  #plt.show()

  if confirmed:
    percentiles = np.quantile(C, q = required_conf_quantiles)
  else:
    percentiles = np.quantile(C, q = required_death_quantiles)

  y_hat = model.predict(x0)
  #percentiles = [y_hat + percentile for percentile in percentiles]
  return percentiles, y_hat

stop = int(len(daily_active_arr_checker)) + 40 - 1
start = len(daily_active) - 1
    
quantiles_confirmed, quantiles_deaths = [], []
points_confirmed, points_deaths = [], []

daily_confirmed_for_ridge_orig = daily_confirmed_for_ridge[:]
daily_deaths_for_ridge_orig = daily_deaths_for_ridge[:]

# Make ~6 weeks worth of predictions
t = start
while t < stop:
    # Make predictions
    this_day_quantiles_confirmed, this_day_point_confirmed = Prediction_Interval(ridge_confirmed, daily_confirmed_pred_matrix, daily_confirmed_for_ridge_orig, Create_Samples([daily_confirmed_for_ridge], case_order), True)
    this_day_quantiles_deaths, this_day_point_deaths = Prediction_Interval(ridge_deaths, daily_deaths_pred_matrix, daily_deaths_for_ridge_orig, Create_Samples([daily_deaths_for_ridge], order, False))
    # Append predictions to model
    daily_deaths_for_ridge, daily_deaths, daily_confirmed_for_ridge, daily_confirmed = Append_To_Col_Vect([daily_deaths_for_ridge, daily_deaths, daily_confirmed_for_ridge, daily_confirmed], [this_day_point_deaths, this_day_point_deaths, this_day_point_confirmed, this_day_point_confirmed])
    
    # Quantiles are additive - like SDev
    quantiles_confirmed.append(this_day_quantiles_confirmed)
    points_confirmed.append(this_day_point_confirmed)
    quantiles_deaths.append(this_day_quantiles_deaths)
    points_deaths.append(this_day_point_deaths)

    t += 1

points_confirmed_aggregate, points_deaths_aggregate = dailyToCumulative([(points_confirmed, confirmed_cumulative_start + confirmedCases[-1]), (points_deaths, deaths_cumulative_start + deaths[-1])])

########################################################
#                                                      #
#                   Sort Quantiles                     #
#                                                      #
########################################################

# Separate lists for each quantile, for recording data in csv
# "dc" for "death count"
quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99 = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

# Separate lists for each quantile
# "cc" for "case count"
quantiles_cc2_5, quantiles_cc10, quantiles_cc25, quantiles_cc50, quantiles_cc75, quantiles_cc90, quantiles_cc97_5 = [], [], [], [], [], [], []
#quantiles_cc1, quantiles_cc2_5, quantiles_cc5, quantiles_cc10, quantiles_cc15, quantiles_cc20, quantiles_cc25, quantiles_cc30, quantiles_cc35, quantiles_cc40, quantiles_cc45, quantiles_cc50, quantiles_cc55, quantiles_cc60, quantiles_cc65, quantiles_cc70, quantiles_cc75, quantiles_cc80, quantiles_cc85, quantiles_cc90, quantiles_cc95, quantiles_cc97_5, quantiles_cc99 = [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], [], []

# A list of quantile lists for deaths
deaths_quant_list = [quantiles_dc1, quantiles_dc2_5, quantiles_dc5, quantiles_dc10, quantiles_dc15, quantiles_dc20, quantiles_dc25, quantiles_dc30, quantiles_dc35, quantiles_dc40, quantiles_dc45, quantiles_dc50, quantiles_dc55, quantiles_dc60, quantiles_dc65, quantiles_dc70, quantiles_dc75, quantiles_dc80, quantiles_dc85, quantiles_dc90, quantiles_dc95, quantiles_dc97_5, quantiles_dc99]

# A list of quantile lists for cases
confirmed_quant_list = [quantiles_cc2_5, quantiles_cc10, quantiles_cc25, quantiles_cc50, quantiles_cc75, quantiles_cc90, quantiles_cc97_5]
#confirmed_quant_list = [quantiles_cc1, quantiles_cc2_5, quantiles_cc5, quantiles_cc10, quantiles_cc15, quantiles_cc20, quantiles_cc25, quantiles_cc30, quantiles_cc35, quantiles_cc40, quantiles_cc45, quantiles_cc50, quantiles_cc55, quantiles_cc60, quantiles_cc65, quantiles_cc70, quantiles_cc75, quantiles_cc80, quantiles_cc85, quantiles_cc90, quantiles_cc95, quantiles_cc97_5, quantiles_cc99]

# Total number of points to have generated by end of each quantile bracket
points_to_include_deaths = [2, 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150, 160, 170, 180, 190, 195, 198, 200]
points_to_include_deaths = [points_to_include_deaths[i] for i in range(len(points_to_include_deaths))]

points_to_include_confirmed = [5, 20, 50, 100, 150, 180, 195, 200]
points_to_include_confirmed = [points_to_include_confirmed[i] for i in range(len(points_to_include_confirmed))]

##################################################
#                                                #
#          Additive Quantile Creation            #
#                                                #
##################################################

expanded_quantiles_confirmed = [quantiles_confirmed[0]]
expanded_quantiles_deaths = [quantiles_deaths[0]]

for i in range(1, len(quantiles_confirmed)):
    previous_quantiles_confirmed = []
    previous_quantiles_deaths = []

    this_day_quantile_sums_confirmed = []
    this_day_quantile_sums_deaths = []

    for j in range(i + 1):
        previous_quantiles_confirmed.append(quantiles_confirmed[j])
        previous_quantiles_deaths.append(quantiles_deaths[j])

    for j in range(100):
        random_pulls_quantiles_confirmed = []
        random_pulls_quantiles_deaths = []

        for quantile_set in previous_quantiles_confirmed:
            representative_set_confirmed = []
            
            for k in range(0, len(points_to_include_confirmed)):
                if k == 0:
                    samples_to_take = 2
                    representative_set_list = np.random.uniform(0, quantile_set[0], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_confirmed.append(rep)

                elif k < len(points_to_include_confirmed) - 1:

                    samples_to_take = points_to_include_confirmed[k] - points_to_include_confirmed[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], quantile_set[k], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_confirmed.append(rep)

                else: 
                    samples_to_take = points_to_include_confirmed[k] - points_to_include_confirmed[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], 1.05*quantile_set[k-1], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_confirmed.append(rep)

            random_pulls_quantiles_confirmed.append(random.choice(representative_set_confirmed))

        this_day_quantile_sums_confirmed.append(np.sum(random_pulls_quantiles_confirmed))
 
        for quantile_set in previous_quantiles_deaths:
            representative_set_deaths = []

            for k in range(0, len(points_to_include_deaths)):
                if k == 0:
                    samples_to_take = 5
                    representative_set_list = np.random.uniform(0, quantile_set[0], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_deaths.append(rep)

                elif k < len(points_to_include_deaths) - 1:
                    samples_to_take = points_to_include_deaths[k] - points_to_include_deaths[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], quantile_set[k], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_deaths.append(rep)

                else:
                    samples_to_take = points_to_include_deaths[k] - points_to_include_deaths[k-1]
                    representative_set_list = np.random.uniform(quantile_set[k-1], 1.05*quantile_set[k-1], samples_to_take)
                    for rep in representative_set_list:
                        representative_set_deaths.append(rep)

            random_pulls_quantiles_deaths.append(random.choice(representative_set_deaths))

        this_day_quantile_sums_deaths.append(np.sum(random_pulls_quantiles_deaths))

    expanded_quantiles_confirmed.append(np.quantile(this_day_quantile_sums_confirmed, q = required_conf_quantiles))
    expanded_quantiles_deaths.append(np.quantile(this_day_quantile_sums_deaths, q = required_death_quantiles))
    '''
    print('original: ' + str(quantiles_confirmed[i]))
    quantiles_confirmed[i] = expanded_quantiles_confirmed[i]
    print('expanded: ' + str(expanded_quantiles_confirmed[i]))
    '''
quantiles_confirmed = expanded_quantiles_confirmed
quantiles_deaths = expanded_quantiles_deaths

##################################################
#                                                #
#          Adding Quantiles to Points            #
#                                                #
##################################################

deaths_quant_list_aggregate = []
confirmed_quant_list_aggregate = []
summed_confirmed_quantiles = []
summed_deaths_quantiles = []

for i in range(len(quantiles_confirmed)):
    # Get quantiles for each individual day
    daily_quantiles = quantiles_confirmed[i]
    # Add these quantiles to the day's point prediction to get prediction spectrum
    summed_confirmed_quantiles.append((daily_quantiles + points_confirmed_aggregate[i]).tolist())
    
# Index over every day of summed quantiles
for i in range(len(summed_confirmed_quantiles)):
    # Set a single day's quantiles
    this_days_quantiles = summed_confirmed_quantiles[i]
    # Index over each quantile for that day, and append it to the appropriate chronological quantile list
    for j in range(len(this_days_quantiles)):
        confirmed_quant_list[j].append(this_days_quantiles[j])

for i in range(len(quantiles_deaths)):
    # Get quantiles for each individual day
    daily_quantiles = quantiles_deaths[i]
    # Add these quantiles to the day's point prediction to get prediction spectrum
    summed_deaths_quantiles.append((daily_quantiles + points_deaths_aggregate[i]).tolist())
    
# Index over every day of summed quantiles
for i in range(len(summed_deaths_quantiles)):
    # Set a single day's quantiles
    this_days_quantiles = summed_deaths_quantiles[i]
    # Index over each quantile for that day, and append it to the appropriate chronological quantile list
    for j in range(len(this_days_quantiles)):
        deaths_quant_list[j].append(this_days_quantiles[j])

################################################
#                                              #
#               Plot Quantiles                 #
#                                              #
################################################
'''
Model_Baseline([points_deaths, points_confirmed], [daily_deaths, daily_confirmed])

for i in range(41):
    daily_active_arr_checker.append(0)
    daily_recovered_arr_checker.append(0)
    daily_deaths_arr_checker.append(0)
    daily_confirmed_arr_checker.append(0)

for i in range(start):
    deaths_quant_list[2].insert(0, 0)
    deaths_quant_list[-3].insert(0, 0)
    confirmed_quant_list[2].insert(0, 0)
    confirmed_quant_list[-3].insert(0, 0)

#print(deaths_quant_list_aggregate[2])

plt.plot(deaths[:start+28], 'g', linestyle = '--', label = 'model')
plt.plot(deaths_quant_list[2][:start+28])
plt.plot(deaths_quant_list[-3][:start+28])
plt.plot(deaths_arr_checker[:start+28], 'k', label = 'data')
plt.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Deaths')
plt.title('Deaths Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_deaths.pdf')
plt.show()

plt.plot(confirmed[:start+28], 'g', linestyle = '--', label = 'model')
plt.plot(confirmed_quant_list[2][:start+28])
plt.plot(confirmed_quant_list[-3][:start+28])
plt.plot(confirmed_arr_checker[:start+28], 'k', label = 'data')
plt.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Deaths')
plt.title('Deaths Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_deaths.pdf')
plt.show()

plt.plot(daily_deaths[:start+28], 'g', linestyle = '--', label = 'model')
plt.plot(daily_deaths_arr_checker[:start+28], 'k', label = 'data')
#plt.plot(deaths_quant_list[2][:start+28])
#plt.plot(deaths_quant_list[-3][:start+28])
plt.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Deaths')
plt.title('Death Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()

plt.plot(daily_confirmed[:start+28], 'g', linestyle = '--', label = 'model')
plt.plot([i for i in range(len(daily_confirmed_arr_checker[:start+28]))], daily_confirmed_arr_checker[:start+28], color = 'k', label = 'data')
plt.axvline(x = start, color = 'k', linestyle = '--', label = 'prediction point')
plt.xlabel('Days')
plt.ylabel('Confirmed')
plt.title('Confirmed Prediction\n' + str(location))
plt.legend(loc = 'upper left')
plt.savefig(location + '/' + location + '_confirmed.pdf')
plt.show()
'''
########################################################
#                                                      #
#                    Data Writing                      #
#                                                      #
########################################################

# NEW (9-7-2020) -- Use python's datetime library, set the current date

# get today's date
date_today = calendar.today()
# get the next Saturday's date
date_saturday = date_today + timedelta((5 - date_today.weekday()) % 7)

submission_frame = pd.DataFrame(columns = ['forecast_date', 'target', 'target_end_date', 'location', 'type', 'quantile', 'value'])
# Edit days here
forecast_date = date_today.strftime("%Y-%m-%d")
target_end_date = date_saturday.strftime("%Y-%m-%d")

case_count_list = []
death_count_list = []

cumulative_deaths_for_adding = [most_recent_deaths]

# Edit days here
days_for_pred = [7, 14, 21, 28]

# Edit days here
for i in range(len(days_for_pred)):
    if i == 1:
        target_end_date = (date_saturday + timedelta(days = 7)).strftime("%Y-%m-%d")
    if i == 2:
        target_end_date = (date_saturday + timedelta(days = 14)).strftime("%Y-%m-%d")
    if i == 3:
        target_end_date = (date_saturday + timedelta(days = 21)).strftime("%Y-%m-%d")

    # No -1, since aggregates include most recent data point
    pred_day = days_for_pred[i]
    
    for j in range(len(confirmed_quant_list)):
        quantile = required_conf_quantiles[j]
        row = confirmed_quant_list[j]
        #if quantile == 0.975:
        #    break
        #else:
        if i == 0:
            case_inc = row[pred_day] - points_confirmed_aggregate[0]
            case_inc = case_inc
        else:
            case_inc = row[pred_day] - case_count_list[-1]
            case_inc = case_inc[0]
        if case_inc < 0:
            case_inc = 0.
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case'.format(week = i+1), target_end_date, location, 'quantile', quantile, case_inc]], columns = submission_frame.columns))

    for j in range(len(deaths_quant_list)):
        quantile = required_death_quantiles[j]
        row = deaths_quant_list[j]
        if i == 0:
            deaths_inc = row[pred_day] - points_deaths_aggregate[0]
            deaths_inc = float(deaths_inc)
        else:
            deaths_inc = row[pred_day] - death_count_list[-1]
            deaths_inc = float(deaths_inc)
        if deaths_inc < 0:
            deaths_inc = 0.
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'quantile', quantile, deaths_inc]], columns = submission_frame.columns))
        submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'quantile', quantile, row[pred_day]]], columns = submission_frame.columns))

    #pred_day = np.asarray(pred_day)
    #pred_day = pred_day.reshape(1,-1)
 
    if i == 0:
        case_inc = points_confirmed_aggregate[pred_day] - points_confirmed_aggregate[0]
    else:
        case_inc = points_confirmed_aggregate[pred_day] - case_count_list[-1]
    if case_inc < 0:
        case_inc = [0.]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc case'.format(week = i+1), target_end_date, location, 'point', 'NA', float(case_inc[0])]], columns = submission_frame.columns))

    if i == 0:
        deaths_inc = points_deaths_aggregate[pred_day] - points_deaths_aggregate[0]
    else:
        deaths_inc = points_deaths_aggregate[pred_day] - death_count_list[-1]
    if deaths_inc < 0:
        deaths_inc = [0.]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead inc death'.format(week = i+1), target_end_date, location, 'point', 'NA', float(deaths_inc[0])]], columns = submission_frame.columns))

    #this_cumulative = deaths_inc + cumulative_deaths_for_adding[-1]
    submission_frame = submission_frame.append(pd.DataFrame([[forecast_date, '{week} wk ahead cum death'.format(week = i+1), target_end_date, location, 'point', 'NA', float(points_deaths_aggregate[pred_day][0])]], columns = submission_frame.columns))
    #cumulative_deaths_for_adding.append(float(this_cumulative[0])) 
    
    case_count_list.append(points_confirmed_aggregate[pred_day])
    death_count_list.append(points_deaths_aggregate[pred_day])

# Edit days here
submission_frame.to_csv(location + '/' + date_today.strftime("%Y-%m-%d") + '-UMich-RidgeTfReg.csv')

